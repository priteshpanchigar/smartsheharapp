
var rejectedReason;
var issueCat;
var wardNo;
var issueName;
var date;
var approvedRejectDateTime='';
var address;
var imgFirst;
var imgSecond;
var radioRejected;
var radionSend;
var rejectedFlag;
var sendToAuthFlag;
var selectReason;
var reasonCode='';
var otrComment='';
var txtComment;
var issueid='';
var imgSrcOne='';
var imgSrcTwo='';

function loadPage(){	
		
		
		radioRejected = document.getElementById("reject");
		radionSend = document.getElementById("send");
		selectReason = document.getElementById("selectReason");
		selectReason.style.visibility="hidden";
		txtComment = document.getElementById("txtComment");
		txtComment.style.visibility="hidden";
		getRejectedReasons();
		
}  

function getRejectedReasons()
{
	var rejectedUrl = getSmartSheharPhpScriptsPath() + 'issue/get_reason_code.php';
   
    $.ajax({
        type: 'GET',
        url: rejectedUrl,
        dataType: 'html',
        success: function(data) {
            var arrReason = JSON.parse(data);
		   var reasonOp;
		   reasonOp = document.createElement("option");
			reasonOp.value= "";
			reasonOp.text = "Select Reason";
			selectReason.add(reasonOp);
		    for (j = 0; j < arrReason.length; j++) {
				reasonOp = document.createElement("option");
				reasonOp.value = arrReason[j].reason_code;
				reasonOp.text = arrReason[j].reject_reason;
				selectReason.add(reasonOp);
			 }
        },
        error: function(xhr, textStatus, errorThrown) {
            var err = textStatus + ', ' + errorThrown;
        }
        
    });
}
function radioReject()
{
	approvedRejectDateTime='';
	rejectedFlag=1;
	sendToAuthFlag = 0;
	date = new Date();
	approvedRejectDateTime = date.getFullYear()  + "-"
                + (date.getMonth()+1)  + "-" 
                + date.getDate() + " "  
                + date.getHours() + ":"  
                + date.getMinutes() + ":" 
                + date.getSeconds();
	selectReason.style.visibility="visible";
		
}
function radioSend()
{
	approvedRejectDateTime='';
	rejectedFlag=0;
	sendToAuthFlag = 1;	
	date = new Date();
	approvedRejectDateTime = date.getFullYear()  + "-"
                + (date.getMonth()+1)  + "-" 
                + date.getDate() + " "  
                + date.getHours() + ":"  
                + date.getMinutes() + ":" 
                + date.getSeconds();
	selectReason.style.visibility="hidden";
	txtComment.style.visibility="hidden";
		
}
function gotWardnIssueCat()
{
	var displayIssue =  document.getElementById("displayIssue");
	imgSecond =  document.getElementById("imgSecond");
	
	wardNo = document.getElementById("selectWard").value;
	issueCat = document.getElementById("selectIssue").value;

	if(wardNo!="select"&& issueCat!="")
	{
		getIssues();
	}
	else{
		displayIssue.style.visibility="hidden";
		imgSecond.style.visibility="hidden";
		selectReason.style.visibility="hidden";
		txtComment.style.visibility="hidden";
	}
}
function getIssues()
{
	var unapprovedIssueUrl = getSmartSheharPhpScriptsPath() + 'issue/get_unapproved_single_issue.php?ward='+wardNo+"&issueTypeCode="+issueCat;
    
    $.ajax({
        type: 'GET',
        url: unapprovedIssueUrl,
        dataType: 'html',
        success: function(data) {
			
            displayIssueReport(data);
        },
        error: function(xhr, textStatus, errorThrown) {
            var err = textStatus + ', ' + errorThrown;
        }
        
    });
}
function displayIssueReport(data)
{
	imgSrcOne='';
	imgSrcTwo='';
	rejectedFlag=0;
	sendToAuthFlag = 0;	
	approvedRejectDateTime='';
	displayIssue.style.visibility="visible";
	issueName=  document.getElementById("issueName");
	date=  document.getElementById("date");
	address=  document.getElementById("address");
	imgFirst=  document.getElementById("imgFirst");
	selectReason.style.visibility="hidden";
	txtComment.style.visibility="hidden";
	radioRejected.checked=false;
	radionSend.checked=false;
	var arrIssue =  JSON.parse(data);
	var count = arrIssue.count;
	reasonCode='';
	otrComment='';
	issueid='';
	
	if(count == '0')
	{
		displayIssue.style.visibility="hidden";
		imgSecond.style.visibility="hidden";
		selectReason.style.visibility="hidden";
	}
	
	 for (k= 0; k < arrIssue.length; k++) 
	 {
		 if(k==0)
		 {
			 imgSecond.style.visibility="hidden";
			 imgFirst.src =getImages() + arrIssue[k].issue_image_path + arrIssue[k].issue_image_name;
			 imgSrcOne=getImages() + arrIssue[k].issue_image_path + arrIssue[k].issue_image_name;
	
		 }
		 if(k==1)
		 {   
			  imgSecond.style.visibility="visible";
			  imgSecond.src= getImages() + arrIssue[k].issue_image_path + arrIssue[k].issue_image_name;
			  imgSrcTwo=getImages() + arrIssue[k].issue_image_path + arrIssue[k].issue_image_name;
		 }
		 issueid = +arrIssue[k].issue_id;
		 issueName.innerHTML='Issue: '+arrIssue[k].issue_item_description;
		 date.innerHTML='Date: '+arrIssue[k].issue_time;
		 address.innerHTML='Address: '+arrIssue[k].formatted_address;
		
	 }
	 
	
}
function changeReason()
{
	reasonCode = document.getElementById("selectReason").value;

	if(reasonCode=='OTR')
	{
		txtComment.style.visibility="visible";
	}
	else{
		txtComment.style.visibility="hidden";
	}
}
function getComment()
{
	otrComment  = txtComment.value;	
}
function onSubmit()
{

		if(rejectedFlag=='0'&& sendToAuthFlag=='0')
		{
			alert("Please select radio button")
		}
		else{
			if(rejectedFlag=='1' && reasonCode=='')
			{
				alert("please select reason of rejection");
			}
			else{
				if(rejectedFlag=='1' && reasonCode=='OTR' && otrComment=='')
				{
					alert("Please add a comment");
				}
				else{
					sendData();
				}
			}
			if(rejectedFlag=='0' && sendToAuthFlag=='1' )
			{
				sendData();
			}
		}
}
function sendData() 
{
		var updateApprovedOffenceUrl = getSmartSheharPhpScriptsPath() + 'issue/update_approved_issue.php?tobesenttoauthority=' +sendToAuthFlag+'&rejected='+rejectedFlag+'&issueid='+issueid+'&reasoncode='+reasonCode+'&clientdatetime='+
					approvedRejectDateTime+'&othercomment='+otrComment;
    $.ajax({
        type: 'POST',
        url: updateApprovedOffenceUrl,
        dataType: 'html',
        success: function(data) {
	
		if(sendToAuthFlag == '1' && rejectedFlag=='0')
		{
				sentToAuthMail();
		}
		if(sendToAuthFlag == '0' && rejectedFlag=='1')
		{
			rejectedMail();
		}
        getIssues();
		
        },
        error: function(xhr, textStatus, errorThrown) {
            var err = textStatus + ', ' + errorThrown;
        }
   
    });
}
function rejectedMail()   
{
		var rejectedUrl = getSmartSheharPhpScriptsPath() + 'issue/rejected_issue_email.php?issueid=' + issueid;
    
			$.ajax({
			type: 'POST',    
			url: rejectedUrl,
			dataType: 'html',
			success: function(data) {
						//alert("Mail sent!");
			},
			error: function(xhr, textStatus, errorThrown) {
			var err = textStatus + ', ' + errorThrown;
			}
		
		});
}

function sentToAuthMail()
{
	var sendToAuthUrl = getSmartSheharPhpScriptsPath() + 'issue/send_to_authority_issue_email.php?issueid=' + issueid+'&clientdatetime='+
					approvedRejectDateTime;
    
				$.ajax({
					type: 'POST',
					url: sendToAuthUrl,
					dataType: 'html',
					success: function(data) {

						//alert("Mail sent!");  
					},
				error: function(xhr, textStatus, errorThrown) {
					var err = textStatus + ', ' + errorThrown;
				}
			
				});   
}
function openImgOne()
{
	var myWindow = window.open(getHtmlScriptsPath()+"imagezooming.html");
    myWindow.document.write("<img src='"+imgSrcOne+"'/>");
}
function openImgTwo()
{
	var myWindow = window.open(getHtmlScriptsPath()+"imagezooming.html");
    myWindow.document.write("<img src='"+imgSrcTwo+"'/>");
}

