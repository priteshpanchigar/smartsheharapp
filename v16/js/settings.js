/*global $, window, document, getCookie, setCookie */
var TWOWHEELER = '2 Wheeler', FOURWHEELER = '4 Wheeler', FRIENDS = 'Friends only',
	EVERYBODY = 'Every Body', capacityStr, accessStr, regNumber, carColor;
function setupRadio() {
	// whenever a button is clicked, set the hidden helper
	if (document.getElementById('capacity2').checked) {
		capacityStr = TWOWHEELER;
	}
	if (document.getElementById('capacity4').checked) {
		capacityStr = FOURWHEELER;
	}
	if (document.getElementById('radioAccessF').checked) {
		accessStr = FRIENDS;
	}
	if (document.getElementById('radioAccessE').checked) {
		accessStr = EVERYBODY;
	}
	regNumber = document.getElementById('regNo').value;
	carColor = document.getElementById('color').value;
	setCookie("vehlcleInfo", JSON.stringify({'vehlType': capacityStr, 'regno': regNumber, 'color': carColor, 'access': accessStr}), 365);
	history.back();
}
$(document).ready(function () {
	$("#homeBtn").bind("click", function () {
		window.location = 'index.html';
	});
	$("#OkBtn").bind("click", function () {
		setupRadio();
		history.back();
	});
	$("#gpsBtn").bind("click", function () {
		getLocation();
	});
	var city = getCookie("city");
	if (city) {
		$('#cityName').html(city);
	} else {
		$('#cityName').html('None');
	}
	$('#cityName').click(function () {
		// update the block message 
		window.location = 'city.html';
		return false;
	});
});
