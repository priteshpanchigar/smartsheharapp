DROP PROCEDURE IF EXISTS issue_by_category;
DELIMITER$$
CREATE  PROCEDURE `issue_by_category`()

BEGIN

SELECT ap.email, i.issue_id, issue_time, 
 formatted_address as issue_address, postal_code, 
ilt.issue_type_code, issue_type_description, ils.issue_sub_type_code, issue_sub_type_description,  
ili.issue_item_code, issue_item_description, d.department_code, department_description, ward,
submit_report,discard_report ,approved, rejected, to_be_sent_to_authority, sent_to_authority,  resolved, unresolved,
closed, opened, approved_rejected_clientdatetime, sent_to_authority_datetime, resolved_unresolved_datetime, 
closed_opened_datetime, reason_code, other_reason_code_comment, unresolved_comment, 
(SELECT  group_name FROM group_members g WHERE g.appuser_id  = i.resolved_unresolved_by LIMIT 1 )resolved_unresolved_by, 
(SELECT  group_name FROM group_members g WHERE g.appuser_id  = i.closed_opened_by LIMIT 1) closed_opened_by, closed_opened_comment, latitude lat, longitude lng, i.appuser_id, vehicle_no, unique_key, 
(SELECT  group_name FROM group_members g WHERE g.appuser_id  = i.letter_submit_by LIMIT 1) letter_submit_by, 
issue_registration_notification, letter_submitted_to, letter_submitted_dept, letter_submitted_desg,
letter_submitted_datetime, letter_remark, letter_upload_notification, resolved_unresolved_notification, letter_client_date_time,
letter_image_name, letter_image_path,
(SELECT group_name FROM group_members g WHERE g.appuser_id = i.appuser_id LIMIT 1) group_name,
(SELECT issue_image_name FROM issue_image oi WHERE oi.issue_id = i.issue_id GROUP BY issue_id)issue_image_name,
(SELECT GROUP_CONCAT(issue_image_name) issue_images FROM issue_image oi WHERE oi.issue_id = i.issue_id GROUP BY issue_id)issue_images,
(SELECT issue_image_path FROM issue_image oi WHERE oi.issue_id = i.issue_id GROUP BY issue_id)issue_image_path  , COUNT(*) as cnt  FROM issue i
															 LEFT JOIN issue_lookup_items ili
															 ON i.issue_item_code = ili.issue_item_code
															 LEFT JOIN issue_lookup_subtypes  ils
															 ON ili.issue_sub_type_code = ils.issue_sub_type_code
															 LEFT JOIN issue_lookup_types ilt
															 ON ilt.issue_type_code = ils.issue_type_code
															 LEFT JOIN address a
															 ON  a.address_id = i.address_id
															 LEFT JOIN department d
															 ON d.department_code = ilt.department_code
															 LEFT JOIN appuser ap 
															 ON ap.appuser_id = i.appuser_id 
 WHERE  submit_report = 1
 AND  closed = 0
 AND  d.department_code = "MU"
 AND   i.issue_item_code IS NOT NULL 
 AND   i.discard_report = 0 
 AND   i.rejected = 0 
 AND i.resolved =0
 GROUP BY issue_sub_type_code
 ORDER BY cnt desc;
END$$
DELIMITER ;

call issue_by_category;