DROP PROCEDURE IF EXISTS issue_by_ward;
DELIMITER$$
CREATE  PROCEDURE `issue_by_ward`()

BEGIN

	SELECT ward, COUNT(ward) cnt FROM vw_issue_with_lookup
	WHERE department_code = 'MU' AND ward is NOT NULL
	AND submit_report = 1
	GROUP BY ward
	ORDER BY cnt DESC;

 
END$$
DELIMITER ;

call issue_by_ward;