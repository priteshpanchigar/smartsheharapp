DROP PROCEDURE IF EXISTS issue_summary;
DELIMITER$$
CREATE  PROCEDURE `issue_summary`(IN insubcategory varchar(255))

BEGIN

	SELECT i.issue_id, item.issue_item_description, img.issue_image_name  FROM issue i 
	LEFT JOIN issue_image img ON i.issue_id = img.issue_id
	LEFT JOIN issue_lookup_items item ON i.issue_item_code = item.issue_item_code
	LEFT JOIN issue_lookup_subtypes ist ON item.issue_sub_type_code = ist.issue_sub_type_code
	LEFT JOIN issue_lookup_types it ON ist.issue_type_code = it.issue_type_code
	LEFT JOIN department d ON it.department_code = d.department_code
	WHERE i.submit_report = 1
	AND i.issue_item_code IS NOT NULL
	AND ist.issue_sub_type_code = insubcategory
	GROUP BY i.issue_id
	ORDER BY i.issue_id DESC;

END$$
DELIMITER ;