DROP PROCEDURE IF EXISTS smartshehar_summary;
DELIMITER$$
CREATE  PROCEDURE `smartshehar_summary`()

BEGIN

DECLARE v_open_issues int;
DECLARE v_resolved_issues int;
DECLARE v_traffic_issues int;
DECLARE v_municipal_issues int;
DECLARE v_contributors int;

	
	SELECT COUNT(i.issue_id) INTO v_open_issues FROM issue i
	INNER JOIN issue_lookup_items item ON item.issue_item_code = i.issue_item_code
	INNER JOIN issue_lookup_subtypes ist ON ist.issue_sub_type_code = item.issue_sub_type_code
	INNER JOIN issue_lookup_types it ON it.issue_type_code = ist.issue_type_code
	INNER JOIN department d ON d.department_code = it.department_code
	where submit_report = 1 AND
	rejected = 0 AND
	resolved = 0
	AND d.department_code IS NOT NULL;

	SELECT COUNT(i.issue_id) INTO v_resolved_issues FROM issue i
	INNER JOIN issue_lookup_items item ON item.issue_item_code = i.issue_item_code
	INNER JOIN issue_lookup_subtypes ist ON ist.issue_sub_type_code = item.issue_sub_type_code
	INNER JOIN issue_lookup_types it ON it.issue_type_code = ist.issue_type_code
	INNER JOIN department d ON d.department_code = it.department_code
	where i.resolved = 1
	AND d.department_code IS NOT NULL;


	SELECT COUNT(i.issue_id) INTO v_traffic_issues FROM issue i
	INNER JOIN issue_lookup_items item ON item.issue_item_code = i.issue_item_code
	INNER JOIN issue_lookup_subtypes ist ON ist.issue_sub_type_code = item.issue_sub_type_code
	INNER JOIN issue_lookup_types it ON it.issue_type_code = ist.issue_type_code
	INNER JOIN department d ON d.department_code = it.department_code
	where submit_report = 1 AND
	rejected = 0 AND
	resolved = 0
	AND d.department_code = 'TR';

	SELECT COUNT(i.issue_id) INTO v_municipal_issues FROM issue i
	INNER JOIN issue_lookup_items item ON item.issue_item_code = i.issue_item_code
	INNER JOIN issue_lookup_subtypes ist ON ist.issue_sub_type_code = item.issue_sub_type_code
	INNER JOIN issue_lookup_types it ON it.issue_type_code = ist.issue_type_code
	INNER JOIN department d ON d.department_code = it.department_code
	where submit_report = 1 AND
	rejected = 0 AND
	resolved = 0
	AND d.department_code = 'MU';

	SELECT COUNT(DISTINCT(appuser_id)) INTO v_contributors FROM issue
	where submit_report = 1;

	SELECT v_open_issues,v_resolved_issues,v_traffic_issues,v_municipal_issues,v_contributors;


END$$
DELIMITER ;