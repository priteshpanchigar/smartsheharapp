DROP PROCEDURE IF EXISTS department_summary;
DELIMITER$$
CREATE  PROCEDURE `department_summary`(IN indepartment varchar(255))

BEGIN

	SELECT it.issue_type_code, ist.issue_sub_type_code  , ist.issue_sub_type_description  , COUNT(ist.issue_sub_type_code) cnt FROM issue i 
	LEFT JOIN issue_lookup_items item ON i.issue_item_code = item.issue_item_code
	LEFT JOIN issue_lookup_subtypes ist ON item.issue_sub_type_code = ist.issue_sub_type_code
	LEFT JOIN issue_lookup_types it ON ist.issue_type_code = it.issue_type_code
	LEFT JOIN department d ON it.department_code = d.department_code
	WHERE i.submit_report = 1
	AND i.issue_item_code IS NOT NULL
	AND d.department_code = indepartment
	GROUP BY ist.issue_sub_type_code
	ORDER BY COUNT(ist.issue_sub_type_code) DESC;

END$$
DELIMITER ;