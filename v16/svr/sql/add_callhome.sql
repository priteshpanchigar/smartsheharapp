DROP PROCEDURE IF EXISTS add_callhome;
DELIMITER $$
CREATE  PROCEDURE `add_callhome`(IN inappuserid INT(15), IN inappusageid INT(15), IN `inclientdatetime` datetime, IN `inlat` double, IN `inlng` double, IN `inacc` int, IN `inlocationdatetime` datetime, IN `inmodule` varchar(20), IN `inapp` varchar(05))
BEGIN

INSERT  IGNORE INTO callhome(appusage_id, appuser_id, clientdatetime, serverdatetime, lat, lng,
	accuracy, locationdatetime, module, app)
VALUES(inappusageid, inappuserid, inclientdatetime ,NOW(),  inlat, inlng, 
inacc, inlocationdatetime, inmodule, inapp);

SELECT LAST_INSERT_ID() as callhome_id;

END$$
DELIMITER;