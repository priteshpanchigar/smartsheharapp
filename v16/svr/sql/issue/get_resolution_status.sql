DROP PROCEDURE IF EXISTS get_resolution_status;
DELIMITER$$
CREATE  PROCEDURE `get_resolution_status`(IN inward VARCHAR(20), 
IN incomplainttypecode VARCHAR(20)) 

BEGIN
IF inward is NULL THEN
			SELECT od.offence_detail_id, offence_address, offence_time, client_datetime, offence_type, 
			vehicle_no, unique_key, submit_report, approved_rejected_clientdatetime,approved,rejected, resolved, unresolved, unresolved_comment,
			(SELECT reject_reason FROM rejected_reason 
			 WHERE od.reason_code = rejected_reason.reason_code) reject_reason,
			sent_to_authority,
			address_id, location_id, complaint_type_code, complaint_sub_type_code, lat, lng,
			offence_image_name, offence_image_path, od.appuser_id  FROM offence_details od
			LEFT JOIN offence_image oi on od.offence_detail_id = oi.offence_detail_id
			WHERE od.offence_detail_id = (SELECT offence_detail_id FROM offence_details 
			WHERE sent_to_authority = 1
			AND resolved = 0
			AND unresolved = 0
			AND complaint_type_code = incomplainttypecode
			AND ward =  'I don\'t know'  ) 
			ORDER BY offence_detail_id limit 1;
ELSE
			SELECT od.offence_detail_id, offence_address, offence_time, client_datetime, offence_type, 
			vehicle_no, unique_key, submit_report, approved_rejected_clientdatetime,approved,rejected, resolved, unresolved, unresolved_comment,
			(SELECT reject_reason FROM rejected_reason 
			 WHERE od.reason_code = rejected_reason.reason_code) reject_reason,
			sent_to_authority,
			address_id, location_id, complaint_type_code, complaint_sub_type_code, lat, lng,
			offence_image_name, offence_image_path, od.appuser_id  FROM offence_details od
			LEFT JOIN offence_image oi on od.offence_detail_id = oi.offence_detail_id
			WHERE od.offence_detail_id = (SELECT offence_detail_id FROM offence_details 
			WHERE sent_to_authority = 1
			AND resolved = 0
			AND unresolved = 0
			AND complaint_type_code = inward
			AND ward = incomplainttypecode  )
			ORDER BY od.offence_detail_id limit 1 ;
END IF;


END$$
DELIMITER;

call get_resolution_status('K/E','MU_CLN')