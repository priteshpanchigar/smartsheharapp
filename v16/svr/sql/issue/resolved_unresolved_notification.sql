DROP PROCEDURE IF EXISTS resolved_unresolved_notification;
DELIMITER$$
CREATE PROCEDURE `resolved_unresolved_notification`(IN inissueid INT )
BEGIN

SELECT i.unique_key, i.issue_time, a.formatted_address, 
au.gcm_registration_id,au.appuser_id, item.issue_item_description as 'subcategory',
ist.issue_sub_type_description as 'category'
FROM issue i
		INNER JOIN address a ON i.address_id = a.address_id
		INNER JOIN groupdetails gd ON a.postal_code = gd.postal_code
		INNER JOIN group_members gm ON gd.group_id = gm.group_id
		INNER JOIN appuser au ON au.appuser_id = gm.appuser_id
		INNER JOIN issue_lookup_items item ON item.issue_item_code = i.issue_item_code
		INNER JOIN issue_lookup_subtypes ist ON item.issue_sub_type_code = ist.issue_sub_type_code
WHERE i.issue_id = inissueid  
AND i.resolved_unresolved_notification = 0 
AND au.show_notification = 1

UNION

(SELECT i.unique_key, i.issue_time, a.formatted_address, 
au.gcm_registration_id,au.appuser_id, item.issue_item_description as 'subcategory',
ist.issue_sub_type_description as 'category'
FROM issue i
		INNER JOIN address a ON i.address_id = a.address_id
		INNER JOIN appuser au ON au.appuser_id = i.appuser_id
		INNER JOIN issue_lookup_items item ON item.issue_item_code = i.issue_item_code
		INNER JOIN issue_lookup_subtypes ist ON item.issue_sub_type_code = ist.issue_sub_type_code
WHERE i.issue_id = inissueid 
AND i.resolved_unresolved_notification = 0
AND au.show_notification = 1);



UPDATE issue SET resolved_unresolved_notification = 1
WHERE issue_id = inissueid ;
END$$
DELIMITER;