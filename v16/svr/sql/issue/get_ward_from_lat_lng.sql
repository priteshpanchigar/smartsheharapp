DROP PROCEDURE IF EXISTS get_ward_from_lat_lng;
DELIMITER$$
CREATE  PROCEDURE `get_ward_from_lat_lng`(IN inlat DOUBLE, IN inlng DOUBLE) 
BEGIN
SELECT DISTINCT ward FROM municipal_ward
WHERE ST_Contains(boundary, 
GeomFromText(CONCAT('POINT(',inlng, ' ', inlat,')')));
END$$
DELIMITER;

CALL get_ward_from_lat_lng(19.1,72.85);