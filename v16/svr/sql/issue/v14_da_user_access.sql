DROP PROCEDURE IF EXISTS v14_da_user_access;
DELIMITER$$
CREATE  PROCEDURE `v14_da_user_access`( IN `inregid` varchar(500), IN `inclientdatetime` datetime,
	IN `inemailid`  varchar(100),
	IN `inlatno` double, IN `inlngno` double,IN `inacc` int,
	IN `inlocationdatetime` datetime, IN `inprovider` varchar(10),
	IN `inspeed` float, IN `inbearing` float,IN `inaltitude` double,
	IN `inimeino` varchar(30),
	IN `incarrier` varchar(30),
	IN `inproduct` varchar(30), IN `inmanufacturer` varchar(30),
	IN `inapp` varchar(05), IN `inversion` varchar(25), 
	IN `inversioncode` INT, 
	IN `inmodule` varchar(20),
	IN `inip` varchar(50), IN `inuseragent` varchar(300),IN `inandroid_release_version` varchar(30),IN `inandroid_sdk_version` int)
BEGIN
	DECLARE v_APPUSER_ID int;
	DECLARE v_APPUSAGE_ID int;
	DECLARE v_clientdttm datetime;
	DECLARE v_serverdttm datetime;
	DECLARE v_location_id INT DEFAULT 0;
	SET @isInsert = 0;

	SELECT appuser_id FROM appuser
	WHERE email = inemailid
	INTO v_APPUSER_ID;

#	SELECT 'appuser_id : ', v_APPUSER_ID;

	IF (v_APPUSER_ID IS NULL) THEN
		INSERT INTO appuser(gcm_registration_id, email, imei, 
			lat, lng, accuracy, provider,
			carrier, product, manufacturer, android_release_version, android_sdk_version, clientlastaccessdatetime)
		VALUES(inregid, inemailid,  inimeino, 
		   inlatno, inlngno, inacc, inprovider, 
		   incarrier, inproduct, inmanufacturer, inandroid_release_version, inandroid_sdk_version,  inclientdatetime);
		   SELECT LAST_INSERT_ID() INTO v_APPUSER_ID;
		   SET @isInsert = 1;
		INSERT INTO appusage(appuser_id, clientfirstaccessdatetime, 
			clientlastaccessdatetime, serverfirstaccessdatetime, 
			serverlastaccessdatetime, version, versioncode, app)
		VALUES(v_APPUSER_ID, inclientdatetime, inclientdatetime, 
			NOW(), NOW(), inversion, inversioncode, inapp)
			ON DUPLICATE KEY UPDATE    
			version = inversion;
	END IF;
	
	
	IF (v_APPUSER_ID IS NOT NULL) THEN
#		SELECT v_APPUSER_ID;
		SET v_location_id = insert_location(v_APPUSER_ID, inemailid, 1, 0, 
			inclientdatetime, inlatno, inlngno, inacc, inlocationdatetime,
			inprovider, inspeed, inbearing, inaltitude);
#		SELECT 'Location id : ', v_location_id;
		IF v_location_id IS NULL THEN
			SET v_location_id = -1;
		END IF;
		
		IF (v_location_id > 0) THEN	
			IF @isInsert = 1 THEN
#				SELECT v_location_id;
				UPDATE appuser
				SET first_location_id = v_location_id,
					last_location_id = v_location_id, 
					clientlastaccessdatetime = inclientdatetime
				WHERE appuser_id = v_APPUSER_ID; 
			ELSE
#				SELECT v_APPUSER_ID, v_location_id;
				UPDATE appuser
					SET last_location_id = v_location_id,
					clientlastaccessdatetime = inclientdatetime
				WHERE appuser_id = v_APPUSER_ID; 
			END IF;
		END IF;	
		UPDATE appuser
			SET first_location_id = COALESCE(first_location_id, last_location_id),
				gcm_registration_id = COALESCE(inregid, gcm_registration_id),
				ip = (COALESCE(inip, ip)),
				useragent = (COALESCE(inuseragent, useragent)),
				android_release_version = (COALESCE(inandroid_release_version, android_release_version)),
				android_sdk_version = (COALESCE(inandroid_sdk_version, android_sdk_version)),
				clientlastaccessdatetime = inclientdatetime
			WHERE appuser_id = v_appuser_id;
		INSERT INTO appusage(appuser_id, clientfirstaccessdatetime, 
			clientlastaccessdatetime, serverfirstaccessdatetime, 
			serverlastaccessdatetime, version, versioncode, app)
		VALUES(v_APPUSER_ID, inclientdatetime, inclientdatetime, 
			NOW(), NOW(), inversion, inversioncode, inapp)
			ON DUPLICATE KEY UPDATE    
			clientlastaccessdatetime = COALESCE(inclientdatetime, clientlastaccessdatetime), 
			serverlastaccessdatetime = COALESCE(NOW(), serverlastaccessdatetime),
			version = COALESCE(inversion, version), 
			versioncode = COALESCE(inversioncode, versioncode);
	
	END IF;
/*
SELECT appuser_id FROM appuser
 WHERE email = inemailid;
 */
 SELECT au.appuser_id, au.appusage_id ,a.show_notification,  
 (SELECT approve_other FROM governance_flag g WHERE g.appuser_id = v_appuser_id) approve_other,
 (SELECT approve_mine FROM governance_flag g WHERE g.appuser_id = v_appuser_id) approve_mine
 FROM appusage au
	INNER JOIN appuser a on au.appuser_id = a.appuser_id
 WHERE au.appuser_id = v_appuser_id ;
 
 
END$$
DELIMITER ;