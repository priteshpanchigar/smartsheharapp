DELIMITER$$
CREATE  FUNCTION `insert_location`(`inappuserid` int, `inemailid` varchar(100),
	`inisdriver` bit, `inispassenger` bit, `inclientdatetime` datetime, `inlat` double, `inlng` double, 
	`inaccuracy` INT, `inlocationdatetime` datetime, `inprovider` varchar(100), 
	`inspeed` float, `inbearing` float, `inaltitude` double) RETURNS int(11)
BEGIN
	DECLARE v_location_id INT DEFAULT NULL;
	DECLARE v_lat DOUBLE DEFAULT 0.0;
	DECLARE v_lng DOUBLE DEFAULT 0.0;
	DECLARE v_lat2 DOUBLE DEFAULT 0.0;
	DECLARE v_lng2 DOUBLE DEFAULT 0.0;

	DECLARE v_loctime DATETIME;
	SET @TAG = "fn_insert_location";
	SET @isUpdate = 0;
	SET @MIN_DIST = .005;
	IF (inaltitude IS NULL) THEN
		SET inaltitude = 0.0;
	END IF;
	IF (inbearing IS NULL) THEN
		SET inbearing = 0.0;
	END IF;
	IF (inspeed IS NULL) THEN
			SET inspeed = 0.0;
	END IF;
	SELECT location_id, lat, lng, loctime 
		FROM location
		WHERE appuser_id  = inappuserid
		ORDER BY location_id DESC LIMIT 1
		INTO v_location_id, v_lat, v_lng, v_loctime;
#	INSERT INTO errorlog (tag, debugstatement) VALUES (v_loctime, inlocationdatetime);	
	IF (inlat IS NOT NULL AND inlng IS NOT NULL AND inappuserid IS NOT NULL) THEN
		IF (v_location_id) THEN
			IF (v_loctime > inlocationdatetime) THEN 
#				INSERT INTO errorlog (tag, debugstatement) VALUES (v_loctime, inlocationdatetime);	
				SET @isUpdate = -1;
			ELSE 	
				IF (getDistanceBetweenPoints(inlat, inlng, v_lat, v_lng) < @MIN_DIST
						AND inspeed = 0) THEN
					SET @isUpdate = 1;
				END IF;
			END IF;	
		END IF;
#		INSERT INTO errorlog (tag, debugstatement) VALUES (@TAG, @isUpdate);
		IF @isUpdate = 1 THEN
#			INSERT INTO errorlog (tag, debugstatement) VALUES (@TAG, "in update" ) ;
				UPDATE location
				SET lat = (SELECT CASE WHEN inlat IS NOT NULL THEN inlat ELSE lat END),
					lng = (SELECT CASE WHEN inlng IS NOT NULL THEN inlng ELSE lng END),
					accuracy = (SELECT CASE WHEN accuracy IS NOT NULL THEN inaccuracy ELSE lng END),
					altitude = (SELECT CASE WHEN inaltitude IS NOT NULL THEN inaltitude ELSE altitude END),
					bearing = (SELECT CASE WHEN inbearing IS NOT NULL THEN inbearing ELSE bearing END),
					loctime = (SELECT CASE WHEN inlocationdatetime IS NOT NULL THEN inlocationdatetime ELSE loctime END),
					clientaccessdatetime = (SELECT CASE WHEN inclientdatetime IS NOT NULL THEN inclientdatetime ELSE clientaccessdatetime END),
					speed = (SELECT CASE WHEN inspeed IS NOT NULL THEN inspeed ELSE speed END),
					provider = (SELECT CASE WHEN inprovider IS NOT NULL THEN inprovider ELSE provider END) 
				WHERE location_id = v_location_id;

		ELSE
			IF (@isUpdate != -1) THEN
				INSERT INTO location(active, appuser_id, email, isdriver, ispassenger, lat,
						lng, accuracy, altitude, bearing, firsttime, loctime, 
						clientaccessdatetime, speed, provider)
						VALUES(1, inappuserid, inemailid, 1, 0, inlat, inlng, inaccuracy, 
							inaltitude, inbearing, 
							 inlocationdatetime, inclientdatetime, inclientdatetime, inspeed,
								inprovider);
#					ON DUPLICATE KEY UPDATE clientaccessdatetime = inclientdatetime, 
#						speed = inspeed, accuracy = inaccuracy;
				SELECT LAST_INSERT_ID() INTO v_location_id;
			END IF;		
		END IF;
	SELECT lat, lng
		FROM location 
		WHERE location_id < v_location_id AND appuser_id = inappuserid
		ORDER BY location_id DESC
		LIMIT 1
		INTO v_lat2, v_lng2;
	
	UPDATE location
		SET clientaccessdatetime = (SELECT CASE WHEN inclientdatetime IS NOT NULL THEN inclientdatetime ELSE clientaccessdatetime END),
			cumulative_distance = getDistanceBetweenPoints(lat, lng, v_lat2, v_lng2)
	WHERE location_id = v_location_id;
	END IF;
	RETURN v_location_id;

END$$
DELIMITER;