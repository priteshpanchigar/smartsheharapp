DROP PROCEDURE IF EXISTS add_intermediate_progress;
DELIMITER $$
CREATE PROCEDURE `add_intermediate_progress`(IN inappuserid INT, IN inissueid INT,
IN inintermediate_progress_comment varchar(100),
IN inintermediate_progress_clientdatetime datetime)
BEGIN
	
INSERT INTO intermediate_progress(appuser_id, issue_id, intermediate_progress_comment, 
intermediate_progress_clientdatetime)
VALUES(inappuserid,inissueid, inintermediate_progress_comment, 
inintermediate_progress_clientdatetime);

SELECT LAST_INSERT_ID() as intermediate_progress_id;

END$$
DELIMITER;