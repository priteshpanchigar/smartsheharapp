DROP PROCEDURE IF EXISTS get_Emailid_Using_Ward;
DELIMITER$$
CREATE  PROCEDURE `get_Emailid_Using_Ward`(IN inissueid INT)
BEGIN
SELECT  (SELECT DISTINCT(ward_email_id) FROM issue i INNER JOIN municipal_ward mw ON i.ward = mw.ward WHERE i.issue_id = inissueid ) email_id ,
a.formatted_address, a.latitude, a.longitude, 
CONCAT(DAY(issue_time)," ",MONTHNAME(issue_time), ", ", YEAR(issue_time)," at ", TIME_FORMAT(issue_time,'%h:%i%p'))as issue_time,
i.unique_key,i.ward ,item.issue_item_description ,it.issue_type_description as 'category' ,ist.issue_sub_type_description as 'subcategory',
COUNT(im.issue_image_name) image_count ,it.department_code,
(SELECT GROUP_CONCAT(",images/",issue_image_name) FROM issue i
LEFT  OUTER JOIN issue_image im ON i.issue_id = im.issue_id
where i.issue_id = inissueid GROUP BY i.issue_id)'issue_images'
FROM issue i
LEFT JOIN issue_image im ON i.issue_id = im.issue_id
LEFT JOIN address a ON i.address_id = a.address_id
LEFT JOIN issue_lookup_items item ON i.issue_item_code = item.issue_item_code
LEFT JOIN issue_lookup_subtypes ist ON item.issue_sub_type_code = ist.issue_sub_type_code
LEFT JOIN issue_lookup_types it ON ist.issue_type_code = it.issue_type_code
WHERE i.issue_id = inissueid;

END
