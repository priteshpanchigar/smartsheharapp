DROP PROCEDURE IF EXISTS update_letter_details;
DELIMITER$$
CREATE  PROCEDURE `update_letter_details`(IN inissueid INT, 
IN inlettersubmittedto VARCHAR(255), IN inlettersubmitteddept VARCHAR(300),
IN inlettersubmitteddesg  VARCHAR(255), IN inclientdatetime datetime,
IN inletterremark  VARCHAR(300))
BEGIN

UPDATE issue
SET letter_submitted_to = inlettersubmittedto,
letter_submitted_dept = inlettersubmitteddept,
letter_submitted_desg = inlettersubmitteddesg,
letter_submitted_datetime = inclientdatetime,
letter_remark = inletterremark
WHERE issue_id = inissueid;

SELECT ROW_COUNT() AS result;

END$$
DELIMITER;

CALL update_letter_details(1,'','','','','');