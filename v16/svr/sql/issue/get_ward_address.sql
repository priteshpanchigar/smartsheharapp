DROP PROCEDURE IF EXISTS get_ward_address;
DELIMITER$$
CREATE  PROCEDURE `get_ward_address`( IN inward VARCHAR(15))
BEGIN
	
SELECT  ward_id, ward, ward_office_address FROM municipal_ward
WHERE ward = inward
ORDER BY ward_id LIMIT 1;



END$$
DELIMITER ;

CALL get_ward_address('A');