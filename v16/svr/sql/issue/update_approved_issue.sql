DROP PROCEDURE IF EXISTS update_approved_issue;
DELIMITER$$
CREATE  PROCEDURE `update_approved_issue`(IN intobesenttoauthority INT, IN inrejected INT, 
IN inissueid INT,IN inreasoncode  VARCHAR(255), IN inclientdatetime datetime,
IN inotherreasoncodecomment  VARCHAR(300))
BEGIN

UPDATE issue
SET to_be_sent_to_authority = intobesenttoauthority, 
rejected = inrejected,
approved = intobesenttoauthority,
approved_rejected_clientdatetime = inclientdatetime
WHERE issue_id = inissueid;

IF inrejected =1 THEN
UPDATE issue
SET reason_code  = inreasoncode,
other_reason_code_comment = inotherreasoncodecomment
WHERE issue_id = inissueid;
END IF;


select to_be_sent_to_authority, rejected from issue
WHERE issue_id = inissueid;



END$$
DELIMITER;

call update_approved_issue(1,0,20,"ICC","2015-10-14 10:48:15");