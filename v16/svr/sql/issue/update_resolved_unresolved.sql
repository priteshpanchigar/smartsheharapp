DROP PROCEDURE IF EXISTS update_resolved_unresolved;
DELIMITER$$
CREATE  PROCEDURE `update_resolved_unresolved`(IN inresolved INT, IN inunresolved INT, 
IN inissueid INT,IN inunresolvedcomment  VARCHAR(300),IN inclientdatetime datetime,
IN inappuserid INT)
BEGIN

UPDATE issue
SET resolved = inresolved, 
unresolved = inunresolved,
resolved_unresolved_datetime = inclientdatetime,
unresolved_comment  = inunresolvedcomment,
resolved_unresolved_by = inappuserid 
WHERE issue_id = inissueid;

select resolved, unresolved, unresolved_comment,resolved_unresolved_by from issue
WHERE issue_id = inissueid;


END$$
DELIMITER;

call update_resolved_unresolved(0,1,2,"asdasdasdsad", "2015-10-14 10:48:15");