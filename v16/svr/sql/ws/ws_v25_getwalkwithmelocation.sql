DROP PROCEDURE IF EXISTS ws_v25_getwalkwithmelocation;
DELIMITER$$
CREATE PROCEDURE `ws_v25_getwalkwithmelocation`(IN `inuser_emilid` varchar(1000),
IN `inrandomstring` varchar(100))
BEGIN
	SELECT a.fk_w_c_id as  frnkey, a.datetm as date , a.lat as lat, 
					a.lon as lon, a.accuracy as accuracy, b.stopflag as stopflag
	FROM walkwithmelocupd a
	INNER JOIN walkwithmepermissions b ON
	 b.randomstring = inrandomstring
	 ORDER BY _id DESC limit 1;
END$$
DELIMITER ;