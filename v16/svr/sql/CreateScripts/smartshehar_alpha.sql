
-- ----------------------------
-- Table structure for address
-- ----------------------------
DROP TABLE IF EXISTS `address`;
CREATE TABLE `address` (
  `address_id` int(11) NOT NULL AUTO_INCREMENT,
  `formatted_address` varchar(255) NOT NULL,
  `locality` varchar(255) DEFAULT NULL,
  `sublocality` varchar(255) DEFAULT NULL,
  `postal_code` varchar(255) DEFAULT NULL,
  `route` varchar(255) DEFAULT NULL,
  `neighborhood` varchar(255) DEFAULT NULL,
  `administrative_area_level_2` varchar(255) DEFAULT NULL,
  `administrative_area_level_1` varchar(255) DEFAULT NULL,
  `client_datetime` datetime DEFAULT NULL,
  `latitude` double DEFAULT NULL,
  `longitude` double DEFAULT NULL,
  PRIMARY KEY (`address_id`),
  UNIQUE KEY `od_formatted_address` (`formatted_address`) USING BTREE
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for appusage
-- ----------------------------
DROP TABLE IF EXISTS `appusage`;
CREATE TABLE `appusage` (
  `appusage_id` int(11) NOT NULL AUTO_INCREMENT,
  `appuser_id` int(11) NOT NULL DEFAULT '0',
  `first_location_id` int(11) DEFAULT NULL,
  `last_location_id` int(11) DEFAULT NULL,
  `clientfirstaccessdatetime` datetime DEFAULT NULL,
  `clientlastaccessdatetime` datetime DEFAULT NULL,
  `serverfirstaccessdatetime` datetime DEFAULT NULL,
  `serverlastaccessdatetime` datetime DEFAULT NULL,
  `previousversioncode` int(4) DEFAULT NULL,
  `versioncode` int(4) DEFAULT NULL,
  `version` varchar(25) DEFAULT NULL,
  `app` varchar(5) NOT NULL DEFAULT '',
  `usertype` varchar(2) DEFAULT NULL,
  PRIMARY KEY (`appusage_id`),
  UNIQUE KEY `appuser_id` (`appuser_id`,`app`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for appuser
-- ----------------------------
DROP TABLE IF EXISTS `appuser`;
CREATE TABLE `appuser` (
  `appuser_id` int(11) NOT NULL AUTO_INCREMENT,
  `gcm_registration_id` varchar(500) DEFAULT NULL,
  `email` varchar(40) NOT NULL DEFAULT '',
  `phoneno` varchar(20) DEFAULT NULL,
  `clientlastaccessdatetime` datetime DEFAULT NULL,
  `first_location_id` int(11) DEFAULT NULL,
  `last_location_id` int(11) DEFAULT NULL,
  `imei` varchar(50) NOT NULL DEFAULT '',
  `lat` double DEFAULT NULL,
  `lng` double DEFAULT NULL,
  `accuracy` double DEFAULT NULL,
  `provider` varchar(10) DEFAULT NULL,
  `carrier` varchar(50) DEFAULT NULL,
  `product` varchar(50) DEFAULT NULL,
  `manufacturer` varchar(50) DEFAULT NULL,
  `android_release_version` varchar(20) DEFAULT NULL,
  `android_sdk_version` int(5) DEFAULT NULL,
  `ip` varchar(255) DEFAULT NULL,
  `useragent` varchar(1000) DEFAULT NULL,
  `username` varchar(50) DEFAULT NULL,
  `fullname` varchar(80) DEFAULT NULL,
  `age` int(5) DEFAULT NULL,
  `sex` varchar(10) DEFAULT NULL,
  `show_notification` int(11) DEFAULT '1',
  PRIMARY KEY (`appuser_id`),
  UNIQUE KEY `dashboad__appuser_email` (`email`) USING BTREE
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for appversion
-- ----------------------------
DROP TABLE IF EXISTS `appversion`;
CREATE TABLE `appversion` (
  `_id` int(11) NOT NULL AUTO_INCREMENT,
  `versioncode` int(10) NOT NULL,
  `app` varchar(20) NOT NULL DEFAULT '',
  `link` varchar(300) CHARACTER SET utf8 NOT NULL DEFAULT '',
  PRIMARY KEY (`_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for b_routepath
-- ----------------------------
DROP TABLE IF EXISTS `b_routepath`;
CREATE TABLE `b_routepath` (
  `routepath_id` int(11) NOT NULL AUTO_INCREMENT,
  `routecode` varchar(255) DEFAULT NULL,
  `direction` varchar(255) DEFAULT NULL,
  `serial` int(10) DEFAULT NULL,
  `lat` double DEFAULT NULL,
  `lng` double DEFAULT NULL,
  PRIMARY KEY (`routepath_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for ba_routedetail
-- ----------------------------
DROP TABLE IF EXISTS `ba_routedetail`;
CREATE TABLE `ba_routedetail` (
  `_id` int(11) NOT NULL DEFAULT '0',
  `routecode` varchar(10) DEFAULT NULL,
  `stopcode` int(11) DEFAULT NULL,
  `stopserial` int(11) DEFAULT NULL,
  `direction` varchar(5) NOT NULL,
  `distance` decimal(5,3) DEFAULT NULL,
  `stopunique_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for ba_stopmaster
-- ----------------------------
DROP TABLE IF EXISTS `ba_stopmaster`;
CREATE TABLE `ba_stopmaster` (
  `_id` int(11) NOT NULL DEFAULT '0',
  `stopcode` bigint(21) unsigned DEFAULT NULL,
  `noofbuses` int(11) DEFAULT NULL,
  `stopdisplayname` varchar(300) DEFAULT NULL,
  `latu` double DEFAULT NULL,
  `latd` double DEFAULT NULL,
  `lonu` double DEFAULT NULL,
  `lond` double DEFAULT NULL,
  `lat` double DEFAULT NULL,
  `lon` double DEFAULT NULL,
  `stop_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for bus_trips
-- ----------------------------
DROP TABLE IF EXISTS `bus_trips`;
CREATE TABLE `bus_trips` (
  `trip_id` int(11) NOT NULL AUTO_INCREMENT,
  `triptype` varchar(3) DEFAULT NULL,
  `appuser_id` int(11) DEFAULT NULL,
  `start_location_id` int(11) DEFAULT NULL,
  `last_location_id` int(11) DEFAULT NULL,
  `usertype` varchar(5) DEFAULT NULL,
  `fromaddress` varchar(250) DEFAULT NULL,
  `fromshortaddress` varchar(200) DEFAULT NULL,
  `toaddress` varchar(250) DEFAULT NULL,
  `toshortaddress` varchar(200) DEFAULT NULL,
  `fromsublocality` varchar(300) DEFAULT NULL,
  `tosublocality` varchar(300) DEFAULT NULL,
  `fromlat` double DEFAULT NULL,
  `fromlng` double DEFAULT NULL,
  `tolat` double DEFAULT NULL,
  `tolng` double DEFAULT NULL,
  `trips_unique_ms` bigint(20) DEFAULT NULL,
  `tripcreationtime` datetime DEFAULT NULL,
  `planned_start_time` varchar(10) DEFAULT NULL,
  `planned_start_datetime` datetime DEFAULT NULL,
  `trip_action` varchar(2) DEFAULT NULL,
  `trip_action_time` datetime DEFAULT NULL,
  `triptime` int(100) DEFAULT NULL,
  `tripdistance` double DEFAULT NULL,
  `driversnearestnodeno` int(11) DEFAULT NULL,
  `hasdriver_moved` int(1) DEFAULT '0',
  `hasdriver_moved_time` datetime DEFAULT NULL,
  `hasdriver_started` int(1) DEFAULT '0',
  `trip_end_notification_sent` int(1) NOT NULL DEFAULT '0',
  `car_desc` varchar(255) DEFAULT NULL,
  `start_landmark` varchar(255) DEFAULT NULL,
  `trip_note` varchar(255) DEFAULT NULL,
  `trip_directions_polyline` varbinary(2000) DEFAULT NULL,
  `empty_seats` int(3) DEFAULT NULL,
  `rate` int(3) DEFAULT NULL,
  `show_only_invited_friends` int(1) NOT NULL DEFAULT '0',
  `open_trip` int(5) DEFAULT '0',
  `shared_cab` int(5) NOT NULL DEFAULT '0',
  `open_trip_notification_sent` int(11) NOT NULL DEFAULT '0',
  `open_trip_notification_datetime` datetime DEFAULT NULL,
  `last_invited` int(11) NOT NULL DEFAULT '0',
  `friend_notify_list` varchar(500) DEFAULT NULL,
  `community_name` varchar(100) DEFAULT NULL,
  `premium_seats` int(5) DEFAULT NULL,
  `window_seats` int(5) DEFAULT NULL,
  `standard_seats` int(5) DEFAULT NULL,
  `premium_seat_fare` int(5) DEFAULT NULL,
  `window_seat_fare` int(5) DEFAULT NULL,
  `standard_seat_fare` int(5) DEFAULT NULL,
  PRIMARY KEY (`trip_id`),
  KEY `st_trip_id` (`trip_id`),
  KEY `st_appuser_id` (`appuser_id`),
  KEY `st_trip_action` (`trip_action`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for bus_trips_path
-- ----------------------------
DROP TABLE IF EXISTS `bus_trips_path`;
CREATE TABLE `bus_trips_path` (
  `trip_path_id` int(11) NOT NULL AUTO_INCREMENT,
  `trip_id` int(11) DEFAULT NULL,
  `nodeno` int(11) DEFAULT NULL,
  `lat` double DEFAULT NULL,
  `lng` double DEFAULT NULL,
  PRIMARY KEY (`trip_path_id`),
  KEY `stp_trip_id` (`trip_id`),
  KEY `stp_lat_lng` (`lat`,`lng`),
  KEY `stp_nodeno` (`nodeno`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for bustrip
-- ----------------------------
DROP TABLE IF EXISTS `bustrip`;
CREATE TABLE `bustrip` (
  `trip_id` int(11) NOT NULL AUTO_INCREMENT,
  `appuser_id` int(11) DEFAULT NULL,
  `last_location_id` int(11) DEFAULT NULL,
  `vehicle_no` varchar(255) DEFAULT NULL,
  `buslabel` varchar(255) DEFAULT NULL,
  `createdatetime` datetime DEFAULT NULL,
  `fromstop` varchar(1000) DEFAULT NULL,
  `tostop` varchar(1000) DEFAULT NULL,
  `fromstopserial` int(11) DEFAULT NULL,
  `tostopserial` int(11) DEFAULT NULL,
  `tripstatus` varchar(255) DEFAULT NULL,
  `direction_sign` int(11) DEFAULT NULL,
  `direction` varchar(5) DEFAULT NULL,
  PRIMARY KEY (`trip_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for callhome
-- ----------------------------
DROP TABLE IF EXISTS `callhome`;
CREATE TABLE `callhome` (
  `callhome_id` int(11) NOT NULL AUTO_INCREMENT,
  `appuser_id` int(11) NOT NULL DEFAULT '0',
  `appusage_id` int(11) DEFAULT NULL,
  `clientdatetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `serverdatetime` datetime DEFAULT NULL,
  `locationdatetime` datetime DEFAULT NULL,
  `lat` double DEFAULT NULL,
  `lng` double DEFAULT NULL,
  `accuracy` int(11) DEFAULT NULL,
  `module` varchar(20) NOT NULL DEFAULT '',
  `app` varchar(10) NOT NULL DEFAULT '',
  PRIMARY KEY (`appuser_id`,`app`,`module`,`clientdatetime`),
  UNIQUE KEY `callhome_id` (`callhome_id`),
  KEY `ch_module` (`module`),
  KEY `ch_serverdatetime` (`serverdatetime`),
  KEY `ch_clientdatetime` (`clientdatetime`),
  KEY `ch_primary` (`appuser_id`,`clientdatetime`,`module`,`app`) USING BTREE
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for city_authority_emailids
-- ----------------------------
DROP TABLE IF EXISTS `city_authority_emailids`;
CREATE TABLE `city_authority_emailids` (
  `city_authority_emailids_id` int(11) NOT NULL AUTO_INCREMENT,
  `city` varchar(255) DEFAULT NULL,
  `email_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`city_authority_emailids_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for city_pincode_authority_emailids
-- ----------------------------
DROP TABLE IF EXISTS `city_pincode_authority_emailids`;
CREATE TABLE `city_pincode_authority_emailids` (
  `city_pincode_authority_emailids_id` int(11) NOT NULL AUTO_INCREMENT,
  `city` varchar(50) DEFAULT NULL,
  `postal_code` varchar(6) DEFAULT NULL,
  `email_id` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`city_pincode_authority_emailids_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for customer
-- ----------------------------
DROP TABLE IF EXISTS `customer`;
CREATE TABLE `customer` (
  `customer_id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `company` varchar(255) DEFAULT NULL,
  `country_code` varchar(255) DEFAULT NULL,
  `phone` varchar(15) DEFAULT NULL,
  `flat` varchar(11) DEFAULT NULL,
  `wing` varchar(255) DEFAULT NULL,
  `floor` varchar(20) DEFAULT NULL,
  `complex` varchar(255) DEFAULT NULL,
  `building` varchar(255) DEFAULT NULL,
  `area` varchar(255) DEFAULT NULL,
  `road` varchar(255) DEFAULT NULL,
  `landmark_1` varchar(255) DEFAULT NULL,
  `landmark_2` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `postal_code` int(11) DEFAULT NULL,
  `created` varchar(255) DEFAULT NULL,
  `last_updated` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `client_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`customer_id`),
  UNIQUE KEY `c_phoneno` (`phone`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for customer_calls
-- ----------------------------
DROP TABLE IF EXISTS `customer_calls`;
CREATE TABLE `customer_calls` (
  `customer_calls_id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) DEFAULT NULL,
  `vendor_id` int(11) DEFAULT NULL,
  `vendor_phones_id` int(11) DEFAULT NULL,
  `call_datetime` datetime DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  PRIMARY KEY (`customer_calls_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for department
-- ----------------------------
DROP TABLE IF EXISTS `department`;
CREATE TABLE `department` (
  `department_id` int(11) NOT NULL AUTO_INCREMENT,
  `department_code` varchar(255) DEFAULT NULL,
  `department_description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`department_id`),
  KEY `department_code_department` (`department_code`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for governance_flag
-- ----------------------------
DROP TABLE IF EXISTS `governance_flag`;
CREATE TABLE `governance_flag` (
  `governance_id` int(11) NOT NULL AUTO_INCREMENT,
  `appuser_id` int(11) DEFAULT NULL,
  `approve_other` int(11) DEFAULT NULL,
  `approve_mine` int(11) DEFAULT NULL,
  PRIMARY KEY (`governance_id`),
  UNIQUE KEY `governance_appuserid` (`appuser_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for group_members
-- ----------------------------
DROP TABLE IF EXISTS `group_members`;
CREATE TABLE `group_members` (
  `group_member_id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `appuser_id` int(11) NOT NULL,
  `member_name` varchar(255) DEFAULT NULL,
  `member_emailid` varchar(100) DEFAULT NULL,
  `member_contactno` varchar(100) DEFAULT NULL,
  `group_name` varchar(255) NOT NULL,
  PRIMARY KEY (`group_member_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for groupdetails
-- ----------------------------
DROP TABLE IF EXISTS `groupdetails`;
CREATE TABLE `groupdetails` (
  `group_id` int(11) NOT NULL AUTO_INCREMENT,
  `group_name` varchar(255) NOT NULL,
  `group_type` varchar(255) DEFAULT NULL,
  `group_area` varchar(255) NOT NULL,
  `group_description` varchar(255) DEFAULT NULL,
  `appuser_id` int(11) DEFAULT NULL,
  `postal_code` varchar(11) DEFAULT NULL,
  `group_phoneno` int(15) DEFAULT NULL,
  `group_email` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`group_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for intermediate_progress
-- ----------------------------
DROP TABLE IF EXISTS `intermediate_progress`;
CREATE TABLE `intermediate_progress` (
  `intermediate_progress_id` int(11) NOT NULL AUTO_INCREMENT,
  `appuser_id` int(11) DEFAULT NULL,
  `issue_id` int(255) DEFAULT NULL,
  `intermediate_progress_comment` varchar(255) DEFAULT NULL,
  `intermediate_progress_clientdatetime` datetime DEFAULT NULL,
  PRIMARY KEY (`intermediate_progress_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for issue
-- ----------------------------
DROP TABLE IF EXISTS `issue`;
CREATE TABLE `issue` (
  `issue_id` int(255) NOT NULL AUTO_INCREMENT,
  `issue_time` datetime DEFAULT NULL,
  `client_datetime` datetime DEFAULT NULL,
  `vehicle_no` varchar(15) DEFAULT NULL,
  `unique_key` varchar(255) NOT NULL,
  `submit_report` int(11) NOT NULL DEFAULT '0',
  `approved` int(11) NOT NULL DEFAULT '0',
  `rejected` int(11) NOT NULL DEFAULT '0',
  `approved_rejected_clientdatetime` datetime DEFAULT NULL,
  `reason_code` varchar(255) DEFAULT NULL,
  `other_reason_code_comment` varchar(255) DEFAULT NULL,
  `to_be_sent_to_authority` int(11) NOT NULL DEFAULT '0',
  `sent_to_authority` int(11) NOT NULL DEFAULT '0',
  `sent_to_authority_datetime` datetime DEFAULT NULL,
  `resolved` int(11) NOT NULL DEFAULT '0',
  `unresolved` int(11) NOT NULL DEFAULT '0',
  `resolved_unresolved_datetime` datetime DEFAULT NULL,
  `unresolved_comment` varchar(300) DEFAULT NULL,
  `address_id` int(11) NOT NULL,
  `location_id` int(11) DEFAULT NULL,
  `ward` varchar(255) DEFAULT NULL,
  `group_id` int(11) DEFAULT NULL,
  `issue_item_code` varchar(255) DEFAULT NULL,
  `mla_id` int(5) DEFAULT NULL,
  `mp_id` int(5) DEFAULT NULL,
  `lat` double DEFAULT NULL,
  `lng` double DEFAULT NULL,
  `appuser_id` int(11) DEFAULT NULL,
  `discard_report` int(11) DEFAULT '0',
  `letter_submit_by` int(11) DEFAULT NULL,
  `issue_registration_notification` int(11) DEFAULT '0',
  `letter_submitted_to` varchar(255) DEFAULT '',
  `letter_submitted_dept` varchar(255) DEFAULT NULL,
  `letter_submitted_desg` varchar(255) DEFAULT NULL,
  `letter_submitted_datetime` datetime DEFAULT NULL,
  `letter_remark` varchar(255) DEFAULT NULL,
  `resolved_unresolved_by` int(11) DEFAULT NULL,
  `letter_upload_notification` int(11) DEFAULT '0',
  `resolved_unresolved_notification` int(11) DEFAULT '0',
  `letter_client_date_time` datetime DEFAULT NULL,
  `letter_image_name` varchar(255) DEFAULT NULL,
  `letter_image_path` varchar(255) DEFAULT NULL,
  `closed` int(11) DEFAULT '0',
  `opened` int(11) DEFAULT '0',
  `closed_opened_datetime` datetime DEFAULT NULL,
  `closed_opened_by` int(11) DEFAULT NULL,
  `closed_opened_comment` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`issue_id`),
  UNIQUE KEY `od_unique_key` (`unique_key`) USING BTREE
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for issue_image
-- ----------------------------
DROP TABLE IF EXISTS `issue_image`;
CREATE TABLE `issue_image` (
  `issue_image_id` int(11) NOT NULL AUTO_INCREMENT,
  `issue_id` int(11) NOT NULL,
  `issue_video_path` varchar(255) DEFAULT NULL,
  `issue_video_name` varchar(255) DEFAULT NULL,
  `issue_image_name` varchar(255) DEFAULT NULL,
  `issue_image_path` varchar(255) CHARACTER SET latin7 DEFAULT NULL,
  `creation_datetime` datetime DEFAULT NULL,
  `issue_uniquekey` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`issue_image_id`),
  UNIQUE KEY `unique_creation_datetime` (`creation_datetime`) USING BTREE
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for issue_lookup_items
-- ----------------------------
DROP TABLE IF EXISTS `issue_lookup_items`;
CREATE TABLE `issue_lookup_items` (
  `issue_lookup_item_id` int(11) NOT NULL AUTO_INCREMENT,
  `issue_sub_type_code` varchar(255) DEFAULT NULL,
  `issue_item_code` varchar(255) DEFAULT NULL,
  `issue_item_description` varchar(255) DEFAULT NULL,
  `issue_item_order` smallint(2) DEFAULT NULL,
  `photo_mandatory` bit(1) DEFAULT NULL,
  PRIMARY KEY (`issue_lookup_item_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for issue_lookup_subtypes
-- ----------------------------
DROP TABLE IF EXISTS `issue_lookup_subtypes`;
CREATE TABLE `issue_lookup_subtypes` (
  `issue_lookup_sub_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `issue_type_code` varchar(255) DEFAULT NULL,
  `issue_sub_type_code` varchar(255) DEFAULT NULL,
  `issue_sub_type_description` varchar(255) DEFAULT NULL,
  `issue_sub_type_order` smallint(2) DEFAULT NULL,
  `vehicle_number_mandatory` bit(1) DEFAULT NULL,
  PRIMARY KEY (`issue_lookup_sub_type_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for issue_lookup_types
-- ----------------------------
DROP TABLE IF EXISTS `issue_lookup_types`;
CREATE TABLE `issue_lookup_types` (
  `issue_lookup_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `issue_type_code` varchar(255) DEFAULT NULL,
  `issue_type_description` varchar(255) DEFAULT NULL,
  `department_code` varchar(255) DEFAULT NULL,
  `colour` varchar(255) DEFAULT NULL,
  `android_colour` int(255) DEFAULT '0',
  PRIMARY KEY (`issue_lookup_type_id`),
  KEY `department_code_itc` (`department_code`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for issue_rating
-- ----------------------------
DROP TABLE IF EXISTS `issue_rating`;
CREATE TABLE `issue_rating` (
  `issue_rating_id` int(11) NOT NULL AUTO_INCREMENT,
  `appuser_id` int(11) DEFAULT NULL,
  `issue_id` int(11) DEFAULT NULL,
  `issue_like` int(11) DEFAULT NULL,
  `issue_rating_comment` varchar(255) DEFAULT NULL,
  `issue_rating_clientdatetime` datetime DEFAULT NULL,
  PRIMARY KEY (`issue_rating_id`),
  UNIQUE KEY `ir_appuser_id_offence_detail_id` (`appuser_id`,`issue_id`,`issue_rating_clientdatetime`) USING BTREE
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for location
-- ----------------------------
DROP TABLE IF EXISTS `location`;
CREATE TABLE `location` (
  `location_id` int(11) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `appuser_id` int(11) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `active` int(1) DEFAULT NULL,
  `isdriver` int(1) DEFAULT NULL,
  `ispassenger` int(1) DEFAULT NULL,
  `iscommercial` int(1) DEFAULT NULL,
  `isuser` int(1) DEFAULT NULL,
  `lat` double DEFAULT NULL,
  `lng` double DEFAULT NULL,
  `accuracy` int(11) DEFAULT NULL,
  `altitude` double DEFAULT NULL,
  `bearing` float DEFAULT NULL,
  `address` varchar(100) DEFAULT NULL,
  `firsttime` datetime DEFAULT NULL,
  `loctime` datetime DEFAULT NULL,
  `clientaccessdatetime` datetime DEFAULT NULL,
  `speed` float DEFAULT NULL,
  `cumulative_distance` decimal(5,3) DEFAULT NULL,
  `provider` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`location_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for muncipal_mp
-- ----------------------------
DROP TABLE IF EXISTS `muncipal_mp`;
CREATE TABLE `muncipal_mp` (
  `mp_id` int(11) NOT NULL AUTO_INCREMENT,
  `mp` varchar(255) DEFAULT NULL,
  `city_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`mp_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for municipal_city
-- ----------------------------
DROP TABLE IF EXISTS `municipal_city`;
CREATE TABLE `municipal_city` (
  `city_id` int(11) NOT NULL AUTO_INCREMENT,
  `city` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`city_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for municipal_mla
-- ----------------------------
DROP TABLE IF EXISTS `municipal_mla`;
CREATE TABLE `municipal_mla` (
  `mla_id` int(11) NOT NULL AUTO_INCREMENT,
  `mla` varchar(255) DEFAULT NULL,
  `mla_phoneno` varchar(15) DEFAULT NULL,
  `mla_email` varchar(255) DEFAULT NULL,
  `mla_age` int(5) DEFAULT NULL,
  `mla_gender` varchar(10) DEFAULT NULL,
  `mla_Constituency` varchar(255) DEFAULT NULL,
  `political_Affiliations` varchar(255) DEFAULT NULL,
  `mp_id` int(11) DEFAULT NULL,
  `city_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`mla_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for municipal_ward
-- ----------------------------
DROP TABLE IF EXISTS `municipal_ward`;
CREATE TABLE `municipal_ward` (
  `ward_id` int(11) NOT NULL AUTO_INCREMENT,
  `ward` varchar(255) DEFAULT NULL,
  `mla` varchar(255) DEFAULT NULL,
  `mla_id` int(11) DEFAULT NULL,
  `mp_id` int(11) DEFAULT NULL,
  `city_id` int(11) DEFAULT NULL,
  `ward_email_id` varchar(255) DEFAULT NULL,
  `ward_office_address` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ward_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for municipal_ward_copy
-- ----------------------------
DROP TABLE IF EXISTS `municipal_ward_copy`;
CREATE TABLE `municipal_ward_copy` (
  `ward_id` int(11) NOT NULL DEFAULT '0',
  `ward` varchar(255) DEFAULT NULL,
  `mla` varchar(255) DEFAULT NULL,
  `mla_id` int(11) DEFAULT NULL,
  `mp_id` int(11) DEFAULT NULL,
  `city_id` int(11) DEFAULT NULL,
  `ward_email_id` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for order
-- ----------------------------
DROP TABLE IF EXISTS `order`;
CREATE TABLE `order` (
  `order_id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) DEFAULT NULL,
  `vendor_id` int(11) DEFAULT NULL,
  `order_datetime` datetime DEFAULT NULL,
  `order_amount` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`order_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for postal_code
-- ----------------------------
DROP TABLE IF EXISTS `postal_code`;
CREATE TABLE `postal_code` (
  `postal_code_id` int(11) NOT NULL AUTO_INCREMENT,
  `postal_code` int(255) DEFAULT NULL,
  `area` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`postal_code_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for rejected_reason
-- ----------------------------
DROP TABLE IF EXISTS `rejected_reason`;
CREATE TABLE `rejected_reason` (
  `rejected_id` int(11) NOT NULL AUTO_INCREMENT,
  `reject_reason` varchar(255) DEFAULT NULL,
  `reason_code` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`rejected_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for rs_cityinfo
-- ----------------------------
DROP TABLE IF EXISTS `rs_cityinfo`;
CREATE TABLE `rs_cityinfo` (
  `city_id` int(11) NOT NULL,
  `city_code` longtext,
  `city` longtext,
  `country` longtext,
  `distance_unit` longtext,
  `currency` longtext,
  `centerlat` double DEFAULT NULL,
  `centerlon` double DEFAULT NULL,
  `citytoplat` double DEFAULT NULL,
  `citytoplon` double DEFAULT NULL,
  `citybottomlat` double DEFAULT NULL,
  `citybottomlon` double DEFAULT NULL,
  PRIMARY KEY (`city_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for rs_farecalculation
-- ----------------------------
DROP TABLE IF EXISTS `rs_farecalculation`;
CREATE TABLE `rs_farecalculation` (
  `_id` int(11) NOT NULL AUTO_INCREMENT,
  `srno` int(11) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `transport` varchar(255) DEFAULT NULL,
  `vehicle_type` varchar(255) DEFAULT NULL,
  `item` varchar(255) DEFAULT NULL,
  `calculation` varchar(255) DEFAULT NULL,
  `style` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for rs_lookup
-- ----------------------------
DROP TABLE IF EXISTS `rs_lookup`;
CREATE TABLE `rs_lookup` (
  `field_id` int(11) NOT NULL,
  `fieldname` longtext,
  `code` longtext,
  `description` longtext,
  PRIMARY KEY (`field_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for share_bus_location
-- ----------------------------
DROP TABLE IF EXISTS `share_bus_location`;
CREATE TABLE `share_bus_location` (
  `share_bus_location_id` int(11) NOT NULL AUTO_INCREMENT,
  `appuser_id` int(11) DEFAULT NULL,
  `buslabel` varchar(255) DEFAULT NULL,
  `stopserial` int(11) DEFAULT NULL,
  `direction` varchar(255) DEFAULT NULL,
  `location_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`share_bus_location_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Table structure for t_megablock
-- ----------------------------
DROP TABLE IF EXISTS `t_megablock`;
CREATE TABLE `t_megablock` (
  `_id` int(11) NOT NULL AUTO_INCREMENT,
  `linecode` varchar(11) DEFAULT NULL,
  `up` int(1) DEFAULT NULL,
  `down` int(1) DEFAULT NULL,
  `slow` int(1) DEFAULT NULL,
  `fast` int(1) DEFAULT NULL,
  `fromdatetime` datetime DEFAULT NULL,
  `todatetime` datetime DEFAULT NULL,
  `fromstation` varchar(20) DEFAULT NULL,
  `tostation` varchar(20) DEFAULT NULL,
  `fromstation_id` int(3) DEFAULT NULL,
  `tostation_id` int(3) DEFAULT NULL,
  `fromstationcode` varchar(11) DEFAULT NULL,
  `tostationcode` varchar(11) DEFAULT NULL,
  `display` longtext,
  `link` longtext,
  `message` longtext,
  `repeatdays` int(2) DEFAULT '0',
  PRIMARY KEY (`_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Table structure for vehiclefareparameter
-- ----------------------------
DROP TABLE IF EXISTS `vehiclefareparameter`;
CREATE TABLE `vehiclefareparameter` (
  `vehiclefareparameter_id` int(11) NOT NULL,
  `serialno` int(11) DEFAULT NULL,
  `date` longtext,
  `city` longtext,
  `city_id` int(11) DEFAULT NULL,
  `vehicletype` longtext,
  `transport` longtext,
  `vehiclecapacity` int(11) DEFAULT NULL,
  `minimumdistance` double DEFAULT NULL,
  `minimumfare` double DEFAULT NULL,
  `fareperkm` double DEFAULT NULL,
  `metermovesperkm` double DEFAULT NULL,
  `meterrounding` double DEFAULT NULL,
  `minimumwaitingminutes` int(11) DEFAULT NULL,
  `waitchargeperhour` int(11) DEFAULT NULL,
  `waitingspeed` int(11) DEFAULT NULL,
  `nightextra` double DEFAULT NULL,
  `luggagechargeperpiece` double DEFAULT NULL,
  `nightstart` double DEFAULT NULL,
  `nightend` double DEFAULT NULL,
  `minimumnightfare` double DEFAULT NULL,
  `metertype` longtext,
  `farepercentincreaseperpassenger` int(11) DEFAULT NULL,
  `courtesy` longtext,
  `link` longtext,
  `vehicletypedescription` varchar(255) DEFAULT NULL,
  `transportdescription` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`vehiclefareparameter_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for vendor
-- ----------------------------
DROP TABLE IF EXISTS `vendor`;
CREATE TABLE `vendor` (
  `vendor_id` int(11) NOT NULL AUTO_INCREMENT,
  `vendor_name` varchar(255) DEFAULT NULL,
  `country_code` varchar(255) DEFAULT NULL,
  `phoneno` varchar(255) DEFAULT NULL,
  `floor_no` varchar(20) DEFAULT NULL,
  `wing` varchar(255) DEFAULT NULL,
  `building_name` varchar(255) DEFAULT NULL,
  `area` varchar(255) DEFAULT NULL,
  `roadname` varchar(255) DEFAULT NULL,
  `landmark_1` varchar(255) DEFAULT NULL,
  `landmark_2` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `postal_code` int(11) DEFAULT NULL,
  `client_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`vendor_id`),
  UNIQUE KEY `v_phoneno` (`phoneno`) USING BTREE
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for vendor_customers
-- ----------------------------
DROP TABLE IF EXISTS `vendor_customers`;
CREATE TABLE `vendor_customers` (
  `vendor_customers_id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) DEFAULT NULL,
  `vendor_id` int(11) DEFAULT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `company` varchar(255) DEFAULT NULL,
  `country_code` varchar(255) DEFAULT NULL,
  `phone` varchar(15) DEFAULT NULL,
  `flat` int(11) DEFAULT NULL,
  `wing` varchar(255) DEFAULT NULL,
  `floor` varchar(20) DEFAULT NULL,
  `complex` varchar(255) DEFAULT NULL,
  `building` varchar(255) DEFAULT NULL,
  `area` varchar(255) DEFAULT NULL,
  `road` varchar(255) DEFAULT NULL,
  `landmark_1` varchar(255) DEFAULT NULL,
  `landmark_2` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `postal_code` int(11) DEFAULT NULL,
  `created` varchar(255) DEFAULT NULL,
  `last_updated` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `client_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`vendor_customers_id`),
  UNIQUE KEY `vc_unique` (`phone`,`vendor_id`) USING BTREE
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for vendor_phones
-- ----------------------------
DROP TABLE IF EXISTS `vendor_phones`;
CREATE TABLE `vendor_phones` (
  `vendor_phones_id` int(11) NOT NULL AUTO_INCREMENT,
  `vendor_id` int(11) DEFAULT NULL,
  `phoneno` varchar(255) DEFAULT NULL,
  `appuser_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`vendor_phones_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for walkwithme_contacts
-- ----------------------------
DROP TABLE IF EXISTS `walkwithme_contacts`;
CREATE TABLE `walkwithme_contacts` (
  `_id` int(11) NOT NULL AUTO_INCREMENT,
  `w_contact_user_emailid` varchar(75) DEFAULT NULL,
  `w_contact_name` varchar(75) DEFAULT NULL,
  `w_contact_emailid` varchar(75) DEFAULT NULL,
  `w_creationdatetime` datetime NOT NULL,
  PRIMARY KEY (`_id`),
  UNIQUE KEY `unique_walkwithme` (`w_contact_user_emailid`,`w_contact_emailid`,`w_creationdatetime`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for walkwithmelocupd
-- ----------------------------
DROP TABLE IF EXISTS `walkwithmelocupd`;
CREATE TABLE `walkwithmelocupd` (
  `_id` int(11) NOT NULL AUTO_INCREMENT,
  `fk_w_c_id` int(11) DEFAULT NULL,
  `user_emailid` varchar(100) DEFAULT NULL,
  `imei` varchar(255) DEFAULT NULL,
  `datetm` datetime DEFAULT NULL,
  `lat` double DEFAULT NULL,
  `lon` double DEFAULT NULL,
  `accuracy` double DEFAULT NULL,
  `serverdatetime` datetime DEFAULT NULL,
  `intrip` bit(1) DEFAULT NULL,
  `mode` varchar(5) DEFAULT NULL,
  PRIMARY KEY (`_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for walkwithmepermissions
-- ----------------------------
DROP TABLE IF EXISTS `walkwithmepermissions`;
CREATE TABLE `walkwithmepermissions` (
  `w_c_id` int(11) NOT NULL AUTO_INCREMENT,
  `w_c_user_emailid` varchar(100) DEFAULT NULL,
  `randomstring` varchar(1000) DEFAULT NULL,
  `mode` varchar(255) DEFAULT NULL,
  `stopflag` varchar(1) DEFAULT NULL,
  `intrip` bit(1) DEFAULT NULL,
  `user_link` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`w_c_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for walkwithmepermissionsstop
-- ----------------------------
DROP TABLE IF EXISTS `walkwithmepermissionsstop`;
CREATE TABLE `walkwithmepermissionsstop` (
  `_id` int(11) NOT NULL AUTO_INCREMENT,
  `w_s_user_emailid` varchar(100) DEFAULT NULL,
  `u_randomstring` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for ws_photos
-- ----------------------------
DROP TABLE IF EXISTS `ws_photos`;
CREATE TABLE `ws_photos` (
  `image_id` int(11) NOT NULL AUTO_INCREMENT,
  `lat` double DEFAULT NULL,
  `lon` double DEFAULT NULL,
  `address` varchar(300) DEFAULT NULL,
  `imagefilename` varchar(100) DEFAULT NULL,
  `phoneno` varchar(30) DEFAULT NULL,
  `photodate` datetime DEFAULT NULL,
  `imei` varchar(30) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `to_email` varchar(100) DEFAULT NULL,
  `from_email` varchar(100) DEFAULT NULL,
  `cc_email` varchar(100) DEFAULT NULL,
  `serverdatetime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`image_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
