DROP PROCEDURE IF EXISTS share_bus_location;
DELIMITER $$
CREATE  PROCEDURE `share_bus_location`(IN `inappuserid` INT, IN `inclientdatetime` datetime,
	IN `inemailid` varchar(100), 
	IN `inlatno` double, IN `inlngno` double, IN `inacc` int, 
	IN `inlocationdatetime` datetime, IN `inprovider` varchar(10),
	IN `inspeed` float, IN `inbearing` float, IN `inaltitude` double,
	IN  inbuslabel varchar (25), IN instopserial INT, IN  indirection varchar (10))
BEGIN
	DECLARE vLocationId INT DEFAULT 0;
	
	SET @appuserId = inappuserid;
	
	SET vLocationId = insert_location(@appUserId, inemailid, 1, 0, inclientdatetime,
			inlatno, inlngno, inacc, inlocationdatetime,  inprovider, inspeed, inbearing, 
			inaltitude);
	
	INSERT INTO	share_bus_location(appuser_id, buslabel, stopserial, direction , location_id)
	VALUES (@appuserId, inbuslabel, instopserial, indirection, vLocationId);

END$$
DELIMITER;


call share_bus_location(1,"2016-01-30 11:45:28",'soumenmaity.cse@gmail.com',"19.104567300","72.850594900","30.0","2016-01-30 11:45:24","N","0.0","0.0","0.0",'AS-2',29,'D')