
DROP TABLE IF EXISTS t1;
CREATE TABLE t1 AS
SELECT frequency_id, startheadwayhh, endheadwayhh, startheadwaymm, endheadwaymm, frequency,  startdayweek, enddayweek,
holiday, f.routecode, firstfromhh, lastfromhh, firstfrommm, lastfrommm, `schedule`, 
stopcodefrom, stopserial  AS stopserialfrom,
stopcodeto, f.direction  FROM ba_frequency f
LEFT JOIN ba_routedetail rd
ON f.direction = rd.direction
AND f.routecode = rd.routecode
AND f.stopcodefrom = rd.stopcode;

DROP TABLE IF EXISTS ba_frequency_2016;
CREATE TABLE ba_frequency_2016 AS
SELECT frequency_id, startheadwayhh, endheadwayhh, startheadwaymm, endheadwaymm, frequency,  startdayweek, enddayweek,
holiday, f.routecode, firstfromhh, lastfromhh, firstfrommm, lastfrommm, `schedule`, 
stopcodefrom,  stopserialfrom,
stopcodeto, stopserial as stopserialto, f.direction  FROM t1 f
LEFT JOIN ba_routedetail rd
ON f.direction = rd.direction
AND f.routecode = rd.routecode
AND f.stopcodeto = rd.stopcode;

DROP TABLE IF EXISTS t3;
CREATE TABLE t3 AS
SELECT 
frequency_id, startheadwayhh, endheadwayhh, startheadwaymm, endheadwaymm, frequency, startdayweek,
enddayweek, holiday, f.routecode, firsttohh, lasttohh, firsttomm, lasttomm, schedule,
stopcodefrom, stopserial  AS stopserialfrom,
stopcodeto, f.direction  FROM ba_frequency2 f
LEFT JOIN ba_routedetail rd
ON f.direction = rd.direction
AND f.routecode = rd.routecode
AND f.stopcodefrom = rd.stopcode;

DROP TABLE IF EXISTS ba_frequency2_2016;
CREATE TABLE ba_frequency2_2016 AS
SELECT 
frequency_id, startheadwayhh, endheadwayhh, startheadwaymm, endheadwaymm, frequency, startdayweek,
enddayweek, holiday,f.routecode, firsttohh, lasttohh, firsttomm, lasttomm, schedule,
stopcodefrom,  stopserialfrom,
stopcodeto, stopserial as stopserialto, f.direction  FROM t3 f
LEFT JOIN ba_routedetail rd
ON f.direction = rd.direction
AND f.routecode = rd.routecode
AND f.stopcodeto = rd.stopcode;


DROP TABLE IF EXISTS ts1;
CREATE TABLE ts1 AS
SELECT summary_id, startheadwayhh, endheadwayhh, startheadwaymm, endheadwaymm, frequency, f.routecode,
firstfrom, lastfrom, SCHEDULE,  firstfrominmin, lastfrominmin, firstfromhh, lastfromhh, firstfrommm,
lastfrommm, startdayweek, enddayweek, holiday,
stopcodefrom, stopserial  AS stopserialfrom,
stopcodeto, f.direction  FROM ba_frequency_summary f
LEFT JOIN ba_routedetail rd
ON f.direction = rd.direction
AND f.routecode = rd.routecode
AND f.stopcodefrom = rd.stopcode;


DROP TABLE IF EXISTS ba_frequency_summary_2016;
CREATE TABLE ba_frequency_summary_2016 AS
SELECT summary_id, startheadwayhh, endheadwayhh, startheadwaymm, endheadwaymm, frequency, f.routecode,
firstfrom, lastfrom, SCHEDULE,  firstfrominmin, lastfrominmin, firstfromhh, lastfromhh, firstfrommm,
lastfrommm, startdayweek, enddayweek, holiday,
stopcodefrom,  stopserialfrom,
stopcodeto, stopserial as stopserialto, f.direction  FROM ts1 f
LEFT JOIN ba_routedetail rd
ON f.direction = rd.direction
AND f.routecode = rd.routecode
AND f.stopcodeto = rd.stopcode;


DROP TABLE IF EXISTS ts3;
CREATE TABLE ts3 AS
SELECT summary_id, startheadwayhh, endheadwayhh, startheadwaymm, endheadwaymm, frequency, f.routecode, firstto,
lastto, SCHEDULE, firsttoinmin, lasttoinmin,
firsttohh, lasttohh, firsttomm, lasttomm, startdayweek, enddayweek, holiday,
stopcodefrom, stopserial  AS stopserialfrom,
stopcodeto, f.direction  FROM ba_frequency_summary2 f
LEFT JOIN ba_routedetail rd
ON f.direction = rd.direction
AND f.routecode = rd.routecode
AND f.stopcodefrom = rd.stopcode;


DROP TABLE IF EXISTS ba_frequency_summary2_2016;
CREATE TABLE ba_frequency_summary2_2016 AS
SELECT summary_id, startheadwayhh, endheadwayhh, startheadwaymm, endheadwaymm, frequency, f.routecode, firstto,
lastto, SCHEDULE, firsttoinmin, lasttoinmin,
firsttohh, lasttohh, firsttomm, lasttomm, startdayweek, enddayweek, holiday,
stopcodefrom,  stopserialfrom,
stopcodeto, stopserial as stopserialto, f.direction  FROM ts3 f
LEFT JOIN ba_routedetail rd
ON f.direction = rd.direction
AND f.routecode = rd.routecode
AND f.stopcodeto = rd.stopcode;



DROP TABLE IF EXISTS ts4;
CREATE TABLE ts4 AS
SELECT f._id, startdayweek, enddayweek, holiday, f.routecode,  schedule, firststop,
laststop, hh, mm,
stopcodefrom, stopserial  AS stopserialfrom,
stopcodeto, f.direction FROM ba_frequencyac f
LEFT JOIN ba_routedetail rd
ON f.direction = rd.direction
AND f.routecode = rd.routecode
AND f.stopcodefrom = rd.stopcode; 

DROP TABLE IF EXISTS ba_frequencyac_2016;
CREATE TABLE ba_frequencyac_2016 AS
SELECT f._id, startdayweek, enddayweek, holiday, f.routecode,  schedule, firststop,
laststop, hh, mm,
stopcodefrom,  stopserialfrom,
stopcodeto, stopserial as stopserialto, f.direction FROM ts4 f
LEFT JOIN ba_routedetail rd
ON f.direction = rd.direction
AND f.routecode = rd.routecode
AND f.stopcodeto = rd.stopcode;


