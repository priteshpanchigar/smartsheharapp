DROP PROCEDURE IF EXISTS get_route_stoppoints;
DELIMITER $$
CREATE  PROCEDURE `get_route_stoppoints`(IN inroutecode varchar (40))
BEGIN


SELECT CONCAT('"', REPLACE(s.stopdisplayname, "\\", "/"), '"') stopname, 
CONCAT('{"Geometry":{"Latitude":', lat,',"Longitude":', lon, '}}') routepoints
FROM  ba_routedetail r 
LEFT JOIN ba_stopmaster s
ON r.stopcode = s.stopcode
AND direction = 'U'
WHERE routecode = inroutecode
AND lat IS NOT NULL AND lon IS NOT NULL;


/*	
SELECT COUNT(*) cnt,CONCAT('[', GROUP_CONCAT(
CONCAT('{"Geometry":{"Latitude":', lat,',"Longitude":', lon, '}}')),']') routepoints
FROM  ba_routedetail r 
LEFT JOIN ba_stopmaster s
ON r.stopcode = s.stopcode
AND direction = 'U'
WHERE routecode = inroutecode
GROUP BY routecode;
*/
END$$
DELIMITER;

call get_route_stoppoints('0027')