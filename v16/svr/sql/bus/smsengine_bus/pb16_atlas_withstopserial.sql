
update pb_routeatlas_withfromtostopcodes_updated  a
set stopcodefrom = (select corrected_stopcodefrom  from corrected_stopcode b 
WHERE a.best_routeatlas_id = b.best_routeatlas_id)
WHERE a.best_routeatlas_id 
IN (SELECT best_routeatlas_id FROM corrected_stopcode);



update pb_routeatlas_withfromtostopcodes_updated a
set stopcodeto = (select corrected_stopcodeto  from corrected_stopcode b
WHERE a.best_routeatlas_id = b.best_routeatlas_id)
WHERE a.best_routeatlas_id 
IN (SELECT best_routeatlas_id FROM corrected_stopcode);




#second condition in direction has to U or D? not sure
	DROP TABLE IF EXISTS pb_atlas_withstopserial;

	CREATE TABLE pb_atlas_withstopserial as
	SELECT a.*,
		(SELECT min(stopserial) FROM b_routedetail b WHERE b.direction = 'U' 
			AND a.routecode = b.routecode
			and a.stopcodefrom = b.stopcode )as stopserialfrom ,
		(SELECT max(stopserial) FROM b_routedetail b WHERE b.direction = 'U' 
			AND a.routecode = b.routecode
			AND a.stopcodeto = b.stopcode ) as stopserialto 
	from pb_routeatlas_withcorrectedstopcodes a;
