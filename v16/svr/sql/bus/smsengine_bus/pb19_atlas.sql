DROP TABLE IF EXISTS bb_atlas;
	CREATE TABLE bb_atlas AS
	SELECT  a.* ,b.Headway1, b.Headway2, b.Headway3, b.Headway4, b.Headway5, b.`schedule` 
	FROM pb_atlas_with_direction a
	LEFT OUTER JOIN pb_raw_routeatlas b ON
	a.best_routeatlas_id = b.best_routeatlas_id;
/*
	ALTER TABLE bb_atlas
	ADD COLUMN stopserialfrom INT;

	ALTER TABLE bb_atlas
	ADD COLUMN stopserialto INT;
*/
	ALTER TABLE bb_atlas
	ADD COLUMN firstfrominmin INT;
	
	ALTER TABLE bb_atlas
	ADD COLUMN lastfrominmin INT;
	
	ALTER TABLE bb_atlas
	ADD COLUMN firsttoinmin INT;
	
	ALTER TABLE bb_atlas
	ADD COLUMN lasttoinmin INT;





	UPDATE bb_atlas
	SET firstfrominmin = fn_convert_decimal_to_minutes(firstfrom),
			lastfrominmin	= fn_convert_decimal_to_minutes(lastfrom),
			firsttoinmin = fn_convert_decimal_to_minutes(firstto),
			lasttoinmin = fn_convert_decimal_to_minutes(lastto);

/*
	UPDATE bb_atlas a
	SET stopserialfrom = (SELECT min(stopserial) FROM b_routedetail b WHERE b.direction = a.direction AND a.routecode = b.routecode
	and a.stopcodefrom = b.stopcode );

	UPDATE bb_atlas a
	SET stopserialto = (SELECT max(stopserial) FROM b_routedetail b WHERE b.direction = a.direction AND a.routecode = b.routecode
	and a.stopcodeto = b.stopcode );
*/

DROP TABLE  IF EXISTS b_atlas;
CREATE TABLE b_atlas AS
SELECT best_routeatlas_id,  routecode,  firstfrom,  lastfrom,  firstto,  lastto,
			 Headway1,  Headway2,  Headway3,   Headway4,   Headway5,
			 stopcodefrom , stopcodeto ,  `schedule`, direction , 
			 TRUNCATE(firstfrom,0) as firstfromhh, 
			 TRUNCATE(lastfrom,0) as lastfromhh,
			 TRUNCATE(firstto,0) as firsttohh, 
			 TRUNCATE(lastto,0) as lasttohh,
			 CAST(SUBSTR(firstfrom, LOCATE('.',  firstfrom) + 1) AS UNSIGNED) as firstfrommm,
			 CAST(SUBSTR(lastfrom, LOCATE('.',  lastfrom) + 1) AS UNSIGNED) as lastfrommm,
			 CAST(SUBSTR(firstto, LOCATE('.',  firstto) + 1) AS UNSIGNED) as firsttomm,
			 CAST(SUBSTR(lastto, LOCATE('.',  lastto) + 1) AS UNSIGNED) as lasttomm,
			 stopserialfrom,stopserialto,firstfrominmin,lastfrominmin,firsttoinmin,lasttoinmin
from bb_atlas;


DELETE FROM b_atlas
WHERE stopserialfrom is null or stopserialto is NULL;



UPDATE  b_atlas
SET lasttoinmin = 24*60 + lasttoinmin
WHERE lasttoinmin < firsttoinmin;


UPDATE  b_atlas
SET lastfrominmin = 24*60 + lastfrominmin 
WHERE lastfrominmin < firstfrominmin;

