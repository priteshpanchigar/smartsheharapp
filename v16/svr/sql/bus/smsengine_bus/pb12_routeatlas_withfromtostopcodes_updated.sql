DROP TABLE IF EXISTS pb_routeatlas_withfromtostopcodes_updated;
	CREATE TABLE pb_routeatlas_withfromtostopcodes_updated
  (_id INT NOT NULL AUTO_INCREMENT PRIMARY KEY)
	SELECT a.routeno,a.fromstop,a.tostop,a.fromstopnameid,a.tostopnameid,
       a.best_routeatlas_id,a.routecode,b.stopcodefrom,c.stopcodeto
FROM pb_routeatlas_withfromtostopcodes a
INNER JOIN pb_routeatlas_updated_withfromstopcodes b
ON a.best_routeatlas_id = b.best_routeatlas_id
AND a.routecode = b.routecode
AND a.fromstop = b.fromstop
INNER JOIN pb_routeatlas_updated_withtostopcodes c
ON a.best_routeatlas_id = c.best_routeatlas_id
AND a.routecode = c.routecode
AND a.tostop = c.tostop
ORDER BY a.best_routeatlas_id;