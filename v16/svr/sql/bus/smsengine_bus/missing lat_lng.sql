SELECT a.stopcode, a.latu, a.latd, a.lonu, a.lond , a.lat, a.lon,
b.latu, b.latd, b.lonu, b.lond , b.lat, b.lon FROM b_stopmaster_old a
LEFT JOIN ba_stopmaster b
ON a.stopcode = b.stopcode
WHERE a.latu IS NULL and  a.latd IS NULL 
	 

UPDATE b_stopmaster_old 
SET latu = (SELECT latu FROM ba_stopmaster 
							WHERE b_stopmaster_old.stopcode = ba_stopmaster.stopcode)
	WHERE latu IS NULL;

UPDATE b_stopmaster_old 
SET latd = (SELECT latd FROM ba_stopmaster 
							WHERE b_stopmaster_old.stopcode = ba_stopmaster.stopcode)
	WHERE latd IS NULL;


UPDATE b_stopmaster_old 
SET lonu = (SELECT lonu FROM ba_stopmaster 
							WHERE b_stopmaster_old.stopcode = ba_stopmaster.stopcode)
	WHERE lonu IS NULL;

UPDATE b_stopmaster_old 
SET lond = (SELECT lond FROM ba_stopmaster 
							WHERE b_stopmaster_old.stopcode = ba_stopmaster.stopcode)
	WHERE lond IS NULL;

