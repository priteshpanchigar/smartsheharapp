DROP PROCEDURE IF EXISTS update_position_bus;
DELIMITER //
CREATE  PROCEDURE `update_position_bus`(IN `inappuserid` INT, IN intripid INT,
	IN `inclientdatetime` datetime,IN `inemailid` varchar(100), 
	IN `inlatno` double, IN `inlngno` double, IN `inacc` int, 
	IN `inlocationdatetime` datetime, IN `inprovider` varchar(10),
	IN `inspeed` float, IN `inbearing` float, IN `inaltitude` double)
quit_proc: BEGIN
	DECLARE v_location_id INT DEFAULT 0;
	DECLARE v_trip_id INT DEFAULT 0;
	/*DECLARE v_trip_action VARCHAR(10) DEFAULT "";
	DECLARE v_start_lat DOUBLE DEFAULT 0;
	DECLARE v_start_lng DOUBLE DEFAULT 0;
	DECLARE v_end_lat DOUBLE DEFAULT 0;
	DECLARE v_end_lng DOUBLE DEFAULT 0;
	DECLARE v_total_distance DOUBLE DEFAULT 0;
	DECLARE v_current_distance DOUBLE DEFAULT 0;
	DECLARE vHasTripEnded INT DEFAULT 0;
	DECLARE vCountryCode VARCHAR(10) DEFAULT "";
	DECLARE vPhoneNo VARCHAR(10) DEFAULT "";
	DECLARE vUserName VARCHAR(30) DEFAULT "";
	DECLARE vFullName VARCHAR(30) DEFAULT "";
*/
	
SET @appuserId = inappuserid;
	
	
	
	/*
	SELECT country_code, phoneno, fromlat, fromlng, tolat, tolng, trip_action, username, fullname FROM sar_trips t
		INNER JOIN appuser a ON a.appuser_id = t.appuser_id
	WHERE trip_id = v_trip_id
	INTO vCountryCode, vPhoneNo, v_start_lat, v_start_lng, v_end_lat, v_end_lng, v_trip_action, vUserName, vFullName;
	*/

	SET v_location_id = insert_location(@appUserId, inemailid, 1, 0, inclientdatetime,
			inlatno, inlngno, inacc, inlocationdatetime,  inprovider, inspeed, inbearing, 
			inaltitude);
	
	UPDATE bustrip
		SET last_location_id = v_location_id
	WHERE trip_id = intripid;		

/*
	DROP TEMPORARY TABLE IF EXISTS tmp_regs;
# Sending gcm notification code
	CREATE TEMPORARY TABLE tmp_regs AS
	SELECT a.appuser_id, a.email, a.gcm_registration_id, vCountryCode country_code, 
	vUserName username, vFullName fullname,
	vPhoneNo phoneno,t.trip_id,	 t.fromaddress, t.fromshortaddress, t.toaddress, 
	t.toshortaddress, t.hasdriver_moved, t.hasdriver_started, t.fromlat, t.fromlng, 
	t.tolat, t.tolng,  fromsublocality, tosublocality, t.triptype,
		DATE_FORMAT(inclientdatetime,'%H:%i') as trip_action_time,
		getDistanceBetweenPoints(inlatno, inlngno, l.lat, l.lng) dist, 
			ABS(inlatno - l.lat) distlat, ABS(inlngno - l.lng) distlng
		FROM appuser a
		LEFT JOIN sar_trips t ON t.trip_id = v_trip_id
		LEFT JOIN sar_location l ON 
			l.location_id = (SELECT MAX(l2.location_id) FROM sar_location l2 
							WHERE l2.appuser_id = a.appuser_id)
		WHERE a.appuser_id IN 
			(SELECT p.appuser_id FROM sar_passenger_track_trip p
					WHERE p.trip_id = v_trip_id AND v_trip_action IN ("B", "R", "P")
						AND   has_joined = 1 
						AND p.proximity_notification_distance IS NULL )
		HAVING dist < 2;
		UPDATE sar_passenger_track_trip
			INNER JOIN tmp_regs t ON (sar_passenger_track_trip.appuser_id = t.appuser_id) 
			SET proximity_notification_distance = t.dist * 1000 
			WHERE t.trip_id = v_trip_id ;
		SELECT * FROM tmp_regs;
		DROP TEMPORARY TABLE IF EXISTS tmp_regs;

*/
END //
DELIMITER ;
# Sample query
call update_position_bus(1,2,"2016-01-25 16:41:54",'soumenmaity.cse@gmail.com',"18.966082000",
"72.932638700","39.59799865722656","2016-01-25 16:41:54","F","22.22222328186035","192.0","0.0")