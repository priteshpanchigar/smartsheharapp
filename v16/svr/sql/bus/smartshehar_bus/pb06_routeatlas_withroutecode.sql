DROP TABLE IF EXISTS pb_route;
CREATE TABLE `pb_route` (
  `pb_route_id` int(11) NOT NULL AUTO_INCREMENT,
  `routeno` varchar(255) DEFAULT NULL,
  `routecode` varchar(255) DEFAULT NULL,
	busno INT,	 
	bustype INT,
  PRIMARY KEY (`pb_route_id`)
) ENGINE=InnoDB;

INSERT INTO pb_route( routecode)
SELECT  RNO FROM routedetails
GROUP BY RNO;

UPDATE pb_route
SET busno = CAST(left(routecode,3) AS signed);

UPDATE pb_route
SET bustype = CAST(right(routecode,1) AS signed);


UPDATE pb_route
SET routeno =
CASE WHEN bustype= 1 OR bustype= 3 THEN concat(busno , "Ltd") ELSE
CASE WHEN bustype=2 THEN concat(busno, "Ext") ELSE
CASE WHEN bustype=4 THEN concat(busno, "R") ELSE
CASE WHEN bustype=5 THEN concat(busno, "ExtR") ELSE
CASE WHEN bustype=6 THEN concat("C-" , busno, "Exp" ) ELSE 
CASE WHEN bustype=7 THEN concat("AS-" , busno) ELSE 
CASE WHEN bustype BETWEEN 8 AND 9 THEN concat("A-" , busno , "Exp") ELSE
CAST(busno AS unsigned)
END
END
END
END
END
END
END;

DROP TABLE IF EXISTS pb_routeatlas_withroutecode;
	CREATE TABLE pb_routeatlas_withroutecode AS
	select a.routeno,	a.fromstop,	a.tostop,	a.fromstopnameid,	a.tostopnameid,	a.best_routeatlas_id,
         a.schedule, b.routecode
	from pb_routeatlas_basic a
	Left Outer Join pb_route b ON
	b.routeno = a.routeno;

 CREATE INDEX pb_routeatlas_withroutecode_routeno ON pb_routeatlas_withroutecode (routeno);


UPDATE pb_routeatlas_withroutecode pr
SET routecode = (SELECT fa.routecode FROM fill_routecode_in_atlas fa
 WHERE pr.best_routeatlas_id = fa.best_routeatlas_id) WHERE routecode is NULL;


#SELECT * FROM pb_routeatlas_withroutecode
#WHERE routecode is NULL
