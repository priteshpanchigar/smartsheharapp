DROP TABLE IF EXISTS best_routedetails_stopnamechange;
CREATE TABLE `best_routedetails_stopnamechange` (
  `best_routedetails_id` int(15) NOT NULL AUTO_INCREMENT,
  `RNO` varchar(255) DEFAULT NULL,
  `STOPSR` int(255) DEFAULT NULL,
  `STOPCD` int(255) DEFAULT NULL,
  `Stop Name` varchar(255) DEFAULT NULL,
  `Area Name`	VARCHAR (255),	
   `Road Name`	VARCHAR (255),
		`Stage No`	VARCHAR (255),
  `STAGE` varchar(255) DEFAULT NULL,
  `KM` double(255,0) DEFAULT NULL,
  PRIMARY KEY (`best_routedetails_id`)
) ENGINE=InnoDB;

INSERT INTO best_routedetails_stopnamechange(RNO, STOPSR, STOPCD,  `Stop Name`, `Area Name`, `Road Name`, 
STAGE, `Stage No`, KM)
SELECT  RNO, STOPSR, STOPCD, s.`Stop Name` , s.AreaName, s.RoadName,  STAGE, Stage No,  KM 
FROM routedetails brs
LEFT JOIN stopmaster s
ON brs.STOPCD = s.Stop_Code
ORDER BY RNO, STOPSR ;

/*
UPDATE best_routedetails_stopnamechange a
SET `Area Name` = (SELECT `Area Name` FROM 2013_routedetail b WHERE  a.STOPCD = b.STOPCD LIMIT 1),
		`Road Name` = (SELECT `Road Name` FROM 2013_routedetail b WHERE  a.STOPCD = b.STOPCD LIMIT 1),
		 direction	= (SELECT Direction FROM 2013_routedetail b WHERE  a.RNO = b.RNO AND a.STOPCD = b.STOPCD LIMIT 1  );
*/

update best_routedetails_stopnamechange
set `Stop Name` = 'S.P.Mukherji Chowk'
where `Stop Name` = 'DR.SHAMAPRASAD MUKHERJI GARDEN';


update best_routedetails_stopnamechange
set `Stop Name` = 'S.P.Mukherji Chowk'
where `Stop Name` = 'DR.SHAMAPRASAD MUKHERJI GARDEN';

update best_routedetails_stopnamechange
set `Stop Name` = 'S.T.OFFICE/JARIMARI MATA MANDIR'
where STOPCD = 451;
