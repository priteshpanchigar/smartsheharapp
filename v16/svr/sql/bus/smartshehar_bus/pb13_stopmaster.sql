DROP TABLE if EXISTS pb_stopmaster;

CREATE TABLE pb_stopmaster
(best_stopmaster_id int(11) NOT NULL AUTO_INCREMENT PRIMARY key)
SELECT stopcode,stopname,areaname,roadname from pb_routedetails_foratlasmatching
GROUP BY stopcode;

DROP TABLE IF EXISTS pb_stopmasterdetail;
CREATE TABLE `pb_stopmasterdetail` (
  `stopmaster_id` int(11) NOT NULL AUTO_INCREMENT,
  `stopcode` bigint(21) unsigned DEFAULT NULL,
  `stopname` varchar(255) DEFAULT NULL,
  `areaname` varchar(255) DEFAULT NULL,
  `roadname` varchar(255) DEFAULT NULL,
  `latu` double DEFAULT NULL,
  `latd` double DEFAULT NULL,
  `lonu` double DEFAULT NULL,
  `lond` double DEFAULT NULL,
  `lat` double DEFAULT NULL,
  `lon` double DEFAULT NULL,
  `map` int(11) DEFAULT NULL,
  `cancelledstop` int(11) DEFAULT NULL,
  `notfound` int(11) DEFAULT NULL,
  `stopnamedetailid` varchar(300) DEFAULT NULL,
  `stopdisplayname` varchar(300) DEFAULT NULL,
  `noofbuses` int(11) DEFAULT NULL,
  `stop_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`stopmaster_id`),
  KEY `u_stopcode` (`stopcode`),
  KEY `u_noofbuses` (`noofbuses`),
  KEY `d_stopnamedetailid` (`stopnamedetailid`),
  KEY `lat` (`lat`),
  KEY `lon` (`lon`)
) ENGINE=InnoDB;

INSERT INTO pb_stopmasterdetail(stopcode, areaname, roadname)
SELECT stopcode, areaname, roadname FROM pb_stopmaster;

UPDATE pb_stopmasterdetail a
SET stopname = (SELECT `Stop Name` FROM stopmaster b WHERE a.stopcode = b.Stop_Code);

UPDATE pb_stopmasterdetail a 
SET latu = (SELECT latu FROM b_stopmaster_old b WHERE a.stopcode = b.stopcode
							AND changed_stopcode is NULL);

UPDATE pb_stopmasterdetail a 
SET latd = (SELECT latd FROM b_stopmaster_old b WHERE a.stopcode = b.stopcode
						AND changed_stopcode is NULL);

UPDATE pb_stopmasterdetail a 
SET lonu = (SELECT lonu FROM b_stopmaster_old b WHERE a.stopcode = b.stopcode
						AND changed_stopcode is NULL);

UPDATE pb_stopmasterdetail a 
SET lond = (SELECT lond FROM b_stopmaster_old b WHERE a.stopcode = b.stopcode
						AND changed_stopcode is NULL);

UPDATE pb_stopmasterdetail a 
SET lat = (SELECT lat FROM b_stopmaster_old b WHERE a.stopcode = b.stopcode
						AND changed_stopcode is NULL);

UPDATE pb_stopmasterdetail a 
SET lon = (SELECT lon FROM b_stopmaster_old b WHERE a.stopcode = b.stopcode
						AND changed_stopcode is NULL);


##########################################################################

UPDATE pb_stopmasterdetail a 
SET latu = (SELECT b.latu FROM b_stopmaster_old b WHERE a.stopcode = b.new_stopcode
							AND changed_stopcode = 1 LIMIT 1 ) WHERE latu IS NULL;

UPDATE pb_stopmasterdetail a 
SET latd = (SELECT latd FROM b_stopmaster_old b WHERE a.stopcode = b.new_stopcode
						AND changed_stopcode = 1 LIMIT 1)WHERE latd IS NULL;

UPDATE pb_stopmasterdetail a 
SET lonu = (SELECT lonu FROM b_stopmaster_old b WHERE a.stopcode = b.new_stopcode
						AND changed_stopcode = 1 LIMIT 1 )WHERE lonu IS NULL;

UPDATE pb_stopmasterdetail a 
SET lond = (SELECT lond FROM b_stopmaster_old b WHERE a.stopcode = b.new_stopcode
						AND changed_stopcode = 1 LIMIT 1)WHERE lond IS NULL;

UPDATE pb_stopmasterdetail a 
SET lat = (SELECT lat FROM b_stopmaster_old b WHERE a.stopcode = b.new_stopcode
						AND changed_stopcode = 1 LIMIT 1)WHERE lat IS NULL;

UPDATE pb_stopmasterdetail a 
SET lon = (SELECT lon FROM b_stopmaster_old b WHERE a.stopcode = b.new_stopcode
						AND changed_stopcode = 1 LIMIT 1)WHERE lon IS NULL;




##########################################################################










UPDATE pb_stopmasterdetail a 
SET map = (SELECT map FROM b_stopmaster_old b WHERE a.stopcode = b.stopcode);

UPDATE pb_stopmasterdetail a 
SET cancelledstop = (SELECT cancelledstop FROM b_stopmaster_old b WHERE a.stopcode = b.stopcode);

UPDATE pb_stopmasterdetail a 
SET notfound = (SELECT notfound FROM b_stopmaster_old b WHERE a.stopcode = b.stopcode);

/*
CREATE TABLE pb_stopmasterdetail 
(stopmaster_id int(11) NOT NULL AUTO_INCREMENT PRIMARY key)
SELECT a.stopcode,a.stopname as routedetailstopname,
b.stopname,b.areaname,
b.roadname,
b.latu,
b.latd,
b.lonu,
b.lond,
b.lat,
b.lon,
b.map,
b.cancelledstop,
b.notfound FROM  pb_stopmaster a LEFT OUTER JOIN
b_stopmaster_old b ON a.stopcode = b.stopcode;

UPDATE pb_stopmasterdetail a
SET areaname  = (SELECT areaname FROM pb_stopmaster b WHERE a.stopcode = b.stopcode) WHERE areaname is NULL;


UPDATE pb_stopmasterdetail a
SET roadname  = (SELECT roadname FROM pb_stopmaster b WHERE a.stopcode = b.stopcode) WHERE roadname is NULL;
*/

UPDATE pb_stopmasterdetail a
SET stopname  = proper(stopname);


UPDATE pb_stopmasterdetail a
SET areaname  = proper(areaname);


UPDATE pb_stopmasterdetail a
SET roadname  = proper(roadname);

UPDATE pb_stopmasterdetail
SET roadname = NULL WHERE roadname like '%name to %';


UPDATE pb_stopmasterdetail
SET stopnamedetailid = fn_RemoveSpecialCharacters(shortname(CONCAT
													(areaname,SUBSTRING_INDEX(SUBSTRING_INDEX(stopname, "/", 1), "(", 1))));



UPDATE pb_stopmasterdetail
SET stopdisplayname= CONCAT (CASE WHEN  length(areaname)>0 THEN CONCAT('[',areaname,'] ') ELSE '' END,
														 stopname) ;


UPDATE pb_stopmasterdetail
       SET noofbuses = 
       (SELECT COUNT(routecode) AS c
               FROM pb_routedetails_foratlasmatching
               WHERE pb_routedetails_foratlasmatching.stopcode = pb_stopmasterdetail.stopcode
               GROUP BY stopcode);


DROP TABLE IF EXISTS b_stopmaster;
CREATE TABLE b_stopmaster 
(_id int(11) NOT NULL AUTO_INCREMENT PRIMARY key)
select * FROM pb_stopmasterdetail ;


UPDATE b_stopmaster
SET stopnamedetailid = fn_RemoveSpecialCharacters(shortname(CONCAT
													(areaname,SUBSTRING_INDEX(SUBSTRING_INDEX(stopname, "/", 1), "(", 1))));



UPDATE b_stopmaster
SET lat = IFNULL (latu, latd),
		lon = IFNULL(lonu, lond);




CREATE INDEX noofbuses
ON b_stopmaster (noofbuses);

CREATE INDEX stopnamedetailid
ON b_stopmaster (stopnamedetailid);


CREATE INDEX stopcode
ON b_stopmaster (stopcode);


CREATE INDEX lat
ON b_stopmaster (lat);


CREATE INDEX lon
ON b_stopmaster (lon);

# take b_stopmaster_2016 from swapnil and  update remaining lat and lon which are NULLs

/*DROP TABLE IF EXISTS t_stopmaster;
CREATE TABLE t_stopmaster AS
SELECT * FROM b_stopmaster_2016
WHERE stopcode IN (SELECT stopcode from b_stopmaster WHERE lat is NULL OR lon is NULL)
																											


UPDATE b_stopmaster a 
SET latu = (SELECT latu FROM t_stopmaster b WHERE a.stopcode = b.stopcode) WHERE  latu IS NULL;

UPDATE b_stopmaster a 
SET latd = (SELECT latd FROM t_stopmaster b WHERE a.stopcode = b.stopcode)WHERE  latd IS NULL;

UPDATE b_stopmaster a 
SET lonu = (SELECT lonu FROM t_stopmaster b WHERE a.stopcode = b.stopcode)WHERE  lonu IS NULL;

UPDATE b_stopmaster a 
SET lond = (SELECT lond FROM t_stopmaster b WHERE a.stopcode = b.stopcode)WHERE  lond IS NULL;

UPDATE b_stopmaster a 
SET lat = (SELECT lat FROM t_stopmaster b WHERE a.stopcode = b.stopcode)WHERE  lat IS NULL;

UPDATE b_stopmaster a 
SET lon = (SELECT lon FROM t_stopmaster b WHERE a.stopcode = b.stopcode)WHERE  lon IS NULL;
*/

UPDATE b_stopmaster
SET lat = IFNULL (latu, latd),
		lon = IFNULL(lonu, lond);