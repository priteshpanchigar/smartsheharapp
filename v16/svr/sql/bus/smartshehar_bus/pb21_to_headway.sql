	
DROP TABLE IF EXISTS pb_to_headway1;

CREATE TABLE `pb_to_headway1` (
  `best_routeatlas_id` int(11) DEFAULT NULL,
  `startheadwayhh` int(11) DEFAULT NULL,
  `endheadwayhh` int(11) DEFAULT NULL,
  `startheadwaymm` int(11) DEFAULT NULL,
  `endheadwaymm` int(11) DEFAULT NULL,
  `frequency` int(11) DEFAULT NULL,
  `startdayweek` int(11) DEFAULT NULL,
  `enddayweek` int(11) DEFAULT NULL,
  `holiday` varchar(10) DEFAULT NULL,
  `routecode` varchar(10) DEFAULT NULL,
  `firstto` varchar(10) DEFAULT NULL,
  `lastto` varchar(10) DEFAULT NULL,
  `firsttohh` int(11) DEFAULT NULL,
  `lasttohh` int(11) DEFAULT NULL,
  `firsttomm` int(11) DEFAULT NULL,
  `lasttomm` int(11) DEFAULT NULL,
  `schedule` varchar(100) DEFAULT NULL,
  `stopcodefrom` int(11) DEFAULT NULL,
  `stopcodeto` int(11) DEFAULT NULL,
  `stopserialfrom` int(11) DEFAULT NULL,
  `stopserialto` int(11) DEFAULT NULL,
  `direction` varchar(5) DEFAULT NULL,
  `startheadway` int(11) DEFAULT NULL,
  `endheadway` int(11) DEFAULT NULL,
  `headwayserial` int(11) DEFAULT NULL,
  `firsttoinmin` int(11) DEFAULT NULL,
  `lasttoinmin` int(11) DEFAULT NULL
);





INSERT INTO pb_to_headway1(best_routeatlas_id, routecode, firstto, lastto,  frequency, stopcodefrom, stopcodeto, SCHEDULE,direction, 
stopserialfrom, stopserialto, firsttoinmin, lasttoinmin, firsttohh, lasttohh, firsttomm, lasttomm,
startheadway,endheadway,headwayserial)
SELECT best_routeatlas_id, routecode, firstto, lastto,  Headway1, stopcodefrom, stopcodeto, SCHEDULE,direction, 
stopserialfrom, stopserialto, firsttoinmin, lasttoinmin, firsttohh, lasttohh, firsttomm, lasttomm,
CASE WHEN firsttoinmin < 420 
THEN firsttoinmin
END AS startheadway,
	(firsttoinmin + (TRUNCATE(((420 - firsttoinmin) /Headway1),0) + 1) * Headway1)as endheadway,
1 FROM b_atlas 
WHERE firsttoinmin <420;

UPDATE pb_to_headway1
SET endheadway = lasttoinmin 
WHERE lasttoinmin <= endheadway;



DROP TABLE IF EXISTS pb_to_headway2;

CREATE TABLE `pb_to_headway2` (
  `best_routeatlas_id` int(11) DEFAULT NULL,
  `startheadwayhh` int(11) DEFAULT NULL,
  `endheadwayhh` int(11) DEFAULT NULL,
  `startheadwaymm` int(11) DEFAULT NULL,
  `endheadwaymm` int(11) DEFAULT NULL,
  `frequency` int(11) DEFAULT NULL,
  `startdayweek` int(11) DEFAULT NULL,
  `enddayweek` int(11) DEFAULT NULL,
  `holiday` varchar(10) DEFAULT NULL,
  `routecode` varchar(10) DEFAULT NULL,
  `firstto` varchar(10) DEFAULT NULL,
  `lastto` varchar(10) DEFAULT NULL,
  `firsttohh` int(11) DEFAULT NULL,
  `lasttohh` int(11) DEFAULT NULL,
  `firsttomm` int(11) DEFAULT NULL,
  `lasttomm` int(11) DEFAULT NULL,
  `schedule` varchar(100) DEFAULT NULL,
  `stopcodefrom` int(11) DEFAULT NULL,
  `stopcodeto` int(11) DEFAULT NULL,
  `stopserialfrom` int(11) DEFAULT NULL,
  `stopserialto` int(11) DEFAULT NULL,
  `direction` varchar(5) DEFAULT NULL,
  `startheadway` int(11) DEFAULT NULL,
  `endheadway` int(11) DEFAULT NULL,
  `headwayserial` int(11) DEFAULT NULL,
  `firsttoinmin` int(11) DEFAULT NULL,
  `lasttoinmin` int(11) DEFAULT NULL
);





INSERT INTO pb_to_headway2(best_routeatlas_id, routecode, firstto, lastto,  frequency, stopcodefrom, stopcodeto, SCHEDULE,direction, 
stopserialfrom, stopserialto, firsttoinmin, lasttoinmin, firsttohh, lasttohh, firsttomm, lasttomm,
startheadway,endheadway,headwayserial)

SELECT best_routeatlas_id, routecode, firstto, lastto,  headway2, stopcodefrom, stopcodeto, SCHEDULE,direction, 
stopserialfrom, stopserialto, firsttoinmin, lasttoinmin, firsttohh, lasttohh, firsttomm, lasttomm,
IFNULL((SELECT a.endheadway from pb_to_headway1 a
				WHERE a.best_routeatlas_id =b.best_routeatlas_id
				AND headwayserial =1 LIMIT 1), firsttoinmin) AS startheadway,	
	IFNULL((SELECT a.endheadway from pb_to_headway1 a
				WHERE a.best_routeatlas_id =b.best_routeatlas_id
				AND headwayserial =1 LIMIT 1), firsttoinmin) + 
				(TRUNCATE(((660 - 
	IFNULL((SELECT a.endheadway from pb_to_headway1 a
				WHERE a.best_routeatlas_id =b.best_routeatlas_id
				AND headwayserial =1 LIMIT 1), firsttoinmin)) 
/Headway2),0) + 1) * Headway2 AS endheadway,
2 FROM b_atlas b
WHERE IFNULL((SELECT a.endheadway from pb_to_headway1 a
				WHERE a.best_routeatlas_id =b.best_routeatlas_id
				AND headwayserial =1 LIMIT 1), firsttoinmin) BETWEEN 420 AND 659;

UPDATE pb_to_headway2
SET endheadway = lasttoinmin 
WHERE lasttoinmin <= endheadway;


DROP TABLE IF EXISTS pb_to_headway3;

CREATE TABLE `pb_to_headway3` (
  `best_routeatlas_id` int(11) DEFAULT NULL,
  `startheadwayhh` int(11) DEFAULT NULL,
  `endheadwayhh` int(11) DEFAULT NULL,
  `startheadwaymm` int(11) DEFAULT NULL,
  `endheadwaymm` int(11) DEFAULT NULL,
  `frequency` int(11) DEFAULT NULL,
  `startdayweek` int(11) DEFAULT NULL,
  `enddayweek` int(11) DEFAULT NULL,
  `holiday` varchar(10) DEFAULT NULL,
  `routecode` varchar(10) DEFAULT NULL,
  `firstto` varchar(10) DEFAULT NULL,
  `lastto` varchar(10) DEFAULT NULL,
  `firsttohh` int(11) DEFAULT NULL,
  `lasttohh` int(11) DEFAULT NULL,
  `firsttomm` int(11) DEFAULT NULL,
  `lasttomm` int(11) DEFAULT NULL,
  `schedule` varchar(100) DEFAULT NULL,
  `stopcodefrom` int(11) DEFAULT NULL,
  `stopcodeto` int(11) DEFAULT NULL,
  `stopserialfrom` int(11) DEFAULT NULL,
  `stopserialto` int(11) DEFAULT NULL,
  `direction` varchar(5) DEFAULT NULL,
  `startheadway` int(11) DEFAULT NULL,
  `endheadway` int(11) DEFAULT NULL,
  `headwayserial` int(11) DEFAULT NULL,
  `firsttoinmin` int(11) DEFAULT NULL,
  `lasttoinmin` int(11) DEFAULT NULL
);





INSERT INTO pb_to_headway3(best_routeatlas_id, routecode, firstto, lastto,  frequency, stopcodefrom, stopcodeto, SCHEDULE,direction, 
stopserialfrom, stopserialto, firsttoinmin, lasttoinmin, firsttohh, lasttohh, firsttomm, lasttomm,
startheadway,endheadway,headwayserial)


SELECT best_routeatlas_id, routecode, firstto, lastto,  headway3, stopcodefrom, stopcodeto, SCHEDULE,direction, 
stopserialfrom, stopserialto, firsttoinmin, lasttoinmin, firsttohh, lasttohh, firsttomm, lasttomm,
IFNULL((SELECT a.endheadway from pb_to_headway2 a
				WHERE a.best_routeatlas_id =b.best_routeatlas_id
				AND headwayserial =2 LIMIT 1), firsttoinmin) AS startheadway,	
IFNULL((SELECT a.endheadway from pb_to_headway2 a
				WHERE a.best_routeatlas_id =b.best_routeatlas_id
				AND headwayserial =2 LIMIT 1), firsttoinmin) +
	(TRUNCATE(((1020 -
IFNULL((SELECT a.endheadway from pb_to_headway2 a
				WHERE a.best_routeatlas_id =b.best_routeatlas_id
				AND headwayserial =2 LIMIT 1), firsttoinmin)) 
/Headway3),0) + 1) * Headway3  AS endheadway,
3 FROM b_atlas b
WHERE IFNULL((SELECT a.endheadway from pb_to_headway2 a
				WHERE a.best_routeatlas_id =b.best_routeatlas_id
				AND headwayserial =2 LIMIT 1), firsttoinmin) BETWEEN 660 AND 1019;

UPDATE pb_to_headway3
SET endheadway = lasttoinmin 
WHERE lasttoinmin <= endheadway;


DROP TABLE IF EXISTS pb_to_headway4;

CREATE TABLE `pb_to_headway4` (
  `best_routeatlas_id` int(11) DEFAULT NULL,
  `startheadwayhh` int(11) DEFAULT NULL,
  `endheadwayhh` int(11) DEFAULT NULL,
  `startheadwaymm` int(11) DEFAULT NULL,
  `endheadwaymm` int(11) DEFAULT NULL,
  `frequency` int(11) DEFAULT NULL,
  `startdayweek` int(11) DEFAULT NULL,
  `enddayweek` int(11) DEFAULT NULL,
  `holiday` varchar(10) DEFAULT NULL,
  `routecode` varchar(10) DEFAULT NULL,
  `firstto` varchar(10) DEFAULT NULL,
  `lastto` varchar(10) DEFAULT NULL,
  `firsttohh` int(11) DEFAULT NULL,
  `lasttohh` int(11) DEFAULT NULL,
  `firsttomm` int(11) DEFAULT NULL,
  `lasttomm` int(11) DEFAULT NULL,
  `schedule` varchar(100) DEFAULT NULL,
  `stopcodefrom` int(11) DEFAULT NULL,
  `stopcodeto` int(11) DEFAULT NULL,
  `stopserialfrom` int(11) DEFAULT NULL,
  `stopserialto` int(11) DEFAULT NULL,
  `direction` varchar(5) DEFAULT NULL,
  `startheadway` int(11) DEFAULT NULL,
  `endheadway` int(11) DEFAULT NULL,
  `headwayserial` int(11) DEFAULT NULL,
  `firsttoinmin` int(11) DEFAULT NULL,
  `lasttoinmin` int(11) DEFAULT NULL
);





INSERT INTO pb_to_headway4(best_routeatlas_id, routecode, firstto, lastto,  frequency, stopcodefrom, stopcodeto, SCHEDULE,direction, 
stopserialfrom, stopserialto, firsttoinmin, lasttoinmin, firsttohh, lasttohh, firsttomm, lasttomm,
startheadway,endheadway,headwayserial)

SELECT best_routeatlas_id, routecode, firstto, lastto,  headway4, stopcodefrom, stopcodeto, SCHEDULE,direction, 
stopserialfrom, stopserialto, firsttoinmin, lasttoinmin, firsttohh, lasttohh, firsttomm, lasttomm,
	IFNULL((SELECT a.endheadway from pb_to_headway3 a
				WHERE a.best_routeatlas_id =b.best_routeatlas_id
				AND headwayserial =3 LIMIT 1), firsttoinmin) AS startheadway,	
	IFNULL((SELECT a.endheadway from pb_to_headway3 a
				WHERE a.best_routeatlas_id =b.best_routeatlas_id
				AND headwayserial =3 LIMIT 1), firsttoinmin)  +
	(TRUNCATE(((1200 -
	IFNULL((SELECT a.endheadway from pb_to_headway3 a
				WHERE a.best_routeatlas_id =b.best_routeatlas_id
				AND headwayserial =3 LIMIT 1), firsttoinmin))
/Headway4),0) + 1) * Headway4 AS endheadway,
4 FROM b_atlas b
WHERE IFNULL((SELECT a.endheadway from pb_to_headway3 a
				WHERE a.best_routeatlas_id =b.best_routeatlas_id
				AND headwayserial =3 LIMIT 1), firsttoinmin) BETWEEN 1020 AND 1199;


UPDATE pb_to_headway4
SET endheadway = lasttoinmin 
WHERE lasttoinmin <= endheadway;


DROP TABLE IF EXISTS pb_to_headway5;

CREATE TABLE `pb_to_headway5` (
  `best_routeatlas_id` int(11) DEFAULT NULL,
  `startheadwayhh` int(11) DEFAULT NULL,
  `endheadwayhh` int(11) DEFAULT NULL,
  `startheadwaymm` int(11) DEFAULT NULL,
  `endheadwaymm` int(11) DEFAULT NULL,
  `frequency` int(11) DEFAULT NULL,
  `startdayweek` int(11) DEFAULT NULL,
  `enddayweek` int(11) DEFAULT NULL,
  `holiday` varchar(10) DEFAULT NULL,
  `routecode` varchar(10) DEFAULT NULL,
  `firstto` varchar(10) DEFAULT NULL,
  `lastto` varchar(10) DEFAULT NULL,
  `firsttohh` int(11) DEFAULT NULL,
  `lasttohh` int(11) DEFAULT NULL,
  `firsttomm` int(11) DEFAULT NULL,
  `lasttomm` int(11) DEFAULT NULL,
  `schedule` varchar(100) DEFAULT NULL,
  `stopcodefrom` int(11) DEFAULT NULL,
  `stopcodeto` int(11) DEFAULT NULL,
  `stopserialfrom` int(11) DEFAULT NULL,
  `stopserialto` int(11) DEFAULT NULL,
  `direction` varchar(5) DEFAULT NULL,
  `startheadway` int(11) DEFAULT NULL,
  `endheadway` int(11) DEFAULT NULL,
  `headwayserial` int(11) DEFAULT NULL,
  `firsttoinmin` int(11) DEFAULT NULL,
  `lasttoinmin` int(11) DEFAULT NULL
);





INSERT INTO pb_to_headway5(best_routeatlas_id, routecode, firstto, lastto,  frequency, stopcodefrom, stopcodeto, SCHEDULE,direction, 
stopserialfrom, stopserialto, firsttoinmin, lasttoinmin, firsttohh, lasttohh, firsttomm, lasttomm,
startheadway,endheadway,headwayserial)


SELECT best_routeatlas_id, routecode, firstto, lastto,  headway5, stopcodefrom, stopcodeto, SCHEDULE,direction, 
stopserialfrom, stopserialto, firsttoinmin, lasttoinmin, firsttohh, lasttohh, firsttomm, lasttomm,
	IFNULL((SELECT a.endheadway from pb_to_headway4 a
				WHERE a.best_routeatlas_id =b.best_routeatlas_id
				AND headwayserial =4 LIMIT 1), firsttoinmin) AS startheadway,	
	IFNULL((SELECT a.endheadway from pb_to_headway4 a
				WHERE a.best_routeatlas_id =b.best_routeatlas_id
				AND headwayserial =4 LIMIT 1), firsttoinmin)  +
	(TRUNCATE(((lasttoinmin -
	IFNULL((SELECT a.endheadway from pb_to_headway4 a
				WHERE a.best_routeatlas_id =b.best_routeatlas_id
				AND headwayserial =4 LIMIT 1), firsttoinmin))
/headway5),0) + 1) * headway5 AS endheadway,
5 FROM b_atlas b 
WHERE IFNULL((SELECT a.endheadway from pb_to_headway4 a
				WHERE a.best_routeatlas_id =b.best_routeatlas_id
				AND headwayserial =4 LIMIT 1), firsttoinmin) BETWEEN 1200 AND lasttoinmin;

UPDATE pb_to_headway5
SET endheadway = lasttoinmin 
WHERE lasttoinmin <= endheadway;
