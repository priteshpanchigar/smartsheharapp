#SKIP this PROCEDURE
/*
DROP TABLE IF EXISTS pb_routeatlas_updated_withfromstopcodes;
CREATE TABLE pb_routeatlas_updated_withfromstopcodes
(_id INT NOT NULL AUTO_INCREMENT PRIMARY KEY)
SELECT a.routecode, a.fromstop, a.tostop, a.fromstopnameid, a.tostopnameid,
			a.best_routeatlas_id, a.stopcodefrom, b.stopcode as stopcodefromupdt
FROM pb_routeatlas_withfromtostopcodes a
LEFT OUTER JOIN pb_atlas_newstopname b ON
		a.routecode = b.routecode AND b.routedetailstopname = a.fromstop
		AND b.fromto = 'F'
ORDER BY a.best_routeatlas_id;

###############################################################################################################

	DROP TABLE IF EXISTS pb_routeatlas_updated_withtostopcodes;
CREATE TABLE pb_routeatlas_updated_withtostopcodes
(_id INT NOT NULL AUTO_INCREMENT PRIMARY KEY)
SELECT a.routecode, a.fromstop, a.tostop, a.fromstopnameid, a.tostopnameid,
			a.best_routeatlas_id, a.stopcodeto, b.stopcode as stopcodetoupdt
FROM pb_routeatlas_withfromtostopcodes a
LEFT OUTER JOIN pb_atlas_newstopname_tobeupdated b ON
		a.routecode = b.routecode AND b.routedetailstopname = a.tostop
		AND b.fromto = 'T'
ORDER BY a.best_routeatlas_id;
#oldatlasstopname 
*/