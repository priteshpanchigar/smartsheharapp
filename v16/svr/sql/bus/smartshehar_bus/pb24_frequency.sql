DROP table if EXISTS schedulelookup;

 

create table schedulelookup (
`schedule` VARCHAR(50) PRIMARY KEY,
startday integer,
endday integer,
holiday VARCHAR(10)
);


insert into schedulelookup (`schedule`,startday,endday)
values ('Mon-Sat',1,6);



insert into schedulelookup (`schedule`,holiday)
values ('Hol','Y');



insert into schedulelookup (`schedule`,startday,endday)
values ('Sun',7,7);



insert into schedulelookup (`schedule`,startday,endday,holiday)
values ('Mon-Fri-Hol',1,5,'Y');



insert into schedulelookup (`schedule`,startday,endday)
values ('Sat',6,6);



insert into schedulelookup (`schedule`,startday,endday)
values ('Mon-Fri',1,5);



insert into schedulelookup (`schedule`,startday,endday,holiday)
values ('Sun-Hol',7,7,'Y');



insert into schedulelookup (`schedule`,startday,endday,holiday)
values ('All',1,7,'Y');



insert into schedulelookup (`schedule`,startday,endday)
values ('Sat-Sun',6,7);



insert into schedulelookup (`schedule`,startday,endday,holiday)
values ('Mon-Sat-Hol',1,6,'Y');



insert into schedulelookup (`schedule`,startday,endday,holiday)
values ('Sat-Sun-Hol',6,7,'Y');



insert into schedulelookup (`schedule`,startday,holiday)
values ('Sat-Hol',6,'Y');



insert into schedulelookup (`schedule`,startday,endday)
values ('FW',1,7);


insert into schedulelookup (`schedule`,startday,endday)
values ('MF',1,5);


insert into schedulelookup (`schedule`,startday,endday,holiday)
values ('MF&HOL',1,5,'Y');

insert into schedulelookup (`schedule`,startday,endday)
values ('MS',1,6);


insert into schedulelookup (`schedule`,startday,endday,holiday)
values ('MS&HOL',1,6,'Y');


insert into schedulelookup (`schedule`,startday,endday,holiday)
values ('SAT&HOL',6,6,'Y');


insert into schedulelookup (`schedule`,startday,endday,holiday)
values ('SH',7,7,'Y');





insert into schedulelookup (`schedule`,startday,endday,holiday)
values ('Mon-Fri & Hol',1,5,'Y');


insert into schedulelookup (`schedule`,startday,endday,holiday)
values ('Mon-Sat & Hol',1,6,'Y');


insert into schedulelookup (`schedule`,startday,endday,holiday)
values ('Mon-Sun & Hol',1,7,'Y');


insert into schedulelookup (`schedule`,startday,endday,holiday)
values ('S/H',7,7,'Y');


insert into schedulelookup (`schedule`,startday,endday,holiday)
values ('Sat & Hol',6,6,'Y');


insert into schedulelookup (`schedule`,startday,endday,holiday)
values ('SAT/SH',6,7,'Y');


insert into schedulelookup (`schedule`,startday,endday,holiday)
values ('Sun & Hol',7,7,'Y');


insert into schedulelookup (`schedule`,startday,endday,holiday)
values ('All Days',1,7,'Y');


insert into schedulelookup (`schedule`,startday,endday,holiday)
values ('FH',5,5,'Y');




DROP TABLE
IF EXISTS b_frequency;

CREATE TABLE b_frequency (
	frequency_id INTEGER NOT NULL auto_increment PRIMARY KEY,
	startheadwayhh INT,
	endheadwayhh INT,
	startheadwaymm INT,
	endheadwaymm INT,
	frequency INT,
	startdayweek INTEGER,
	enddayweek INTEGER,
	holiday VARCHAR (10),
	routecode VARCHAR (10),
	firstfrom VARCHAR (10),
	lastfrom VARCHAR (10),
	firstfromhh INT,
	lastfromhh INT,
	firstfrommm INT,
	lastfrommm INT,
	`schedule` VARCHAR (100),
	stopcodefrom INT,
	stopcodeto INT,
	stopserialfrom INT,
	stopserialto INT,
	direction VARCHAR (5),
	firstfrominmin INT,
	lastfrominmin INT,
	firsttoinmin INT,
	lasttoinmin INT
);

INSERT INTO b_frequency (
	startheadwayhh,	endheadwayhh,	startheadwaymm,	endheadwaymm,	frequency,	routecode,	firstfromhh,	lastfromhh,	firstfrommm,	lastfrommm,
`schedule`,	stopcodefrom,	stopcodeto,	stopserialfrom,	stopserialto,	direction,	firstfrom,	lastfrom,	firstfrominmin,	lastfrominmin
) SELECT
	startheadwayhh,	endheadwayhh,	startheadwaymm,	endheadwaymm,	frequency,	routecode,	firstfromhh, lastfromhh, firstfrommm,	lastfrommm,
	`SCHEDULE`,	stopcodefrom,	stopcodeto,	stopserialfrom,	stopserialto,	direction,	firstfrom,	lastfrom,	firstfrominmin,	lastfrominmin
FROM
	pb_frequency_headway;




CREATE INDEX f_direction
ON b_frequency (direction);

CREATE INDEX f_routecode
ON b_frequency (routecode);


UPDATE  b_frequency 
set `schedule` = 'All Days' 
where length(`schedule`)=0;



UPDATE b_frequency
SET startdayweek=  
(SELECT startday from schedulelookup
where schedulelookup.`schedule`=b_frequency.`Schedule`); 


UPDATE b_frequency
SET enddayweek=  
(SELECT endday from schedulelookup
 WHERE schedulelookup.`schedule`=b_frequency.`Schedule`);



UPDATE b_frequency
set holiday=(SELECT holiday from schedulelookup 
where schedulelookup.`schedule`=b_frequency.`Schedule`);

#check routecode which are not there in either of these TABLES
/*
 SELECT *  from b_frequency 
WHERE  routecode NOT In (SELECT routecode FROM b_route)
GROUP BY routecode;


  SELECT * from b_routedetail
WHERE  routecode NOT In (SELECT routecode FROM b_route)
GROUP BY routecode;
*/


DELETE  from b_frequency 
WHERE  routecode NOT In (SELECT routecode FROM b_route);

DELETE  from b_routedetail
WHERE  routecode NOT In (SELECT routecode FROM b_route);

DROP TABLE
IF EXISTS b_frequency_summary;

CREATE TABLE b_frequency_summary (
	summary_id INTEGER NOT NULL auto_increment PRIMARY KEY
) SELECT
	startheadwayhh,	endheadwayhh,	startheadwaymm,	endheadwaymm,	frequency,	routecode,	firstfrom,	lastfrom,	`SCHEDULE`,	stopcodefrom,
	stopcodeto,	stopserialfrom,	stopserialto,	direction,	min(firstfrominmin) firstfrominmin,	max(lastfrominmin) lastfrominmin,
	firstfromhh,	lastfromhh,	firstfrommm,	lastfrommm,	startdayweek,	enddayweek,	holiday
FROM
	b_frequency
GROUP BY
	routecode,	`schedule`, stopcodefrom, stopcodeto;
