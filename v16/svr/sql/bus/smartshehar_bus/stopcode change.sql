UPDATE b_stopmaster
SET stopnameid = fn_RemoveSpecialCharacters(shortname(stopname));

UPDATE b_stopmaster_old
SET stopnameid = fn_RemoveSpecialCharacters(shortname(stopname));

UPDATE b_stopmaster
SET areanameid = fn_RemoveSpecialCharacters(areaname);

UPDATE b_stopmaster_old
SET areanameid = fn_RemoveSpecialCharacters(areaname);



select a.areaname, b.areaname old_areaname, a.stopcode,  b.stopcode as old_stopcode, a.stopname, b.stopname as  old_stopname ,
a.roadname old_roadname from b_stopmaster a
left JOIN b_stopmaster_old b 
ON a.areanameid = b.areanameid
AND a.stopnameid = b.stopnameid
WHERE b.stopcode IS NULL;

