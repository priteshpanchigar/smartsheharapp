	
DROP TABLE IF EXISTS pb_from_headway1;

CREATE TABLE `pb_from_headway1` (
  `best_routeatlas_id` int(11) DEFAULT NULL,
  `startheadwayhh` int(11) DEFAULT NULL,
  `endheadwayhh` int(11) DEFAULT NULL,
  `startheadwaymm` int(11) DEFAULT NULL,
  `endheadwaymm` int(11) DEFAULT NULL,
  `frequency` int(11) DEFAULT NULL,
  `startdayweek` int(11) DEFAULT NULL,
  `enddayweek` int(11) DEFAULT NULL,
  `holiday` varchar(10) DEFAULT NULL,
  `routecode` varchar(10) DEFAULT NULL,
  `firstfrom` varchar(10) DEFAULT NULL,
  `lastfrom` varchar(10) DEFAULT NULL,
  `firstfromhh` int(11) DEFAULT NULL,
  `lastfromhh` int(11) DEFAULT NULL,
  `firstfrommm` int(11) DEFAULT NULL,
  `lastfrommm` int(11) DEFAULT NULL,
  `schedule` varchar(100) DEFAULT NULL,
  `stopcodefrom` int(11) DEFAULT NULL,
  `stopcodeto` int(11) DEFAULT NULL,
  `stopserialfrom` int(11) DEFAULT NULL,
  `stopserialto` int(11) DEFAULT NULL,
  `direction` varchar(5) DEFAULT NULL,
  `startheadway` int(11) DEFAULT NULL,
  `endheadway` int(11) DEFAULT NULL,
  `headwayserial` int(11) DEFAULT NULL,
  `firstfrominmin` int(11) DEFAULT NULL,
  `lastfrominmin` int(11) DEFAULT NULL
);





INSERT INTO pb_from_headway1(best_routeatlas_id, routecode, firstfrom, lastfrom,  frequency, stopcodefrom, stopcodeto, SCHEDULE,direction, 
stopserialfrom, stopserialto, firstfrominmin, lastfrominmin, firstfromhh, lastfromhh, firstfrommm, lastfrommm,
startheadway,endheadway,headwayserial)
SELECT best_routeatlas_id, routecode, firstfrom, lastfrom,  Headway1, stopcodefrom, stopcodeto, SCHEDULE,direction, 
stopserialfrom, stopserialto, firstfrominmin, lastfrominmin, firstfromhh, lastfromhh, firstfrommm, lastfrommm,
CASE WHEN firstfrominmin < 420 
THEN firstfrominmin
END AS startheadway,
	(firstfrominmin + (TRUNCATE(((420 - firstfrominmin) /Headway1),0) + 1) * Headway1)as endheadway,
1 FROM b_atlas 
WHERE firstfrominmin <420;

UPDATE pb_from_headway1
SET endheadway = lastfrominmin 
WHERE lastfrominmin <= endheadway;


DROP TABLE IF EXISTS pb_from_headway2;

CREATE TABLE `pb_from_headway2` (
  `best_routeatlas_id` int(11) DEFAULT NULL,
  `startheadwayhh` int(11) DEFAULT NULL,
  `endheadwayhh` int(11) DEFAULT NULL,
  `startheadwaymm` int(11) DEFAULT NULL,
  `endheadwaymm` int(11) DEFAULT NULL,
  `frequency` int(11) DEFAULT NULL,
  `startdayweek` int(11) DEFAULT NULL,
  `enddayweek` int(11) DEFAULT NULL,
  `holiday` varchar(10) DEFAULT NULL,
  `routecode` varchar(10) DEFAULT NULL,
  `firstfrom` varchar(10) DEFAULT NULL,
  `lastfrom` varchar(10) DEFAULT NULL,
  `firstfromhh` int(11) DEFAULT NULL,
  `lastfromhh` int(11) DEFAULT NULL,
  `firstfrommm` int(11) DEFAULT NULL,
  `lastfrommm` int(11) DEFAULT NULL,
  `schedule` varchar(100) DEFAULT NULL,
  `stopcodefrom` int(11) DEFAULT NULL,
  `stopcodeto` int(11) DEFAULT NULL,
  `stopserialfrom` int(11) DEFAULT NULL,
  `stopserialto` int(11) DEFAULT NULL,
  `direction` varchar(5) DEFAULT NULL,
  `startheadway` int(11) DEFAULT NULL,
  `endheadway` int(11) DEFAULT NULL,
  `headwayserial` int(11) DEFAULT NULL,
  `firstfrominmin` int(11) DEFAULT NULL,
  `lastfrominmin` int(11) DEFAULT NULL
);





INSERT INTO pb_from_headway2(best_routeatlas_id, routecode, firstfrom, lastfrom,  frequency, stopcodefrom, stopcodeto, SCHEDULE,direction, 
stopserialfrom, stopserialto, firstfrominmin, lastfrominmin, firstfromhh, lastfromhh, firstfrommm, lastfrommm,
startheadway,endheadway,headwayserial)

SELECT best_routeatlas_id, routecode, firstfrom, lastfrom,  headway2, stopcodefrom, stopcodeto, SCHEDULE,direction, 
stopserialfrom, stopserialto, firstfrominmin, lastfrominmin, firstfromhh, lastfromhh, firstfrommm, lastfrommm,
IFNULL((SELECT a.endheadway from pb_from_headway1 a
				WHERE a.best_routeatlas_id =b.best_routeatlas_id
				AND headwayserial =1 LIMIT 1), firstfrominmin) AS startheadway,	
	IFNULL((SELECT a.endheadway from pb_from_headway1 a
				WHERE a.best_routeatlas_id =b.best_routeatlas_id
				AND headwayserial =1 LIMIT 1), firstfrominmin) + 
				(TRUNCATE(((660 - 
	IFNULL((SELECT a.endheadway from pb_from_headway1 a
				WHERE a.best_routeatlas_id =b.best_routeatlas_id
				AND headwayserial =1 LIMIT 1), firstfrominmin)) 
/Headway2),0) + 1) * Headway2 AS endheadway,
2 FROM b_atlas b WHERE IFNULL((SELECT a.endheadway from pb_from_headway1 a
				WHERE a.best_routeatlas_id =b.best_routeatlas_id
				AND headwayserial =1 LIMIT 1), firstfrominmin) BETWEEN 420 AND 659;

UPDATE pb_from_headway2
SET endheadway = lastfrominmin 
WHERE lastfrominmin <= endheadway;


DROP TABLE IF EXISTS pb_from_headway3;

CREATE TABLE `pb_from_headway3` (
  `best_routeatlas_id` int(11) DEFAULT NULL,
  `startheadwayhh` int(11) DEFAULT NULL,
  `endheadwayhh` int(11) DEFAULT NULL,
  `startheadwaymm` int(11) DEFAULT NULL,
  `endheadwaymm` int(11) DEFAULT NULL,
  `frequency` int(11) DEFAULT NULL,
  `startdayweek` int(11) DEFAULT NULL,
  `enddayweek` int(11) DEFAULT NULL,
  `holiday` varchar(10) DEFAULT NULL,
  `routecode` varchar(10) DEFAULT NULL,
  `firstfrom` varchar(10) DEFAULT NULL,
  `lastfrom` varchar(10) DEFAULT NULL,
  `firstfromhh` int(11) DEFAULT NULL,
  `lastfromhh` int(11) DEFAULT NULL,
  `firstfrommm` int(11) DEFAULT NULL,
  `lastfrommm` int(11) DEFAULT NULL,
  `schedule` varchar(100) DEFAULT NULL,
  `stopcodefrom` int(11) DEFAULT NULL,
  `stopcodeto` int(11) DEFAULT NULL,
  `stopserialfrom` int(11) DEFAULT NULL,
  `stopserialto` int(11) DEFAULT NULL,
  `direction` varchar(5) DEFAULT NULL,
  `startheadway` int(11) DEFAULT NULL,
  `endheadway` int(11) DEFAULT NULL,
  `headwayserial` int(11) DEFAULT NULL,
  `firstfrominmin` int(11) DEFAULT NULL,
  `lastfrominmin` int(11) DEFAULT NULL
);





INSERT INTO pb_from_headway3(best_routeatlas_id, routecode, firstfrom, lastfrom,  frequency, stopcodefrom, stopcodeto, SCHEDULE,direction, 
stopserialfrom, stopserialto, firstfrominmin, lastfrominmin, firstfromhh, lastfromhh, firstfrommm, lastfrommm,
startheadway,endheadway,headwayserial)


SELECT best_routeatlas_id, routecode, firstfrom, lastfrom,  headway3, stopcodefrom, stopcodeto, SCHEDULE,direction, 
stopserialfrom, stopserialto, firstfrominmin, lastfrominmin, firstfromhh, lastfromhh, firstfrommm, lastfrommm,
IFNULL((SELECT a.endheadway from pb_from_headway2 a
				WHERE a.best_routeatlas_id =b.best_routeatlas_id
				AND headwayserial =2 LIMIT 1), firstfrominmin) AS startheadway,	
IFNULL((SELECT a.endheadway from pb_from_headway2 a
				WHERE a.best_routeatlas_id =b.best_routeatlas_id
				AND headwayserial =2 LIMIT 1), firstfrominmin) +
	(TRUNCATE(((1020 -
IFNULL((SELECT a.endheadway from pb_from_headway2 a
				WHERE a.best_routeatlas_id =b.best_routeatlas_id
				AND headwayserial =2 LIMIT 1), firstfrominmin)) 
/Headway3),0) + 1) * Headway3  AS endheadway,
3 FROM b_atlas b
WHERE IFNULL((SELECT a.endheadway from pb_from_headway2 a
				WHERE a.best_routeatlas_id =b.best_routeatlas_id
				AND headwayserial =2 LIMIT 1), firstfrominmin) BETWEEN 660 AND 1019;

UPDATE pb_from_headway3
SET endheadway = lastfrominmin 
WHERE lastfrominmin <= endheadway;


DROP TABLE IF EXISTS pb_from_headway4;

CREATE TABLE `pb_from_headway4` (
  `best_routeatlas_id` int(11) DEFAULT NULL,
  `startheadwayhh` int(11) DEFAULT NULL,
  `endheadwayhh` int(11) DEFAULT NULL,
  `startheadwaymm` int(11) DEFAULT NULL,
  `endheadwaymm` int(11) DEFAULT NULL,
  `frequency` int(11) DEFAULT NULL,
  `startdayweek` int(11) DEFAULT NULL,
  `enddayweek` int(11) DEFAULT NULL,
  `holiday` varchar(10) DEFAULT NULL,
  `routecode` varchar(10) DEFAULT NULL,
  `firstfrom` varchar(10) DEFAULT NULL,
  `lastfrom` varchar(10) DEFAULT NULL,
  `firstfromhh` int(11) DEFAULT NULL,
  `lastfromhh` int(11) DEFAULT NULL,
  `firstfrommm` int(11) DEFAULT NULL,
  `lastfrommm` int(11) DEFAULT NULL,
  `schedule` varchar(100) DEFAULT NULL,
  `stopcodefrom` int(11) DEFAULT NULL,
  `stopcodeto` int(11) DEFAULT NULL,
  `stopserialfrom` int(11) DEFAULT NULL,
  `stopserialto` int(11) DEFAULT NULL,
  `direction` varchar(5) DEFAULT NULL,
  `startheadway` int(11) DEFAULT NULL,
  `endheadway` int(11) DEFAULT NULL,
  `headwayserial` int(11) DEFAULT NULL,
  `firstfrominmin` int(11) DEFAULT NULL,
  `lastfrominmin` int(11) DEFAULT NULL
);





INSERT INTO pb_from_headway4(best_routeatlas_id, routecode, firstfrom, lastfrom,  frequency, stopcodefrom, stopcodeto, SCHEDULE,direction, 
stopserialfrom, stopserialto, firstfrominmin, lastfrominmin, firstfromhh, lastfromhh, firstfrommm, lastfrommm,
startheadway,endheadway,headwayserial)

SELECT best_routeatlas_id, routecode, firstfrom, lastfrom,  headway4, stopcodefrom, stopcodeto, SCHEDULE,direction, 
stopserialfrom, stopserialto, firstfrominmin, lastfrominmin, firstfromhh, lastfromhh, firstfrommm, lastfrommm,
	IFNULL((SELECT a.endheadway from pb_from_headway3 a
				WHERE a.best_routeatlas_id =b.best_routeatlas_id
				AND headwayserial =3 LIMIT 1), firstfrominmin) AS startheadway,	
	IFNULL((SELECT a.endheadway from pb_from_headway3 a
				WHERE a.best_routeatlas_id =b.best_routeatlas_id
				AND headwayserial =3 LIMIT 1), firstfrominmin)  +
	(TRUNCATE(((1200 -
	IFNULL((SELECT a.endheadway from pb_from_headway3 a
				WHERE a.best_routeatlas_id =b.best_routeatlas_id
				AND headwayserial =3 LIMIT 1), firstfrominmin))
/Headway4),0) + 1) * Headway4 AS endheadway,
4 FROM b_atlas b
WHERE IFNULL((SELECT a.endheadway from pb_from_headway3 a
				WHERE a.best_routeatlas_id =b.best_routeatlas_id
				AND headwayserial =3 LIMIT 1), firstfrominmin) BETWEEN 1020 AND 1199;


UPDATE pb_from_headway4
SET endheadway = lastfrominmin 
WHERE lastfrominmin <= endheadway;


DROP TABLE IF EXISTS pb_from_headway5;

CREATE TABLE `pb_from_headway5` (
  `best_routeatlas_id` int(11) DEFAULT NULL,
  `startheadwayhh` int(11) DEFAULT NULL,
  `endheadwayhh` int(11) DEFAULT NULL,
  `startheadwaymm` int(11) DEFAULT NULL,
  `endheadwaymm` int(11) DEFAULT NULL,
  `frequency` int(11) DEFAULT NULL,
  `startdayweek` int(11) DEFAULT NULL,
  `enddayweek` int(11) DEFAULT NULL,
  `holiday` varchar(10) DEFAULT NULL,
  `routecode` varchar(10) DEFAULT NULL,
  `firstfrom` varchar(10) DEFAULT NULL,
  `lastfrom` varchar(10) DEFAULT NULL,
  `firstfromhh` int(11) DEFAULT NULL,
  `lastfromhh` int(11) DEFAULT NULL,
  `firstfrommm` int(11) DEFAULT NULL,
  `lastfrommm` int(11) DEFAULT NULL,
  `schedule` varchar(100) DEFAULT NULL,
  `stopcodefrom` int(11) DEFAULT NULL,
  `stopcodeto` int(11) DEFAULT NULL,
  `stopserialfrom` int(11) DEFAULT NULL,
  `stopserialto` int(11) DEFAULT NULL,
  `direction` varchar(5) DEFAULT NULL,
  `startheadway` int(11) DEFAULT NULL,
  `endheadway` int(11) DEFAULT NULL,
  `headwayserial` int(11) DEFAULT NULL,
  `firstfrominmin` int(11) DEFAULT NULL,
  `lastfrominmin` int(11) DEFAULT NULL
);





INSERT INTO pb_from_headway5(best_routeatlas_id, routecode, firstfrom, lastfrom,  frequency, stopcodefrom, stopcodeto, SCHEDULE,direction, 
stopserialfrom, stopserialto, firstfrominmin, lastfrominmin, firstfromhh, lastfromhh, firstfrommm, lastfrommm,
startheadway,endheadway,headwayserial)


SELECT best_routeatlas_id, routecode, firstfrom, lastfrom,  headway5, stopcodefrom, stopcodeto, SCHEDULE,direction, 
stopserialfrom, stopserialto, firstfrominmin, lastfrominmin, firstfromhh, lastfromhh, firstfrommm, lastfrommm,
	IFNULL((SELECT a.endheadway from pb_from_headway4 a
				WHERE a.best_routeatlas_id =b.best_routeatlas_id
				AND headwayserial =4 LIMIT 1), firstfrominmin) AS startheadway,	
	IFNULL((SELECT a.endheadway from pb_from_headway4 a
				WHERE a.best_routeatlas_id =b.best_routeatlas_id
				AND headwayserial =4 LIMIT 1), firstfrominmin)  +
	(TRUNCATE(((lastfrominmin -
	IFNULL((SELECT a.endheadway from pb_from_headway4 a
				WHERE a.best_routeatlas_id =b.best_routeatlas_id
				AND headwayserial =4 LIMIT 1), firstfrominmin))
/headway5),0) + 1) * headway5 AS endheadway,
5 FROM b_atlas b 
WHERE IFNULL((SELECT a.endheadway from pb_from_headway4 a
				WHERE a.best_routeatlas_id =b.best_routeatlas_id
				AND headwayserial =4 LIMIT 1), firstfrominmin) BETWEEN 1200 AND lastfrominmin;

UPDATE pb_from_headway5
SET endheadway = lastfrominmin 
WHERE lastfrominmin <= endheadway;