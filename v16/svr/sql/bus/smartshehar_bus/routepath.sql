SELECT GROUP_CONCAT(
CONCAT('{"Geometry":{"Latitude":', lat,',"Longitude":', lon, '}}'))
FROM  b_routedetail r 
LEFT JOIN b_stopmaster s
ON r.stopcode = s.stopcode
AND direction = 'U'
WHERE routecode = '0841'
GROUP BY routecode;
