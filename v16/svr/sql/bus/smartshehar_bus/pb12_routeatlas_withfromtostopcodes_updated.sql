/*
DROP TABLE IF EXISTS pb_routeatlas_withfromtostopcodes_updated;
	CREATE TABLE pb_routeatlas_withfromtostopcodes_updated
  (_id INT NOT NULL AUTO_INCREMENT PRIMARY KEY)
	SELECT a.routeno,a.fromstop,a.tostop,a.fromstopnameid,a.tostopnameid,
       a.best_routeatlas_id,a.routecode,b.stopcodefrom,c.stopcodeto
FROM pb_routeatlas_withfromtostopcodes a
INNER JOIN pb_routeatlas_updated_withfromstopcodes b
ON a.best_routeatlas_id = b.best_routeatlas_id
AND a.routecode = b.routecode
AND a.fromstop = b.fromstop
INNER JOIN pb_routeatlas_updated_withtostopcodes c
ON a.best_routeatlas_id = c.best_routeatlas_id
AND a.routecode = c.routecode
AND a.tostop = c.tostop
ORDER BY a.best_routeatlas_id;


DROP TABLE IF EXISTS pb_routeatlas_withfromtostopcodes_updated;
	CREATE TABLE pb_routeatlas_withfromtostopcodes_updated
  (_id INT NOT NULL AUTO_INCREMENT PRIMARY KEY)
SELECT routeno, fromstop, tostop, fromstopnameid, tostopnameid,
        best_routeatlas_id, routecode, stopcodefrom, stopcodeto, `schedule`
FROM pb_routeatlas_withfromtostopcodes
ORDER BY a.best_routeatlas_id;
*/


DROP TABLE IF EXISTS pb_routeatlas_withfromtostopcodes_updated;
CREATE TABLE `pb_routeatlas_withfromtostopcodes_updated` (
  `_id` int(11) NOT NULL AUTO_INCREMENT,
  `routeno` varchar(255) DEFAULT NULL,
  `fromstop` varchar(255) DEFAULT NULL,
  `tostop` varchar(255) DEFAULT NULL,
  `fromstopnameid` text CHARACTER SET utf8,
  `tostopnameid` text CHARACTER SET utf8,
  `best_routeatlas_id` int(11) NOT NULL DEFAULT '0',
  `routecode` varchar(255) DEFAULT NULL,
  `stopcodefrom` bigint(21) unsigned DEFAULT NULL,
  `stopcodeto` bigint(21) unsigned DEFAULT NULL,
  `schedule` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`_id`),
  KEY `pb_routeatlas_withfromtostopcodes_updated_best_routeatlas_id` (`best_routeatlas_id`)
) ENGINE=InnoDB;
# Take file from swapnil with filled stopcode and make this TABLE 
