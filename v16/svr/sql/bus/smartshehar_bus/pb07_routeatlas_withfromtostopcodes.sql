DELETE FROM pb_routeatlas_withroutecode
	WHERE CAST(right(routecode,1) AS unsigned)=7
	OR CAST(right(routecode,1) AS unsigned)= 8
	OR CAST(right(routecode,1) AS unsigned)= 9;


  DROP TABLE IF EXISTS pb_routeatlas_withfromtostopcodes;
	CREATE TABLE pb_routeatlas_withfromtostopcodes
  (_id int not null auto_increment PRIMARY KEY)
   SELECT a.routeno,a.fromstop,a.tostop,a.fromstopnameid,a.tostopnameid,a.best_routeatlas_id,a.routecode,
				(select r.stopcode from pb_routedetails_foratlasmatching r where a.routecode = r.routecode and a.fromstopnameid = r.stopnameid Limit 1) as stopcodefrom,
				(select r.stopcode from pb_routedetails_foratlasmatching r where a.routecode = r.routecode and a.tostopnameid = r.stopnameid Limit 1) as stopcodeto,
        schedule
   FROM pb_routeatlas_withroutecode a;

   CREATE INDEX pb_routeatlas_fromtostopcode_best_routeatlas_id ON 
   pb_routeatlas_withfromtostopcodes (best_routeatlas_id);


UPDATE pb_routeatlas_withfromtostopcodes a
SET stopcodefrom = (SELECT stopcode FROM pb_routedetails_foratlasmatching b 
									WHERE a. routecode = b.routecode and UPPER(a.fromstop) = UPPER(b.stopname)) WHERE a.stopcodefrom is NULL;

UPDATE pb_routeatlas_withfromtostopcodes a
SET stopcodeto = (SELECT stopcode FROM pb_routedetails_foratlasmatching b 
									WHERE a.routecode = b.routecode and UPPER(a.tostop) = UPPER(b.stopname)) WHERE a.stopcodeto is NULL;

#give pb_routeatlas_withfromtostopcodes to swapnil to fill stopcodefrom and stopcodeto by watching pb_routedetails_foratlasmatching
