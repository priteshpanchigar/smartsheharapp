
DROP table if EXISTS pb_route_direction_u;

CREATE TABLE `pb_route_direction_u` (
  `route_id` int(11) NOT NULL AUTO_INCREMENT,
  `routecode` varchar(100) DEFAULT NULL,
  `laststop` int(11) DEFAULT NULL,
  `busno` int(10) DEFAULT NULL,
  `bustype` varchar(10) DEFAULT NULL,
  `stopcodefrom` int(11) DEFAULT NULL,
  `stopcodeto` int(11) DEFAULT NULL,
  `fstopname` varchar(50) DEFAULT NULL,
  `lstopname` varchar(50) DEFAULT NULL,
  `buslabel` varchar(20) DEFAULT NULL,
	 direction varchar(30),
  PRIMARY KEY (`route_id`)
) ;



INSERT into pb_route_direction_u(routecode,direction)
SELECT  routecode,direction from b_routedetail
WHERE direction='U'
GROUP BY ltrim(rtrim(routecode))
ORDER BY routecode;


UPDATE pb_route_direction_u
SET laststop=
(SELECT max(stopserial)from b_routedetail
where b_routedetail.routecode = pb_route_direction_u.routecode AND b_routedetail.direction = 'U');



update pb_route_direction_u
set stopcodefrom=(select case when LENGTH(rd.stopcode)>0 then stopcode ELSE NULL END from b_routedetail rd 
									 WHERE   stopserial =1 and rd.routecode=pb_route_direction_u.routecode AND direction= 'U'  );






update pb_route_direction_u
set stopcodeto=(select case when LENGTH(rd.stopcode)>0 then stopcode ELSE null END from b_routedetail rd 
									 WHERE   stopserial =pb_route_direction_u.laststop and rd.routecode=pb_route_direction_u.routecode and  direction ='U' );



update pb_route_direction_u
SET fstopname=(SELECT stopname from b_stopmaster where pb_route_direction_u.stopcodefrom= b_stopmaster.stopcode  );



update pb_route_direction_u
SET lstopname=(SELECT stopname from b_stopmaster where pb_route_direction_u.stopcodeto=b_stopmaster.stopcode );



UPDATE pb_route_direction_u
SET busno = CAST(left(routecode,3) AS signed);


UPDATE pb_route_direction_u
SET bustype = CAST(right(routecode,1) AS signed);


UPDATE pb_route_direction_u
SET buslabel =
CASE WHEN bustype= 1 OR bustype= 3 THEN concat(busno , " L") ELSE
CASE WHEN bustype=6 THEN concat("C-" , busno ) ELSE 
CASE WHEN bustype=7 THEN concat("AS-" , busno) ELSE 
CASE WHEN bustype BETWEEN 8 AND 9 THEN concat("A-" , busno , " E") ELSE
CAST(busno AS unsigned)
END
END
END
END;





DROP table if EXISTS pb_route_direction_d;

CREATE TABLE `pb_route_direction_d` (
  `route_id` int(11) NOT NULL AUTO_INCREMENT,
  `routecode` varchar(100) DEFAULT NULL,
  `laststop` int(11) DEFAULT NULL,
  `busno` int(10) DEFAULT NULL,
  `bustype` varchar(10) DEFAULT NULL,
  `stopcodefrom` int(11) DEFAULT NULL,
  `stopcodeto` int(11) DEFAULT NULL,
  `fstopname` varchar(50) DEFAULT NULL,
  `lstopname` varchar(50) DEFAULT NULL,
  `buslabel` varchar(20) DEFAULT NULL,
	 direction varchar(30),
  PRIMARY KEY (`route_id`)
) ;



INSERT into pb_route_direction_d(routecode,direction)
SELECT  routecode,direction from b_routedetail
WHERE direction='D'
GROUP BY ltrim(rtrim(routecode))
ORDER BY routecode;







UPDATE pb_route_direction_d
SET laststop=
(SELECT max(stopserial)from b_routedetail
where b_routedetail.routecode=pb_route_direction_d.routecode AND direction ='D');




update pb_route_direction_d
set stopcodefrom=(select case when LENGTH(rd.stopcode)>0 then stopcode ELSE NULL END from b_routedetail rd 
									 WHERE   stopserial =1 and rd.routecode=pb_route_direction_d.routecode AND direction= 'D'  );






update pb_route_direction_d
set stopcodeto=(select case when LENGTH(rd.stopcode)>0 then stopcode ELSE null END from b_routedetail rd 
									 WHERE   stopserial =pb_route_direction_d.laststop and rd.routecode=pb_route_direction_d.routecode and  direction ='D' );



update pb_route_direction_d
SET fstopname=(SELECT stopname from b_stopmaster where pb_route_direction_d.stopcodefrom= b_stopmaster.stopcode  );



update pb_route_direction_d
SET lstopname=(SELECT stopname from b_stopmaster where pb_route_direction_d.stopcodeto=b_stopmaster.stopcode );





UPDATE pb_route_direction_d
SET busno = CAST(left(routecode,3) AS signed);


UPDATE pb_route_direction_d
SET bustype = CAST(right(routecode,1) AS signed);


UPDATE pb_route_direction_d
SET buslabel =
CASE WHEN bustype= 1 OR bustype= 3 THEN concat(busno , " L") ELSE
CASE WHEN bustype=6 THEN concat("C-" , busno) ELSE 
CASE WHEN bustype=7 THEN concat("AS-" , busno) ELSE 
CASE WHEN bustype BETWEEN 8 AND 9 THEN concat("A-" , busno , " E") ELSE
CAST(busno AS unsigned)
END
END
END
END;

DROP table if EXISTS b_route;

CREATE TABLE `b_route` (
  `route_id` int(11) NOT NULL AUTO_INCREMENT,
  `routecode` varchar(100) DEFAULT NULL,
  `laststop` int(11) DEFAULT NULL,
  `busno` int(10) DEFAULT NULL,
  `bustype` varchar(10) DEFAULT NULL,
  `stopcodefrom` int(11) DEFAULT NULL,
  `stopcodeto` int(11) DEFAULT NULL,
  `fstopname` varchar(50) DEFAULT NULL,
  `lstopname` varchar(50) DEFAULT NULL,
  `buslabel` varchar(20) DEFAULT NULL,
	 direction varchar(30),
  PRIMARY KEY (`route_id`)
) ;


INSERT INTO b_route(routecode,laststop,busno,bustype,stopcodefrom,stopcodeto,fstopname,lstopname,buslabel,direction)
SELECT routecode,laststop,busno,bustype,stopcodefrom,stopcodeto,fstopname,lstopname,buslabel,direction from pb_route_direction_u;


INSERT INTO b_route(routecode,laststop,busno,bustype,stopcodefrom,stopcodeto,fstopname,lstopname,buslabel,direction)
SELECT routecode,laststop,busno,bustype,stopcodefrom,stopcodeto,fstopname,lstopname,buslabel,direction from pb_route_direction_d;


CREATE INDEX r_direction
ON b_route (direction);

CREATE INDEX r_stopcodefrom
ON b_route (stopcodefrom);

CREATE INDEX r_stopcodeto
ON b_route (stopcodeto);

CREATE INDEX r_routecode
ON b_route (routecode);

DELETE  FROM b_route
WHERE bustype=2 OR bustype=3;

UPDATE b_stopmaster a
SET busesatstop = (SELECT GROUP_CONCAT(' ',r.buslabel) FROM b_routedetail rd
INNER JOIN  b_route r
ON rd.routecode = r.routecode
AND rd.direction = r.direction
WHERE rd.stopcode = a.stopcode);

/*
#can't find buslabel_non_matching table
UPDATE b_route a
SET buslabel = (SELECT buslabel FROM buslabel_non_matching b
WHERE a.routecode = b.routecode) WHERE a.routecode IN (SELECT routecode from buslabel_non_matching);
*/