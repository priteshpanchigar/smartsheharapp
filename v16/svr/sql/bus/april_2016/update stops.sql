UPDATE pb_routeatlas_withfromtostopcodes_updated a
SET stopcodefrom  = (SELECT stopcodefrom FROM correct_atlas_stopcode_new b
WHERE a.routecode = b.routecode
AND a.fromstopnameid  = b.fromstopnameid
AND a.tostopnameid = b.tostopnameid LIMIT 1);


UPDATE pb_routeatlas_withfromtostopcodes_updated a
SET stopcodeto  = (SELECT stopcodeto FROM correct_atlas_stopcode_new b
WHERE a.routecode = b.routecode
AND a.fromstopnameid  = b.fromstopnameid
AND a.tostopnameid = b.tostopnameid LIMIT 1);