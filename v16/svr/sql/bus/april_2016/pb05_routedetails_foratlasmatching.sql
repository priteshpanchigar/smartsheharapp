DROP TABLE IF EXISTS pb_routedetails_foratlasmatching;
	CREATE TABLE pb_routedetails_foratlasmatching 
  (_id int not null auto_increment PRIMARY KEY)
  SELECT RNO as routecode,
				CAST(stopcode AS UNSIGNED) as stopcode,
        CAST(stopserial as UNSIGNED) as stopserial,
				stopname,
        roadname,
        areaname,
				direction,
				best_routedetails_id as best_routedetails_id,
				fn_RemoveVowels(fn_RemoveSpecialCharacters(shortname(SUBSTRING_INDEX(stopname, '(', 1)))) as stopnameid
	FROM best_routedetails_stopnamechange
  GROUP BY best_routedetails_stopnamechange.RNO,best_routedetails_stopnamechange.stopcode
  ORDER BY best_routedetails_stopnamechange.RNO, stopserial;
  
  CREATE INDEX routedetail_index ON pb_routedetails_foratlasmatching (routecode);	
	CREATE INDEX stopcode_index ON pb_routedetails_foratlasmatching (stopcode);
	CREATE INDEX stopname_index ON pb_routedetails_foratlasmatching (stopname);	

