DROP VIEW IF EXISTS vw_issue_items;

CREATE  VIEW `vw_issue_items` AS
SELECT `ilt`.`issue_lookup_item_id` AS `issue_lookup_item_id`,
       `ilt`.`issue_item_code` AS `issue_item_code`,
       `ilt`.`issue_item_description` AS `issue_item_description`,
       `ilt`.`issue_item_order` AS `issue_item_order`,
       `ilt`.`photo_mandatory` AS `photo_mandatory`,
       `ist`.`issue_lookup_sub_type_id` AS `issue_lookup_sub_type_id`,
       `ist`.`issue_sub_type_code` AS `issue_sub_type_code`,
       `ist`.`issue_sub_type_description` AS `issue_sub_type_description`,
       `ist`.`issue_sub_type_order` AS `issue_sub_type_order`,
       `ist`.`vehicle_number_mandatory` AS `vehicle_number_mandatory`,
       `it`.`issue_lookup_type_id` AS `issue_lookup_type_id`,
       `it`.`issue_type_code` AS `issue_type_code`,
       `it`.`issue_type_description` AS `issue_type_description`,
       `it`.`department_code` AS `department_code`,
       `it`.`colour` AS `colour`,
       `it`.`android_colour` AS `android_colour`
FROM ((`issue_lookup_items` `ilt`
       LEFT JOIN `issue_lookup_subtypes` `ist` on((`ist`.`issue_sub_type_code` = `ilt`.`issue_sub_type_code`)))
      LEFT JOIN `issue_lookup_types` `it` on((`it`.`issue_type_code` = `ist`.`issue_type_code`)))
