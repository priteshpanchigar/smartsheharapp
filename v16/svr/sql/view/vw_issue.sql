DROP VIEW IF EXISTS vw_issue;

CREATE VIEW vw_issue AS

SELECT i.issue_id AS issue_id, 
(SELECT ad.formatted_address FROM address ad 
	WHERE(ad.address_id = i.address_id)) AS formatted_address, 
(SELECT ad.latitude FROM address ad 
	WHERE(ad.address_id = i.address_id)) AS lat, 
(SELECT ad.longitude FROM address ad 
	WHERE(ad.address_id = i.address_id)) AS lng,
	i.issue_time AS issue_time,
	i.client_datetime AS client_datetime,
	i.vehicle_no AS vehicle_no,
	i.unique_key AS unique_key,
	i.submit_report AS submit_report,
	i.approved AS approved,
	i.rejected AS rejected,
	i.approved_rejected_clientdatetime AS approved_rejected_clientdatetime,
	i.reason_code AS reason_code,
	i.other_reason_code_comment AS other_reason_code_comment,
	i.to_be_sent_to_authority AS to_be_sent_to_authority,
	i.sent_to_authority AS sent_to_authority,
	i.sent_to_authority_datetime AS sent_to_authority_datetime,
	i.resolved AS resolved,
	i.unresolved AS unresolved,
	i.resolved_unresolved_datetime AS resolved_unresolved_datetime,
	i.unresolved_comment AS unresolved_comment,
	i.address_id AS address_id,
	i.location_id AS location_id, 
(SELECT ad.ward FROM address ad 
	WHERE(ad.address_id = i.address_id)) AS ward,
	i.group_id AS group_id, 
(SELECT ad.issue_item_description FROM issue_lookup_items ad 
	WHERE(ad.issue_item_code = i.issue_item_code)) AS issue_item_description,
	i.issue_item_code AS issue_item_code,
	i.mla_id AS mla_id,
	i.mp_id AS mp_id, 
(SELECT email FROM appuser a WHERE a.appuser_id = i.appuser_id) email,
	i.appuser_id AS appuser_id,
	i.discard_report AS discard_report,
	i.letter_submit_by AS letter_submit_by,
	i.issue_registration_notification AS issue_registration_notification,
	i.letter_submitted_to AS letter_submitted_to,
	i.letter_submitted_dept AS letter_submitted_dept,
	i.letter_submitted_desg AS letter_submitted_desg,
	i.letter_submitted_datetime AS letter_submitted_datetime,
	i.letter_remark AS letter_remark,
	i.resolved_unresolved_by AS resolved_unresolved_by,
	i.letter_upload_notification AS letter_upload_notification,
	i.resolved_unresolved_notification AS resolved_unresolved_notification,
	i.letter_client_date_time AS letter_client_date_time,
	i.letter_image_name AS letter_image_name,
	i.letter_image_path AS letter_image_path,
	i.closed AS closed,
	i.opened AS opened,
	i.closed_opened_datetime AS closed_opened_datetime,
	i.closed_opened_by AS closed_opened_by,
	i.closed_opened_comment AS closed_opened_comment,
	ii.issue_image_id AS issue_image_id,
	ii.issue_image_name AS issue_image_name,
	ii.issue_image_path AS issue_image_path,
	ii.creation_datetime AS creation_datetime,
	ii.issue_uniquekey AS issue_uniquekey
FROM(issue i LEFT JOIN issue_image ii ON((i.issue_id = ii.issue_id)))
WHERE(i.issue_item_code != '');

SELECT * FROM vw_issue;

