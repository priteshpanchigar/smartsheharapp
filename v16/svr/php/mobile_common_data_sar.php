<?php	
error_reporting(E_ALL);
$php_version='v16';
if(empty($php_name)) {
	$php_name = "GenericPhp";
}
$verbose = isset($_REQUEST['verbose']) ? $_REQUEST['verbose'] : 'N';
$actual_link = "http://$_SERVER[HTTP_HOST]";
$fname = isset($_REQUEST['email']) ? $_REQUEST['email']  : "genericemail";
$actual_link = "http://$_SERVER[HTTP_HOST]" . $_SERVER['REQUEST_URI'];
$fp = fopen('log/' . $php_name . "_" . $fname . '.log', 'a');
$delim = "?";
$params = "";
foreach ($_POST as $param_name => $param_val) {
	$params = $params . $delim . $param_name . '=' . $param_val;
	$delim = "&";
}
$actual_link = $actual_link . $params . "&verbose=Y\n";
fwrite($fp, $actual_link);
fclose($fp);

//Key Data

$appuserid = empty($_REQUEST['appuserid']) || !isset($_REQUEST['appuserid']) ? 'NULL' :
	$_REQUEST['appuserid'];
$countrycode = isset($_REQUEST['countrycode']) ?
"\"" . (strpos($_REQUEST['countrycode'], "+") !== false ? "" : "+") . trim($_REQUEST['countrycode']) . "\"" : 'NULL';
$phoneno = isset($_REQUEST['phoneno']) ?
"\"" . $_REQUEST['phoneno'] . "\"" : 'NULL';

//Location Data	
$lat = isset($_REQUEST['lat']) ? "\"" . $_REQUEST['lat'] . "\"" : 'NULL';

$lng = isset($_REQUEST['lng']) ? "\"" . $_REQUEST['lng'] . "\"" : 'NULL';
$accuracy = isset($_REQUEST['accuracy']) ? 	"\"" . $_REQUEST['accuracy'] . "\"" : 'NULL';		
$locationdatetime = isset($_REQUEST['locationdatetime']) ? 
"\"" . $_REQUEST['locationdatetime'] . "\"" : 'NULL';
$provider = isset($_REQUEST['provider']) ?
"\"" . $_REQUEST['provider'] . "\"" : 'NULL';
$speed = isset($_REQUEST['speed']) ? "\"" . $_REQUEST['speed'] . "\"" : 'NULL';
$bearing = isset($_REQUEST['bearing']) ? "\"" . $_REQUEST['bearing'] . "\"" : 'NULL';
$altitude  = isset($_REQUEST['altitude']) ?
"\"" . $_REQUEST['altitude'] . "\"" : 'NULL';

//client access date time
$clientdatetime = isset($_REQUEST['clientdatetime']) ? "\"" . $_REQUEST['clientdatetime'] . "\"" : 'NULL';	

//identification data
$imei = isset($_REQUEST['imei']) ? "\"" . $_REQUEST['imei'] . "\"" : 'NULL';

$email = empty($_REQUEST['email']) || !isset($_REQUEST['email']) ? 'NULL' : "'" . $_REQUEST['email'] . "'" ;

//phone data
$date = isset($_REQUEST['date']) ? 	"\"" . $_REQUEST['date'] . "\"" : 'NULL';
$time = isset($_REQUEST['time']) ? 	"\"" . $_REQUEST['time'] . "\"" : 'NULL';

$carrier = isset($_REQUEST['carrier']) ?  "\"" . $_REQUEST['carrier'] . "\"" : 'NULL';
$product = isset($_REQUEST['product']) ? "\"" . $_REQUEST['product'] . "\"" : 'NULL';
$manufacturer = isset($_REQUEST['manufacturer']) ?
"\"" . $_REQUEST['manufacturer'] . "\"" : 'NULL';			

//app data
$app = isset($_REQUEST['app']) ? "\"" . $_REQUEST['app'] . "\"" : 'NULL';
$version = isset($_REQUEST['version']) ?
	"\"" . $_REQUEST['version'] . "\"" : 'NULL';
$versioncode = isset($_REQUEST['versioncode']) ?
	"\"" . $_REQUEST['versioncode'] . "\"" : 'NULL';
$module = isset($_REQUEST['module']) ?
	"\"" . $_REQUEST['module'] . "\"" : 'NULL';


//ip data
$ip = isset($_SERVER['REMOTE_ADDR']) ?
"\"" . $_SERVER['REMOTE_ADDR'] . "\"" : 'NULL';
$useragent = isset($_SERVER['HTTP_USER_AGENT']) ?
"\"" . $_SERVER['HTTP_USER_AGENT'] . "\"" : 'NULL';

//miscellanious data	
$address = isset($_REQUEST['address']) ? 
"\"" . $_REQUEST['address'] . "\"" : 'NULL';

// Grouping Data

$key_data = $countrycode . "," . $phoneno;
$location_data = $lat . "," . $lng . "," . $accuracy . "," .
$locationdatetime . "," . $provider . "," .
$speed . "," . $bearing . "," . $altitude;
$identification_data = $email . "," . $imei;
$phone_data =  $carrier . "," . $product . "," . 
$manufacturer;
$app_data = $app . "," . $version . "," . $versioncode . "," . $module;
$ip_data = $ip . "," . $useragent;	

$mobile_common_data = $clientdatetime . "," . 
$email . "," . 	$location_data . "," . $imei . "," .
$phone_data . "," . $app_data . "," . $ip_data;

if ($verbose != "N") {
	//echo "<br>App user id: " . $appuserid . "<br>";
}
?>