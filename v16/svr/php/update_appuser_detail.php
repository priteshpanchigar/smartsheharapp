<?php
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
$php_name = 'updateAppuserDetail';
include("dbconn_sar_apk.php");
include("mobile_common_data_sar.php");
if ( $mysqli ) {
	
	$username = empty($_REQUEST['username']) || !isset($_REQUEST['username']) ? 'NULL' : "'" . $_REQUEST['username'] . "'" ;
	$fullname = empty($_REQUEST['fullname']) || !isset($_REQUEST['fullname']) ? 'NULL' : "'" . $_REQUEST['fullname'] . "'" ;	
	$sex = empty($_REQUEST['sex']) || !isset($_REQUEST['sex']) ? 'NULL' : "'" . $_REQUEST['sex'] . "'" ;	
	$age = isset($_REQUEST['age']) ? $_REQUEST['age'] : 'NULL';
	$phoneno = empty($_REQUEST['phoneno']) || !isset($_REQUEST['phoneno']) ? 'NULL' : "'" . $_REQUEST['phoneno'] . "'" ;	
	
	$sql = "call update_appuser_detail(" . $appuserid . "," . $username .
	"," . $fullname . ",".  $age . "," . $sex . "," . $phoneno .")";
	
	if ($verbose != 'N') {
		echo '<br>sql:<br>' . $sql;
	}
	
	if ($result = $mysqli->query($sql)) {	
		if ($result && is_object($result))  {
			while ($row = $result->fetch_assoc()) {
				echo json_encode($row);
				break;
			}
		}
		$mysqli->close();
	}		else {
		echo "-1"; // something went wrong, probably sql failed
	}
} else {
	echo "-2"; // "Connection to db failed";
}