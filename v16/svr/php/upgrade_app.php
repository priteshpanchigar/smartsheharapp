<?php header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
$php_name = 'upgradeApp';
include("mobile_common_data_sar.php");
$verbose = isset($_REQUEST['verbose']) ? "'" . $_REQUEST['verbose'] . "'" : 'N';
$appUserId = isset($_REQUEST['appuserid']) ? $_REQUEST['appuserid']  : "0";
$versioncode = isset($_REQUEST['versioncode']) ? $_REQUEST['versioncode']  : "-1";
if ($versioncode == -1) {
	echo 'Please enter the current versioncode for app as parameter';
	return;
}include("dbconn_sar_apk.php");
if ($mysqli) {
	$sql = "call upgrade_app (" . $app . "," . $versioncode .")";
	if ($verbose != 'N') {
		echo $sql . '<br>';
	}
	if ($result = $mysqli->query($sql)) {	
		while ($row = $result->fetch_assoc()) {
			echo json_encode($row);
			break;
		} 
	}		else {
		echo "-1"; // something went wrong, probably sql failed
	}
	$mysqli->close();
} else {
	echo "-2"; // "Connection to db failed";
}