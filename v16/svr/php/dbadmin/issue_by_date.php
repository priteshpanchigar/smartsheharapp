<?php
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
$php_name = 'issueByDate';
include("../dbconn_sar_apk.php");
include("../mobile_common_data_sar.php");  
$issuebydateRows = array();
if ($mysqli) {
	
	$sql = " call issue_by_date()";
	
	if ($verbose != 'N') {
		echo $sql . '<br>';
	}	
	if ($result = $mysqli->query($sql)) {
		if ($verbose != 'N') {
			echo "<br>sql result: ";
			var_dump($result);
			echo "<br>";
		}
	}
	if ($result && is_object($result))  {
		
		while ($row = $result->fetch_assoc()) {
		
			$issuebydateRows[] = $row;			
		}	
		$result->free();	// free result set
	}
	
	$mysqli->close();		// close connection
	if (count($issuebydateRows) > 0) {
		echo json_encode($issuebydateRows);
	} else {
		echo "-1";
	}
	
}else {
	echo "-1";
}