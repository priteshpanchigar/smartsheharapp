<?php
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
$php_name = 'smartshehar_summary';
include("../dbconn_sar_apk.php");
include("../mobile_common_data_sar.php");   
if ($mysqli) {
	
	$sql = " call smartshehar_summary()";
	if ($verbose != 'N') {
		echo $sql . '<br>';
	}	
	if ($result = $mysqli->query($sql)) {
		if ($verbose != 'N') {
			echo "<br>sql result: ";
			var_dump($result);
			echo "<br>";
		}
	}
	if ($result && is_object($result))  {
		$rowcount=mysqli_num_rows($result);
		
		while ($row = $result->fetch_assoc()) {
			echo json_encode ($row);
			break;	
		}		
		$result->free();	// free result set
	}
	
	$mysqli->close();		// close connection

	
}else {
	echo "-1";
}