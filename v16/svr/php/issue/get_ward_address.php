<?php
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
$php_name = 'getWardAddress';
include("../dbconn_sar_apk.php"); 
include("../mobile_common_data_sar.php");
 
if ( $mysqli ) {
	
	$wardno = empty($_REQUEST['wardno']) || !isset($_REQUEST['wardno']) ? 'NULL' :
		"'" . $_REQUEST['wardno'] . "'" ;

	$sql = "call get_ward_address(" .$wardno .")";

if ($verbose != 'N') {
		//echo $sql; 
	}
	if ($result = $mysqli->query($sql)) {	
		while ($row = $result->fetch_assoc()) {
			//echo json_encode($row);
			echo $row['ward_office_address'];
			break;
		} 
		$mysqli->close();
	}		else {
		echo "-1"; // something went wrong, probably sql failed
	}
} else {  
	echo "-2"; // "Connection to db failed";
}