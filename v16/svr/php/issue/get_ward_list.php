<?php
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
$php_name = 'getWardList';
include("../dbconn_sar_apk.php"); 
include("../mobile_common_data_sar.php");
if ($mysqli) {
	$getwardRows = array();
	
	
	$sql = " call get_ward_list()";
	if ($verbose != 'N') {
		echo $sql . '<br>';
	}	
	if ($result = $mysqli->query($sql)) {
		while ($row = $result->fetch_assoc()) {
			$getwardRows[] = $row;			
		}		
		$result->free();	// free result set
	}
	$mysqli->close();		// close connection
	
	$getwardRows = array_filter($getwardRows);
	if(!empty($getwardRows))
	{	echo json_encode($getwardRows);}
	else{
			echo "-1";
		}
		
	 
}else {
		echo "-1";
	}