<?php 
header('Access-Control-Allow-Origin: *');
$php_name = "getWardFromLatLng";
include("../mobile_common_data_sar.php");
include("../dbconn_sar_apk.php");

if ($mysqli) {
	
	$sql = " call get_ward_from_lat_lng("  . $lat . "," . $lng . ")";
	if ($verbose != 'N') {
		echo $sql . '<br>';
	}
	$result = $mysqli->query($sql) or die(mysql_error());
	$rowcount=mysqli_num_rows($result);
	if ($result) {
		while ($row = $result->fetch_assoc())  {
			
			echo json_encode($row);
			break;
		}
		$result->free();	// free result set
	}
	if ($rowcount == 0) {
		echo '';
	}
	$mysqli->close();		// close connection
}