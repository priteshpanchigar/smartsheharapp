<?php
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
$php_name = 'updateresolvedOffence';
include("../dbconn_sar_apk.php"); 
include("../mobile_common_data_sar.php");
$errors = array();
if ( $mysqli ) {
		
	$resolved = isset($_REQUEST['resolved']) ? $_REQUEST['resolved'] : 'NULL';
	
	$unresolved = isset($_REQUEST['unresolved']) ? $_REQUEST['unresolved'] : 'NULL';
	
	$issueid = isset($_REQUEST['issueid']) ? $_REQUEST['issueid'] : 'NULL';
	
	$unresolvedcomment = isset($_REQUEST['unresolvedcomment']) ? "\"" . $_REQUEST['unresolvedcomment'] . "\"" : 'NULL';
	
	$clientdatetime = isset($_REQUEST['clientdatetime']) ? "\"" . $_REQUEST['clientdatetime'] . "\"" : 'NULL';	
	
	$sql = "call update_resolved_unresolved(" .$resolved .  ",".  $unresolved .  
	",". $issueid . ",". $unresolvedcomment . ",". $clientdatetime .
	",". $appuserid .")";
	if ($verbose != 'N') {
		echo $sql;
	}  
	if ($result = $mysqli->query($sql)) {	
		while ($row = $result->fetch_assoc()) {  
			echo json_encode($row);
			break;
		} 
		$mysqli->close();
	}		else {    
		echo "-1"; // something went wrong, probably sql failed
	}
	 
	$message_type = 'Resolution status of issue';
	if($resolved == '1' && $unresolved == '0')
	{$message_type = 'The complaint has been resolved';}
	if($resolved == '0' && $unresolved == '1')
	{$message_type = 'No resolution for this issue';}
	include("resolved_unresolved_notification.php"); 
} else {
	echo "-2"; // "Connection to db failed";
}