<?php
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
$php_name = 'AddIssue';
include("../dbconn_sar_apk.php");
$errors = array();
$unregistered = false;
include("../mobile_common_data_sar.php");

if ( $mysqli ) {
	
	
	$issueaddress = empty($_REQUEST['issueaddress']) || !isset($_REQUEST['issueaddress']) ? 'NULL' : "'" .
	$_REQUEST['issueaddress'] . "'" ;
	
	$issueitemcode = empty($_REQUEST['issueitemcode']) || !isset($_REQUEST['issueitemcode']) ? 'NULL' : "'" .
	$_REQUEST['issueitemcode'] . "'" ;
	$vehicleno = empty($_REQUEST['vehicleno']) || !isset($_REQUEST['vehicleno']) ? 'NULL' : "'" .
	$_REQUEST['vehicleno'] . "'" ;	
	$submitreport = isset($_REQUEST['submitreport']) ? "'" . $_REQUEST['submitreport'] . "'" : 0;
	$uniquekey = isset($_REQUEST['uniquekey']) ? $_REQUEST['uniquekey'] : 'NULL';
	
	$issuedatetime = isset($_REQUEST['issuedatetime']) ? "\"" . $_REQUEST['issuedatetime'] . "\"" : 'NULL';
	
	$locality = isset($_REQUEST['locality']) ? "\"" . $_REQUEST['locality'] . "\"" : 'NULL';
	$sublocality = isset($_REQUEST['sublocality']) ? "\"" . $_REQUEST['sublocality'] . "\"" : 'NULL';
	$postalcode = isset($_REQUEST['postalcode']) ? "\"" . $_REQUEST['postalcode'] . "\"" : 'NULL';	
	$route = isset($_REQUEST['route']) ? "\"" . $_REQUEST['route'] . "\"" : 'NULL';	
	
	$neighborhood = isset($_REQUEST['neighborhood']) ? "\"" . $_REQUEST['neighborhood'] . "\"" : 'NULL';	
	
	$administrative_area_level_2 = isset($_REQUEST['administrative_area_level_2']) ? "\"" . $_REQUEST['administrative_area_level_2'] . "\"" : 'NULL';
	
	$administrative_area_level_1 = isset($_REQUEST['administrative_area_level_1']) ? "\"" . $_REQUEST['administrative_area_level_1'] . "\"" : 'NULL';	
	
	$latitude = isset($_REQUEST['latitude']) ? "\"" . $_REQUEST['latitude'] . "\"" : 'NULL';	
	
	$longitude = isset($_REQUEST['longitude']) ? "\"" . $_REQUEST['longitude'] . "\"" : 'NULL';	
	
	
	
	$mlaid = isset($_REQUEST['mlaid']) ? "\"" . $_REQUEST['mlaid'] . "\"" : 'NULL';
	
	$groupid = isset($_REQUEST['groupid']) ? "\"" . $_REQUEST['groupid'] . "\"" : 'NULL';
	
	$mpid = isset($_REQUEST['mpid']) ? "\"" . $_REQUEST['mpid'] . "\"" : 'NULL';
	
	$subtypecode = isset($_REQUEST['subtypecode']) ? "\"" . $_REQUEST['subtypecode'] . "\"" : 'NULL';
	$discard_report = isset($_REQUEST['discard_report']) ? "\"" . $_REQUEST['discard_report'] . "\"" : '0';	

	$ward = empty($_REQUEST['ward']) || !isset($_REQUEST['ward']) ? 'NULL' :
		"'" . $_REQUEST['ward'] . "'" ;
	
	$sql = "call add_issue(" .$issueaddress . "," .$issuedatetime .",  " .$clientdatetime .
	","  .$vehicleno . "," .$uniquekey . "," .$submitreport .", " .$locality .", " .$sublocality .
	", ".$postalcode . "," .$route ."," .$neighborhood . "," .$administrative_area_level_2 .", ". $administrative_area_level_1 .
	",".$latitude.", ".$longitude . ", ".$appuserid.", " .$issueitemcode.", ". $email . ", ".$accuracy.", ".$locationdatetime.
	", ".$provider.", ".$speed.", ".$bearing.", " .$altitude.", " .$discard_report. ", " .$mlaid. ", " .$mpid. ", " .$groupid.
	", " .$ward.")";
	
	
	if ($verbose != 'N') {
		echo '<br>' . $sql . '<br>';
	}
	$result = $mysqli->query($sql);
	if(is_object($result)) {	
		while ($row = $result->fetch_assoc()) {
			echo json_encode($row);
			break;
		}
		$mysqli->close();
	}
	
	
} else {
	echo "-1";
}