<?php
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
$php_name = 'updateLetterDetails';
include("../dbconn_sar_apk.php"); 
include("../mobile_common_data_sar.php");
$errors = array();
if ( $mysqli ) {
		
	
	$issueid = isset($_REQUEST['issueid']) ? $_REQUEST['issueid'] : 'NULL';
	
	$lettersubmittedto = empty($_REQUEST['lettersubmittedto']) || !isset($_REQUEST['lettersubmittedto']) ? 'NULL' :
		"'" . $_REQUEST['lettersubmittedto'] . "'" ;
	$lettersubmitteddept = empty($_REQUEST['lettersubmitteddept']) || !isset($_REQUEST['lettersubmitteddept']) ? 'NULL' :
		"'" . $_REQUEST['lettersubmitteddept'] . "'" ;
	$lettersubmitted_desg = empty($_REQUEST['lettersubmitteddesg']) || !isset($_REQUEST['lettersubmitteddesg']) ? 'NULL' :
		"'" . $_REQUEST['lettersubmitteddesg'] . "'" ;
	$letterremark = empty($_REQUEST['letterremark']) || !isset($_REQUEST['letterremark']) ? 'NULL' :
		"'" . $_REQUEST['letterremark'] . "'" ;	
	
	
	
	 
	$sql = "call update_letter_details(" .$issueid .  ",".  $lettersubmittedto .  
		",". $lettersubmitteddept . ",". $lettersubmitted_desg . ",". $clientdatetime .  ",". $letterremark .")";
	if ($verbose != 'N') {
		echo $sql . '<br>';
	}
	if ($result = $mysqli->query($sql)) {	
		while ($row = $result->fetch_assoc()) {
			echo json_encode($row);
			break;
		} 
		$mysqli->close();
	}		else {
		echo "-1"; // something went wrong, probably sql failed
	}
	$notification_type = 'A Stamped Letter has been uploaded';
	$notification_sub_type = 'A Stamped Letter sent to authority';
	include("letter_upload_notification.php");     
} else {
	echo "-1"; // "Connection to db failed";
}