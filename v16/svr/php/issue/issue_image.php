<?php 
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
$php_name = 'IssueImage';
include("../dbconn_sar_apk.php");
include("../mobile_common_data_sar.php"); 
$errors = array();
$unregistered = false;
$issueimagefilename=NULL;
$imsrc = '';

if(isset($_REQUEST['issueimagefilename']) &&!empty( $_REQUEST['issueimagefilename'])){
	$issueimagefilename = $_REQUEST['issueimagefilename'];
	if ($verbose != 'N') {
		echo '<br>issueimagefilename: ' .$_REQUEST['issueimagefilename'];
	}	
	$imsrc = base64_decode($_REQUEST['issueimage']);
	$fp = fopen('images/' . $issueimagefilename, 'w');
	
	fwrite($fp, $imsrc);
	if(fclose($fp)){
		echo "Image uploaded";
	}else{
		echo "Error uploading image";
	}
	
} else {
	$issueimagefilename= 'NULL';
}

if ( $mysqli ) {
	
	
	$issueuniquekey = isset($_REQUEST['issueuniquekey']) ? $_REQUEST['issueuniquekey'] : 'NULL';
	$issueimagefilename = empty($_REQUEST['issueimagefilename']) || 
		!isset($_REQUEST['issueimagefilename']) ? 'NULL' : 
		"'" . $_REQUEST['issueimagefilename'] . "'" ;
	$issueimagefilepath= "'/" . $php_version . "/svr/php/issue/images/'" ; 
		
		
	$issueid = isset($_REQUEST['issueid']) ? $_REQUEST['issueid'] : 'NULL';	
		
	$creationdatetime = isset($_REQUEST['creationdatetime']) ? "\"" . $_REQUEST['creationdatetime'] . "\"" : 'NULL';	
	
	
	$sql = "call issue_image(" .$issueimagefilename . ", " . $issueuniquekey .
		 ", " . $creationdatetime . ", " . $issueid . ", " . $issueimagefilepath .")";
	if ($verbose != 'N') {
		echo '<br>sql:<br>' . $sql;
	}	
	if ($result = $mysqli->query($sql)) {	
		if ($result && is_object($result))  {
			while ($row = $result->fetch_assoc()) {
				echo json_encode($row);
				break;
			} 
		}
		$mysqli->close();
	}
	
}else {
		echo "-1";
	}