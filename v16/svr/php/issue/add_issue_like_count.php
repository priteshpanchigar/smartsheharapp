<?php
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
$php_name = 'addIssuelike';
include("../dbconn_sar_apk.php");
include("../mobile_common_data_sar.php");
if ( $mysqli ) {
	
	$liked = isset($_REQUEST['liked']) ? $_REQUEST['liked'] : 'NULL';
	$unliked = isset($_REQUEST['unliked']) ? $_REQUEST['unliked'] : 'NULL';
	$issueid = isset($_REQUEST['issueid']) ? $_REQUEST['issueid'] : 'NULL';
	
	
	
	$sql = "call add_like_count(" . $appuserid . "," . $issueid .
	"," . $liked ."," . $unliked . "," . $clientdatetime .")";
	
	if ($verbose != 'N') {
		echo '<br>sql:<br>' . $sql;
	}
	
	if ($result = $mysqli->query($sql)) {	
		if ($result && is_object($result))  {
			while ($row = $result->fetch_assoc()) {
				echo json_encode($row);
				break;
			}
		}
		$mysqli->close();
	}		else {
		echo "-1"; // something went wrong, probably sql failed
	}
} else {
	echo "-2"; // "Connection to db failed";
}