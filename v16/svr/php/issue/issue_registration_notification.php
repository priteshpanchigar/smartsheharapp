
<?php
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
$php_name = 'issue_registration_notification';
include("../dbconn_sar_apk.php");
include("../mobile_common_data_sar.php");
include("../gcmSendMessage.php");
$resultrows  = array();
$basemessage = "";
if ($mysqli) {
    $sql = "call issue_registration_notification(" . $issueid . ")";
    if ($verbose != 'N') {        
        
    }
    $result = $mysqli->query($sql);
    
    if (is_object($result)) {
        while ($row = $result->fetch_assoc()) {
            
            array_push($resultrows, $row['gcm_registration_id']);
            $issue_category    = $row['category'];
            $issue_subcategory = $row['subcategory'];
            $formatted_address = $row['formatted_address'];
            $issue_time        = $row['issue_time'];
            $uniquekey         = $row['unique_key'];
            
            $basemessage = "{\"issue_subcategory\":\"" .
			$issue_subcategory . "\", \"issue_category\":\"" . $issue_category . 
            "\", \"issue_address\":\"" . $formatted_address . 
			"\", \"issue_time\":\"" . $issue_time . 
			"\", \"uniquekey\":\"" . $uniquekey . 
			"\", \"issueid\":\"" . $issueid . 
			"\", \"message\":\"" .
			"New Complaint is registered" . "\"}";
                    
        }
		if ($verbose == 'Y') {
			echo $basemessage;	
            }   
    }
    if ($verbose == 'Y') {
        var_dump($resultrows);
    }
    $mysqli->close();
    gcmSendMessage($resultrows, $basemessage);
    
} else {
    echo "-1";
}

