<?php 
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
$php_name = 'addLetterHeadDetails';
include("../dbconn_sar_apk.php");
include("../mobile_common_data_sar.php"); 
$errors = array();
$unregistered = false;
$letterimagename=NULL;
$imsrc = '';

if(isset($_REQUEST['letterimagename']) &&!empty( $_REQUEST['letterimagename'])){
	$letterimagename = $_REQUEST['letterimagename'];
	if ($verbose != 'N') {
		echo '<br>letterimagename: ' .$_REQUEST['letterimagename'];
	}	
	$imsrc = base64_decode($_REQUEST['letterimage']);
	$fp = fopen('images/' . $letterimagename, 'w');
	
	fwrite($fp, $imsrc);
	if(fclose($fp)){
		//echo "Image uploaded";
	}else{
		echo "Error uploading image";
	}
	
} else {
	$letterimagename= 'NULL';
}

if ( $mysqli ) {
	

	$letterimagename = empty($_REQUEST['letterimagename']) || 
		!isset($_REQUEST['letterimagename']) ? 'NULL' : 
		"'" . $_REQUEST['letterimagename'] . "'" ;
	$letterimagepath= "'/" . $php_version . "/svr/php/issue/images/'" ; 
		
		
	$issueid = isset($_REQUEST['issueid']) ? $_REQUEST['issueid'] : 'NULL';	
		
	$letterdatetime = isset($_REQUEST['letterdatetime']) ? "\"" . $_REQUEST['letterdatetime'] . "\"" : 'NULL';	
	
	$lettersubmittedby = isset($_REQUEST['appuserid']) ?$_REQUEST['appuserid'] : 'NULL';
	$sql = "call add_letterhead_details(" .$letterimagename . 
		", " . $letterdatetime . ", " . $issueid . ", " . $letterimagepath .", " . $lettersubmittedby . ")";
	if ($verbose != 'N') {
		echo '<br>sql:<br>' . $sql;
	}	
	if ($result = $mysqli->query($sql)) {	
		if ($result && is_object($result))  {
			while ($row = $result->fetch_assoc()) {
				//echo json_encode($row);
				break;  
			} 
		}
		$mysqli->close();
	}
	else{
		echo "-1";
	}  
	
	    
}else { 
		echo "-1";
	}