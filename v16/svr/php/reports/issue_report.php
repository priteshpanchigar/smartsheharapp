<?php 
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
$php_name = 'issueReport';
include("../mobile_common_data_sar.php");
include("../dbconn_sar_apk.php");
$postalcodecomplaintRows = array();
if ($mysqli) {
	
	$issueid = empty($_REQUEST['issueid']) || !isset($_REQUEST['issueid']) ? 'NULL' :
		"'" . $_REQUEST['issueid'] . "'" ;	
	$fromissuedate = empty($_REQUEST['fromissuedate']) || !isset($_REQUEST['fromissuedate']) ? 'NULL' :
		"'" . $_REQUEST['fromissuedate'] . "'" ;
	$toissuedate = empty($_REQUEST['toissuedate']) || !isset($_REQUEST['toissuedate']) ? 'NULL' :
		"'" . $_REQUEST['toissuedate'] . "'" ;
	$postalcode = empty($_REQUEST['postalcode']) || !isset($_REQUEST['postalcode']) ? 'NULL' :
		"'" . $_REQUEST['postalcode'] . "'" ;

	$isnotss = empty($_REQUEST['isnotss']) || !isset($_REQUEST['isnotss']) ? 'NULL' : $_REQUEST['isnotss'];			
	
	$PV = empty($_REQUEST['PV']) || !isset($_REQUEST['PV']) ? 'NULL' :
		"'" . $_REQUEST['PV'] . "'" ;
	$MV = empty($_REQUEST['MV']) || !isset($_REQUEST['MV']) ? 'NULL' :
		"'" . $_REQUEST['MV'] . "'" ;
	$RT = empty($_REQUEST['RT']) || !isset($_REQUEST['RT']) ? 'NULL' :
		"'" . $_REQUEST['RT'] . "'" ;		
	$AT = empty($_REQUEST['AT']) || !isset($_REQUEST['AT']) ? 'NULL' :
		"'" . $_REQUEST['AT'] . "'" ;
	$RD = empty($_REQUEST['RD']) || !isset($_REQUEST['RD']) ? 'NULL' :
		"'" . $_REQUEST['RD'] . "'" ;
	$CL = empty($_REQUEST['CL']) || !isset($_REQUEST['CL']) ? 'NULL' :
		"'" . $_REQUEST['CL'] . "'" ;
	$EN = empty($_REQUEST['EN']) || !isset($_REQUEST['EN']) ? 'NULL' :
		"'" . $_REQUEST['EN'] . "'" ;
	$SF = empty($_REQUEST['SF']) || !isset($_REQUEST['SF']) ? 'NULL' :
		"'" . $_REQUEST['SF'] . "'" ;
	$WE = empty($_REQUEST['WE']) || !isset($_REQUEST['WE']) ? 'NULL' :
		"'" . $_REQUEST['WE'] . "'" ;
	$OG = empty($_REQUEST['OG']) || !isset($_REQUEST['OG']) ? 'NULL' :
		"'" . $_REQUEST['OG'] . "'" ;
	$RS = empty($_REQUEST['RS']) || !isset($_REQUEST['RS']) ? 'NULL' :
		"'" . $_REQUEST['RS'] . "'" ;	
		
	 
	$myissue = empty($_REQUEST['myissue']) || !isset($_REQUEST['myissue']) ? 0 : $_REQUEST['myissue'];
	$closed = empty($_REQUEST['closed']) || !isset($_REQUEST['closed']) ? 0 : $_REQUEST['closed'];
	$markerlat = empty($_REQUEST['markerlat']) || !isset($_REQUEST['markerlat']) ? 'NULL' : $_REQUEST['markerlat'];
	$markerlng = empty($_REQUEST['markerlng']) || !isset($_REQUEST['markerlng']) ? 'NULL' : $_REQUEST['markerlng'];
	$submitreport = empty($_REQUEST['submitreport']) || !isset($_REQUEST['submitreport']) ?  'NULL' : $_REQUEST['submitreport'];
	$rejected = empty($_REQUEST['rejected']) || !isset($_REQUEST['rejected']) ? 'NULL'  :$_REQUEST['rejected'];
	
	$resolved = 'NULL'; 
	if(isset($_REQUEST['resolved'])) {
		if($_REQUEST['resolved'] == 0 || $_REQUEST['resolved'] == 1)
			$resolved = $_REQUEST['resolved'];
		if (empty($_REQUEST['resolved']))
			$resolved = 'NULL'; 
	}	
	
	$closed =  empty($_REQUEST['closed']) || !isset($_REQUEST['closed']) ? 'NULL'  :$_REQUEST['closed'];
	$limit = empty($_REQUEST['limit']) || !isset($_REQUEST['limit']) ? 'NULL' : $_REQUEST['limit'];	
	$maxlat = empty($_REQUEST['maxlat']) || !isset($_REQUEST['maxlat']) ? 'NULL' : $_REQUEST['maxlat'];
	$maxlng = empty($_REQUEST['maxlng']) || !isset($_REQUEST['maxlng']) ? 'NULL' : $_REQUEST['maxlng'] ;
	$minlat = empty($_REQUEST['minlat']) || !isset($_REQUEST['minlat']) ? 'NULL' : $_REQUEST['minlat'];		
	$minlng = empty($_REQUEST['minlng']) || !isset($_REQUEST['minlng']) ? 'NULL' : $_REQUEST['minlng'];
	   
	$departmentcode = empty($_REQUEST['departmentcode']) || !isset($_REQUEST['departmentcode']) ? 'NULL' :
		"'" . $_REQUEST['departmentcode'] . "'" ;
	$ward = empty($_REQUEST['ward']) || !isset($_REQUEST['ward']) ? 'NULL' :
		"'" . $_REQUEST['ward'] . "'" ;
	$issuesubtypedescription = empty($_REQUEST['issuesubtypedescription']) || !isset($_REQUEST['issuesubtypedescription']) ? 'NULL' :
		"'" . $_REQUEST['issuesubtypedescription'] . "'" ;
	$issuesubtypecode = empty($_REQUEST['issuesubtypecode']) || !isset($_REQUEST['issuesubtypecode']) ? 'NULL' :
		"'" . $_REQUEST['issuesubtypecode'] . "'" ;		
	
	$groupby = empty($_REQUEST['groupby']) || !isset($_REQUEST['groupby']) ? 'NULL' :
		"'" . $_REQUEST['groupby'] . "'" ;
	$orderby = empty($_REQUEST['orderby']) || !isset($_REQUEST['orderby']) ? 'NULL' :
		"'" . $_REQUEST['orderby'] . "'" ;
		
	$count = empty($_REQUEST['count']) || !isset($_REQUEST['count']) ? 0 :  $_REQUEST['count'] ;	
	$offset = empty($_REQUEST['offset']) || !isset($_REQUEST['offset']) ? 0 :  $_REQUEST['offset'] ;
	
	
	$sql = " call issue_report(".$issueid."," . $postalcode."," .$fromissuedate.
		   "," .$toissuedate."," . $PV."," . $MV."," . $RT."," . $AT. "," . $RD.  
		   "," . $CL."," . $EN."," . $SF."," . $WE. "," . $OG."," . $RS.
		   "," . $myissue."," . $appuserid.
		   "," . $markerlat."," . $markerlng. "," . $limit. "," . $submitreport. "," . $rejected.
		   "," . $closed. "," . $resolved. "," . $maxlat. "," . $maxlng.
		   "," . $minlat. "," . $minlng. "," . $departmentcode. "," . $ward. "," . $issuesubtypecode.
		   "," . $groupby. "," . $orderby. "," . $count. "," . $offset. 
		   "," .$issuesubtypedescription . "," .$isnotss .")";
		
		
	if ($verbose != 'N') {
		echo $orderby . '<br>';
		echo $sql . '<br>';
	}	
	if ($result = $mysqli->query($sql)) {
	
		while ($row = $result->fetch_assoc()) {
		
			$postalcodecomplaintRows[] = $row;			
		}
 		
		$result->free();	// free result set
	}
	$mysqli->close();		// close connection
	
	if (count($postalcodecomplaintRows) > 0) {
		echo json_encode($postalcodecomplaintRows);
	} else {
		echo "-1";
	}
}
