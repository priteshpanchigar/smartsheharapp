<?php
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
$php_name = 'updatePositionBus';
include("../dbconn_sar_apk.php");
include("../mobile_common_data_short.php");
if ( $mysqli ) {
	
	$tripid = isset($_REQUEST['ti']) ? $_REQUEST['ti'] : 'NULL';
	
	
	$sql = "call update_position_bus(" . $appuserid . "," . $tripid . "," .
	$clientdatetime . "," . $email . "," . $location_data . ")";
	
	if ($verbose != 'N') {
		echo '<br>sql:<br>' . $sql;
	}
	
	if ($result = $mysqli->query($sql)) {	
		if ($result && is_object($result))  {
			while ($row = $result->fetch_assoc()) {
				echo json_encode($row);
				break;
			}
		}
		$mysqli->close();
	}		else {
		echo "-1"; // something went wrong, probably sql failed
	}
} else {
	echo "-2"; // "Connection to db failed";
}