<?php 
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
$php_name = "CreateTripBus";
$resultrows = array();
$tripid = 0;
include("../json_error.php");
include("../dbconn_sar_apk.php");
include("../mobile_common_data_short.php");
if ($mysqli) { 	
	
	date_default_timezone_set('Asia/Calcutta');
	$currentdate = date('m/d/Y h:i:s a', time());
	$timeinms = strtotime($currentdate) * 1000;
	$triptype = isset($_REQUEST['triptype']) ?  $_REQUEST['triptype']  : 'NULL';
	$fromaddress = isset($_REQUEST['fromaddress']) ?  $_REQUEST['fromaddress']  : 'NULL';
	$fromshortaddress = isset($_REQUEST['fromshortaddress']) ?  $_REQUEST['fromshortaddress'] : 'NULL';
	$toaddress = isset($_REQUEST['toaddress']) ?  $_REQUEST['toaddress']  : 'NULL';
	$toshortaddress = isset($_REQUEST['toshortaddress']) ? $_REQUEST['toshortaddress'] : 'NULL';
	$fromsublocality = isset($_REQUEST['fromsublocality']) ?"'" .  $_REQUEST['fromsublocality']. "'" : 'NULL';
	$tosublocality = isset($_REQUEST['tosublocality']) ?"'" .  $_REQUEST['tosublocality'] . "'" : 'NULL';
	$trip_directions_polyline = isset($_REQUEST['trip_directions_polyline']) ? "'" . $_REQUEST['trip_directions_polyline'] . "'" : 'NULL';
	$fromlat = isset($_REQUEST['fromlat']) ? "'" . $_REQUEST['fromlat'] . "'" : 'NULL';
	$fromlng = isset($_REQUEST['fromlng']) ? "'" . $_REQUEST['fromlng'] . "'" : 'NULL';
	$tolat = isset($_REQUEST['tolat']) ? "'" . $_REQUEST['tolat'] . "'" : 'NULL';
	$tolng = isset($_REQUEST['tolng']) ? "'" . $_REQUEST['tolng'] . "'" : 'NULL';
	$tripaction = isset($_REQUEST['tripaction']) ? "'" . $_REQUEST['tripaction'] . "'" : 'NULL';
	$triptime = isset($_REQUEST['triptime']) ? "'" . $_REQUEST['triptime'] . "'" : 'NULL';
	$tripdistance = isset($_REQUEST['tripdistance']) ? "'" . $_REQUEST['tripdistance'] . "'" : 'NULL';
	$inpath= "NULL";
	$verbose = isset($_REQUEST['verbose']) ? $_REQUEST['verbose'] : 'N';
	$lastinvited = isset($_REQUEST['lastinvited']) ? $_REQUEST['lastinvited'] : 'NULL';
	$plannedstartdatetime = isset($_REQUEST['plannedstartdatetime']) ?  "\"" . $_REQUEST['plannedstartdatetime'] . "\"": 'NULL';
	$fromaddress = addslashes($fromaddress);
	$fromshortaddress = addslashes($fromshortaddress);
	$toaddress = addslashes($toaddress);
	$toshortaddress = addslashes($toshortaddress);
	$triptype = addslashes($triptype);
	
	$sql = "call create_trip_bus('" . $triptype . "','" . $fromaddress . "','" . $fromshortaddress . "','" .
	$toaddress . "','" . $toshortaddress . "'," . $fromsublocality . "," . $tosublocality . "," .
	$trip_directions_polyline . "," .
	$fromlat . "," . $fromlng . "," . $tolat . "," . $tolng . "," . $timeinms . "," .
	$tripaction . "," . $triptime . "," .
	$tripdistance . ","  . $appuserid .  "," .
	$clientdatetime . "," . $email .  "," . $lastinvited .  "," . $plannedstartdatetime .")";
	
	if ($verbose != 'N') {
		echo "<br>Sub locality: " . $tosublocality . "<br>";
		echo $sql . "<br>";
	}
	$tripFound = 0;
	if ($result = $mysqli->query($sql)) {
		if ($row = $result->fetch_assoc()) {
			$resultrows[] = $row;
			$tripid = $row['trip_id'];
			$tripFound = $row['tripFound'];
			if ($verbose != 'N') {	
				echo "<br>tripRow: ";
				var_dump ($row);
				echo "<br>tripFound: " . $tripFound . "<br>";
			}
			echo json_encode($row);
			
		}
		$result->free();
	} else {
		echo -1; // something went wrong, probably sql failed
		$tripid = -1;
	}
	if ($verbose != 'N') {	
		echo "<br>tripFound: " . $tripFound . "<br>";
	}
	$mysqli->close();
}