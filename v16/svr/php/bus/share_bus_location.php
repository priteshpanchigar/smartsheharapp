<?php
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
$php_name = 'shareBusLocation';
include("../dbconn_sar_apk.php");
include("../mobile_common_data_short.php");
if ( $mysqli ) {
	
	$buslabel = empty($_REQUEST['buslabel']) || !isset($_REQUEST['buslabel']) ? 'NULL' : 
		"'" . $_REQUEST['buslabel'] . "'" ;
	$direction = empty($_REQUEST['direction']) || !isset($_REQUEST['direction']) ? 'NULL' : 
		"'" . $_REQUEST['direction'] . "'" ;	
	$stopserial = isset($_REQUEST['ss']) ? $_REQUEST['ss'] : 'NULL';
	
	
	$sql = "call share_bus_location(" . $appuserid .  "," .
	$clientdatetime . "," . $email . "," . $location_data ."," .
	$buslabel . "," . $stopserial . "," . $direction .	")";
	
	if ($verbose != 'N') {
		echo '<br>sql:<br>' . $sql;
	}
	
	if ($result = $mysqli->query($sql)) {	
		if ($result && is_object($result))  {
			while ($row = $result->fetch_assoc()) {
				echo json_encode($row);
				break;
			}
		}
		$mysqli->close();
	}		else {
		echo "-1"; // something went wrong, probably sql failed
	}
} else {
	echo "-2"; // "Connection to db failed";
}