var departmentcode = getParameter("departmentcode")? getParameter("departmentcode") : '';
var ward = getParameter("ward") ? getParameter("ward") : '';
var postalcode='';
var startDate='';
var endDate='';
var sWard='';
var spnFilter='';
var date='';
var issuetype='';
   

function getDepartmentSummary( successCallBack) {

	var parameters = '';
	if(departmentcode=='' || departmentcode ==null)
	{
		departmentcode='';
	}  
	else{ parameters = 'departmentcode='+departmentcode;}
	if(postalcode=='' || postalcode ==null )
	{
		postalcode='';
	}else{ parameters = parameters + '&postalcode='+postalcode;}
	if(startDate=='' || startDate ==null)
	{
		startDate = '';
	}else{
		date = startDate.split("-");
		startDate= date[2] + '-' + date[1]  + '-' + date[0];
		parameters = parameters + '&fromissuedate='+startDate;};
	if(endDate=='' || endDate ==null)
	{
		endDate = '';
	}else{
		date = endDate.split("-");
		endDate = date[2] + '-' + date[1]  + '-' + date[0];
		parameters = parameters + '&toissuedate='+endDate;}
	if(ward== '' || ward ==null)
	{
		ward ='';
	}else{parameters = parameters + '&ward='+escape(ward);}
      var getDepartmentSummaryUrl = getIssueReportPhpScriptsPath() + 
        'rp_filter_pincodewise.php?'+parameters;
    $.ajax({
        type: 'GET',
        url: getDepartmentSummaryUrl,
        dataType: 'json',
        success: function(data) {
            successCallBack(data);
        },
        error: function(xhr, textStatus, errorThrown) {
            var err = textStatus + ', ' + errorThrown;
        }
        
    });
}
function getWard(successCallBack) {
      var getWardUrl = getIssuePhpScriptsPath() +
        'get_ward.php';
    $.ajax({
        type: 'GET',
        url: getWardUrl,
        dataType: 'json',
        success: function(data) {
            successCallBack(data);
        },
        error: function(xhr, textStatus, errorThrown) {
            var err = textStatus + ', ' + errorThrown;
        }
       
    });
}

$(document).ready(function() {
	var tbl=document.getElementById("issueTable");
	var img;
	var tr='';
	var j = 0;
	var cls;
	
	var color='';
	var heading='';  
	var row ='';
    var cell ='';
	
	var tableRef = document.getElementById('issueTable').getElementsByTagName('tbody')[0];
	   if (typeof(Storage) !== "undefined") {
		startDate = localStorage.getItem("startDate");
		endDate = localStorage.getItem("endDate");
		postalcode = localStorage.getItem("postalcode");
		sWard = localStorage.getItem("ward", ward);
		issuetype = localStorage.getItem("issuetype", issuetype);
		if(sWard==null || sWard =="")
		{}else{
			ward = sWard;
		}
		}  
		if(startDate == '' ||  startDate == null)
			{}else{
			date = startDate.split("-");
			startDate= date[0] + '-' + date[1]  + '-' + date[2];}
			if(endDate == '' || endDate == null)
			{}else{
			date = endDate.split("-");
			endDate= date[0] + '-' + date[1]  + '-' + date[2];}
			document.getElementById('startDateText').value = startDate;
			document.getElementById('endDateText').value = endDate;
			document.getElementById('postalCode').value = postalcode;
			document.getElementById('selectIssueType').value = issueType;
		
		getWard(
			function(data) {
				var selectWard = document.getElementById("selectWard");
				var option;
				option = document.createElement("option");
				option.value= "select";
				option.text = "Select your Ward";
				selectWard.add(option);
				len = data.length;
				for (i = 0; i < len; i = i + 1) {
					
					row = data[i];
					option = document.createElement("option");
					option.id = row.ward;
					option.value= row.ward;
					option.text = row.ward;
					selectWard.add(option);
					
				}
				option.value="";
				option.text = "Other";
				selectWard.add(option);
				if(ward == null || ward == '')
				{}
				else{
				document.getElementById('selectWard').value = ward;}
        });
		
		
		
			setFilterSummary();
			
			showDepartmentSummary();
}); // document.ready


function goFilter(){
	
	postalcode = document.getElementById('postalCode').value;
	date = $("#startDateText").val().split("-");
	startDate= date[0] + '-' + date[1]  + '-' + date[2];

	date = $("#endDateText").val().split("-");
	endDate= date[0] + '-' + date[1]  + '-' + date[2];
	
	sWard = document.getElementById("selectWard").value; 
	issuetype = document.getElementById("selectIssueType").value;
	if(sWard !== 'select')
	{
		ward = sWard;
	}
	if(sWard === 'select')
	{
		sWard = '';
	}
	if(startDate === endDate)
	{
		alert("Start Date and end date should not be same!");
	}
	else{
		
		setFilterSummary();
		if (typeof(Storage) !== "undefined") {
			localStorage.setItem("startDate", startDate);
			localStorage.setItem("endDate", endDate);
			localStorage.setItem("postalcode", postalcode);
			localStorage.setItem("ward", ward);
			localStorage.setItem("issuetype", issuetype);
		}
		
		showDepartmentSummary();
	}
	
}
function setFilterSummary()
{	spnFilter = '';
	spnFilter = spnFilter + "Filter by ";
	if(startDate == '' || startDate == null)
	{}else{	spnFilter= spnFilter + "Date From "+startDate; }

	if(endDate == '' || endDate == null)
	{}else{spnFilter= spnFilter +" To " + endDate;}
	
	if(postalcode == '' || postalcode == null)
	{}else{spnFilter = spnFilter +", Postal Code "+postalcode;}

	if(ward == '' || ward == null)
	{}else{spnFilter = spnFilter +", Ward "+ward;}

	if(startDate == null || startDate == '' 
		&& endDate == null || endDate == '' 
		&& postalcode == null || postalcode == ''
		&& ward == null || ward == '')
		{
			spnFilter = '';
		}
	document.getElementById("spnFilter").innerHTML = spnFilter ;
}
function getParameter(theParameter) { 
  var params = window.location.search.substr(1).split('&');
 
  for (var i = 0; i < params.length; i++) {
    var p=params[i].split('=');
	if (p[0] == theParameter) {
	  return decodeURIComponent(p[1]);
	}
  }
  return false;
}
function showDepartmentSummary()
{
	getDepartmentSummary(
        function(data) {
			var cnt = 0;
            len = data.length;
			var sHtml = '';
			sHtml =  sHtml + '<tr><th>Issue Type</th><th><p class="text-right">Total Issues</p></th></tr>';
            for (i = 0; i < len; i = i + 1) {
				if(i%2==0){
					cls = 'active';
				}
				else{
					cls = 'warning';
				}
                row = data[i];
				sHtml =  sHtml + '<tr class= '+cls+'><td><a href="'+getReportHtmlPath()+
					'rp_filter_pincodewise.html?issuesubtypecode=' + 
					row.issue_sub_type_code + 
					'&ward=' + escape(ward) +
					'&departmentcode=' + departmentcode +
					'&fromissuedate=' + startDate +
					'&toissuedate=' + endDate +
					'&=issuesubtypecode=' + row.issue_sub_type_code +
					'&' + row.issue_type_code + '=' + row.issue_type_code+'">' + row.issue_sub_type_description + '</a>' + 
					'</td><td align="right">' + row.cnt + '</td></tr>';
					
				   
				cnt = cnt + parseInt(row.cnt);
				/*
				tr = tableRef.insertRow(tableRef.rows.length);
				tr.className = cls;
				cell = tr.insertCell(0);
				cell.innerHTML = row.description;
				cell = tr.insertCell(1);
				cell.innerHTML = row.cnt;
				cell.style.textAlign = "right";
				tr.setAttribute('onclick', "(function(){ alert('"+row.code+"'); })()");
				*/
			}
			document.getElementById("issueTable").innerHTML = sHtml;
			//$('#tbody').html(tbody);
			if(departmentcode == 'TR'){
				heading = 'Traffic Issues '
				img = 'fa fa-car fa-5x';
				color = 'panel panel-yellow';
			}else{
				heading = 'Municipal Issues '
				img = 'fa fa-trash-o fa-5x';
				color = 'panel panel-red';
			}
        });
		
}