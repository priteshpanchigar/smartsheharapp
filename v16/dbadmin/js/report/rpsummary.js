var groupby = ' department_code';
   
function getSmartsheharSummary( successCallBack) {
     var getSmartsheharSummaryUrl = getReportsPhpScriptsPath() +
        'smartshehar_summary.php';
    $.ajax({
        type: 'GET',
        url: getSmartsheharSummaryUrl,
        dataType: 'json',
        success: function(data) {
            successCallBack(data);
        },
        error: function(xhr, textStatus, errorThrown) {
            var err = textStatus + ', ' + errorThrown;
        }
    });
}

function showIssue(issueId) {
	window.location.href = "../../issue_report.html?issueid=" + issueId;
	
}
function getIssueByDate( successCallBack) {
     var getIssueByDateUrl = getReportsPhpScriptsPath() +
        'issue_by_date.php';
    $.ajax({
        type: 'GET',
        url: getIssueByDateUrl,
        dataType: 'json',
        success: function(data) {
            successCallBack(data);
        },
        error: function(xhr, textStatus, errorThrown) {
            var err = textStatus + ', ' + errorThrown;
        }
    });
}

function getIssueWard( successCallBack) {
     var getIssueWardUrl = getIssueReportPhpScriptsPath() + 
        'issue_report.php?departmentcode=MU&groupby=ward'+
		 '&submitreport=1&orderby=' + encodeURIComponent('cnt desc');
    $.ajax({
        type: 'GET',
        url: getIssueWardUrl,
        dataType: 'json',
        success: function(data) {
            successCallBack(data);
        },
        error: function(xhr, textStatus, errorThrown) {  
            var err = textStatus + ', ' + errorThrown;
        }  
    });
}

function getIssueByCategory( successCallBack) {
    var getIssueByCatUrl = getIssueReportPhpScriptsPath() + 
        'issue_report.php?departmentcode=MU&groupby=issue_sub_type_code'+
		 '&submitreport=1&orderby=' + encodeURIComponent('cnt desc');
    $.ajax({
        type: 'GET',
        url: getIssueByCatUrl,
        dataType: 'json',
        success: function(data) {
            successCallBack(data);
        },
        error: function(xhr, textStatus, errorThrown) {
            var err = textStatus + ', ' + errorThrown;
        }
    });
}

function getIssueTimeline( successCallBack) {
     var getIssueTimelineUrl = getReportsPhpScriptsPath() +
        'get_issue_timeline.php';
    $.ajax({
        type: 'GET',
        url: getIssueTimelineUrl,
        dataType: 'json',
        success: function(data) {
            successCallBack(data);
        },
        error: function(xhr, textStatus, errorThrown) {
            var err = textStatus + ', ' + errorThrown;
        }
    });
}
$(document).ready(function() {
	var countOpenIssues ;
	var countTrafficIssues ;
	var countMunicipalIssues ;
	var contributors;
	var department_breakup_data = null;
	var flipBy;  
	if (typeof(Storage) !== "undefined") {
		 flipBy = localStorage.getItem("flip");
		 
		 if(flipBy === 'bycategory')
		 {      
			document.getElementById('divWard').setAttribute('style','display:none');
			document.getElementById('btnWard').setAttribute('style','display:none');
			document.getElementById('btnCategory').setAttribute('style','display:block');
			document.getElementById('divCategory').setAttribute('style','display:block');
		 }else
		 {
			document.getElementById('divWard').setAttribute('style','display:block');
			document.getElementById('btnWard').setAttribute('style','display:block');
			document.getElementById('btnCategory').setAttribute('style','display:none');
			document.getElementById('divCategory').setAttribute('style','display:none'); 
		 }
		 
	 }
	
	document.getElementById("city").innerHTML = 'Mumbai';

	getIssueByDate(function (data) {
		department_breakup_data = data;
		new Morris.Line({
			  // ID of the element in which to draw the chart.
			  element: 'morris-area-chart',
			  // Chart data records -- each entry in this array corresponds to a point on
			  // the chart.
			  data: department_breakup_data,
			
			  // The name of the data record attribute that contains x-values.
			  xkey: 'date',
			  // A list of names of data record attributes that contain y-values.
			  ykeys: ['mu_cnt', 'tr_cnt'],
			  // Labels for the ykeys -- will be displayed when you hover over the
			  // chart.
			  labels: ['Mun.', 'Traf.'],
			  lineColors: ["#d9534f","#f0ad4e"]
		});
		

	});
	
	getSmartsheharSummary(
        function(data) { 
			document.getElementById("openIssues").innerHTML  = data.v_open_issues;
			document.getElementById("traffic").innerHTML = data.v_traffic_issues;
			document.getElementById("municipal").innerHTML = data.v_municipal_issues;
			document.getElementById("contributors").innerHTML = data.v_contributors; 
			document.getElementById("resolvedIssues").innerHTML = data.v_resolved_issues;
        });
		
	getIssueTimeline(
        function(data) {
			var sHtml = '', sAddr;
			len = data.length;
			var faImage = 'fa-check';
            for (i = 0; i < len; i = i + 1) {
				row = data[i];
				if (row.department_code === 'MU')
					faImage = 'fa-trash-o { color: #464646; }';
				if (row.department_code === 'TR')
					faImage = 'fa-car';
				sAddr = '';
				if(typeof(row.route) !== 'undefined'  && row.route ) {
					sAddr = row.route;
				}
				if(typeof(row.sublocality) !== 'undefined'  && row.sublocality && row.sublocality != null) {
					sAddr += (sAddr ? ', ' : '') + row.sublocality;
				}
				if(typeof(row.area) !== 'undefined'  && row.area && row.area != null) {
					sAddr += (sAddr ? ', ' : '') + row.area;
				}
				if(!sAddr) {
					sAddr = row.formatted_address;
				}
				sHtml = sHtml +  
				 
				'<li  ' 
				+ (i%2 == 0 ? "" : 'class="timeline-inverted"') + ' ><div class="timeline-badge"><i class="fa ' + faImage + '"></i>' +
                '</div>' +
                '<div class="timeline-panel" onclick = "showIssue(' + row.issue_id + ')">' + 
                 '<div class="timeline-heading">' +
                 '<h4 class="timeline-title">' + row.department_description + ' issue</h4>' +
                  '<p><small class="text-muted"><i class="fa fa-clock-o"></i>' + " " +
				moment(moment(row.issue_time, "YYYYMMDD h:m:s"), "YYYYMMDDhms").fromNow() + 
				'</small>' +
                '</p>' +
                '</div>' +
				'<div class="timeline-body">' +
				'<p>' + row.issue_item_description + '</p>' +
			   	'<p>in Ward ' + row.ward + '</p>' +
				'<p>at ' + sAddr 
				+ '</p>' +
                '</div>' +
                '</div>' +
                '</a></li>';
			}
			document.getElementById("issuetimeline").innerHTML = sHtml;
							
        });	
		getIssueWard(
        function(data) {
         
			var tbody='';
			var tr='';
			var cls;
			var uri = '';
			var openCnt = 0;
			var totalCnt = 0;
			var resolvedCnt = 0;
			var len = data.length;
            for (i = 0; i < len; i = i + 1) {
				if(i%2==0){
					cls = 'active';
				}
				else{
					cls = 'warning';
				}
				row = data[i];
				totalCnt = parseInt(row.cnt,10);  
				resolvedCnt =  parseInt(row.resolvedCnt,10); 
				opneCnt =  totalCnt - resolvedCnt;
				if(row.ward!=='Select')
				{
					tr = '<tr class="'+cls+'"><td>' +
					'<a href="' + 'department.html?departmentcode=MU&groupby=issue_sub_type_code&ward=' + escape(row.ward) + '">' 
					+row.ward+'</a></td>';
					if(opneCnt===0)
						{  
							tr = tr +'<td align="right">'+opneCnt+'</td>';
						}
						else{  
							tr = tr +'<td align="right"><a href="'+getReportHtmlPath()+
							'issue_report.html?departmentcode=MU&ward='+row.ward+
							'&submitreport=1&resolved=0&orderby=' + encodeURIComponent('cnt desc')+'">' + opneCnt + '</a></td>';
						}  
					if(resolvedCnt===0)
						{
							tr = tr +'<td align="right">'+resolvedCnt+'</td>';
						}
						else{ 
							tr = tr +'<td align="right"><a href="'+getReportHtmlPath()+
							'issue_report.html?departmentcode=MU&ward='+row.ward+
							'&submitreport=1&resolved=1&orderby=' + encodeURIComponent('cnt desc')+'">' + resolvedCnt + '</a></td>';
						}
					tr = tr +'<td align="right">'+totalCnt+'</td></tr>';
					tbody = tbody + tr;
				}
			}
			$('#tbodyWard').html(tbody);
        });
		
		// ISSUE BY CATEGORY
		getIssueByCategory(
        function(data) {
         
			var tbody='';
			var tr='';
			var cls;
			var uri = '';
			var len = data.length;
			var openCnt = 0;
			var totalCnt = 0;
			var resolvedCnt = 0;
            for (i = 0; i < len; i = i + 1) {
				if(i%2==0){
					cls = 'active';
				}
				else{   
					cls = 'warning';   
				}  
				  row = data[i];
				totalCnt = parseInt(row.cnt,10);  
				resolvedCnt =  parseInt(row.resolvedCnt,10); 
				opneCnt =  totalCnt - resolvedCnt;
				tr = '<tr class="'+cls+'"><td>' +
				'<a href="' + 'department.html?departmentcode=MU&groupby=ward&issuesubtypedescription=' + escape(row.issue_sub_type_description) + '"</a>'
				 +row.issue_sub_type_description+'</td>';
				 if(opneCnt===0)
					{  
						tr = tr +'<td align="right">'+opneCnt+'</td>';
					}else{  
						tr = tr +'<td align="right"><a href="'+getReportHtmlPath()+
						'issue_report.html?departmentcode=MU&issuesubtypedescription='+escape(row.issue_sub_type_description) +
						'&submitreport=1&resolved=0&orderby=' + encodeURIComponent('cnt desc')+'">' + opneCnt + '</a></td>';
					} 
				if(resolvedCnt===0)
					{
						tr = tr +'<td align="right">'+resolvedCnt+'</td>';
					}
					else{ 
						tr = tr +'<td align="right"><a href="'+getReportHtmlPath()+
						'issue_report.html?departmentcode=MU&issuesubtypedescription='+escape(row.issue_sub_type_description) +
						'&submitreport=1&resolved=1&orderby=' + encodeURIComponent('cnt desc')+'">' + resolvedCnt + '</a></td>';
						}
					
					
				tr = tr +'<td align="right">'+totalCnt+'</td></tr>';
				tbody = tbody + tr;
					
					  
			}
			$('#tbodyCategory').html(tbody);
        });
		// ISSUE BY CATEGORY
		
		
}); // document.ready