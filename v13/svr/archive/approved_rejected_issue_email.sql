DROP PROCEDURE IF EXISTS approved_rejected_issue_email;
DELIMITER$$
CREATE  PROCEDURE `approved_rejected_issue_email`(IN inoffencedetailid INT)
BEGIN

select offence_detail_id, offence_address, offence_time, client_datetime, offence_type, vehicle_no, unique_key,
submit_report, discard_report, address_id, location_id, ward_no, complaint_type_code, complaint_sub_type_code,
municipal_department, approved, rejected, 
(SELECT reject_reason FROM rejected_reason 
	WHERE vw_offence.reason_code = rejected_reason.reason_code) reject_reason,
sent_to_authority, lat, lng, appuser_id, email as email_id, offence_image_id, offence_image_name
offence_image_path, creation_datetime FROM vw_offence
WHERE offence_detail_id  = inoffencedetailid;

END$$
DELIMITER ;
call approved_rejected_issue_email(6); 