DROP PROCEDURE IF EXISTS get_unapproved_offence;
DELIMITER$$
CREATE  PROCEDURE `get_unapproved_offence`(IN inward VARCHAR(20), 
IN incomplainttypecode VARCHAR(20)) 

BEGIN
SELECT offence_detail_id, offence_address, offence_time, client_datetime, offence_type, 
vehicle_no, unique_key, submit_report, approved,rejected, 
(SELECT reject_reason FROM rejected_reason 
	WHERE vw_offence.reason_code = rejected_reason.reason_code) reject_reason,
sent_to_authority, approved_rejected_clientdatetime,  sent_to_authority_datetime,  
resolved_unresolved_datetime, 	other_reason_code_comment,
address_id, location_id, complaint_type_code, complaint_sub_type_code,municipal_department, lat, lng,
offence_image_name, offence_image_path, appuser_id FROM vw_offence
WHERE submit_report = 1 
AND sent_to_authority = 0
AND complaint_type_code =incomplainttypecode
AND ward = inward;

END$$
DELIMITER;

CALL get_unapproved_offence('K/W','TR_VPO' );
