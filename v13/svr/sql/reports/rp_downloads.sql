
DROP PROCEDURE IF EXISTS `rp_downloads`;
DELIMITER $$
CREATE  PROCEDURE `rp_downloads`(IN infromdatetime datetime, IN intodatetime datetime)
BEGIN


SELECT ap.appuser_id,		email,		ap.clientlastaccessdatetime,
ap.first_location_id,	ap.last_location_id,		username,	fullname,
age,	sex,	phoneno,		lat,	lng,		product,	manufacturer,	
android_release_version FROM appuser a
LEFT JOIN appusage ap 
ON a.appuser_id = ap.appuser_id
WHERE #email not in (select email from ss_employees) AND 
ap.clientfirstaccessdatetime BETWEEN infromdatetime AND intodatetime
AND ap.clientfirstaccessdatetime IS NOT NULL;


END$$
DELIMITER;
