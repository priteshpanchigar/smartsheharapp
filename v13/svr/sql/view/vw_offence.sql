DROP VIEW
IF EXISTS vw_offence;

create VIEW vw_offence as
SELECT
	`od`.`offence_detail_id` AS `offence_detail_id`,
	`od`.`offence_address` AS `offence_address`,
	`od`.`offence_time` AS `offence_time`,
	`od`.`client_datetime` AS `client_datetime`,
	`od`.`offence_type` AS `offence_type`,
	`od`.`vehicle_no` AS `vehicle_no`,
	`od`.`unique_key` AS `unique_key`,
	`od`.`submit_report` AS `submit_report`,
	`od`.`discard_report` AS `discard_report`,
	`od`.`address_id` AS `address_id`,
	`od`.`location_id` AS `location_id`,
	`od`.`ward` AS `ward`,
	`od`.`complaint_type_code` AS `complaint_type_code`,
	`od`.`complaint_sub_type_code` AS `complaint_sub_type_code`,
	`od`.`group_id` AS `group_id`,
	`c2`.`colour` AS `colour`,
	`c2`.`android_colour` AS `android_colour`,
	`cl`.`municipal_department` AS `municipal_department`,
	`od`.`approved` AS `approved`,
	`od`.`rejected` AS `rejected`,
	`od`.`approved_rejected_clientdatetime` AS `approved_rejected_clientdatetime`,
	`od`.`sent_to_authority_datetime` AS `sent_to_authority_datetime`,
	`od`.`resolved_unresolved_datetime` AS `resolved_unresolved_datetime`,
	`od`.`other_reason_code_comment` AS `other_reason_code_comment`,
	`od`.`resolved` AS `resolved`,
	`od`.`unresolved` AS `unresolved`,
	`od`.`unresolved_comment` AS `unresolved_comment`,
	 od.reason_code AS reason_code,
	`od`.`sent_to_authority` AS `sent_to_authority`,
	`od`.`lat` AS `lat`,
	`od`.`lng` AS `lng`,
	`od`.`appuser_id` AS `appuser_id`,
	`a`.`email` AS `email`,
	`a`.`phoneno` AS `phoneno`,
	`a`.`username` AS `username`,
	`a`.`fullname` AS `fullname`,
	`a`.`age` AS `age`,
	`a`.`sex` AS `sex`,	
	`oi`.`offence_image_id` AS `offence_image_id`,
	`oi`.`offence_video_path` AS `offence_video_path`,
	`oi`.`offence_video_name` AS `offence_video_name`,
	`oi`.`offence_image_name` AS `offence_image_name`,
	`oi`.`offence_image_path` AS `offence_image_path`,
	`oi`.`creation_datetime` AS `creation_datetime`,
	`oi`.`letter_datetime` AS `letter_datetime`,
	`oi`.`letter_image_name` AS `letter_image_name`,
	`oi`.`letter_image_path` AS `letter_image_path`,
	`oi`.`letter_submited` AS `letter_submited`,
FROM 	`offence_details` `od`
		JOIN `offence_image` `oi` 
		ON `od`.`offence_detail_id` = `oi`.`offence_detail_id`
		JOIN complaint_lookup_subtypes cl
		ON od.complaint_sub_type_code = cl.complaint_sub_type_code
		JOIN complaint_lookup_types c2
		ON od.complaint_type_code = c2.complaint_type_code
		JOIN appuser a
		ON od.appuser_id = a.appuser_id;
		
		
