DROP PROCEDURE IF EXISTS t_megablock;
DELIMITER $$
CREATE PROCEDURE `t_megablock`(IN `indatetime` datetime)
BEGIN
	
SELECT linecode,display, up, down, slow,`fast`, fromdatetime, todatetime, fromstation, tostation, fromstation_id, tostation_id, fromstationcode,
tostationcode, link, message, repeatdays FROM t_megablock
WHERE  indatetime < todatetime
ORDER BY linecode;

END$$
DELIMITER ;