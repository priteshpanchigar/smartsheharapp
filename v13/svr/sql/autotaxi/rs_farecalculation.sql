DROP PROCEDURE IF EXISTS  rs_getfarecalculation;
DELIMITER$$
CREATE  PROCEDURE  rs_getfarecalculation(IN incity varchar(25))
BEGIN
SELECT _id, srno, city, transport, vehicle_type, item, calculation, style
	FROM  rs_farecalculation
	WHERE city = incity
	ORDER BY city, transport, vehicle_type, srno;
END$$
DELIMITER ;

call  rs_getfarecalculation('Mumbai');

