
DROP PROCEDURE IF EXISTS offence_rep_violation_code;

CREATE  PROCEDURE `offence_rep_violation_code`(IN offencecode varchar(20))
BEGIN
	
SELECT DISTINCT(offence_detail_id), offence_address,
CONCAT(DAY(offence_time)," ",MONTHNAME(offence_time), ", ", YEAR(offence_time)," at ", TIME_FORMAT(offence_time,'%h:%i%p'))as offence_time, 
offence_type , client_datetime, creation_datetime, appuser_id, offence_image_name  
FROM vw_offence
	WHERE offence_image_name != "" 
		AND complaint_type_code = offencecode
		AND offence_image_id IN
		(select  MIN(offence_image_id) offence_image_id 
			from  offence_image
		GROUP BY offence_detail_id);	



END

CALL offence_rep_violation_code('MU_CLNN');

CREATE DEFINER=`smartshehar`@`%` PROCEDURE `offence_betw_date`(IN fromdatetime  datetime , IN todatetime datetime)
BEGIN
	
SELECT DISTINCT(offence_detail_id), offence_address,
CONCAT(DAY(offence_time)," ",MONTHNAME(offence_time), ", ", YEAR(offence_time)," at ", TIME_FORMAT(offence_time,'%h:%i%p'))as offence_time, 
offence_type , client_datetime, creation_datetime, appuser_id, offence_image_name  
FROM vw_offence
	WHERE offence_image_name != "" 
		AND Date(offence_time) BETWEEN date(fromdatetime) and date(todatetime)
		AND offence_image_id IN
		(select  MIN(offence_image_id) offence_image_id 
			from  offence_image
		GROUP BY offence_detail_id);	


END
