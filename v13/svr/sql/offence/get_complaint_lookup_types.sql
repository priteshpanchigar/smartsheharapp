DROP PROCEDURE IF EXISTS get_complaint_lookup_types;
DELIMITER$$
CREATE  PROCEDURE `get_complaint_lookup_types`()
BEGIN
	
SELECT complaint_lookup_id,
complaint_type_code,
complaint_description FROM complaint_lookup_types;
END$$
DELIMITER ;