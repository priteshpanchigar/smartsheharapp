DROP PROCEDURE IF EXISTS show_issues_on_map;
DELIMITER$$
CREATE  PROCEDURE `show_issues_on_map`(IN incomplainttypecode varchar(20))
BEGIN
	SELECT offence_type, 
	(DATE_FORMAT(offence_time,'%b %d %Y %h:%i %p')) AS offence_time,
	lat,lng,offence_time,offence_address,offence_image_name, approved,rejected, 
(SELECT reject_reason FROM rejected_reason 
	WHERE vw_offence.reason_code = rejected_reason.reason_code) reject_reason, sent_to_authority,
resolved, unresolved, unresolved_comment
FROM vw_offence
WHERE  complaint_type_code = incomplainttypecode
		AND offence_image_id IN
		(select  MIN(offence_image_id) offence_image_id 
			from  offence_image
		GROUP BY offence_detail_id);	

	
END$$
DELIMITER;

CALL show_issues_on_map('');