DROP PROCEDURE IF EXISTS get_resolution_status;
DELIMITER$$
CREATE  PROCEDURE `get_resolution_status`(IN inward VARCHAR(20), 
IN incomplainttypecode VARCHAR(20)) 

BEGIN
IF inward = NULL THEN
SELECT offence_detail_id, offence_address, offence_time, client_datetime, offence_type, 
vehicle_no, unique_key, submit_report, approved_rejected_clientdatetime,approved,rejected, resolved, unresolved, unresolved_comment,
(SELECT reject_reason FROM rejected_reason 
	WHERE vw_offence.reason_code = rejected_reason.reason_code) reject_reason,
sent_to_authority,
address_id, location_id, complaint_type_code, complaint_sub_type_code,municipal_department, lat, lng,
offence_image_name, offence_image_path, appuser_id FROM vw_offence
WHERE offence_detail_id = (SELECT offence_detail_id FROM offence_details 
WHERE sent_to_authority = 1
AND resolved = 0
AND unresolved = 0
AND complaint_type_code = incomplainttypecode
AND ward =  ''  ORDER BY offence_detail_id limit 1) ;
ELSE
SELECT offence_detail_id, offence_address, offence_time, client_datetime, offence_type, 
vehicle_no, unique_key, submit_report, approved_rejected_clientdatetime,approved,rejected, resolved, unresolved, unresolved_comment,
(SELECT reject_reason FROM rejected_reason 
	WHERE vw_offence.reason_code = rejected_reason.reason_code) reject_reason,
sent_to_authority,
address_id, location_id, complaint_type_code, complaint_sub_type_code,municipal_department, lat, lng,
offence_image_name, offence_image_path, appuser_id FROM vw_offence
WHERE offence_detail_id = (SELECT offence_detail_id FROM offence_details 
WHERE sent_to_authority = 1
AND resolved = 0
AND unresolved = 0
AND complaint_type_code = incomplainttypecode
AND ward = inward  ORDER BY offence_detail_id limit 1 );
END IF;

END$$
DELIMITER;

call get_resolution_status('K/E','MU_CLN')