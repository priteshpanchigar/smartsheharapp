DROP PROCEDURE IF EXISTS get_Emailid_Using_Postal_Code;
DELIMITER$$
CREATE  PROCEDURE `get_Emailid_Using_Postal_Code`(IN inuniquekey varchar(255))
BEGIN

SELECT 'rtocomplaint@smartshehar.com' as email_id, od.offence_detail_id,
offence_address,
offence_time,
client_datetime,
offence_type,
vehicle_no,
unique_key,
submit_report,
discard_report,
address_id,
location_id,
complaint_type_code,
complaint_sub_type_code,
approved,
rejected,
sent_to_authority,
lat,
lng,
od.appuser_id,
offence_image_id,
offence_video_path,
offence_video_name,
offence_image_name,
offence_image_path,
creation_datetime FROM offence_details od
INNER JOIN offence_image oi ON od.offence_detail_id  = oi.offence_detail_id
where unique_key = inuniquekey;

END$$
DELIMITER ;
call get_Emailid_Using_Postal_Code(1433225068750)
