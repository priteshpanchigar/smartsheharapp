
DROP PROCEDURE IF EXISTS add_letterhead_details;
DELIMITER$$
CREATE  PROCEDURE `add_letterhead_details`(IN inletterimagename varchar(255), IN inletterdatetime datetime,  IN inoffencedetailid INT, IN inletterimagepath VARCHAR(255))
BEGIN

UPDATE  offence_image SET letter_datetime = inletterdatetime,
letter_image_name = inletterimagename,
letter_image_path  =inletterimagepath,
letter_submited = 1
WHERE offence_detail_id = inoffencedetailid ;


END$$
DELIMITER ;