DROP PROCEDURE IF EXISTS update_approved_offence;
DELIMITER$$
CREATE  PROCEDURE `update_approved_offence`(IN intobesenttoauthority INT, IN inrejected INT, 
IN inoffencedetailid INT,IN inreasoncode  VARCHAR(255), IN inclientdatetime datetime,
IN inotherreasoncodecomment  VARCHAR(300))
BEGIN

UPDATE offence_details
SET to_be_sent_to_authority = intobesenttoauthority, 
rejected = inrejected,
approved_rejected_clientdatetime = inclientdatetime
WHERE offence_detail_id = inoffencedetailid;

IF inrejected =1 THEN
UPDATE offence_details
SET reason_code  = inreasoncode,
other_reason_code_comment = inotherreasoncodecomment
WHERE offence_detail_id = inoffencedetailid;
END IF;


select to_be_sent_to_authority, rejected from offence_details
WHERE offence_detail_id = inoffencedetailid;



END$$
DELIMITER;

call update_approved_offence(1,0,20,"ICC","2015-10-14 10:48:15");