DROP PROCEDURE IF EXISTS offence_video;
DELIMITER$$
CREATE  PROCEDURE `offence_video`(IN inappuserid INT, IN increationdatetime datetime,  
IN inoffencedetailid INT,  IN inoffencevideopath VARCHAR(255), IN inoffencevideoname VARCHAR(255))
BEGIN
	

INSERT INTO offence_image( appuser_id, creation_datetime, offence_detail_id, 
	offence_video_path, offence_video_name)
VALUES (inappuserid, increationdatetime, inoffencedetailid, 
	inoffencevideopath ,inoffencevideoname)
		ON DUPLICATE KEY UPDATE		
		offence_video_path = inoffencevideopath,
		offence_video_name = inoffencevideoname;

SELECT LAST_INSERT_ID() offence_image_id;


END$$
DELIMITER ;