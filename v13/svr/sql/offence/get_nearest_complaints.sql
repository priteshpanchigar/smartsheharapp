DROP PROCEDURE IF EXISTS get_nearest_complaints;
DELIMITER$$
CREATE  PROCEDURE `get_nearest_complaints`(IN inlat DOUBLE, IN inlon DOUBLE)
BEGIN


SELECT offence_detail_id, offence_address, offence_time, client_datetime, offence_type, vehicle_no, unique_key, submit_report, 
discard_report, address_id, location_id, ward, complaint_type_code, complaint_sub_type_code, colour, android_colour, municipal_department, 
approved, rejected, approved_rejected_clientdatetime, sent_to_authority_datetime, resolved_unresolved_datetime, other_reason_code_comment, 
resolved, unresolved, unresolved_comment, reason_code, sent_to_authority, lat, lng, appuser_id, email, phoneno, 
username, fullname, age, sex, offence_image_id, offence_image_name, offence_image_path, creation_datetime,
(SELECT group_name FROM groupdetails 
	WHERE group_id = vw_offence.group_id) group_name FROM vw_offence
WHERE rejected=0 AND offence_image_id IN
			(SELECT  MIN(offence_image_id) offence_image_id from 
			offence_image
			GROUP BY offence_detail_id)
			
ORDER BY  getDistanceBetweenPoints( inlat,	inlon, lat, lng)
		LIMIT 15;


END$$
DELIMITER ;

CALL get_nearest_complaints(19.104, 72.84);
 