DROP PROCEDURE IF EXISTS appuserid_show_all_offences;
CREATE  PROCEDURE `appuserid_show_all_offences`(IN inuniquekey varchar(255))
BEGIN
SELECT offence_detail_id, offence_address, offence_time, client_datetime, submit_report, approved, 
rejected,  sent_to_authority, resolved, unresolved, sent_to_authority_datetime, 
resolved_unresolved_datetime, other_reason_code_comment, unresolved_comment,
offence_type, vehicle_no, unique_key, complaint_sub_type_code, complaint_type_code, appuser_id,
offence_image_id, ward, GROUP_CONCAT(offence_image_name) offence_image_name, creation_datetime,
offence_image_path as offence_image_path, letter_datetime, letter_image_name, letter_image_path,
letter_submited FROM vw_offence
WHERE unique_key = inuniquekey  AND offence_image_name != ""
GROUP BY offence_detail_id;

END;


CALL appuserid_show_all_offences('1442314037841');