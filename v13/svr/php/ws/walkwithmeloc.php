<?php
// This PHP script takes the user email id and the random sql string generated by the system
// as the parameters and checks for the walkwithmepermissions record id in walkwithmelocadd table
// for that record it takes the lat, lon, accuracy, clientdatetime, and the walkwithmepermission
// tables record id and plots the users latest position on the map. If it is not able to get
// walkwithmepermission tables record id , then it can not get the lat and lon and the clien
// datetime and plot the users latest position. in that case it will display an error message
// Friends location can not be found, ask him to turn on his GPS.
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
$php_name = 'walkWithMeLoc';
include("../mobile_common_data_sar.php");
include("../dbconn_sar_apk.php");	
date_default_timezone_set('Asia/Calcutta');
$user_email_id = $_REQUEST['user_email'];
$id = $_REQUEST['id'];
$resultrows = array();
$sql = "call ws_v25_getwalkwithmelocation('". $user_email_id . "','" . $id . "')";
if ($verbose != 'N') {
	echo '<br>'. $sql . '<br>';
}if ($mysqli) {		
	if ($result = $mysqli->query($sql)) {
		while ($row = $result->fetch_assoc()) {
			echo json_encode($row);
			break;
		 }
		
		$result->free();	// free result set
		}
		$mysqli->close();		// close connection
		
}
