<?php 
	//This PHP Script takes image, filename, email id's , messages to be sent to these email ids
	//Geo position of the user, his/her name, address, imei,phone no. year , month and date
	// as input parameters, does validation for the email id's , year month and date, composes
	// the message, and calls sendmail function to send the message to the email id's which
	// are the input parameters along with the image.
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
$php_name = 'postImage';
	include ('../ws_sendmailwithattachment.php');
	include ('../dbconn_sar_apk.php');
	include ('../mobile_common_data_sar.php');
	
	date_default_timezone_set('Asia/Calcutta');
	$base = $_REQUEST['image'];
	
	$filename = isset($_REQUEST['filename']) ? $_REQUEST['filename'] : 'NULL';
	$cc1_email = empty($_REQUEST['cc1']) || !isset($_REQUEST['cc1']) ? 'NULL' :  $_REQUEST['cc1'] ;
	$cc1_message = empty($_REQUEST['cc1msg']) || !isset($_REQUEST['cc1msg']) ? 'NULL' : "'" . $_REQUEST['cc1msg'] . "'" ;
	
	$cc2_email = empty($_REQUEST['cc2']) || !isset($_REQUEST['cc2']) ? '' :  $_REQUEST['cc2']  ;
	$cc2_message = empty($_REQUEST['cc2msg']) || !isset($_REQUEST['cc2msg']) ? 'NULL' : "'" . $_REQUEST['cc2msg'] . "'" ;
	
	$cc3_email = empty($_REQUEST['cc3']) || !isset($_REQUEST['cc3']) ? '' :$_REQUEST['cc3'];
	$cc3_message = empty($_REQUEST['cc3msg']) || !isset($_REQUEST['cc3msg']) ? 'NULL' : "'" . $_REQUEST['cc3msg'] . "'" ;
	
	$from = "SmartShehar Safety Shield Android App <photos@smartshehar.com>";
	
	$lat = isset($_REQUEST['lat']) ? $_REQUEST['lat'] : 'NULL';
	$lon = isset($_REQUEST['lng']) ? $_REQUEST['lng'] : 'NULL';
	
	$name = $_REQUEST['name'];
	$phone = isset($_REQUEST['phone']) ? "\"" . $_REQUEST['phone'] . "\"" : 'NULL';
	
	$cc = empty($_REQUEST['cc']) || !isset($_REQUEST['cc']) ? 'NULL' :  $_REQUEST['cc'] ;
	
	
	$year = empty($_REQUEST['y']) || !isset($_REQUEST['y']) ? 'NULL' :  $_REQUEST['y'] ;
	$month = empty($_REQUEST['o']) || !isset($_REQUEST['o']) ? 'NULL' :  $_REQUEST['o'] ;
	$day = empty($_REQUEST['d']) || !isset($_REQUEST['d']) ? 'NULL' :  $_REQUEST['d'] ;
	$hour = empty($_REQUEST['h']) || !isset($_REQUEST['h']) ? 'NULL' :  $_REQUEST['h'] ;
	$minutes = empty($_REQUEST['m']) || !isset($_REQUEST['m']) ? 'NULL' :  $_REQUEST['m'] ;
	$seconds = empty($_REQUEST['s']) || !isset($_REQUEST['s']) ? 'NULL' :  $_REQUEST['s'] ;
	
	
	
	$monthName = date("F", mktime(0, 0, 0, $month, 10));
	$min = str_pad($minutes, 2, 0, STR_PAD_LEFT); 
	$sec = str_pad($seconds, 2, 0, STR_PAD_LEFT);
	$smessage = '';
	$tmessage = '';
	
	echo $base;
	// base64 encoded utf-8 string
	$binary=base64_decode($base);
		
	if (strlen($base) == 0){
		$base = "N/A";
	}
	
	if (strlen($filename) == 0){
		$filename = "pritesgpanchigar@gmail.com_1356498860622";
	}

	//$theFile = base64_decode($image_data);
	$dir = "./photos/";
	$file = fopen( $dir .$filename, 'wb');
	fwrite($file, $binary);
	fclose($file);
	
	$cc1_email=($cc1_email==null)?'smartsheharcitizensphoto@hotmail.com':$cc1_email;
	$cc2_email=($cc2_email==null)?'':$cc2_email;
	$cc3_email=($cc3_email==null)?'':$cc3_email;
	$name=($name=='')?$cc:$name;
	
	if($cc1_email != '') {
		$to = $cc1_email;
	}
	if(($cc1_email != '') && ($cc2_email != '')) {
		$to = $cc1_email . "," . $cc2_email;
	}	
	if(($cc1_email != '') && ($cc2_email != '') && ($cc3_email != '')) {
		$to = $cc1_email . "," . $cc2_email . "," . $cc3_email;
	}
	
	if($to == "smartsheharcitizensphoto@hotmail.com") {
		$cc = $cc;
	}
	if($to != "smartsheharcitizensphoto@hotmail.com") {
		$cc = $cc; 
	}
	$lat=($lat==null)?'19.10':$lat;
	$lon=($lon==null)?'72.85':$lon;
	$address=($address=='')?'Vile Parle':$address;
	$phone=($phone==null)?'':$phone;
	$day=($day==null)?01:$day;
	$month=($month==null)?01:$month;
	$month=($month==00)?01:$month;
	$year=($year==null)?2013:$year;
	//
	if (strlen($cc) == 0){
		$cc = "smartsheharcitizensphoto@hotmail.com";
	} 
	if (strlen($cc) > 0) {
		$cc = $cc; 
	}
	 
	if (strlen($from) == 0){
		$from = "smartsheharcitizensphoto@hotmail.com";
	}
		
	$day=($day==null)?01:$day;
	$month=($month==null)?01:$month;
	$year=($year==null)?2013:$year;
	$tmpdate = $year . "-" . $month . "-" . $day . " " . $hour .
				":" . $min . ":" . $sec ;
	$svrmilisec = microtime(date('y-m-d h:m:s'));	
	$svrdate = 	date('y-m-d h:m:s', $svrmilisec);
	$fromName = (strlen($name) > 0) ? " from " . $name : "";
	$subject = "Emergency Photo " . $fromName . " via SmartShehar Safety Shield app";
	$headers = "From: SmartShehar Safety Shield app <photos@smartshehar.com>\r\n" . 
	"Reply-To: photos@smartshehar.com";
	$headers_cc = $cc . ", smartsheharcitizensphoto@hotmail.com"; 
	
	
	if(strlen($name == 0)) {
		$fmessage = "You Have Received An Emergency Photo ";
		$fmessage .= $fromName . " Via Smartshehar Safety Shield App " . "\n" ;
	}
	if(strlen($name > 0)) {
		$fmessage =  "you Have Received An Emergency Photo ";
		$fmessage .= $fromname . " Via Smartshehar Safety Shield App " . " (" . $fromname . ") / " . 
					$cc . "\n"; 
	}
	
	if($phone != '') {	
		$fmessage .= "Phone : " . $phone . "\n";
	}	
	
	if($cc1_email != 'smartsheharcitizensphoto@hotmail.com') {
		if($cc1_message != '') {
			if($name != '') {
				$f1message = "Emergency Message From (" . $name . ") : " . $cc1_message . 
				"\n" ;
			}
			if($name == '')  {
				$f1message = "Emergency Message From (" . $cc . ") : " . $cc1_message . 
				"\n" ;
			}
		}		
	}
	if($cc2_email != '') {
		if($cc2_message != '') {
			if($name != '') {
				$f2message = "Emergency Message From (" . $name . ") : " . $cc2_message . 
				"\n";
			}
			if($name == '') {
				$f2message = "Emergency Message From (" . $cc . ") : " . $cc2_message . 
				"\n";
			}
		}			
	}
	if($cc3_email != '') {
		if($cc3_message != '') {
			if($name != '') {
				$f3message = "Emergency Message From (" . $name . ") : " . $cc3_message . 
				"\n";
			}
			if($name == '') {
				$f3message = "Emergency Message From (" . $cc . ") : " . $cc3_message . 
				"\n";
			}
		}		
	}
	
	$smessage .= "\nPicture taken at: " . $address . "(" . $lat . "," . $lon . ") \n";
	$smessage .= "Date : " . $day . ' ' . substr($monthName,0,3) . ', ' . $year . '  ' .
	"Time : " . $hour . ':' . $min . ':' . $sec . "\n" ;
	$smessage .= "See the location on a map - \n";
	$smessage .=	"http://maps.google.com/maps?z=16&t=m&q=loc:" . $lat . "+" . $lon . "\n";
	$tmessage .= "\nDownload the SmartShehar app - ";
	$tmessage .=	"\nhttps://play.google.com/store/apps/details?id=com." . 
	"smartshehar.dashboard.app \n"; 
		
	
if ( $mysqli ) {
	
	
	$sql = "call add_emergency_details(" . $lat . "," . $lon . "," .
	$address . "," . $filename . "," . $phone . "," . $imei . "," . $clientdatetime . ",'" . 
	$name . "','" . $to . "','" . $from . "','" . $cc . "')";
	
	
	
	if ($verbose != 'N') {
		echo $sql;
	}
	if ($result = $mysqli->query($sql)) {
		if ($verbose != 'N') {
			echo "<br>sql result: ";
			var_dump($result);
			echo "<br>";
		}
	}
	if ($result && is_object($result))  {
		$rowcount=mysqli_num_rows($result);
		if ($verbose != 'N') {
			echo "rowcount: " .$rowcount . "<br>";
		}
		if ($rowcount > 0) {
			while ($row = $result->fetch_assoc()) {
				$resultrows[] = $row;
				//echo json_encode($row);
				break;
				
			}
		}
		$result->free();
	}$mysqli->close();
}
	if ($cc1_email != '') {
		$message = $fmessage . $f1message . $smessage . $tmessage;
		echo  'cc1 ' . $cc1_email;
		echo $f1message;
		sendMail($cc1_email, $from, $headers_cc, $subject, $message, $dir . $filename);
	}
	
	if ($cc2_email != '') {
		$message = $fmessage .  $f2message . $smessage . $tmessage;
		echo ' cc2 ' . $cc2_email;
		echo $f2message;
		sendMail($cc2_email, $from, $headers_cc, $subject, $message, $dir . $filename);
	}

	if ($cc3_email != '') {
		$message = $fmessage . $f3message . $smessage . $tmessage;
		echo ' cc3 ' . $cc3_email;
		echo $f3message;
		sendMail($cc3_email, $from, $headers_cc, $subject, $message, $dir . $filename);
	}	
		
?>