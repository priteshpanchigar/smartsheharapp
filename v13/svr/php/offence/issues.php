<?php 
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
$php_name = 'issues';
include("../mobile_common_data_sar.php");
include("../dbconn_sar_apk.php");
if ($mysqli) {
	$issueRows = array();
	
	$sql = "call issues()";  
if ($verbose != 'N') {
		echo $sql . '<br>';
	}
	if ($result = $mysqli->query($sql)) {
		/* fetch associative array */
		while ($row = $result->fetch_assoc()) {
			$issueRows[] = $row;			
		}		
		$result->free();	// free result set
	}
	$mysqli->close();		// close connection
	//	$data = array('Rides' => $ridesrows);
	if (count($issueRows) > 0) {
		echo json_encode($issueRows);
	} else {
		echo "-1";// sql failed
	}
}