<?php
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
$php_name = 'getMygroupComplaints';
include("../dbconn_sar_apk.php"); 
include("../mobile_common_data_sar.php");
if ($mysqli) {
	$getmygroupcomplaintsRows = array();
	$groupid = isset($_REQUEST['groupid']) ? $_REQUEST['groupid'] : 'NULL';
	
	$sql = " call get_mygroup_complaints(".$groupid.")";
	if ($verbose != 'N') {
		echo $sql . '<br>';
	}	
	if ($result = $mysqli->query($sql)) {
		while ($row = $result->fetch_assoc()) {
			$getmygroupcomplaintsRows[] = $row;			
		}		
		$result->free();	// free result set
	}
	$mysqli->close();		// close connection
	
	
		echo json_encode($getmygroupcomplaintsRows);
	 
}else {
		echo "-1";
	}