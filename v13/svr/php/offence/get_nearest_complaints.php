<?php
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
$php_name = 'getNearestComplaints';
include("../dbconn_sar_apk.php"); 
include("../mobile_common_data_sar.php");
if ($mysqli) {
	$getnearestcomplaintsRows = array();
$lat = isset($_REQUEST['latitude']) ? "\"" . $_REQUEST['latitude'] . "\"" : 'NULL';

$lng = isset($_REQUEST['longitude']) ? "\"" . $_REQUEST['longitude'] . "\"" : 'NULL';
	$sql = " call get_nearest_complaints(". $lat . "," . $lng .")";
	if ($verbose != 'N') {
		echo $sql . '<br>';
	}	
	if ($result = $mysqli->query($sql)) {
		while ($row = $result->fetch_assoc()) {
			$getnearestcomplaintsRows[] = $row;			
		}		
		$result->free();	// free result set
	}
	$mysqli->close();		// close connection
	
	
		echo json_encode($getnearestcomplaintsRows);
	 
}else {
		echo "-1";
	}