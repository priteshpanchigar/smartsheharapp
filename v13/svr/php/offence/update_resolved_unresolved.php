<?php
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
$php_name = 'updateresolvedOffence';
include("../dbconn_sar_apk.php"); 
include("../mobile_common_data_sar.php");
$errors = array();
if ( $mysqli ) {
		
	$resolved = isset($_REQUEST['resolved']) ? $_REQUEST['resolved'] : 'NULL';
	
	$unresolved = isset($_REQUEST['unresolved']) ? $_REQUEST['unresolved'] : 'NULL';
	
	$offencedetailid = isset($_REQUEST['offencedetailid']) ? $_REQUEST['offencedetailid'] : 'NULL';
	
	$unresolvedcomment = isset($_REQUEST['unresolvedcomment']) ? "\"" . $_REQUEST['unresolvedcomment'] . "\"" : 'NULL';
	
	$clientdatetime = isset($_REQUEST['clientdatetime']) ? "\"" . $_REQUEST['clientdatetime'] . "\"" : 'NULL';	
	
	$sql = "call update_resolved_unresolved(" .$resolved .  ",".  $unresolved .  ",". $offencedetailid . ",". $unresolvedcomment . ",". $clientdatetime .")";
	if ($verbose != 'N') {
		echo $sql;
	}
	if ($result = $mysqli->query($sql)) {	
		while ($row = $result->fetch_assoc()) {
			echo json_encode($row);
			break;
		} 
		$mysqli->close();
	}		else {
		echo "-1"; // something went wrong, probably sql failed
	}
} else {
	echo "-2"; // "Connection to db failed";
}