<?php 
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
$php_name = 'offenceImage';
include("../dbconn_sar_apk.php");
include("../mobile_common_data_sar.php"); 
$errors = array();
$unregistered = false;
$offenceimagefilename=NULL;
$imsrc = '';

if(isset($_REQUEST['offenceimagefilename']) &&!empty( $_REQUEST['offenceimagefilename'])){
	$offenceimagefilename = $_REQUEST['offenceimagefilename'];
	if ($verbose != 'N') {
		echo '<br>offenceimagefilename: ' .$_REQUEST['offenceimagefilename'];
	}	
	$imsrc = base64_decode($_REQUEST['offenceimage']);
	$fp = fopen('images/' . $offenceimagefilename, 'w');
	
	fwrite($fp, $imsrc);
	if(fclose($fp)){
		echo "Image uploaded";
	}else{
		echo "Error uploading image";
	}
	
} else {
	$offenceimagefilename= 'NULL';
}

if ( $mysqli ) {
	
	
	
	$offenceimagefilename = empty($_REQUEST['offenceimagefilename']) || 
		!isset($_REQUEST['offenceimagefilename']) ? 'NULL' : 
		"'" . $_REQUEST['offenceimagefilename'] . "'" ;
	$offenceimagefilepath= "'/images/'" ;
		
		
	$offencedetailid = isset($_REQUEST['offencedetailid']) ? $_REQUEST['offencedetailid'] : 'NULL';	
		
	$creationdatetime = isset($_REQUEST['creationdatetime']) ? "\"" . $_REQUEST['creationdatetime'] . "\"" : 'NULL';	
	
	
	$sql = "call offence_image(" .$offenceimagefilename .   ", " . $appuserid . 
		", " . $creationdatetime . ", " . $offencedetailid . ", " . $offenceimagefilepath .")";
	if ($verbose != 'N') {
		echo '<br>sql:<br>' . $sql;
	}	
	if ($result = $mysqli->query($sql)) {	
		if ($result && is_object($result))  {
			while ($row = $result->fetch_assoc()) {
				echo json_encode($row);
				break;
			} 
		}
		$mysqli->close();
	}
	
}else {
		echo "-1";
	}