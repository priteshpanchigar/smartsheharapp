<?php
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
$php_name = 'issueEmail';
include("../dbconn_sar_apk.php");
include("../mobile_common_data_sar.php");
include("../sendmail.php");
$memberemail = "";
$name = "";
$message = isset($_REQUEST['message']) ? $_REQUEST['message']  : 'NULL';
$email = isset($_REQUEST['email']) ? $_REQUEST['email']  : 'NULL';
$offenceimagefilename = isset($_REQUEST['offenceimagefilename']) ? $_REQUEST['offenceimagefilename']  : 'NULL';
$subject = isset($_REQUEST['subject']) ?  $_REQUEST['subject'] : 'NULL';
$uniquekey= isset($_REQUEST['uniquekey']) ? "'" . $_REQUEST['uniquekey'] . "'" : 'NULL';
$cc = $email ." , rtocomplaint@smartshehar.com";
$bcc =  "rtocomplaint@smartshehar.com";
if($mysqli)
{	$sql = "call get_Emailid_Using_Postal_Code(" .$uniquekey . ")";
	if ($verbose != 'N') {
		echo '<br> sql ' . $sql . '<br>';
		
	}
	$result = $mysqli->query($sql);
	if(is_object($result)) {	
		while ($row = $result->fetch_assoc()) {
			$memberemail = $row["email_id"];
			
			$subject = "Reporting an Issue  " . $row['offence_type'] . " at " . $row['offence_address'] ;
			$message = "Dear Sir/Madam, <br>I am writing to you to report an Issue (" . $row['offence_type'] .
			") at " . $row['offence_address']. " at " . $row['offence_time'] .
			".<br><br>Photo of the issue is attached. <br>" .
			"<br>We hereby request you to take requisite action. <br>" .
			"<br>Thank You, <br>" .
			"A Concerned Citizen ";
			
			
			break;
		}
		$mysqli->close();
	}
}else{
	echo "connection failed";
}
if (!empty($memberemail)) {
	$arr = explode(",", $memberemail);
	foreach($arr as $emailid ) {
		sendMail($emailid, $name, $cc, $subject, $message, $offenceimagefilename,$bcc);
	}
}