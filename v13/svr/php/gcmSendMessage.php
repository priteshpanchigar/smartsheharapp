<?php
	function gcmSendMessage($resultrows, $jmessage) {
		// Set POST variables
         $url = 'https://android.googleapis.com/gcm/send';
		
		$message = array("Notice" => $jmessage);
			$fields = array(
				'registration_ids' => $resultrows,
				'data' => $message,
			);
   
			$headers = array(
				'Authorization: key=AIzaSyA-ANS_HVj27gtksF95_0eTqRmaV2ye6RA',
			 
				'Content-Type: application/json'
			);
			// Open connection
			$ch = curl_init();
   
			// Set the url, number of POST vars, POST data
			curl_setopt($ch, CURLOPT_URL, $url);
   
			curl_setopt($ch, CURLOPT_POST, true);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
   
			// Disabling SSL Certificate support temporarly
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
   
			curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
   
			// Execute post
			$result = curl_exec($ch);
			if ($result === FALSE) {
				die('Curl failed: ' . curl_error($ch));
			}
   
			// Close connection
			curl_close($ch);
			echo $result;

		echo json_encode($resultrows);
	
	}
?>