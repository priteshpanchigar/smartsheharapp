// rideshare.js
// Modified By Sanjay 21-09-2013 13:42 PM.
/*global $, jQuery, pickCity, setCity, cityBlock, getLocation, jsonCityFareParameter, document, touchScroll, select, jqAutocomplete, prompt, showLocation, console, displayLines, updateCurrentTime, google, window, setCookie, localStorage, successCallBack, event, alert, gup, getCookie, setCookie, getFareLondon, distanceLatLon, createCityList, getRides, checkDeviceReady, phpScriptsPath, putupdateRideStatus, PHP_SCRIPTS_PATH, driverLocation, shareMyVehicle, joinRide, initModule, displayFare, history, getFare, joinRide, afterPlotDirections, insertrides, loadScript */
var COOKIE_DAYS = 365, CITYMUMBAI = "MUMBAI", CITYLONDON = "LONDON",
	DEFAULTZOOM = 15, MAXCITYRADIUS = 50, PUBLICVEHICLES = "Public&nbsp",
	PRIVATEVEHICLES = "Private&nbsp", module = '', SHAREMODULE = 'shareMyPrivateVehicle',
	ESTIMATEMODULE = 'estimate', JOINMODULE = 'join', MINZOOMLEVEL = 8,
	MAXLENGTH = 5, DROPPED_PIN = 'Dropped Pin',
	TWOWHEELER = '2&nbspWheeler', FOURWHEELER = '4&nbspWheeler',
	CITYCENTER = 'City Center', MYLOCATION = 'My Location',
	MY_VEHICLE_PRIVATE = 'myVehicle',
	FROMFIELD = 'from', TOFIELD = 'to', estimate = '',
	rideId, vehlType = '', vehicleCapacity = 0,
	myPos = null, lastPos, geocoder, map, html = '', oFare,
	goTrip, city, rideActive = false, vehicle,
	sFlag = true, oCity = {}, aoCity = [], joinFlag, aoTrip = [],
	cityBounds = null, indx = 0, ctr = 0, autoRefreshLocation = true,
	isMyVehiclePrivate = false, isStartShareRide = false;
var directionsService = new google.maps.DirectionsService();
var directionsDisplay = new google.maps.DirectionsRenderer();
var fromMarker = new google.maps.Marker(),
	toMarker = new google.maps.Marker(),
	dMarker = new google.maps.Marker();

function updateRideStatus(clRd, lastId) {
	putupdateRideStatus(clRd, lastId, function () {
		console.log("Last Enterd Ride Is Closed And Updated");
	});
}
function getPassengerLocation(rideid, successCallBack) {
	var passengersurl = PHP_SCRIPTS_PATH + 'getpassengerdetails.php?rideid=' + rideid;
	$.ajax({
		type: 'GET',
		url: passengersurl,
	    dataType: 'json',
	    success: function (data) {
	        successCallBack(data);
	    },
	    error: function (xhr, textStatus, errorThrown) {
	        var err = textStatus + ', ' + errorThrown;
	    },
		async: false
	});
} // getPassengers
function updatePassengerLocation(rideid) {
	getPassengerLocation(rideid, function (aoPassengersLoc) {
		var len = aoPassengersLoc.length, row, i, passengerMarkers = [],
			pointMarker = [];//store marker in array
		for (i = 0; i < passengerMarkers.length; i = i + 1) {
			passengerMarkers[i].setMap(null);
		}
		for (i = 0; i < len; i = i + 1) {
			row = aoPassengersLoc[i];
			pointMarker[i] = new google.maps.Marker({
				position: aoPassengersLoc[i],
				map: map,
				// icon: pointMarkerImage[i],
				title: "Passenger Location"
			});
			pointMarker[i].setMap(map);
			passengerMarkers.push(pointMarker[i]);
		}
	});
}
function closeRide() {
	$.blockUI({ message: $('#closeRd'), css: { width: '275px' } });
	$('#yes').click(function () {
            // update the block message 
		$.blockUI({ message: "<h4>Closing The Ride...</h4>" });
		$.ajax({
			url: 'rideshare.html',
			cache: false,
			complete: function () {
				$.unblockUI();
				var clRd = 1, lastId = parseInt(goTrip.rideid, 10);
				updateRideStatus(clRd, lastId);
			}
		});
	});
	$('#no').click(function () {
		$.unblockUI();
		return false;
	});
	window.setInterval(function () {updatePassengerLocation(parseInt(goTrip.rideid, 10)); }, 3000);
	rideActive = false;
}
function inCity(lat, lon) {
	if (distanceLatLon(lat, lon, oCity.centerLat,
			oCity.centerLon) / 1000 < MAXCITYRADIUS) {
		return true;
	}
	return false;
}
function showLeftPanel() {
	$('#left-panel').toggleClass('show');
	$('#showHidePanel').toggleClass('show');
	if ($('#showHidePanel').val() === 'Hide') {
		$('#showHidePanel').val('Show');
	} else {
		$('#showHidePanel').val('Hide');
	}
}
function addRecentRoutes(aoTrip) {
	var len = aoTrip.length, i, row;
	for (i = (len - 1); i >= 0;  i = i - 1) {
		row = aoTrip[i];
		if (!row) {
			continue;
		}
		aoTrip[i + 1] =  jQuery.extend({}, row);
	}
	if (aoTrip.length >= MAXLENGTH) {
		aoTrip.length = MAXLENGTH;
	}
	aoTrip[0] =  jQuery.extend({}, goTrip);
}
// join a ride that has already been shared
// Show a route using google direction service between two points
function plotDirections(from, to) {
	var totlDist = '', duration = '', fromAddr = '', toAddr = '', delim = '',
		request, myRoute, txtDir = '', i, fromAddressArray = '',
		toAddressArray = '', len, fromAddressStr = '', toAddressStr = '';
	if (!from || !to) {
		return;
	}
	directionsDisplay.setMap(map);
	request = {
		origin: from,
		destination: to,
		travelMode: google.maps.DirectionsTravelMode.DRIVING,
		durationInTraffic: true
	};
	$.blockUI({ message: "<h4>Calculating route ...</h4>" });
	directionsService.route(request, function (response, status) {
		if (status === google.maps.DirectionsStatus.OK) {
			directionsDisplay.setDirections(response);
			myRoute = response.routes[0];
			for (i = 0; i < myRoute.legs[0].steps.length; i = i + 1) {
				txtDir += myRoute.legs[0].steps[i].instructions + "<br />";
			}
			totlDist = myRoute.legs[0].distance.value;
			duration = myRoute.legs[0].duration.text;
			fromAddr = myRoute.legs[0].start_address;
			toAddr = myRoute.legs[0].end_address;
			fromAddressArray = fromAddr.split(", ");
			len = fromAddressArray.length;
			for (i = 0; i < len; i = i + 1) {
				if (fromAddressArray[i] !== oCity.city) {
					fromAddressStr = fromAddressStr + delim + fromAddressArray[i];
				}
				if (fromAddressArray[i] === oCity.city) {
					break;
				}
				delim = ', ';
			}
			delim = '';
			toAddressArray = toAddr.split(", ");
			len = toAddressArray.length;
			for (i = 0; i < len; i = i + 1) {
				if (toAddressArray[i] !== oCity.city) {
					toAddressStr = toAddressStr + delim + toAddressArray[i];
				}
				if (toAddressArray[i] === oCity.city) {
					break;
				}
				delim = ', ';
			}
			var currentDate = new Date(),
				currentTime = currentDate.getHours() + ":"
								+ currentDate.getMinutes() + ":"
								+ currentDate.getSeconds();
			goTrip.triptime = myRoute.legs[0].duration.value; //text;
			goTrip.tripTimeSecs =  myRoute.legs[0].duration.value;
			goTrip.starttime = currentDate.getFullYear() + '-' + (currentDate.getMonth() + 1) + '-' +
								currentDate.getDate() + ' ' + currentTime;// Time;
			goTrip.distance = myRoute.legs[0].distance.value;
			$('#disttime').html((totlDist / 1000).toFixed(2) +
				' Km ' + ' about ' + duration + '<hr>');
			$('#vehiclefare').text(html);
			goTrip.fromAddress = fromAddressStr;
			$('#from').val(fromAddressStr);
			goTrip.toAddress = toAddressStr;
			$('#to').val(toAddressStr);
			afterPlotDirections(oCity, goTrip);
			html = '';
			setCookie(oCity.city + '_' + 'trip' + '_' + indx, JSON.stringify(goTrip), 1);
			console.log(JSON.stringify(goTrip));
			indx = indx + 1;
			$.unblockUI();
		} else {
			$.unblockUI();
		}
	});
	fromMarker.setVisible(false);
	toMarker.setVisible(false);
} // plotDirections

// Place a marker at a from or to pooint 
function placeMarker(lat, lon, isFrom) { // latlng, whether its from or to point
	var latlng = new google.maps.LatLng(lat, lon),
		icn = 'images/green-dot.png';
	if (isFrom) {
		goTrip.fromLat = latlng.lat();
		goTrip.fromLon = latlng.lng();
		fromMarker.setMap(map);
		fromMarker.setIcon(icn);
		fromMarker.setPosition(latlng);
		fromMarker.setVisible(true);
		fromMarker.setTitle('Start - ' + goTrip.fromLat + ', ' + goTrip.fromLon);
	} else {
		goTrip.toLat = latlng.lat();
		goTrip.toLon = latlng.lng();
		icn = 'http://maps.google.com/mapfiles/ms/icons/red-dot.png';
		toMarker.setMap(map);
		toMarker.setIcon(icn);
		toMarker.setPosition(latlng);
		toMarker.setVisible(true);
		toMarker.setTitle('End - ' + goTrip.toLat + ', ' + goTrip.toLon);
	}
	if (goTrip.fromLat && goTrip.toLat) {
		autoRefreshLocation = false;
		plotDirections(new google.maps.LatLng(goTrip.fromLat, goTrip.fromLon),
			new google.maps.LatLng(goTrip.toLat, goTrip.toLon));
	}
} // placeMarker
// clear the from point from the map
function clearFrom() {
	$('#from').val('');
	fromMarker.setVisible(false);
} // clearFrom
// clear the to point from the map
function clearTo() {
	$('#to').val('');
	toMarker.setVisible(false);
} // clearTo

// clear Directions plotted on map
function clearDirections(goTrip) {
	if (!goTrip.fromLat || !goTrip.toLat) {
		directionsDisplay.setMap(null);
	}
	if (goTrip.fromLat) {
		placeMarker(goTrip.fromLat, goTrip.fromLon, true);
	} else {
		clearFrom();
	}
	if (goTrip.toLat) {
		placeMarker(goTrip.toLat, goTrip.toLon, false);
	} else {
		clearTo();
	}
} // clearDirections
function cancelCurrentRoute() {
	goTrip.fromLat = null;
	goTrip.toLat = null;
	$('#ridepanel').hide();
	$('#fare').hide();
	$('#routeList').hide();
	$('#farepanel').show();
	$('#container').show();
	rideActive = false;
}
function clearRoute() {
	goTrip.fromLat = null;
	goTrip.toLat = null;
	clearDirections(goTrip);
	$('#ridepanel').hide();
	$('#fare').hide();
	$('#routeList').hide();
	$('#farepanel').show();
	$('#container').show();
	rideActive = false;
}
function displayVehicleFare() {
	$('#from').val(goTrip.fromAddress);
	$('#to').val(goTrip.toAddress);
	$('#ridepanel').hide();
	$('#farepanel').show();
	$("#clearRoute").bind("click", function () {
		clearRoute();
	});
	rideActive = false;
}
function displayApproval() {
	$('#shareFare').hide();
	$('#approveRide').show();
	var apprHtml = '', apprvBtn;
	apprHtml = 'New Passanger Wants To Share This Ride';
	apprvBtn = apprHtml + '<hr/>' + '<span class="button aqua">' + '<div class="glare" OnClick="">' +
		'</div>' + 'Approve Ride' + '</span>';
	$('#approveMessage').html(apprvBtn);
}
function updtPassenger(goTrip, successCallBack) {
	var	updtridesurl = PHP_SCRIPTS_PATH + 'passenger.php?rides_id=' + parseInt(goTrip.rideid, 10) + '&capacity=' +
		goTrip.VehicleCapacity + '&passlat=' + goTrip.fromLat + '&passlon=' + goTrip.fromLon;
	$.ajax({
	    type: 'GET',
	    url: updtridesurl,
	    dataType: 'json',
	    success: function (data) {
	        successCallBack(data);
	    },
	    error: function (xhr, textStatus, errorThrown) {
	        var err = textStatus + ', ' + errorThrown;
	    },
		async: false
	});
}
function updatePassenger(goTrip) {
	var len = jsonCityFareParameter.length, row, i, vehicleCapacity;
	for (i = 0; i < len; i = i + 1) {
		row = jsonCityFareParameter[i];
		if (oCity.city.toUpperCase() === row.city.toUpperCase()) {
			if (goTrip.VehicleSelected === row.vehicleType) {
				vehicleCapacity = row.vehicleCapacity;
				goTrip.VehicleCapacity = row.vehicleCapacity;
			}
		}
	}
	updtPassenger(goTrip, function () {
		console.log("Updating No.Of Passengers For Current Ride");
	});
} // updatePassenger

function displayCost(vehicle, fare, currency) {
	rideActive = true;
	var contents = goTrip.fromAddress, dcontents = goTrip.toAddress, rideHtml = '', shHtmlBtn = '', noOfPersons = 1,
		vehicleCapacity = 0, len, row, i;
	len = oCity.aoVehicles.length;
	for (i = 0; i < len; i = i + 1) {
		row = oCity.aoVehicles[i];
		if (row.vehicleType === vehicle) {
			goTrip.VehicleSelected = vehicle;
			goTrip.cityFareParameter_Id = row.cityfareparameter_id;
			//goTrip.vehicleCapacity = row.vehiclecapavity;
		}
	}
	$("#shareFrom").html(contents);
	$("#shareTo").html(dcontents);
	$('#farepanel').hide();
	$('#ridepanel').show();
	rideHtml = rideHtml + '<div style="padding: 10px; ">' + 'You are sharing an ' + vehicle + ' at an approximate cost of ' +
		currency + ' ' + fare + '</div>';
	rideHtml = rideHtml + '<div OnClick="displayApproval()">'; //+
	if (vehicle === TWOWHEELER) {
		vehicleCapacity = 2;
	}
	if (vehicle === FOURWHEELER) {
		vehicleCapacity = 5;
	}
	if (vehicle === 'Taxi') {
		vehicleCapacity = 4;
	}
	if (vehicle === 'Auto') {
		vehicleCapacity = 3;
	}
	goTrip.vehicleCapacity = vehicleCapacity;
	goTrip.cost = fare;
	rideHtml = rideHtml + '<div>' + 'Waiting For ' + (vehicleCapacity - noOfPersons)  + ' More Passangers ' + '</div>' + '</div>';
	rideHtml = '<div class="shareText">' + rideHtml + '</div>';
	shHtmlBtn = rideHtml + '<hr/>' + '<span class="button aqua">' +
		'<div class="glare" OnClick="closeRide()">' +
		'</div>' + 'Go' + '</span>' + '<span class="button aqua">' +
		'<div class="glare" style="float: right" OnClick="displayVehicleFare()">' +
		'</div>' + 'Cancel' + '</span>';
	$('#sDistTime').html(shHtmlBtn);
	insertrides(goTrip);
	updatePassenger(goTrip);
}
function insertMyRide(goTrip) {
	driverLocation.showDriverLocation('blue');
	goTrip.cityFareParameter_Id = oCity.aoVehicles[0].cityfareparameter_id;
	goTrip.VehicleSelected = 'MyVehicle';
	var currentDate = new Date(),
		currentTime = currentDate.getHours() + ":"
			+ currentDate.getMinutes() + ":"
			+ currentDate.getSeconds();
	goTrip.starttime =  currentDate.getFullYear() + '-' + (currentDate.getMonth() + 1) + '-' +
								currentDate.getDate() + ' ' + currentTime;
	insertrides(goTrip);
	updatePassenger(goTrip);
}
function goToMain() {
	window.location = 'index.html';
}
function goToSelectRide() {
	window.location = 'waitingrides.html';
}
/*function cancelRide() {
	var closeRd = 0;
	directionsDisplay.setMap(null);
	$('#from').val(goTrip.fromAddress);
	$('#to').val(goTrip.toAddress);
	$('#ridepanel').hide();
	$('#fare').hide();
	$('#farepanel').show();
	updateRideStatus(closeRd, goTrip.rideid);
}*/
function updateMyRide(goTrip) {
	updatePassenger(goTrip);
}
//-----------------------------------------------------------------------
function centerCity(city) {
	var i, row, len = jsonCityFareParameter.length;
	for (i = 0; i < len; i = i + 1) {
		row = jsonCityFareParameter[i];
		if (city === row.city) {
			map.setCenter(new google.maps.LatLng(row.centerLat, row.centerLon));
			goTrip.fromLat = row.centerLat;
			goTrip.fromLon = row.centerLon;
			goTrip.toLat = null;
			goTrip.toLon = null;
			placeMarker(row.centerLat, row.centerLon, true); //fromLatLng
			oCity = row;
			$('#from').val(CITYCENTER);
		}
	}
}
function placeChangedListener(fld) {
	var searchBox, address,
		input = document.getElementById(fld), searchOptions;
	if (oCity) {
		searchOptions = { bounds: cityBounds,
			componentRestrictions: {country: oCity.country}
			};
	}
	searchBox = new google.maps.places.SearchBox(input, searchOptions);

	geocoder = new google.maps.Geocoder();
	google.maps.event.addListener(searchBox, 'places_changed', function () {
		var places = searchBox.getPlaces(), len;
		len = places.length;
		address = document.getElementById(fld).value;
		if (fld === 'from') {
			goTrip.fromAddress = address;
		} else {
			goTrip.toAddress = address;
		}
		geocoder.geocode({ 'address': address}, function (results, status) {
			if (status === google.maps.GeocoderStatus.OK) {
				map.setCenter(results[0].geometry.location);
				if (fld === 'from') {
					goTrip.fromLat = results[0].geometry.location.lat();
					goTrip.fromLon = results[0].geometry.location.lng();
					placeMarker(results[0].geometry.location.lat(), results[0].geometry.location.lng(), true);
				}
				if (fld === 'to') {
					goTrip.toLat = results[0].geometry.location.lat();
					goTrip.toLon = results[0].geometry.location.lng();
					placeMarker(results[0].geometry.location.lat(), results[0].geometry.location.lng(), false);
				}
			} else {
				alert("Geocode was not successful for the following reason: " + status);
			}
		});
		if (goTrip.fromAddress && goTrip.toAddress) {
			autoRefreshLocation = false;
			plotDirections(new google.maps.LatLng(goTrip.fromLat, goTrip.fromLon),
				new google.maps.LatLng(goTrip.toLat, goTrip.toLon));
		}
	});
} // placeChangedListener
function myLocation() {
	getLocation(function (pos) {
		myPos = pos;
		if (!oCity || !autoRefreshLocation) { // City not yet defined so don't auto locate or no auto refresh
			return;
		}
		if (inCity(myPos.coords.latitude, myPos.coords.longitude)) {
			goTrip.fromLat = myPos.coords.latitude;
			goTrip.fromLon = myPos.coords.longitude;
			$('#from').val(MYLOCATION);
		} else {
			goTrip.fromLat = oCity.centerLat;
			goTrip.fromLon = oCity.centerLon;
			$('#from').val(CITYCENTER);
		}
		// Don't replot if already plotted		
		if (lastPos) {
			if (parseInt(goTrip.fromLat * 1000, 10) ===
					parseInt(lastPos.coords.latitude * 1000, 10) &&
					parseInt(goTrip.fromLon * 1000, 10) ===
					parseInt(lastPos.coords.longitude * 1000, 10)
					) {
				return;
			}
		}
		lastPos = myPos;
		placeMarker(goTrip.fromLat, goTrip.fromLon, true); //fromLatLng
		map.setCenter(new google.maps.LatLng(goTrip.fromLat, goTrip.fromLon));
	}, function (err) {
	});
}
function getRideDetails(rideId) {
	var frmadr, frmlat, frmlon, tolat, tolon, toadr, starttm, triptm, tripdist, waitride, noofpers, ridecreatm;
	getRides(rideId, function (aoRides) {
		var len = aoRides.length, row, i;
		for (i = 0; i < len; i = i + 1) {
			row = aoRides[i];
			frmadr = row.fromaddress;
			frmlat = row.fromlat;
			frmlon = row.fromlon;
			toadr = row.toaddress;
			tolat = row.tolat;
			tolon = row.tolon;
			starttm = row.starttime;
			triptm = row.triptime;
			tripdist = row.tripdistance;
			waitride = row.waitingride;
			noofpers = row.noofpassenger;
			ridecreatm = row.ridecreatm;
		}
	});
	goTrip.fromAddress = frmadr;
	goTrip.fromLat = frmlat;
	goTrip.fromLon = frmlon;
	goTrip.toAddress = toadr;
	goTrip.toLat = tolat;
	goTrip.toLon = tolon;
	autoRefreshLocation = false;
	plotDirections(new google.maps.LatLng(frmlat, frmlon),
			new google.maps.LatLng(tolat, tolon));
}
// Set the city, limits, etc.
function setCity(city, successCallBack, failuerCallBack) {
	var i, row, len = jsonCityFareParameter.length;
	setCookie("city", city, COOKIE_DAYS);
	$.getScript("js/getFare" + city + ".js")
			.done(function (script, textStatus) {
			successCallBack();
			console.log(textStatus);
		})
			.fail(function (jqxhr, settings, exception) {
			$("showMsg").text("Triggered ajaxError handler.");
		});
	$('#blockPage').hide();
	$('#selectCity').hide();
	$('#entirePage').fadeTo('slow', 1);
	for (i = 0; i < len; i = i + 1) {
		row = jsonCityFareParameter[i];
		if (city.toUpperCase() === row.city) {
			oCity = row;
			cityBounds = new google.maps.LatLngBounds(
				new google.maps.LatLng(oCity.cityBottomLat, oCity.cityBottomLon),
				new google.maps.LatLng(oCity.cityTopLat, oCity.cityTopLon)
			);
		}
	}
	oCity.aoVehicles = [];
	for (i = 0; i < len; i = i + 1) {
		row = jsonCityFareParameter[i];
		if (city.toUpperCase() === row.city) {
			if (!oCity.aoVehicles) {
				oCity.aoVehicles = [];
			}
			oCity.aoVehicles.push(row);
		}
	}
	oCity.cityBounds = cityBounds;
	map.setZoom(DEFAULTZOOM);
	if (myPos !== null && inCity(myPos.coords.latitude, myPos.coords.longitude)) {
		map.setCenter(new google.maps.LatLng(myPos.coords.latitude, myPos.coords.longitude));
	} else {
		map.setCenter(new google.maps.LatLng(oCity.centerLat, oCity.centerLon));
	}
	// Change listener to prompt for this city and country
	placeChangedListener(FROMFIELD);
	placeChangedListener(TOFIELD);
	lastPos = null;
	rideId = parseInt(gup('rides_id'), 10);
	estimate = gup('mode');
	if (rideId) {
		getRideDetails(rideId);
	} else {
		myLocation();
	}
}
function getCity(fSelectCity) {
	if (!fSelectCity) {
		city = getCookie("city");
		if (city) {
			return city;
		}
	}
	createCityList();
	$.blockUI({ message: $('#selectcity'), css: { width: '275px' } });
	$('#allcities').click(function () {
	// update the block message 
		$.blockUI({ message: "<h4>Selecting city ...</h4>" });
		$.ajax({
			url: 'rideshare.html',
			cache: false,
			complete: function () {
				// unblock when remote call returns 
				$.unblockUI();
			}
		});
	});
	city = getCookie("city");
	return city;
}
function initMap() {
	map = new google.maps.Map(document.getElementById('map-canvas'), {
		zoom: DEFAULTZOOM,
		panControl: false,
		mapTypeId: google.maps.MapTypeId.ROADMAP,
		streetViewControl: false,
	    mapTypeControl: true,
		mapTypeControlOptions: {
			style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
			position: google.maps.ControlPosition.BOTTOM_CENTER,
			mapTypeIds: [google.maps.MapTypeId.ROADMAP, 'no_poi']
		},
		zoomControl: true,
		zoomControlOptions: {
			style: google.maps.ZoomControlStyle.SMALL,
			position: google.maps.ControlPosition.RIGHT_CENTER
		},
		scaleControl: false,
		scaleControlOptions: {
			position: google.maps.ControlPosition.TOP_LEFT
		}
	});
	map.setZoom(DEFAULTZOOM);
	google.maps.event.addListenerOnce(map, 'idle', function () {
	});
} // initMap()
function displayFareCost() {
	var cityname = getCookie("city");
	if (cityname) {
		window.location = 'farecalculation.html?city=' + cityname; //oCity.city;
	}
}
function selectCity(val) {
	var city = val;
	goTrip = {};
	oCity = {};
	aoTrip = [];
	$('#from').val('');
	$('#to').val('');
	$('#fare').hide();
	$('#routeList').hide();
	$('#ridepanel').hide();
	$('#farepanel').show();
	$('#container').show();
	setCookie("city", city, 365);
	setCity(city);
	rideActive = false;
} // selectCity
function initialize() { // sLat, sLon, dLat, dLon,
	var defaultBounds, len, currentActiveRide = getCookie("activeTrip"),
		tripCookieName, tripCookieValue, oTrip,
		latLonStr = '', addressStr = '',
		geocoder = new google.maps.Geocoder(),
		input, jsFile;
	$.blockUI({ message: "<h4>Getting Maps ...</h4>" });
	initMap();
	//initModule();
	rideId = gup('rides_id');
	city = getCookie("city");
	if (city === 'undefined' || city === '') {
		city = 'Mumbai';
	}
	if (city) {
		setCity(city, function () {
			if (module === SHAREMODULE) {
				jsFile = "js/sharemyprivatevehicle.js";
				loadScript(jsFile);
			} else if (module === ESTIMATEMODULE) {
				jsFile = "js/estimatedfare.js";
				loadScript(jsFile);
			} else if (module ===  JOINMODULE) {
				jsFile = "js/joinmyprivatevehicle.js";
				loadScript(jsFile);
			}
		});
	}
	if (rideId === '') {
		joinFlag = false;
	} else {
		joinFlag = true;
	}
	if (rideId) {
		getRideDetails(rideId);
    }
	input = document.getElementById(FROMFIELD);
	placeChangedListener(FROMFIELD);
	placeChangedListener(TOFIELD);
	$.unblockUI();
	google.maps.event.addListener(map, 'click', function (event) {
		if (!goTrip.fromLat) {
			$('#from').val(DROPPED_PIN);
		} else {
			$('#to').val(DROPPED_PIN);
		}
		latLonStr = event.latLng;
		var lat = latLonStr.lat(), lon = latLonStr.lng(), latlon,
			latlngStr, lng, latlng;
		placeMarker(lat, lon, !goTrip.fromLat); // event.latLng
	});
	// Listen for the dragend event
	google.maps.event.addListener(map, 'dragend', function () {
		if (cityBounds.contains(map.getCenter())) {
			return;
		}
		// We're out of bounds - Move the map back within the bounds
		var c = map.getCenter(),
			x = c.lng(),
			y = c.lat(),
			maxX = cityBounds.getNorthEast().lng(),
			maxY = cityBounds.getNorthEast().lat(),
			minX = cityBounds.getSouthWest().lng(),
			minY = cityBounds.getSouthWest().lat();

		if (x < minX) {
			x = minX;
		}
		if (x > maxX) {
			x = maxX;
		}
		if (y < minY) {
			y = minY;
		}
		if (y > maxY) {
			y = maxY;
		}
		map.setCenter(new google.maps.LatLng(y, x));
	});
	 // Limit the zoom level
	google.maps.event.addListener(map, 'zoom_changed', function () {
		if (map.getZoom() < MINZOOMLEVEL) {
			map.setZoom(MINZOOMLEVEL);
		}
	});
}
function getDestination() {
	geocoder = new google.maps.Geocoder();
	var address = document.getElementById('to').value, marker;
	geocoder.geocode({ 'address': address }, function (results, status) {
		if (status === google.maps.GeocoderStatus.OK) {
			map.setCenter(results[0].geometry.location);
			marker = new google.maps.Marker({
				map: map,
				position: results[0].geometry.location
			});
		} else {
			alert('Geocode was not successful for the following reason: ' + status);
		}
	});
	$('#disttime').focus();
}
function clearfromLatLngText() {
	autoRefreshLocation = false;
	document.getElementById("from").value = "";
	fromMarker.setVisible(false);
	$("#from").focus();
	aoTrip = [];
	goTrip.fromLat = null;
	goTrip.fromLon = null;
	clearDirections(goTrip);
	rideActive = false;
}
function clearDestText() {
	autoRefreshLocation = false;
	document.getElementById("to").value = "";
	toMarker.setVisible(false);
	$("#to").focus();
	goTrip.toLat = null;
	goTrip.toLon = null;
	clearDirections(goTrip);
	rideActive = false;
}
function RefreshPage() {
	window.location = "rideshare.html";
}
function autoAdjustInputs() {
	$('#map-canvas').animate({
		height: $(window).innerHeight() - 10
	}, 100, function () {
	});
	$(window).resize(function () {
		$('#map-canvas').animate({
			height: $(window).innerHeight() - 10,
			width: $(window).innerWidth()
		}, 100, function () {
		});
	});
}
function displayContainer(fromPlace, toPlace) {
	var len, row, i;
	$('#routeList').hide();
	len = aoTrip.length;
	for (i = 0; i < len; i = i + 1) {
		row = aoTrip[i];
		if (row.fromAddress === fromPlace && row.toAddress === toPlace) {
			$('#from').val(row.fromAddress);
			$('#to').val(row.toAddress);
			$('#farepanel').show();
			autoRefreshLocation = false;
			plotDirections(new google.maps.LatLng(row.fromLat, row.fromLon),
				new google.maps.LatLng(row.toLat, row.toLon));
		}
	}
	$('#container').show();
}
function createRoutesList() {
	$('#container').hide();
	$('#ridepanel').hide();
	$('#joinride').hide();
	var routesListHtml = '', len, i = 0, row;
	routesListHtml = '<ul style="list-style-type: none;">';
	routesListHtml += '<li>' +  '<div class="styledButton" OnClick="cancelCurrentRoute()">' +
		'Cancel' + '</div>' + '<hr/>' + '</li>';
	len = aoTrip.length;
	for (i = 0; i < len; i = i + 1) {
		row = aoTrip[i];
		routesListHtml += '<li onclick="displayContainer(' + "'" + row.fromAddress + "','" + row.toAddress + "'" + ')">' + row.fromAddress + '<b>' +
			' To ' +  '</b>' + row.toAddress + '<hr/>' + '</li>';
	}
	routesListHtml += '</ul>';
	$('#routeList').html(routesListHtml);
	$('#routeList').show();
}
function loadScript(scriptname) {
	$.getScript(scriptname)
			.done(function (script, textStatus) {
			console.log(textStatus);
			initModule();
		})
			.fail(function (jqxhr, settings, exception) {
			$("showMsg").text("Triggered ajaxError handler.");
		});
}
function onDeviceReady() {
	var jsFile = '', dest, i = 0, tripCookieName = '', tripCookieValue, oTrip = {};
	module = gup('module');
	// sLat, sLon, dLat, dLon, aFlag,
	//currentActiveRide = getCookie("activeTrip");
	$('#fare').hide();
	showLeftPanel();
	autoAdjustInputs();
	oTrip = {}; //new Object();
	$("#to").focus();
	dest = $('#to').val();
	$("#routeList").hide();
	$("#showHidePanel").bind("click", function () {
		showLeftPanel();
	});
	$("#clrstbtn").bind("click", function () {
		clearfromLatLngText();
	});
	$("#clrdestbtn").bind("click", function () {
		clearDestText();
	});
	$("#clearRoute").bind("click", function () {
		clearRoute();
	});
	$("#recentRoutes").bind("click", function () {
		createRoutesList();
	});
	$("#goBtn").bind("click", function () {
		autoRefreshLocation = false;
		plotDirections(new google.maps.LatLng(goTrip.fromLat, goTrip.fromLon),
			new google.maps.LatLng(goTrip.toLat, goTrip.toLon));
	});
	$('#clearRoute').click(function () {
            // update the block message 
		$.blockUI({ message: "<h1>Clearing Routes...</h1>" });
		$.ajax({
			url: 'rideshare.html',
			cache: false,
			complete: function () {
				// unblock when remote call returns 
				$.unblockUI();
			}
		});
    });
	joinFlag = false;
	aoTrip = [];
	for (i = 0; i < MAXLENGTH; i = i + 1) {
		tripCookieName = getCookie("city") + '_' + 'trip' + '_' + i;
		tripCookieValue = getCookie(tripCookieName);
		if (tripCookieValue === null || tripCookieValue === undefined || tripCookieValue === '') {
			break;
		}
		oTrip = JSON.parse(tripCookieValue);
		aoTrip.push(oTrip);
	}
	if (dest !== '') {
		getDestination();
	}
	//initModule();
	google.maps.event.addDomListener(window, 'load', initialize);
	//initModule();
} // onDeviceReady

var addEvent = function addEvent(element, eventName, func) {
	if (element.addEventListener) {
		return element.addEventListener(eventName, func, false);
    }
	if (element.attachEvent) {
        return element.attachEvent("on" + eventName, func);
    }
};
function goBack() {
	history.back();
}
$(document).ready(function () {
	goTrip = {};
	estimate = gup('mode');
	if (gup('shareType') === MY_VEHICLE_PRIVATE) {
		isMyVehiclePrivate = true;
		vehicleCapacity = parseInt(gup('capacity'), 10);
		goTrip.VehicleCapacity = vehicleCapacity;
	}
	checkDeviceReady();
}); // document.ready
// get the shared ride from the db
function getRides(rideId, successCallBack) { // phpScriptsPath
	var ridesurl = PHP_SCRIPTS_PATH + 'getridesdata.php?rides_id=' + rideId;
	$.ajax({
		type: 'GET',
		url: ridesurl,
	    dataType: 'json',
	    success: function (data) {
	        successCallBack(data);
	    },
	    error: function (xhr, textStatus, errorThrown) {
	        var err = textStatus + ', ' + errorThrown;
			console.log(err);
	    },
		async: false
	});
} // getRides
