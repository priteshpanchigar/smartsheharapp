// lib functions - can be used in other sites 
// not specific to this site
//var PHP_SCRIPTS_PATH = 'http://www.smartshehar.com/alpha/dashboardapp/svr/apk/v1/php/offence/';
var app = false;
var windowsApp = false;

function gup( name )
{
	name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
	var regexS = "[\\?&]"+name+"=([^&#]*)";
	var regex = new RegExp( regexS );
	var results = regex.exec( window.location.href );
	if( results === null )
		return "";
	else {
		return results[1].replace(/%20/g, ' ');
	}
}

function getLocation(successCallBack, failureCallBack) {
	var lat, lon, gps, options, geo, watch;
	gps = navigator.geolocation;
	options = {timeout:60000, maximumAge:330000};
	if(gps) {
		browserSupportFlag = true;
		navigator.geolocation.getCurrentPosition(function(position) {
			if(successCallBack) successCallBack(position);
		}, function() {
			handleNoGeolocation(browserSupportFlag);
		});
	// Try Google Gears Geolocation
	} else if (google.gears) {
		browserSupportFlag = true;
		geo = google.gears.factory.create('beta.geolocation');
		geo.getCurrentPosition(function(position) {
			if(successCallBack) successCallBack(position);
		}, function() {
		handleNoGeoLocation(browserSupportFlag);
		});
	// Browser doesn't support Geolocation
	} else {
		browserSupportFlag = false;
		handleNoGeolocation(browserSupportFlag);
	}

	if(gps != undefined) {	
		trackerId = gps.watchPosition(function(position){
			if(successCallBack) successCallBack(position);

		}, failureCallBack, options);
	}
	else if (geo != undefined) {
		watch = geo.watchPosition(function(position){
			if(successCallBack) successCallBack(position);
		}, failureCallBack, options); 
	}
}


  function handleNoGeolocation(errorFlag) {
    if (errorFlag == true) {
//     alert("Geolocation service failed.");
      initialLocation = null;
    } else {
//      alert("Your browser doesn't support geolocation");
      initialLocation = null;
    }
//    map.setCenter(initialLocation);
  }

function geoErrorHandler(err) {

/*  if(err.code == 1) {
    dBug("geo:", "Error: Access is denied!", 5);
  }else if( err.code == 2) {
*/
	console.log('Position error');
//    dBug("geo", "Error: Position is unavailable!", 5);
//  }

}

function setCookie(c_name,value,exdays)
{
	var exdate=new Date();
	exdate.setDate(exdate.getDate() + exdays);
	var c_value=escape(value) + ((exdays===null) ? "" : "; expires="+exdate.toUTCString());
	document.cookie=c_name + "=" + c_value;
}

function getCookie(c_name)
{
	var i,x,y,ARRcookies=document.cookie.split(";");
	for (i=0;i<ARRcookies.length;i++)
	{
		x=ARRcookies[i].substr(0,ARRcookies[i].indexOf("="));
		y=ARRcookies[i].substr(ARRcookies[i].indexOf("=")+1);
		x=x.replace(/^\s+|\s+$/g,"");
		if (x==c_name)
		{
			return unescape(y);
		}
	}
	return '';
}
	
function getWinDim() {
	 winH = 460, winW = 630;

	if (document.body && document.body.offsetWidth) {
	 winW = document.body.offsetWidth;
	 winH = document.body.offsetHeight;
	}
	if (document.compatMode=='CSS1Compat' &&
	    document.documentElement &&
	    document.documentElement.offsetWidth ) {
	 winW = document.documentElement.offsetWidth;
	 winH = document.documentElement.offsetHeight;
	}
	if (window.innerWidth && window.innerHeight) {
		winW = window.innerWidth;
		winH = window.innerHeight;
	}
	// winW = screen.width < winW ? screen.width : winW;
	// winH = screen.height < winH ? screen.height : winH;
//	dBug('$window.width: ' + $(window).width() + ', $document.width(): ' +  $(document).width());
	return {
		"width": parseInt(winW, 10),
		"height": parseInt(winH, 10)
	};
}

// distance between 2 lat, lon pairs
function distanceLatLon(lat1, lon1, lat2, lon2) {
	var R = 6371; // km
	var dLat = (lat2-lat1) * Math.PI / 180;
	var dLon = (lon2-lon1) * Math.PI / 180; 
	var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
			Math.cos(lat1 * Math.PI / 180) * Math.cos(lat2* Math.PI / 180) * 
			Math.sin(dLon/2) * Math.sin(dLon/2); 
	var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
	var d = R * c;
	return Math.ceil(d * 1000);	// return distance in meters
}

function getAddress(lat, lon, successCallBack) {
	var addrUrl = 'http://maps.googleapis.com/maps/api/geocode/json?latlng=' +
			lat + ',' + lon + '&sensor=true_or_false';
	$.ajax({
	    type: 'GET',
	    url: addrUrl,
	    dataType: 'json',
	    success: function (data) {
	        successCallBack(data);
	    },
	    error: function(xhr, textStatus, errorThrown) {
	        var err = textStatus + ', ' + errorThrown;
	    }, 
	    async: false
	})	
}	

function createCityList() {
	var listHtml = '', len, i = 0, row, currentCity = '', cityimage = '';
	listHtml = '<ul id="allcities" style="list-style-type: none;">';
	len = jsonCityFareParameter.length;
	for (i = 0; i < len; i = i + 1) {
		row = jsonCityFareParameter[i];
		if (currentCity !== row.city) {
			cityimage = '<img src="images/logo_' + row.city + '.png" style="display:float">';
			listHtml += '<li id="city" name="city" onclick="selectCity(\'' + row.city + '\')">' + cityimage + 
						'<b>' + row.city + '</b>' + '</li>' ;
		}
		currentCity = row.city;
	}
	listHtml += '</ul>';
	$('#citylist').html(listHtml);
}
function cityBlock() {
	/*if (rideActive == true) {
		$.blockUI({ message: "<h4>Ride is active, You can not select another City...</h4>" });
		$.ajax({
			url: 'rideshare.html',
			cache: false,
			complete: function () {
				//unblock when remote call returns 
				//$.unblockUI();
			}
		});
	}*/
	if (rideActive == true) {
		$().toastmessage('showWarningToast', "<h4>Ride is active, You can not select another City...</h4>");
	}	
	if (rideActive == false) {
		//getCity(true);
		window.location = 'city.html';
		$("#cityname").html(city);
	}	
}
function dateMinutesDifference(inMinutes) {
    var minutesEarlier = new Date();
    minutesEarlier.setTime(minutesEarlier.getTime() + inMinutes * 60000);
	return minutesEarlier.getFullYear() + '-' + (minutesEarlier.getMonth() + 1) + '-' +
				minutesEarlier.getDate() + ' ' + minutesEarlier.getHours() + ":" +
				minutesEarlier.getMinutes() + ":" + minutesEarlier.getSeconds();
}

function pickCity() {
	$('#entirePage').fadeTo('slow', 0.4);
	$('#blockPage').show();
	$("#selectCity").show();
	$("#selectCity").load("city.html");
}
