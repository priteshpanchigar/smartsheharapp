var departmentcode = getParameter("departmentcode");
var issuesubtypecode = getParameter("issuesubtypecode");

function getDepartmentSummary( successCallBack) {
      var getDepartmentSummaryUrl = getReportsPhpScriptsPath() +
        'issuesummary.php?issuesubtypecode='+issuesubtypecode;
    $.ajax({
        type: 'GET',
        url: getDepartmentSummaryUrl,
        dataType: 'json',
        success: function(data) {
            successCallBack(data);
        },
        error: function(xhr, textStatus, errorThrown) {
            var err = textStatus + ', ' + errorThrown;
        }
        
    });
}
	$(document).ready(function() {
		getDepartmentSummary(
        function(data) {
        var color='';
		var heading='';
		var img = '';
		var tbody='';
		var tr='';
		var cls;
		var sHtml = '';
		len = data.length;
		sHtml =  sHtml + '<tr><th>Issue Name</th><th>Issue Photo</th></tr>';
            for (i = 0; i < len; i = i + 1) {
				if(i%2==0){
					cls = 'active';
				}
				else{
					cls = 'warning';
				}
				row = data[i];
				sHtml =  sHtml + '<tr class= '+cls+'><td>' + row.issue_item_description  + 
					'</td><td>photo<td></tr>';
				
			}
			document.getElementById("issueTable").innerHTML = sHtml;
			if(departmentcode == 'TR'){
				heading = 'Traffic Issues '
				img = 'fa fa-car fa-5x';
				color = 'panel panel-yellow';
			}else{
				heading = 'Municipal Issues '
				img = 'fa fa-trash-o fa-5x';
				color = 'panel panel-red';
			}
			document.getElementById("color").className = color ;
			document.getElementById("img").className = img ;
			document.getElementById("head").innerHTML = heading ;
			document.getElementById("cnt").innerHTML = len;
        });
		
		$
	});
function getParameter(theParameter) { 
  var params = window.location.search.substr(1).split('&');
 
  for (var i = 0; i < params.length; i++) {
    var p=params[i].split('=');
	if (p[0] == theParameter) {
	  return decodeURIComponent(p[1]);
	}
  }
  return false;
}