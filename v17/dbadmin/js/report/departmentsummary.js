var departmentcode = getParameter("departmentcode")? getParameter("departmentcode") : '';
var ward = getParameter("ward") ? getParameter("ward") : '';
var trLat = getParameter("trLat") ? getParameter("trLat") : '';
var trLng =  getParameter("trLng") ? getParameter("trLng") : '';
var groupby = getParameter("groupby")?getParameter("groupby") : '';
var issuesubtypedescription = getParameter("issuesubtypedescription") ? getParameter("issuesubtypedescription") : '';
var postalcode='';
var startDate='';
var endDate='';
var sWard='';
var spnFilter='';
var date='';

function getDepartmentSummary( successCallBack) {
	var parameters = '';
	if(departmentcode==='' || departmentcode ===null)
	{
		departmentcode='';
	}  
	else{ parameters = 'departmentcode='+departmentcode;}
	if(postalcode==='' || postalcode ===null )
	{
		postalcode='';
	}else{ parameters = parameters + '&postalcode='+postalcode;}
	if(startDate==='' || startDate ===null)
	{
		startDate = '';
	}else{
		date = startDate.split("-");
		startDate= date[2] + '-' + date[1]  + '-' + date[0];
		parameters = parameters + '&fromissuedate='+startDate;}
	if(endDate==='' || endDate ===null)
	{
		endDate = '';
	}else{
		date = endDate.split("-");
		endDate = date[2] + '-' + date[1]  + '-' + date[0];
		parameters = parameters + '&toissuedate='+endDate;}
	
	if(trLat ==='' || trLat ===null)
	{
		trLat = '';   
	}else{
		parameters = parameters + '&trLat='+trLat;}
	if(trLng ==='' || trLng ===null)
	{
		trLng = '';
	}else{
		parameters = parameters + '&trLng='+trLng;}
	if(issuesubtypedescription!=='')
	{
		ward = '';
	}else{
		if(ward=== '' || ward ===null)
			{
				ward ='';
			}else{
				if(departmentcode==='TR')
				{
					ward ='';
				}
				else{
					parameters = parameters + '&ward='+escape(ward);
				}
			}
	}
	
      var getDepartmentSummaryUrl = getIssueReportPhpScriptsPath() + 
        'issue_report.php?'+parameters +'&groupby='+ groupby +
		 '&submitreport=1&closed=0&issuesubtypedescription='+issuesubtypedescription+'&orderby=' + encodeURIComponent('cnt desc');
    $.ajax({
        type: 'GET',
        url: getDepartmentSummaryUrl,
        dataType: 'json',  
        success: function(data) {
            successCallBack(data);
        },
        error: function(xhr, textStatus, errorThrown) {
            var err = textStatus + ', ' + errorThrown;
        }
    }); 
}


$(document).ready(function() {
	if(departmentcode==='TR')
		{
			document.getElementById("selectWard").setAttribute('style','display:none');
			document.getElementById("ward").setAttribute('style','display:none');
		}
	var tbl=document.getElementById("issueTable");
	var tr='';
	var j = 0;
	var cls;
	var row ='';
    var cell ='';  
	 
		
			if (typeof(Storage) !== "undefined") {
				startDate = localStorage.getItem("startDate");
				endDate = localStorage.getItem("endDate");
				//postalcode = localStorage.getItem("postalcode");
				//sWard = localStorage.getItem("ward");
				//	if(sWard  !==null || sWard !=="")
				//	{ 
				//		ward = sWard;  
				//	}    
				}    
			if(startDate !== '' &&  startDate !== null)
			{
			date = startDate.split("-");
			startDate= date[0] + '-' + date[1]  + '-' + date[2];}
			
			if(endDate !== '' && endDate !== null)
			{       
			date = endDate.split("-");
			endDate= date[0] + '-' + date[1]  + '-' + date[2];}
			document.getElementById('startDateText').value = startDate;
			document.getElementById('endDateText').value = endDate;
			document.getElementById('postalCode').value = localStorage.getItem("postalcode");
		
		startDate='';
		endDate='';
		if(ward===''){
			document.getElementById('selectWard').setAttribute('style','display:none');
			document.getElementById('ward').setAttribute('style','display:none');
			document.getElementById('selectWard').value = localStorage.getItem("ward");}
		else{
			document.getElementById('selectWard').value = ward;
		}
	
		//setFilterSummary();
		showDepartmentSummary();
}); // document.ready


function goFilter(){  
	
	postalcode = document.getElementById('postalCode').value;
	date = $("#startDateText").val().split("-");
	startDate= date[0] + '-' + date[1]  + '-' + date[2];

	date = $("#endDateText").val().split("-");
	endDate= date[0] + '-' + date[1]  + '-' + date[2];
	
	if(ward!=='')  
	{sWard = document.getElementById("selectWard").value; }
	if(sWard !== 'select')
	{
		ward = sWard;
	}
	if(sWard === 'select')
	{
		sWard = '';
	}
	if(startDate === endDate)
	{
		alert("Start Date and end date should not be same!");
	}
	else{
		
		setFilterSummary();
		if (typeof(Storage) !== "undefined") {
			localStorage.setItem("startDate", startDate);
			localStorage.setItem("endDate", endDate);
			localStorage.setItem("postalcode", postalcode);
			localStorage.setItem("ward", ward);
		}
		
		showDepartmentSummary();
	}
	
}
function setFilterSummary()
{	spnFilter = '';
	spnFilter = spnFilter + "Filter by ";
	if(startDate !== '' && startDate !== null)
	{spnFilter= spnFilter + "Date From "+startDate; }

	if(endDate !== '' && endDate !== null)
	{spnFilter= spnFilter +" To " + endDate;}
	
	if(postalcode !== '' && postalcode !== null)
	{spnFilter = spnFilter +"<br> Postal Code "+postalcode;}

	if(issuesubtypedescription !== '')
	{
		if(ward !== '' && ward !== null)
		{  
			spnFilter = spnFilter +"<br> Ward "+ward;
		}  
	}

	if(startDate === null && startDate === '' 
		&& endDate === null && endDate === '' 
		&& postalcode === null && postalcode === ''
		&& ward === null && ward === '')
		{
			spnFilter = '';
		}
	document.getElementById("spnFilter").innerHTML = spnFilter ;
}
function getParameter(theParameter) { 
  var params = window.location.search.substr(1).split('&');
 
  for (var i = 0; i < params.length; i++) {
    var p=params[i].split('=');
	if (p[0] == theParameter) {
	  return decodeURIComponent(p[1]);
	}
  }
  return false;
}
function showDepartmentSummary()
{
	var img;
	var color='';
	var heading='';
	getDepartmentSummary(
        function(data) {
			var cnt = 0;
			var opneCnt = 0;
			var totalCnt = 0;
			var resolvedCnt = 0;
            len = data.length;
			var sHtml = '';
			if(issuesubtypedescription!=='')
				{
					sHtml =  sHtml + '<tr><th>Ward</th><th><p class="text-right">Open</p></th><th><p class="text-right">Resolved</p></th><th><p class="text-right">Total</p></th></tr>'
				}else{
				sHtml =  sHtml + '<tr><th>Issue</th><th><p class="text-right">Open</p></th><th><p class="text-right">Resolved</p></th><th><p class="text-right">Total</p></th></tr>';}
            for (i = 0; i < len; i = i + 1) {
				if(i%2===0){
					cls = 'active';
				}
				else{
					cls = 'warning';
				}
				
                row = data[i];
				totalCnt = parseInt(row.cnt,10);  
				resolvedCnt =  parseInt(row.resolvedCnt,10); 
				opneCnt =  totalCnt - resolvedCnt;
				
				sHtml =  sHtml + '<tr class= '+cls+'><td>'
				if(issuesubtypedescription!=='')
				{  
					sHtml = sHtml + row.ward ; 
				}else{
					sHtml = sHtml + row.issue_sub_type_description; 
				}
				
				if(opneCnt===0)
				{
					sHtml = sHtml +'<td align="right">' + opneCnt + '</td>';
				}
				else{
					sHtml = sHtml +'<td align="right"><a href="'+getReportHtmlPath()+
					'issue_report.html?issuesubtypecode=' + 
					row.issue_sub_type_code + 
					'&ward=' + escape(row.ward) +
					'&resolved=0'+
					'&departmentcode=' + departmentcode +
					'&fromissuedate=' + startDate +  
					'&toissuedate=' + endDate +
					'&=issuesubtypecode=' + row.issue_sub_type_code +
					'&' + row.issue_type_code + '=' + row.issue_type_code+'">' + opneCnt + '</a></td>';
				}
				if(resolvedCnt ===0)
				{
					sHtml = sHtml +'<td align="right">'+row.resolvedCnt+ '</td>';
				}
				else{
					sHtml = sHtml +'<td align="right"><a href="'+getReportHtmlPath()+
					'issue_report.html?issuesubtypecode=' + 
					row.issue_sub_type_code + 
					'&ward=' + escape(row.ward) +
					'&resolved=1'+
					'&departmentcode=' + departmentcode +
					'&fromissuedate=' + startDate +  
					'&toissuedate=' + endDate +
					'&=issuesubtypecode=' + row.issue_sub_type_code +
					'&' + row.issue_type_code + '=' + row.issue_type_code+'">' + resolvedCnt + '</a></td>';
				}
				sHtml = sHtml + '<td align="right">'+totalCnt+ '</td></tr>';
				cnt = cnt + opneCnt;  
			}      
		
			document.getElementById("issueTable").innerHTML = sHtml;
			
			if(departmentcode == 'TR'){    
				heading = "Traffic Issues";
				img = "fa fa-car fa-5x";
				color = "panel panel-yellow";
			}else{  
				heading = "Municipal Issues";
				img = "fa fa-trash-o fa-5x";  
				color = "panel panel-red";   
			}     
			document.getElementById("color").className = color ;
			document.getElementById("img").className = img ;
			document.getElementById("head").innerHTML = heading ;
			document.getElementById("cnt").innerHTML = cnt;
			if(ward=== '' || ward ===null)
			{
				
			}else{
				if(departmentcode==='MU')
				{
					document.getElementById("head").innerHTML = "Municipal Issues - "+ward+" ward"
				}
			}
			
			   
        });
		
}