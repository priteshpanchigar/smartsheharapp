//var fileChooser = document.getElementsByTagName('input')[1];
var fileChooser = document.getElementsByClassName('upload')[0];
var content = document.getElementById('content');

if (typeof window.FileReader === 'undefined') {
    content.className = 'fail';
    content.innerHTML = 'File API &amp; FileReader API are not supported in your browser.  Try on a new-ish Android phone.';
}

fileChooser.onchange = function (e) {
    //e.preventDefault();

    var file = fileChooser.files[0],
        reader = new FileReader();
        
    reader.onerror = function (event) {
        content.innerHTML = "Error reading file";
    }

    reader.onload = function (event) {
        var img = new Image();

        // files from the Gallery need the URL adjusted
        if (event.target.result && event.target.result.match(/^data:base64/)) {
            img.src = event.target.result.replace(/^data:base64/, 'data:image/jpeg;base64');
        } else {
            img.src = event.target.result;
        }

        // Guess photo orientation based on device orientation, works when taking picture, fails when loading from gallery
        if (navigator.userAgent.match(/mobile/i) && window.orientation === 0) {
            img.height = 120;
            img.className = 'rotate';
        } else {
            img.width = 120;
        }

        content.innerHTML = '';
        content.appendChild(img);

        var processExif = function(exifObject) {

        //    $('#cameraModel').val(exifObject.Model);
        //    $('#aperture').val(exifObject.FNumber);

            // Uncomment the line below to examine the
            // EXIF object in console to read other values
            console.log(exifObject);

        }

      try {
            processExif(img);
      }
      catch (e) {
        alert(e);
      }		
		
		
		};
    
    reader.readAsDataURL(file);

    return false;
}
var map;
function initMap() {
  map = new google.maps.Map(document.getElementById('map'), {
    zoom: 8,
    center: {lat: 19.1, lng: 72.85}
  });
}
function geocodePlaceId(geocoder, map, infowindow, placeid) {
  geocoder.geocode({'placeId': placeid}, function(results, status) {
    if (status === google.maps.GeocoderStatus.OK) {
      if (results[0]) {
        map.setZoom(11);
        map.setCenter(results[0].geometry.location);
        var marker = new google.maps.Marker({
          map: map,
          position: results[0].geometry.location
        });
        infowindow.setContent(results[0].formatted_address);
        infowindow.open(map, marker);
      } else {
        window.alert('No results found');
      }
    } else {
      window.alert('Geocoder failed due to: ' + status);
    }
  });
}
function geocodeLatLng(geocoder, map, infowindow, latlng) {
  geocoder.geocode({'location': latlng}, function(results, status) {
    if (status === google.maps.GeocoderStatus.OK) {
      if (results[0]) {
        map.setZoom(11);
        var marker = new google.maps.Marker({
          position: latlng,
          map: map
        });
        infowindow.setContent(results[0].formatted_address);
        infowindow.open(map, marker);
		$('#locationdiv').removeClass("panel panel-warning").addClass("panel panel-success");
		
		$('#gettingLocation').text(results[0].formatted_address);
		geocodePlaceId(geocoder, map, infowindow, results[0].place_id);
      } else {
        window.alert('No results found');
      }
    } else {
      window.alert('Geocoder failed due to: ' + status);
    }
  });
}
function categoryClicked() {
	$("#issuepage").hide();
	$("#categorylist").show();
}

function fillCategory(category) {
	$("#issuepage").show();
	$("#categorylist").hide();
	$("#category").val(category);
}

$(document).ready(function() {

	
	var useragent = navigator.userAgent;
	initMap();
	var d = new Date();
	document.getElementById("currentDate").innerHTML = moment(d).format("D MMM, YYYY") ;
	document.getElementById("currentTime").innerHTML = moment(d).format("h:mm a") ;
	displayLocation = function (position) {
		var myLatLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
		$('#gettingLocation').text("Getting address ...");
		$('#locationdiv').removeClass("panel panel-danger").addClass("panel panel-warning");
		  var geocoder = new google.maps.Geocoder;
			var infowindow = new google.maps.InfoWindow;
			geocodeLatLng(geocoder, map, infowindow, myLatLng);

	}
	handleError = function() {
		console.log('Failed to get location');
	}
		if ( useragent.indexOf('iPhone') !== -1 || useragent.indexOf('Android') !== -1 ) {
			navigator.geolocation.watchPosition( 
				displayLocation, 
				handleError, 
				{ 
					enableHighAccuracy: true, 
					maximumAge: 30000, 
					timeout: 27000 
				}
			);			

		// or let other geolocation capable browsers to get their static position
		} else if ( navigator.geolocation ) {
			navigator.geolocation.getCurrentPosition( displayLocation, handleError );
		}
	
	
	
}); // document.ready

