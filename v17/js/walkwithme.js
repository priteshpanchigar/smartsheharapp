/*global $, jQuery, gup, console, getWalkWithmeLoc, document, google, setTimeout */
//"use strict";
var map, infowindow, placeStr = "", blueMarker, greenMarker, circle, user_email = gup("user_email"), TRAVELSPEED = 10,
	user_name = gup("name"), id = gup("id"), firstTime = true, firstGreenDot = true, latitude,
	longitude, accuracy = 0, centerPosition, user_details, timerStart, updateLocationInterval = 1000;
var directionsService = new google.maps.DirectionsService();
var directionsDisplay = new google.maps.DirectionsRenderer();
function calculateDistance(latitude, longitude, lat, lng) {
	var from = new google.maps.LatLng(latitude, longitude),
		to = new google.maps.LatLng(lat, lng),
		request, myRoute, totlDist, reqdTime;
	request = {
		origin: from,
		destination: to,
		travelMode: google.maps.DirectionsTravelMode.DRIVING,
		durationInTraffic: true
	};
	directionsService.route(request, function (result, status) {
		if (status === google.maps.DirectionsStatus.OK) {
			directionsDisplay.setDirections(result);
			myRoute = result.routes[0];
			totlDist = myRoute.legs[0].distance.value;
			/*if (totlDist < 1000) {
				var distanceDetails = "<li>" + "Distance Between Traveller and The Viewer = " +
							totlDist + " m " + "(+- " + accuracy + " m)" + "</li>";
				//reqdTime = (totlDist * 0.5) / 60;
				reqdTime = (totlDist / 1000) / 30 * 60;
				if (totlDist > 10) {
					if ( reqdTime >= 1) {
						var timeDetails = "<li>" + "Expected In " + parseInt(reqdTime, 10) + " Mins." +
						" </li>";
					} else {
						var timeDetails = '';
						var distanceDetails = "<li>" + "Near You " + totlDist + " m " + 
						"(+- " + accuracy + " m)" + "</li>";	
					}	
				} else {
					var distanceDetails = "<li>" + "Near You " + //totlDist + " m " + 
					"(+- " + accuracy + " m)" + "</li>";
					var timeDetails = '';
				}	
			} else {
					var eHrs = 0, eMins = 0, distanceDetails = "<li>" + 
											"Distance Between Traveller and The Viewer = " +
										(totlDist / 1000) + " Km " + "(+- " + accuracy + " m)" +
										" </li>";
					//reqdTime = (totlDist * 0.5) / 60;
					reqdTime = (totlDist / 1000) / 30 * 60;
					while(reqdTime >= 60) {
						eMins = reqdTime - 60;
						eHrs = eHrs + 1;
						if (eMins < 60) {
							break;
						}
						reqdTime = eMins;
					}
					if (eHrs > 0) {
						var timeDetails = "<li>" + "Expected In " + eHrs + " Hours " + "And " +
						parseInt(eMins, 10) + " Mins." + "</li>";
					} else {
						var timeDetails = "<li>" + "Expected In " + parseInt(reqdTime, 10) + " Mins." +
							" </li>";
					}			
				}*/
			if (totlDist < 1000) {
				var distanceDetails = "<li>" + " You (" + lat.toPrecision(6) + ", " + 
				lng.toPrecision(6) + ") " + 
				"are " + totlDist + " m " + "(+- " + accuracy + " m) " ;
				reqdTime = ((totlDist / 1000) / TRAVELSPEED) * 60;
				if (totlDist > 10) {
					if (parseInt(reqdTime, 10) === 0) {
						distanceDetails = '';
						distanceDetails = "<li>" + "Near You (" +
						lat.toPrecision(6) + ", " + lng.toPrecision(6) + ") " +
						totlDist + " m " + "(+- " + accuracy + " m)" + "</li>";
					} else {	
						distanceDetails += parseInt(reqdTime, 10) + " Mins. away" + "</li>";
					}	
				} else {
					distanceDetails = '';
					distanceDetails +=  "<li>" + "Near You (" +
						lat.toPrecision(6) + ", " + lng.toPrecision(6) + ") " +
						totlDist + " m " + "(+- " + accuracy + " m)" + "</li>";
					var timeDetails = '';
				}	
			} else {
					var eHrs = 0, eMins = 0, distanceDetails = "<li>" + 
					" You (" + lat.toPrecision(6) + ", " + lng.toPrecision(6) + ") are " + (totlDist / 1000).toFixed(2) + " Km " +
					"(+- " + accuracy + " m) " ;
					reqdTime = ((totlDist / 1000) / TRAVELSPEED) * 60;
					while(reqdTime >= 60) {
						eMins = reqdTime - 60;
						eHrs = eHrs + 1;
						if (eMins < 60) {
							break;
						}
						reqdTime = eMins;
					}
					if (eHrs > 0) {
						distanceDetails += "<br />" + eHrs + " hr " + //"And " +
						parseInt(eMins, 10) + " min. away" + "</li>";
					} else {
						distanceDetails += parseInt(reqdTime, 10) + " Mins. away" +
							" </li>";
					}			
			}	
			document.getElementById("distDetails").innerHTML = distanceDetails;
			//document.getElementById("timeReqd").innerHTML = timeDetails;
		}	
	});
}
function myLocation() {
	var lat, lng, acc;
	getLocation(function (pos) {
		myPos = pos;
		if (myPos) {
			lat = myPos.coords.latitude;
			lng = myPos.coords.longitude;
			acc = myPos.coords.accuracy;
		} else {
			lat = 19.104056;
			lng = 72.850465;
			acc = 40;
		}
		if (firstGreenDot) {
			greenMarker = new google.maps.Marker({
				position: new google.maps.LatLng(lat, lng),
				map: map,
				icon:getImages()+"/v17/images/green_dot.png",
				title:"viewer's Location"
			});
			circle = new google.maps.Circle({
				radius: acc,
				map: map,
				fillColor: '#33CC33',
				fillOpacity: 0.1,
				strokeColor: '#33CC33',
				strokeOpacity: 0.2
			});
		} else {
			greenMarker.setPosition(new google.maps.LatLng(lat, lng));
		}	
		//circle.bindTo('center', greenMarker, 'position');
		firstGreenDot = false;		
		calculateDistance(latitude, longitude, lat, lng);
	});
}	
function updateLocation() {
	getWalkWithmeLoc(user_email, id, function (aoFriendLoc) {
		var len = aoFriendLoc.length, row, html = "", fkid, datetm = "", stopFlag = '', stopSharing,
			currentDate, currentTime, currentHours, currentMinutes, currentSeconds, today, options;
		if (len === 0) {
			document.getElementById("nlists").innerHTML = "<li>NO Friend Location information found </li>";
		} else {
			row = aoFriendLoc;
			fkid = row.frnkey;
			latitude = parseFloat(row.lat);
			longitude = parseFloat(row.lon);
			accuracy = parseInt(row.accuracy, 10);
			datetm = mySqlDate(row.date);
			stopFlag = row.stopflag;
		}
		if (latitude === undefined || longitude === undefined) {
			document.getElementById("nlists").innerHTML = "<li>NO Friend Location information found Please Tell Your Friend To Put On GPS </li>";
			return;
		}
		if (stopFlag === 'Y') {
			stopSharing = '(Stopped Sharing)';
			clearInterval(timerStart);
		} else {
			stopSharing = '';
		}
		currentDate = new Date(datetm);
		currentHours = currentDate.getHours();
		currentMinutes = currentDate.getMinutes();
		currentSeconds = currentDate.getSeconds();
		currentTime = currentDate.getHours() + ":"
						+ currentDate.getMinutes() + ":"
						+ currentDate.getSeconds();
		if (currentMinutes < 10) {
			currentMinutes = "0" + currentMinutes;
		}
		if (currentSeconds < 10) {
			currentSeconds = "0" + currentSeconds;
		}	
		today = currentDate.getFullYear() + "-" +
			(currentDate.getMonth() + 1)  + "-" +
				currentDate.getDate();
		if (user_name === '') {//currentTime + currentDate.toDateString() " 's location at "
			user_details = "<li>" + user_email + " (" + latitude.toFixed(4) +
			" , " + longitude.toFixed(4) + ")" + " at " + currentHours + ":" + 
			currentMinutes + //":" + currentSeconds +
			" on " + 
			currentDate.toDateString().substring(4,10) + " " + stopSharing + " </li>";
			//currentDate.getDate() + ', ' + currentDate.getFullYear() + " " + stopSharing + " </li>";
		} else {//currentTime
			if (user_name.substr(user_name.length - 1) === '-') {
				user_name = user_name.substr(0 , (user_name.length-1));
			}
			user_details = "<li>" + user_name.replace("-", " ") + " (" + latitude.toFixed(4) +
			", " + longitude.toFixed(4) + ")" + " at " +   
			currentHours + ":" + currentMinutes + //":" + currentSeconds +
			" on " + 
			currentDate.toDateString().substring(4,10) + ' ' + stopSharing + " </li>";
			//currentDate.getDate() + ', ' + currentDate.getFullYear() + " " + stopSharing + " </li>"; //
		}
		document.getElementById("nlists").innerHTML = user_details;
			//"<li>Friend's location at " + datetm + " " + stopSharing + " </li>";
		centerPosition = new google.maps.LatLng(latitude, longitude);
		if (firstTime) {
			options = {
				zoom: 16,
				center: centerPosition,
				mapTypeId: google.maps.MapTypeId.ROADMAP
			};
			map = new google.maps.Map($('#map')[0], options);
			blueMarker = new google.maps.Marker({
				position: centerPosition,
				map: map,
				icon: 'http://smartshehar.com/alpha/smartsheharapp/v17/images/gpsloc.png'
				
			});
			circle = new google.maps.Circle({
				center: centerPosition,
				radius: accuracy,
				map: map,
				fillColor: '#0000FF',
				fillOpacity: 0.1,
				strokeColor: '#0000FF',
				strokeOpacity: 0.2
			});
			circle.bindTo('center', blueMarker, 'position');
		} else {
			blueMarker.setPosition(centerPosition);
			map.setCenter(centerPosition);
		}
		setTimeout(updateLocation, 3000);
		firstTime = false;
	});
}
function initialize() {
	var rad = 0, len = 0;
	user_email = gup("user_email");
	user_name = gup("name");
	id = gup("id");
	//timerStart = window.setTimeout(function () {updateLocation(); }, updateLocationInterval);
	updateLocation();
	myLocation();
}
$(document).ready(function () {
	google.maps.event.addDomListener(window, 'load', initialize);
});

