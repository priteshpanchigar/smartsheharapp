var postalcode, fromissuedate, toissuedate, PV, MV,
RT, AT, RD, CL, EN, SF, WE, OG, RS,
myissue, appuserid, lat, lng, limit,
submitreport, rejected, closed, resolved;
var ward = getParameter("ward") ? getParameter("ward") : '';
var issueid = getParameter("issueid") ? getParameter("issueid") : '';
var fromissuedate = getParameter("fromissuedate") ? getParameter("fromissuedate") : '';
var toissuedate = getParameter("toissuedate") ? getParameter("toissuedate") : '';
var issuesubtypecode = getParameter("issuesubtypecode") ? getParameter("issuesubtypecode") : '';
var issueid = getParameter("issueid") ? getParameter("issueid") : '';
var markerlat = getParameter("markerlat") ? getParameter("markerlat") : '';
var markerlng = getParameter("markerlng") ? getParameter("markerlng") : '';
var isnotss = getParameter("isnotss") ? getParameter("isnotss") : '';
var ISSUESPERPAGE = 5;

//get the result count offset/
function getPincodewise(postalcode, fromissuedate, toissuedate, PV, MV, RT, AT, RD, CL, EN, SF,
	WE, OG, RS, myissue, appuserid, lat, lng, limit, submitreport, rejected,
	closed, resolved, count, offset, successCallBack) {
	var getPincodewiseurl;
	if(isnotss == 1) {
		getPincodewiseurl = getReportsPhpScriptsPath() +
				"issue_report.php?isnotss=" + isnotss; 
	} else {
			getPincodewiseurl = getReportsPhpScriptsPath() +
		'issue_report.php?postalcode=' + postalcode +
		'&fromissuedate=' + fromissuedate + '&toissuedate=' + toissuedate +
		'&PV=' + PV + '&MV=' + MV + '&RT=' + RT + '&AT=' + AT +  
		'&RD=' + RD + '&CL=' + CL + '&EN=' + EN + '&SF=' + SF +
		'&WE=' + WE + '&OG=' + OG + '&RS=' + RS +
		'&myissue=' + myissue + '&appuserid=' + appuserid + '&lat=' + lat + '&lng=' + lng +
		'&limit=' + limit + '&submitreport=' + submitreport +
		'&rejected=' + rejected  + '&resolved=' + resolved +
		'&ward=' + ward + '&issueid=' + issueid + '&issuesubtypecode=' + issuesubtypecode +
		'&count=' + count + '&offset=' + offset + '&issueid=' + issueid + '&markerlat=' + markerlat +
		'&markerlng=' + markerlng+'&submitreport=1';
		
		if(closed === 1)
			getPincodewiseurl = getPincodewiseurl + '&closed=' + closed;

	}
	$.ajax({
		type : 'GET',
		url : getPincodewiseurl,
		dataType : 'json',
		success : function (data) {
			successCallBack(data);
		},
		error : function (xhr, textStatus, errorThrown) {
			var err = textStatus + ', ' + errorThrown;
		}

	});
}
function getMonth(n) {
	var month = new Array();
	month[0] = "Jan";
	month[1] = "Feb";
	month[2] = "Mar";
	month[3] = "Apr";
	month[4] = "May";
	month[5] = "Jun";
	month[6] = "Jul";
	month[7] = "Aug";
	month[8] = "Sep";
	month[9] = "Oct";
	month[10] = "Nov";
	month[11] = "Dec";

	return month[parseInt(n) - 1];
}

$(document).ready(function () {
	// $('#dp1').datepicker();
	//$('#dp2').datepicker();

	postalcode = gup('postalcode');
	fromissuedate = gup('fromissuedate');
	toissuedate = gup('toissuedate');
	PV = gup('PV');
	MV = gup('MV');
	RT = gup('RT');
	AT = gup('AT');
	RD = gup('RD');
	CL = gup('CL');
	EN = gup('EN');
	SF = gup('SF');
	WE = gup('WE');
	OG = gup('OG');
	RS = gup('RS');
	myissue = gup('myissue');
	appuserid = gup('appuserid');
	lat = gup('lat');
	lng = gup('lng');
	//	limit = gup('limit');
	submitreport = gup('submitreport');
	rejected = gup('rejected');
	closed = gup('closed');
	resolved = gup('resolved');

	var sHTML = '';
	var sVehicleNo = '',
	sWard = '';
	offset = $('#offset').val();
	limit = $('#limit').val();
	$('#loader').show();
	displayIssueRows(offset, limit, function (data) {
		flag = true;
		$('#loader').hide();
		if (data != '') {

			offset = parseInt($('#offset').val());
			limit = parseInt($('#limit').val());
			$('#offset').val(offset + limit);
		} else {
			alert('No more data to show');
			no_data = false;
		}

	});

	/*
	getPincodewise(postalcode, fromissuedate, toissuedate, PV, MV, RT, AT, RD, CL, EN, SF,
	WE, OG, RS, myissue, appuserid, lat, lng, limit, submitreport, rejected, closed, resolved,
	1, 0,
	function(data) {
	var len = data.length, sHTML = '', cnt = 0;
	var issue_time='';
	for (i = 0; i < len; i = i + 1) {
	row = data[i];
	cnt = row.cnt;
	console.log('cnt = ' + cnt);
	var pages = parseInt(cnt / ISSUESPERPAGE) + 1;
	var navHtml = ''; // ' <li> <a href="#" aria-label="Previous"> <span aria-hidden="true">&laquo;</span> </a> </li>';
	for (j = 0; j < pages; j = j + 1) {
	navHtml += '<li><a href="#" onclick="displayIssueRows(ISSUESPERPAGE,' + (ISSUESPERPAGE * j + 1) +
	')">' + (j + 1) + '</a></li>';
	}
	$('#pageno').html(navHtml);
	displayIssueRows(5, 10);
	break;
	}

	});
	 */
	$('#loader').hide();
}); // document.ready

function displayIssueRows(offset, limit, successCallBack) {
	getPincodewise(postalcode, fromissuedate, toissuedate, PV, MV, RT, AT, RD, CL, EN, SF,
		WE, OG, RS, myissue, appuserid, lat, lng, limit, submitreport, rejected, closed, resolved,
		0, offset,
		function (data) {
		len = data.length;
		sHTML = '';
		var resolved = '';
		var resolvedDate = '';
		var issue_time = '', submit_report=''
		for (i = 0; i < len; i = i + 1) {
			row = data[i];
			date = '';
			sVehicleNo = row.vehicle_no ? row.vehicle_no : '';
			sWard = row.ward ? row.ward : '';
			issue_time = row.issue_time;
			var date = issue_time.split("-");
			var day = date[2];
			var iday = day.split(" ");  
			var month = getMonth(date[1]);
			
			issue_time = iday[0] + ' ' + month + ' ' + date[0];
			if(row.resolved==="1")
				{  
					if(!row.resolved_unresolved_datetime){
						resolved = 'Resolved'
					}else{  
						resolvedDate = row.resolved_unresolved_datetime;  
					var date = resolvedDate.split("-");
					var day = date[2];
					var iday = day.split(" ");  
					var month = getMonth(date[1]);
					resolvedDate = iday[0] + ' ' + month + ' ' + date[0];
					resolved = 'Resolved on '+resolvedDate;
					}
				}
			else
				resolved = '';
			  
				sHTML = sHTML +
				'<div class="container" >' +
				'<div class="row">' +
				'<div>' +
				'<div class="twt-wrapper">' +
				'<div class="panel panel-info">' +
				'<div class="panel-heading"><ul class="media-list"><li class="media">' +
				row.issue_item_description + ' (' + row.department_description + ')' +
				'<div class="pull-right">'+resolved+
				'</div></li></ul></div>' +
				'<div class="panel-body">' +
				'<ul class="media-list">' +
				'<li class="media">' +
				'<a href="#" class="pull-left">' +
				'<img width="150" height="150" id="img" class="img-rounded" src=' + getImages() + row.issue_image_path + row.issue_image_name + '>' +
				'</a>' +
				'<a class="pull-right" href="https://www.google.com/maps/place/' + row.lat + ',' + row.lng + '/" >' +
				'<img src="http://maps.googleapis.com/maps/api/staticmap?center=' + row.lat + ',' + row.lng + '&zoom=13&scale=false&size=150x150&maptype=terrain&key=AIzaSyAqpE1l1BgCartirn08RLtUffml9gR0Zpk&format=png&visual_refresh=true&markers=size:mid%7Ccolor:0xff0000%7Clabel:1%7C' + row.lat + ',' + row.lng + '&markers=size:mid%7Ccolor:0xff0000%7Clabel:1%7C' + row.lat + ',' + row.lng + '" alt="Google Map of ' + row.lat + ',' + row.lng + '">' +
				'</a>' +
				'<div class="media-body">' +
				'<span class="text-muted pull-right">' +
				'<small class="text-muted">' +
				issue_time +
				'</small>' +
				'</span>' +
				'<strong class="text-success">' +
				'Ward ' + row.ward +
				'</strong>' +
				'<p>' +
				row.issue_address +
				'</p>' +
				'</div>' +
				'</li>' +
				'<li class="text-muted">id ' + row.issue_id + '</li>' +
				(isnotss == 1 ? '<li class="text-muted">' + row.email + '</li>'  : '') +
				'</ul>' +
				'</div>' +
				'</div>' +
				'</div>' +
				'</div>' +
				'</div>' +
				'</div>';
		}
		if (data == -1) {
			var sCriteria = "";
			sCriteria = postalcode ? "Postal code: " + postalcode + "  " : "" +
				fromissuedate ? "From date: " + fromissuedate + "  " : "" +
				toissuedate ? "To date: " + toissuedate + "  " : "";
			sHTML = sHTML + "No issues found that match the criteria you provided<p>" +
				sCriteria +
				"<p>Please press back and try again";
		}
		$('#rp_pincode').append(sHTML);
		successCallBack(data);
	});
}
function getParameter(theParameter) {
	var params = window.location.search.substr(1).split('&');

	for (var i = 0; i < params.length; i++) {
		var p = params[i].split('=');
		if (p[0] == theParameter) {
			return decodeURIComponent(p[1]);
		}
	}
	return false;
}
/*
function displayIssueRows(issuesperpage, offs) {
console.log("offset: " + offs);

}
 */
flag = true;
$(window).scroll(function () {
	if ($(window).scrollTop() + $(window).height() == $(document).height()) {
		offset = $('#offset').val();
		limit = $('#limit').val();
		no_data = true;
		if (flag && no_data) {
			flag = false;
			$('#loader').show();
			displayIssueRows(offset, limit, function (data) {
				flag = true;
				$('#loader').hide();
				if (data != '') {

					offset = parseInt($('#offset').val());
					limit = parseInt($('#limit').val());
					$('#offset').val(offset + limit);
				} else {
					alert('No more data to show');
					no_data = false;
				}

			});

			/*
			$.ajax({
			url : 'ajax_html.php',
			method: 'post',
			data: {
			start : offset,
			limit : limit
			},
			success: function( data ) {
			flag = true;
			$('#loader').hide();
			if(data !=''){

			offset = parseInt($('#offset').val());
			limit = parseInt($('#limit').val());
			$('#offset').val( offset+limit );
			$('#timeline-conatiner').append( '<li class="year">'+year+'</li>');

			$('#timeline-conatiner').append( data );
			year--;
			}else{
			alert('No more data to show');
			no_data = false;
			}
			},
			error: function( data ){
			flag = true;
			$('#loader').hide();
			no_data = false;
			alert('Something went wrong, Please contact admin');
			}
			});
			 */
		}

	}
});
