<?php
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
$php_name = 'issueByWard';
include("../dbconn_sar_apk.php");
include("../mobile_common_data_sar.php");  
$issuebywardRows = array();
if ($mysqli) {
	
	$sql = " call issue_by_ward()";
	
	if ($verbose != 'N') {
		echo $sql . '<br>';
	}	
	if ($result = $mysqli->query($sql)) {
		if ($verbose != 'N') {
			echo "<br>sql result: ";
			var_dump($result);
			echo "<br>";
		}
	}
	if ($result && is_object($result))  {
		
		while ($row = $result->fetch_assoc()) {
		
			$issuebywardRows[] = $row;			
		}	
		$result->free();	// free result set
	}
	
	$mysqli->close();		// close connection
	if (count($issuebywardRows) > 0) {
		echo json_encode($issuebywardRows);
	} else {
		echo "-1";
	}
	
}else {
	echo "-1";
}