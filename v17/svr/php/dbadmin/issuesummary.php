<?php
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
$php_name = 'issue_summary';
include("../dbconn_sar_apk.php");
include("../mobile_common_data_sar.php");  

$catergoryRows = array();
$issuesubtypecode = empty($_REQUEST['issuesubtypecode']) || !isset($_REQUEST['issuesubtypecode']) ? 'NULL' :
		"'" . $_REQUEST['issuesubtypecode'] . "'" ;

if ($mysqli) {
	
	$sql = " call issue_summary(". $issuesubtypecode .")";
	
	if ($verbose != 'N') {
		echo $sql . '<br>';
	}	
	if ($result = $mysqli->query($sql)) {
		if ($verbose != 'N') {
			echo "<br>sql result: ";
			var_dump($result);
			echo "<br>";
		}
	}
	if ($result && is_object($result))  {
		
		while ($row = $result->fetch_assoc()) {
		
			$catergoryRows[] = $row;			
		}	
		$result->free();	// free result set
	}
	
	$mysqli->close();		// close connection
	if (count($catergoryRows) > 0) {
		echo json_encode($catergoryRows);
	} else {
		echo "-1";
	}
	
}else {
	echo "-1";
}