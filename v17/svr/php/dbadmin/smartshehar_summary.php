<?php
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
$php_name = 'smartshehar_summary';
include("../dbconn_sar_apk.php");
include("../mobile_common_data_sar.php");   
if ($mysqli) {
	
	$latitude = empty($_REQUEST['latitude']) || !isset($_REQUEST['latitude']) ? 'NULL' : $_REQUEST['latitude'];
	$longitude = empty($_REQUEST['longitude']) || !isset($_REQUEST['longitude']) ? 'NULL' : $_REQUEST['longitude'];
	$radious = empty($_REQUEST['radious']) || !isset($_REQUEST['radious']) ? '3' : $_REQUEST['radious'];
	$ward = empty($_REQUEST['ward']) || !isset($_REQUEST['ward']) ? 'NULL' : "'".$_REQUEST['ward']."'";
	$date = empty($_REQUEST['date']) || !isset($_REQUEST['date']) ? 'NULL' :"'". $_REQUEST['date']."'";
	$sql = " call v17_smartshehar_summary(". $appuserid .",".$latitude. ",".$longitude. "," .$radious. ",". $date. ",".$ward.  ")";  
	if ($verbose != 'N') { 
		echo $sql . '<br>';
	}	
	if ($result = $mysqli->query($sql)) {
		if ($verbose != 'N') {
			echo "<br>sql result: ";
			var_dump($result);
			echo "<br>";
		}
	}
	if ($result && is_object($result))  {
		$rowcount=mysqli_num_rows($result);
		
		while ($row = $result->fetch_assoc()) {
			echo json_encode ($row);
			break;	
		}		
		$result->free();	// free result set
	}
	
	$mysqli->close();		// close connection

	
}else {
	echo "-1";
}