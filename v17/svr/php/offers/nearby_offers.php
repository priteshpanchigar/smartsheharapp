<?php 
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
$php_name = 'dynamicNearbyOffers';
include("../mobile_common_data_sar.php");
include("../dbconn_sar_apk.php");
$discountOffersRows = array();
if ($mysqli) {
	
	$markerlat = empty($_REQUEST['markerlat']) || !isset($_REQUEST['markerlat']) ? 'NULL' : $_REQUEST['markerlat'];
	$markerlng = empty($_REQUEST['markerlng']) || !isset($_REQUEST['markerlng']) ? 'NULL' : $_REQUEST['markerlng'];
	$maxlat = empty($_REQUEST['maxlat']) || !isset($_REQUEST['maxlat']) ? 'NULL' : $_REQUEST['maxlat'];
	$maxlng = empty($_REQUEST['maxlng']) || !isset($_REQUEST['maxlng']) ? 'NULL' : $_REQUEST['maxlng'] ;
	$minlat = empty($_REQUEST['minlat']) || !isset($_REQUEST['minlat']) ? 'NULL' : $_REQUEST['minlat'];		
	$minlng = empty($_REQUEST['minlng']) || !isset($_REQUEST['minlng']) ? 'NULL' : $_REQUEST['minlng'];
	$orderby = empty($_REQUEST['orderby']) || !isset($_REQUEST['orderby']) ? 'NULL' :
		"'" . $_REQUEST['orderby'] . "'" ;
		
	//$trLat = empty($_REQUEST['trLat']) || !isset($_REQUEST['trLat']) ? 'NULL' : $_REQUEST['trLat'];		
	//$trLng = empty($_REQUEST['trLng']) || !isset($_REQUEST['trLng']) ? 'NULL' : $_REQUEST['trLng'];	
	
	$sql = " call dynamic_nearby_offers(".$appuserid."," . $clientdatetime.  "," . $markerlat."," . $markerlng. "," . $maxlat. "," . $maxlng."," 
										 . $minlat. "," . $minlng. "," . $orderby."," .$lat . "," .$lng .")";
		
		
	if ($verbose != 'N') {
		echo $sql . '<br>';
	}	
	if ($result = $mysqli->query($sql)) {
	
		while ($row = $result->fetch_assoc()) {
		
			$discountOffersRows[] = $row;			
		}
 		
		$result->free();	// free result set
	}
	$mysqli->close();		// close connection
	
	if (count($discountOffersRows) > 0) {
		echo json_encode($discountOffersRows);
	} else {
		echo "-1";
	}
}
