<?php 
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
$php_name = 'availedOffers';
include("../mobile_common_data_sar.php");
include("../dbconn_sar_apk.php");
$availedOffersRows = array();
if ($mysqli) {
	
	$discountid = empty($_REQUEST['discountid']) || !isset($_REQUEST['discountid']) ? 'NULL' : $_REQUEST['discountid'];
	
	
	$sql = " call availed_offers(".$appuserid."," . $discountid.  "," . $clientdatetime.")";
		
		
	if ($verbose != 'N') {
		echo $sql . '<br>';
	}	
	if ($result = $mysqli->query($sql)) {
	
		while ($row = $result->fetch_assoc()) {
		
			$availedOffersRows[] = $row;			
		}
 		
		$result->free();	// free result set
	}
	$mysqli->close();		// close connection
	
	if (count($availedOffersRows) > 0) {
		echo json_encode($availedOffersRows);
	} else {
		echo "-1";
	}
}
