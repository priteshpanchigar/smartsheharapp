<?php
// Declare the class
class GoogleUrlApi {
	      
	// Constructor 
	function GoogleURLAPI($key,$apiURL = 'https://www.googleapis.com/urlshortener/v1/url') {
		// Keep the API Url
		$this->apiURL = $apiURL.'?key='.$key;
		//echo $this->apiURL;
	}
	
	// Shorten a URL
	function shorten($url) {
		// Send information along
		$response = $this->send($url);
		//echo "response ".$response;
		// Return the result
		return isset($response['id']) ? $response['id'] : false;
	}
	
	
	
	// Send information to Google
	function send($url) {
		// Create cURL
		$ch = curl_init();
	
			curl_setopt($ch,CURLOPT_URL,$this->apiURL);
			curl_setopt($ch,CURLOPT_POST,1);
			curl_setopt($ch,CURLOPT_POSTFIELDS,json_encode(array("longUrl"=>$url)));
			curl_setopt($ch,CURLOPT_HTTPHEADER,array("Content-Type: application/json"));
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLINFO_HEADER_OUT, true);
		
		// Execute the post
		$result = curl_exec($ch);
	//	var_dump($output);
		if (empty($result)){
		print "Nothing returned from url.<p>";
		  }
		  else{
			  print $result;
		  }
		
		if(curl_exec($ch) === false)
{
    echo 'Curl error: ' . curl_error($ch);
}
else
{
    echo 'Operation completed without any errors';
}
		
		if (!curl_errno($ch)) {
  $info = curl_getinfo($ch);
  echo 'Took ', $info['total_time'], ' seconds to send a request to ', $info['url'], "\n";
}
		
		$http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		//echo "code ". $http_code;
         echo "<br>CURLINFO_EFFECTIVE_URL " . curl_getinfo($ch, CURLINFO_EFFECTIVE_URL);
		 echo "<br>CURLINFO_HTTP_CODE " . curl_getinfo($ch, CURLINFO_HTTP_CODE);
		 echo "<br>CURLINFO_FILETIME " . curl_getinfo($ch, CURLINFO_FILETIME);
		 echo "<br>CURLINFO_TOTAL_TIME " . curl_getinfo($ch, CURLINFO_TOTAL_TIME);
		 echo "<br>CURLINFO_REDIRECT_URL " . curl_getinfo($ch, CURLINFO_REDIRECT_URL);
		 echo "<br>CURLINFO_SPEED_UPLOAD " . curl_getinfo($ch, CURLINFO_SPEED_UPLOAD);
		 echo "<br>CURLINFO_HEADER_SIZE " . curl_getinfo($ch, CURLINFO_HEADER_SIZE);
		 echo "<br>CURLINFO_CONTENT_LENGTH_UPLOAD " . curl_getinfo($ch, CURLINFO_CONTENT_LENGTH_UPLOAD);
		 echo "<br>CURLINFO_HTTP_CONNECTCODE  " . curl_getinfo($ch, CURLINFO_HTTP_CONNECTCODE );
		
		// echo "" . ;
		// Close the connection  
		curl_close($ch);
		// Return the result
		return json_decode($result,true);
	}		
}

$key = 'AIzaSyBwVc9zncH9wtJNygaG5yNSLoumg-O1woI';
$googer = new GoogleURLAPI($key);
 
// Test: Shorten a URL
$shorURL = $googer->shorten("http://smartshehar.com/alpha/smartsheharapp/v17/svr/php/url_shortner.php");    
echo $shorURL; // returns http://goo.gl/DbkFol
 
?>