<?php
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
$php_name = 'getFareCalculation';
include("../dbconn_sar_apk.php");
include("../mobile_common_data_sar.php");
//echo $php_name;
if ( $mysqli ) {
	
	$inCity = isset($_REQUEST['city']) ? "\"" . $_REQUEST['city'] . "\"" : 'NULL';
	
	$sql = "call rs_getfarecalculation(" . $inCity. ")";
	
	if ($verbose != 'N') {
		echo $sql;
	}
	if ($result = $mysqli->query($sql)) {
		/* fetch associative array */
		while ($row = $result->fetch_assoc()) {
			$farecalculation[] = $row;
		}
		$result->free();	// free result set
	}
	$mysqli->close();
	if (count($farecalculation) > 0) {
		echo json_encode($farecalculation);
	} else {
		echo "-1";
	}
	
	
} else {
	echo "-2"; // "Connection to db failed";
}
