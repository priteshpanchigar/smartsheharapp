<?php 
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
$php_name = 'daUserAccess';
include("dbconn_sar_apk.php");
$errors = array();
if ($mysqli) {
	include("mobile_common_data_sar.php");
	
	$android_release_version = isset($_REQUEST['android_release_version']) ? "'" . $_REQUEST['android_release_version'] . "'" : 'NULL';
	$android_sdk_version = isset($_REQUEST['android_sdk_version']) ? $_REQUEST['android_sdk_version'] : 'NULL';
	$gcmregistration_id = isset($_REQUEST['gcmregid']) ?
	"'" . $_REQUEST['gcmregid'] . "'" : 'NULL';	

		$sql = "call v14_da_user_access(" . $gcmregistration_id . ",". $mobile_common_data . "," . $android_release_version ."," . $android_sdk_version . ")";
	 

	if ($verbose != 'N') {
		echo '<br>sql; '.  $sql . '<br>';
		echo '<br>mobile_common_data; '.  $mobile_common_data . '<br>';
	}		
	
		if ($result = $mysqli->query($sql)) {
		while ($row = $result->fetch_assoc()) {
			echo json_encode($row);
			break;
		
		}
		$mysqli->close();
	} else {
		echo "-1"; // something went wrong, probably sql failed
	}
} else {
	echo "-2"; // "Connection to db failed";
}?>