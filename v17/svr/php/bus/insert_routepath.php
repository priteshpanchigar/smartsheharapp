<?php
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
$php_name = "insertRoutePath";
$resultrows = array();
include("../json_error.php");
include("../dbconn_sar_apk.php");
include("../mobile_common_data_sar.php");

//print_r($_POST['path']);

if ($mysqli) {
	if(isset($_REQUEST['path']) && !empty($_REQUEST['path'])){
		$inpath = $_REQUEST['path'];
	}
	if(empty($inpath)) {
		echo -1;
		return -1;
	}
	$routecode = empty($_REQUEST['routecode']) || !isset($_REQUEST['routecode']) ? 'NULL' :
		"'" . $_REQUEST['routecode'] . "'" ;
	echo $routecode;
	$path = json_decode($inpath);
	$length = count($path);
	echo $length;
	$valuessql = "";
	$insertsql = 'insert into b_routepath(routecode,  lat, lng) values ';
	$delim = '';
	if ($routecode != -1){
		foreach($path as $obj) {
			
			$lat = $obj->lat;
			$lng = $obj->lng;
			$valuessql =  $valuessql . $delim . '(' . $routecode . "," . $lat . "," . $lng . ")" ;
			$delim = ',';
		}
		
		if ($verbose != 'N') {
			echo "<br>Sub locality: " .$insertsql . $valuessql . "<br>";
		}		

		$runningsql = $insertsql . $valuessql;
		echo $runningsql;
		if ($runresult = $mysqli->query($runningsql)) {
			echo trim($routecode);
		} else {
			printf("Errormessage: %s\n", $mysqli->error);
			// echo -1; // something went wrong, probably sql failed
		}
		
	}	
} else {
	echo -2; ;
}
$mysqli->close();