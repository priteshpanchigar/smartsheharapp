<?php
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
$php_name = 'insertBusTrips';
include("../dbconn_sar_apk.php");
include("../mobile_common_data_short.php");
if ( $mysqli ) {
	$creationdatetime = empty($_REQUEST['cdt']) || !isset($_REQUEST['cdt']) ? 'NULL' : "'" . $_REQUEST['cdt'] . "'" ;
	$vehicleno = empty($_REQUEST['vehicleno']) || !isset($_REQUEST['vehicleno']) ? 'NULL' : "'" . $_REQUEST['vehicleno'] . "'" ;
	$buslabel = empty($_REQUEST['buslabel']) || !isset($_REQUEST['buslabel']) ? 'NULL' : "'" . $_REQUEST['buslabel'] . "'" ;
	$fromstop = empty($_REQUEST['fromstop']) || !isset($_REQUEST['fromstop']) ? 'NULL' : "'" . $_REQUEST['fromstop'] . "'" ;	
	$tostop = empty($_REQUEST['tostop']) || !isset($_REQUEST['tostop']) ? 'NULL' : "'" . $_REQUEST['tostop'] . "'" ;
	$tripstatus = empty($_REQUEST['tripstatus']) || !isset($_REQUEST['tripstatus']) ? 'NULL' : "'" . $_REQUEST['tripstatus'] . "'" ;
	
	$fromstopserial = empty($_REQUEST['fromstopserial']) || !isset($_REQUEST['fromstopserial']) ? 'NULL' : "'" . $_REQUEST['fromstopserial'] . "'" ;
	$tostopserial = empty($_REQUEST['tostopserial']) || !isset($_REQUEST['tostopserial']) ? 'NULL' : "'" . $_REQUEST['tostopserial'] . "'" ;
	$direction = empty($_REQUEST['direction']) || !isset($_REQUEST['direction']) ? 'NULL' : "'" . $_REQUEST['direction'] . "'" ;
	
	
	$sql = "call insert_bustrip(" . $appuserid . "," . $vehicleno . "," . $buslabel . 
	"," . $fromstop .  "," . $tostop . "," . $fromstopserial . "," . $tostopserial . 
	"," . $tripstatus .  "," . $creationdatetime . "," . $clientdatetime .
	"," . $appuserid . "," . $location_data. "," . $direction.")";
	
	if ($verbose != 'N') {
		echo '<br>sql:<br>' . $sql;
	}
	
	if ($result = $mysqli->query($sql)) {	
		if ($result && is_object($result))  {
			while ($row = $result->fetch_assoc()) {
				echo json_encode($row);
				break;
			}
		}
		$mysqli->close();
	}		else {
		echo "-1"; // something went wrong, probably sql failed
	}
} else {
	echo "-2"; // "Connection to db failed";
}