<?php 
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
$php_name = "CreateTripPathBus";
$resultrows = array();
include("../json_error.php");
include("../dbconn_sar_apk.php");
if ($mysqli) { 
	include("../mobile_common_data_short.php");
	date_default_timezone_set('Asia/Calcutta');
	$verbose = isset($_REQUEST['verbose']) ? "'" . $_REQUEST['verbose'] . "'" : 'N';
	$currentdate = date('m/d/Y h:i:s a', time());
	$timeinms = strtotime($currentdate) * 1000;
	if(isset($_REQUEST['path']) && !empty($_REQUEST['path'])){
		$inpath = $_REQUEST['path']; 
	}
	if(empty($inpath)) {
		echo -1; 
		return -1;
	}
	$tripid = isset($_REQUEST['tripid']) ? $_REQUEST['tripid'] : -1;
	$path = json_decode($inpath)->path;
	$ctr = 0;
	$length = count($path);
	$valuessql = "";
	$insertsql = 'insert into bus_trips_path(trip_id, nodeno, lat, lng) values ';
	$delim = '';
	if ($tripid != -1){
		foreach($path as $obj) {
			$ctr = $ctr + 1; // call 
			$lat = $obj->lat;
			$lng = $obj->lng;
			$valuessql =  $valuessql . $delim . '(' . $tripid . "," . $ctr . "," .
			$lat . "," . $lng . ")" ;
			$delim = ',';
		}
		if ($verbose != 'N') {
			echo "<br>Sub locality: " .$insertsql . $valuessql . "<br>";
		}		
		$runningsql = $insertsql . $valuessql;
		if ($runresult = $mysqli->query($runningsql)) {
			echo trim($tripid);
		} else {
			printf("Errormessage: %s\n", $mysqli->error);
			// echo -1; // something went wrong, probably sql failed
		}
	}	
} else {
	echo -2; ;
}$mysqli->close();