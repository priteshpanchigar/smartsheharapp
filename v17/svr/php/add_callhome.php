<?php
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
$php_name = 'add_callhome';
include("dbconn_sar_apk.php");
include("mobile_common_data_sar.php");
if ( $mysqli ) {
	
	$android_release_version = isset($_REQUEST['android_release_version']) ? "'" . $_REQUEST['android_release_version'] . "'" : 'NULL';
	$android_sdk_version = isset($_REQUEST['android_sdk_version']) ? $_REQUEST['android_sdk_version'] : 'NULL';
	$content = isset($_REQUEST['content']) ? "\"" . $_REQUEST['content'] . "\"" : 'NULL';
	
	$sql = "call v17_add_callhome(" . $email . "," . $imei . "," . $lat .  "," . $lng . 
	"," . $accuracy . ",".  $provider . "," . $carrier . 
	"," . $product . "," . $manufacturer .  "," . $android_release_version .  
	"," . $android_sdk_version .  "," . $clientdatetime .  "," . $ip .
	"," . $useragent .  "," . $version . "," . $versioncode . "," . $app . 
	"," . $locationdatetime . "," . $content .")";
	
	if ($verbose != 'N') {
		echo '<br>sql:<br>' . $sql;
	}
	
	if ($result = $mysqli->query($sql)) {	
		if ($result && is_object($result))  {
			while ($row = $result->fetch_assoc()) {
				echo json_encode($row);
				break;
			}
		}
		
	}		else {
		echo "-1"; // something went wrong, probably sql failed
	}
	$mysqli->close();
} else {
	echo "-2"; // "Connection to db failed";
}