<?php
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
$php_name = 'insertStoreDdata';
include("../dbconn_sar_apk.php"); 
include("../mobile_common_data_sar.php");
if ($mysqli ){
	
	$googleaddress = empty($_REQUEST['googleaddress']) || !isset($_REQUEST['googleaddress']) ? 'NULL' :
		"'" . $_REQUEST['googleaddress'] . "'" ;		
	
	$locality = isset($_REQUEST['locality']) ? "\"" . $_REQUEST['locality'] . "\"" : 'NULL';
	
	$sublocality = isset($_REQUEST['sublocality']) ? "\"" . $_REQUEST['sublocality'] . "\"" : 'NULL';
	
	$postalcode = isset($_REQUEST['postalcode']) ? "\"" . $_REQUEST['postalcode'] . "\"" : 'NULL';	
	
	$route = isset($_REQUEST['route']) ? "\"" . $_REQUEST['route'] . "\"" : 'NULL';	
	
	$neighborhood = isset($_REQUEST['neighborhood']) ? "\"" . $_REQUEST['neighborhood'] . "\"" : 'NULL';	
	
	$administrative_area_level_2 = isset($_REQUEST['administrative_area_level_2']) ? "\"" . $_REQUEST['administrative_area_level_2'] . "\"" : 'NULL';
	
	$administrative_area_level_1 = isset($_REQUEST['administrative_area_level_1']) ? "\"" . $_REQUEST['administrative_area_level_1'] . "\"" : 'NULL';	
	
	$latitude = isset($_REQUEST['latitude']) ? "\"" . $_REQUEST['latitude'] . "\"" : 'NULL';	
	
	$longitude = isset($_REQUEST['longitude']) ? "\"" . $_REQUEST['longitude'] . "\"" : 'NULL';	
	
	
	$debitcard = empty($_REQUEST['debitcard']) || !isset($_REQUEST['debitcard']) ? 0 :  $_REQUEST['debitcard'] ;
	
	$creditcard = empty($_REQUEST['creditcard']) || !isset($_REQUEST['creditcard']) ? 0 :  $_REQUEST['creditcard'] ;
	
	$wallet = empty($_REQUEST['wallet']) || !isset($_REQUEST['wallet']) ? 0 :  $_REQUEST['wallet'] ;
	$UPI = empty($_REQUEST['UPI']) || !isset($_REQUEST['UPI']) ? 0 :  $_REQUEST['UPI'] ;
	
	$cheque = empty($_REQUEST['cheque']) || !isset($_REQUEST['cheque']) ? 0 :  $_REQUEST['cheque'] ;

	$storename = empty($_REQUEST['storename']) || !isset($_REQUEST['storename']) ? 'NULL' :
			"'" . $_REQUEST['storename'] . "'" ;

	$storeno = empty($_REQUEST['storeno']) || !isset($_REQUEST['storeno']) ? 'NULL' :
			"'" . $_REQUEST['storeno'] . "'" ;

	$contactperson = empty($_REQUEST['contactperson']) || !isset($_REQUEST['contactperson']) ? 'NULL' :
			"'" . $_REQUEST['contactperson'] . "'" ;

	$contactemail = empty($_REQUEST['contactemail']) || !isset($_REQUEST['contactemail']) ? 'NULL' :
		"'" . $_REQUEST['contactemail'] . "'" ;

	$contactno1 = empty($_REQUEST['contactno1']) || !isset($_REQUEST['contactno1']) ? 'NULL' :
		"'" . $_REQUEST['contactno1'] . "'" ;

	$contactno2 = empty($_REQUEST['contactno2']) || !isset($_REQUEST['contactno2']) ? 'NULL' :
		"'" . $_REQUEST['contactno2'] . "'" ;

	$workinghours = empty($_REQUEST['workinghours']) || !isset($_REQUEST['workinghours']) ? 'NULL' :
		"'" . $_REQUEST['workinghours'] . "'" ;

	$closeddays = empty($_REQUEST['closeddays']) || !isset($_REQUEST['closeddays']) ? 'NULL' :
		"'" . $_REQUEST['closeddays'] . "'" ;
		
	$walletname = empty($_REQUEST['walletname']) || !isset($_REQUEST['walletname']) ? 'NULL' :
		"'" . $_REQUEST['walletname'] . "'" ;
	
	$comment = empty($_REQUEST['comment']) || !isset($_REQUEST['comment']) ? 'NULL' :
		"'" . $_REQUEST['comment'] . "'" ;	

	$categoryid = empty($_REQUEST['categoryid']) || !isset($_REQUEST['categoryid']) ? 'NULL' :"'". $_REQUEST['categoryid']."'";
	
	$dbtransactionfee = empty($_REQUEST['dbtransactionfee']) || !isset($_REQUEST['dbtransactionfee']) ? 'NULL' : "'".$_REQUEST['dbtransactionfee']."'";
	
	$cctransactionfee = empty($_REQUEST['cctransactionfee']) || !isset($_REQUEST['cctransactionfee']) ? 'NULL' :"'". $_REQUEST['cctransactionfee']."'";
	
	$wallettrasactionfee = empty($_REQUEST['wallettrasactionfee']) || !isset($_REQUEST['wallettrasactionfee']) ? 'NULL' : "'".$_REQUEST['wallettrasactionfee']."'";
  

	$sql = "call insert_store_data(" .$clientdatetime ."," .$googleaddress .
			"," .$locality .", " .$sublocality . ", ".$postalcode . "," .$route .
			"," .$neighborhood . "," .$administrative_area_level_2 . ",". $administrative_area_level_1 .
			"," .$latitude.", ".$longitude . ", ".$storename . "," .$storeno .", ". $contactperson .
			"," .$contactemail.", ".$contactno1 . ", ".$contactno2 . ", ".$workinghours .", ".$closeddays .
			"," .$categoryid .", ".$debitcard .", ".$dbtransactionfee .", ".$creditcard . 
			"," .$cctransactionfee . ", ".$wallet . ", ".$walletname .
			"," .$wallettrasactionfee . ", ".$cheque  . ", ".$UPI . ", ".$comment . ")";
	
	
	if ($verbose != 'N') {
		echo '<br>' . $sql . '<br>';
	}
	$result = $mysqli->query($sql);
	if(is_object($result)) {	
		while ($row = $result->fetch_assoc()) {
			echo json_encode($row);
			break;
		}
		
	}	
	$mysqli->close();
} else {
	echo "-1";
}