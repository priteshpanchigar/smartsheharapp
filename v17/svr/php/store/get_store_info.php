<?php
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
$php_name = 'getStoreInfo';
include("../dbconn_sar_apk.php"); 
include("../mobile_common_data_sar.php");
if ($mysqli) {
	$storeInfoRows = array();
	$lat = empty($_REQUEST['lat']) || !isset($_REQUEST['lat']) ? 'NULL' : $_REQUEST['lat'];
	$lng = empty($_REQUEST['lng']) || !isset($_REQUEST['lng']) ? 'NULL' : $_REQUEST['lng'];
	$sql =  " call get_store_info(" .$lat ."," .$lng .")";
	//$sql = " call get_store_category()";
	if ($verbose != 'N') {
		echo $sql . '<br />';
	}
	$result = $mysqli->query($sql);
	$hasResult = false;
	if (is_object($result)) {
		if ($result) {
			while ($row = $result->fetch_assoc()) {
				$hasResult = true;
				$storeInfoRows[] = $row;
				
			}
			$result->free(); // free result set
		}
	}
	
	if ($hasResult) {
		$storeInfoRows = array_filter($storeInfoRows);
		if (!empty($storeInfoRows)) {
			echo json_encode($storeInfoRows);
		}
	}
	else {
		echo 0;
	}
	$mysqli->close(); // close connection
}
else {
	echo "-1";
}
?>