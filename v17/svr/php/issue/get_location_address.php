<?php 
header('Access-Control-Allow-Origin: *');
$php_name = "getLocationAddressLatLng";
include("../mobile_common_data_sar.php");
include("../dbconn_sar_apk.php");

if ($mysqli) {
	$latitude = isset($_REQUEST['latitude']) ? "\"" . $_REQUEST['latitude'] . "\"" : 'NULL';

	$longitude = isset($_REQUEST['longitude']) ? "\"" . $_REQUEST['longitude'] . "\"" : 'NULL';

	$sql = " call get_location_address("  . $latitude . "," . $longitude . ")";
	if ($verbose != 'N') {
		echo $sql . '<br>';
	}
	$result = $mysqli->query($sql) or die(mysql_error());
	$rowcount=mysqli_num_rows($result);
	if ($result) {
		while ($row = $result->fetch_assoc())  {
			
			echo json_encode($row);
			break;
		}
		$result->free();	// free result set
	}
	if ($rowcount == 0) {
		echo '';
	}
	$mysqli->close();		// close connection
}