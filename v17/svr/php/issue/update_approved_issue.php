<?php
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
$php_name = 'UpdateApprovedIssue';
include("../dbconn_sar_apk.php"); 
include("../mobile_common_data_sar.php");
$errors = array();
if ( $mysqli ) {
		
	$tobesenttoauthority = isset($_REQUEST['tobesenttoauthority']) ? $_REQUEST['tobesenttoauthority'] : 'NULL';
	
	$rejected = isset($_REQUEST['rejected']) ? $_REQUEST['rejected'] : 'NULL';
	
	$issueid = isset($_REQUEST['issueid']) ? $_REQUEST['issueid'] : 'NULL';
	
	$reasoncode = isset($_REQUEST['reasoncode']) ? "\"" . $_REQUEST['reasoncode'] . "\"" : 'NULL';
	
	$clientdatetime = isset($_REQUEST['clientdatetime']) ? "\"" . $_REQUEST['clientdatetime'] . "\"" : 'NULL';	
	$othercomment  = isset($_REQUEST['othercomment']) ? "\"" . $_REQUEST['othercomment'] . "\"" : 'NULL';

	
	$sql = "call update_approved_issue(" .$tobesenttoauthority .  ",".  $rejected .  
		",". $issueid . ",". $reasoncode . ",". $clientdatetime .  ",". $othercomment .")";
	if ($verbose != 'N') {
		echo $sql . '<br>';
	}
	if ($result = $mysqli->query($sql)) {	
		while ($row = $result->fetch_assoc()) {
			echo json_encode($row);
			break;
		} 
		$mysqli->close();
	}		else {
		echo "-1"; // something went wrong, probably sql failed
	}
} else {
	echo "-2"; // "Connection to db failed";
}

	
include("issue_approved_notification.php");