<?php
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
$php_name = 'issueEmail';
include("../dbconn_sar_apk.php");
include("../mobile_common_data_sar.php");
include("../sendmail.php");
$memberemail = "";
$name = "";  
$message = isset($_REQUEST['message']) ? $_REQUEST['message']  : 'NULL';
$email = isset($_REQUEST['email']) ? $_REQUEST['email']  : 'NULL';
$issueimagefilename = isset($_REQUEST['issueimagefilename']) ? $_REQUEST['issueimagefilename']  : 'NULL';
$subject = isset($_REQUEST['subject']) ?  $_REQUEST['subject'] : 'NULL';
$uniquekey= isset($_REQUEST['uniquekey']) ?  $_REQUEST['uniquekey'] : 'NULL';
$cc = $email ." , issues_smartshehar@yahoo.com";
$bcc =  "issues_smartshehar@yahoo.com";
$issueid = isset($_REQUEST['issueid']) ? $_REQUEST['issueid'] : 'NULL';	
$ignoresent = isset($_REQUEST['ignoresent']) ? $_REQUEST['ignoresent'] : 0;	

$departmentcode;
if($mysqli)
{	$sql = "call get_Emailid_Using_Postal_Code(" .$issueid . ',' .$ignoresent . ")";
	
		
	if ($verbose != 'N') {
		echo $sql . '<br>';
		
	}
	$result = $mysqli->query($sql);  
	
	
	if(is_object($result)) {
	   
		while ($row = $result->fetch_assoc()) {
			
			$departmentcode = $row['department_code'];
		
			if (empty($departmentcode)) {
				echo "empty";
			}else{  
				$memberemail = $row["email_id"] ;
				$subject = "Reporting an Issue  " .$row['issue_item_description']. " at " . $row['formatted_address'];
				
				if($departmentcode === "MU")
				{
					$message=municipal($row['issue_item_description'],$row['formatted_address'],$row['issue_time'],
					$row['ward'],$row['subcategory'],$row['image_count'],$row['latitude'],$row['longitude']);
				}else{
					$message=traffic($row['issue_item_description'],$row['formatted_address'],$row['issue_time'],
					$row['subcategory'],$row['image_count'],$row['latitude'],$row['longitude']);
				}
			}    
			break;
		}
		
		$mysqli->close();
	}
	
}else{
	echo "-1";
}
function  municipal($description,$address,$issuetime,$ward,$category,$count,$latitude, $longitude)
{	$message;
	
	$space = "";
	$date = "Date – ". $issuetime ;
		
	$to = "To:<br>".
				"Hon. Assistant Commissioner,<br>". 
				"Ward [". $ward.".]"; 
	$letter_subject="<b>Subject – Complaint from the residents in your ward:".
					"<br><br>&nbsp;&nbsp;&nbsp;&nbsp;Category – ". $category.
					"<br>&nbsp;&nbsp;&nbsp;&nbsp;Sub-Category - ". $description."</b>";
	
	if($count == '0')
	{  
		$photo_attached="";
	}
	else{
		$photo_attached="<br><br>Photo of the issue is attached.";
	}  
			   
			$content = "Dear Sir,<br><br>On behalf of the residents 
			residing in ward-[". $ward ."], we would like to bring your attention 
			to the following issue. ".
			"<br><br><p style=\"background-color:lightgrey;\"><b>Issue Submission Time – </b>". $issuetime.
			"<br><br><b>Issue Category – </b>". $category.
			"<br><b>Issue Sub-Category – </b>". $description.
			"<br><br><b>Issue Location  – latitude: </b>". $latitude . "<b>, longitude: </b>". $longitude.
			"<br><b>Issue Address - </b>". $address.
			"<br><a href= \"http://maps.googleapis.com/maps/api/staticmap?center=". $latitude .",+".$longitude. "&zoom=15&scale=false&size=800x800&maptype=terrain&key=AIzaSyAqpE1l1BgCartirn08RLtUffml9gR0Zpk&format=png&visual_refresh=true&markers=size:mid%7Ccolor:0xff0000%7Clabel:1%7C". $latitude .",+".$longitude. "&markers=size:mid%7Ccolor:0xff0000%7Clabel:1%7C". $latitude .",+".$longitude. "\">Click here to see location on the Map</a></p>".
			$photo_attached .
			"<br><br>The following issue was registered using the SmartShehar App, ".
			"and as responsible citizens, we are bringing it to your notice.". 
			"<br><br>We sincerely hope you will direct your staff to give serious ". 
			"attention to this issue and take immediate remedial action. ".
			"<br><br><br>Sincerely,";
			$message = $space."  ".$date.  
						"<br>". $to .
						"<br><br>". $letter_subject .  
						"<br><br>".$content.
						"<br><br> <a class=\"pull-right\" href=\"https://www.google.com/maps/place/". $latitude ." ,+".$longitude. "/\" >
						<img width=300px src=\"http://maps.googleapis.com/maps/api/staticmap?center=". $latitude ." ,+".$longitude. "&zoom=15&scale=false&size=150x150&maptype=terrain&key=AIzaSyAqpE1l1BgCartirn08RLtUffml9gR0Zpk&format=png&visual_refresh=true&markers=size:mid%7Ccolor:0xff0000%7Clabel:1%7C". $latitude ." ,+".$longitude. "&markers=size:mid%7Ccolor:0xff0000%7Clabel:1%7C". $latitude ." ,+".$longitude. " alt=\"Google Map of ". $latitude ." ,+".$longitude. "\">
						</a>";
	return $message;
}
function traffic($description,$address,$issuetime,$category,$count,$latitude, $longitude)
{
	  
	$space = "";
	$date = "Date – ". $issuetime ;
		
	$to = "To:<br>".
				"Addl. Commissioner of Police, Traffic, H.Q";
	$letter_subject="<b>Subject – Traffic Rule Violation reported by citizens".
					"<br><br>&nbsp;&nbsp;&nbsp;&nbsp;Category – ". $category.
					"<br>&nbsp;&nbsp;&nbsp;&nbsp;Sub-Category - ". $description."</b>";
	
	if($count == '0')
	{ 
		$photo_attached="";
	}
	else{
		$photo_attached="<br><br>Photo of the issue is attached.";
	}
			  
			$content = "Dear Sir,<br><br>On behalf of the citizens, we would like to bring your attention to a new".
			"<br>complaint logged using our platform, SmartShehar.".
			"<br><br><p style=\"background-color:lightgrey;\"><b>Issue Submission Time – </b>". $issuetime.
			"<br><br><b>Issue Category – </b>". $category.
			"<br><b>Issue Sub-Category – </b>". $description.
			"<br><br><b>Issue Location  – latitude: </b>". $latitude . "<b>, longitude: </b>". $longitude.
			"<br><b>Issue Address - </b>". $address.
			"<br><a href= \"http://maps.googleapis.com/maps/api/staticmap?center=". $latitude .",+".$longitude. "&zoom=15&scale=false&size=800x800&maptype=terrain&key=AIzaSyAqpE1l1BgCartirn08RLtUffml9gR0Zpk&format=png&visual_refresh=true&markers=size:mid%7Ccolor:0xff0000%7Clabel:1%7C". $latitude .",+".$longitude. "&markers=size:mid%7Ccolor:0xff0000%7Clabel:1%7C". $latitude .",+".$longitude. "\">Click here to see location on the Map</a></p>".  
			$photo_attached .
			"<br><br>SmartShehar is a tool that is designed to help authorities by enabling them to  ".
			"prioritize issues to focus on. These complaints will allow your department to ". 
			"identify where maximum traffic violations take place around the city & take ". 
			"remedial action accordingly. We believe that the limited resources at your  ". 
			"disposal can be used efficiently this way.".   
			"<br><br>We sincerely hope you will direct your staff to pay serious attention to this issue". 
			"and improve Traffic Rule enforcement for better traffic flow. ".
			"<br><br><br>Sincerely,";
			$message = $space."  ".$date.    
						"<br>". $to .
						"<br><br>". $letter_subject .
						"<br><br>".$content.
						"<br><br> <a class=\"pull-right\" href=\"https://www.google.com/maps/place/". $latitude ." ,+".$longitude. "/\" >
						<img width=300px src=\"http://maps.googleapis.com/maps/api/staticmap?center=". $latitude ." ,+".$longitude. "&zoom=15&scale=false&size=150x150&maptype=terrain&key=AIzaSyAqpE1l1BgCartirn08RLtUffml9gR0Zpk&format=png&visual_refresh=true&markers=size:mid%7Ccolor:0xff0000%7Clabel:1%7C". $latitude ." ,+".$longitude. "&markers=size:mid%7Ccolor:0xff0000%7Clabel:1%7C". $latitude ." ,+".$longitude. " alt=\"Google Map of ". $latitude ." ,+".$longitude. "\">
						</a>";
	return $message;
}
if ($verbose != 'N') {
		echo '<br>' . 'memberemail: ' . $memberemail . ' . cc: ' . $cc .
		', bcc: ' . $bcc . '<br> '; 
		
		echo '<br>';
		
	}
if (!empty($memberemail)) {
	if ($verbose != 'N') {
		echo '<br>Sending email to: ' . $memberemail . ', cc: ' . $cc .
		', bcc: ' . $bcc . '<br> ';
		
		echo '<br>';
		
	}
	//$arr = explode(",", $memberemail);
	//$arr = array_filter($arr);
	//foreach($arr as $emailid ) {
		sendMail($memberemail, $name, $cc, $subject, $message, $issueimagefilename,$bcc);
	//}
	

}else{
	echo "-1";
}

	include("issue_registration_notification.php");