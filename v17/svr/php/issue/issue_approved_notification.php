<?php
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
$php_name = 'issue_approved_notification';
include("../dbconn_sar_apk.php");
include("../mobile_common_data_sar.php");
include("../gcmSendMessage.php");
$resultrows = array();  
$basemessage="";

if($mysqli) 
{	$sql = "call issue_approved_notification(" .$issueid . ")";
	if ($verbose != 'N') {
		echo '<br> sql ' . $sql . '<br>';
		
	}
	$result = $mysqli->query($sql);
	//{ var_dump($result); }
	if(is_object($result)) {	
		while ($row = $result->fetch_assoc()) {
			
			array_push($resultrows, $row['gcm_registration_id']);
			$issue_category= $row['category'];
			$issue_subcategory = $row['subcategory'];
			$formatted_address = $row['formatted_address'];
			$issue_time = $row['issue_time'];
			$uniquekey = $row['unique_key'];

		$basemessage = "{\"issue_subcategory\":\"" . $issue_subcategory .
		"\", \"issue_category\":\"" . $issue_category .
		"\", \"issue_address\":\"" . $formatted_address .
		"\", \"issue_time\":\"" . $issue_time .
		"\", \"message\":" ."\"". "Issue Approval Status" . 
		"\", \"issueid\":\"" . $issueid .  
		"\", \"uniquekey\":\"" . $uniquekey.  "\"}";
		 
		//if ($verbose == 'Y') { echo $basemessage . "<br>"; }
		
		
	}
	}
if ($verbose == 'Y') { var_dump($resultrows); }
		$mysqli->close();    
	gcmSendMessage($resultrows, $basemessage);
	//sendNotification($resultrows, $appuserid, $offence_type, $offence_address, 
	//$offence_time,$uniquekey );
}else{
	echo "connection failed";
}



