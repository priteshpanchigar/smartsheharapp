<?php
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
$php_name = 'updateNotificationFlag';
include("../dbconn_sar_apk.php"); 
include("../mobile_common_data_sar.php");
if ($mysqli) {
	
	$notificationflag = isset($_REQUEST['notificationflag']) ? $_REQUEST['notificationflag'] : 'NULL';
	
	$sql = " call update_notification_flag(" .  $notificationflag. ", " . $appuserid  . ")";
	if ($verbose != 'N') {
		echo $sql . '<br>';
	}
	$result = $mysqli->query($sql);
	$mysqli->close();		// close connection 
}

?>