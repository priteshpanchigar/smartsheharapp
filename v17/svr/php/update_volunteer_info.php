<?php
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
$php_name = 'updateAppuserDetail';
include("dbconn_sar_apk.php");
include("mobile_common_data_sar.php");
if ( $mysqli ) {
	
	$describelookup = empty($_REQUEST['describelookup']) || !isset($_REQUEST['describelookup']) ? 
		'NULL' : "'" . $_REQUEST['describelookup'] . "'" ;	
	$comment = empty($_REQUEST['comment']) || !isset($_REQUEST['comment']) ? 
		'NULL' : "'" . $_REQUEST['comment'] . "'" ;	
	
	$sql = "call update_volunteer_info(" . $appuserid . "," . $describelookup .
	 "," . $comment .")";
	
	if ($verbose != 'N') {
		echo '<br>sql:<br>' . $sql;
	}
	
	if ($result = $mysqli->query($sql)) {	
		if ($result && is_object($result))  {
			while ($row = $result->fetch_assoc()) {
				echo json_encode($row);
				break;
			}
		}
		
	}		else {
		echo "-1"; // something went wrong, probably sql failed
	}
	$mysqli->close();
} else {
	echo "-2"; // "Connection to db failed";
}