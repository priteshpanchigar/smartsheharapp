DROP PROCEDURE IF EXISTS get_location_address;
DELIMITER $$
CREATE  PROCEDURE `get_location_address`(IN inlat DOUBLE , IN inlng DOUBLE)
BEGIN

SET @inLat = inlat,
		@inLng = inlng,
		@inDistance = 0.3;


SELECT formatted_address, locality, sublocality, postal_code, route,
neighborhood, administrative_area_level_2,administrative_area_level_1
 from address
WHERE getDistanceBetweenPoints(@inLat, @inLng, latitude, longitude)<@inDistance
ORDER BY getDistanceBetweenPoints(@inLat, @inLng, latitude, longitude) limit 1;

END$$
DELIMITER;

CALL get_location_address('19.10', '72.85');