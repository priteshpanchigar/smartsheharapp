DROP PROCEDURE IF EXISTS update_closed_opened;
DELIMITER$$
CREATE  PROCEDURE `update_closed_opened`(IN inclosed INT, IN inopened INT, 
IN inissueid INT,IN inclosedopenedcomment  VARCHAR(300),
IN inclientdatetime datetime,IN inappuserid INT)
BEGIN

UPDATE issue
SET closed = inclosed, 
opened = inopened,
closed_opened_datetime = inclientdatetime,
closed_opened_comment  = inclosedopenedcomment,
closed_opened_by = inappuserid 
WHERE issue_id = inissueid;

select closed, opened, closed_opened_comment , closed_opened_by from issue
WHERE issue_id = inissueid;



END$$
DELIMITER;

call update_closed_opened(0,1,2,"asdasdasdsad", "2015-10-14 10:48:15");