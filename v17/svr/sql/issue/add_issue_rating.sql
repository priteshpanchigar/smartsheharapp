DROP PROCEDURE IF EXISTS add_issue_rating;
DELIMITER $$
CREATE PROCEDURE `add_issue_rating`(IN inappuserid INT, IN inoffence_detail_id INT,
IN inissue_like INT,
IN inissue_rating_comment varchar(255),
IN inissue_rating_clientdatetime datetime, inaddcomment INT)
BEGIN

IF inaddcomment = 1 THEN	
INSERT INTO issue_rating(appuser_id, offence_detail_id, issue_like,
 issue_rating_comment, issue_rating_clientdatetime)
VALUES(inappuserid,inoffence_detail_id, inissue_like,
inissue_rating_comment, 
inissue_rating_clientdatetime)
ON DUPLICATE KEY UPDATE    
	issue_like = COALESCE(inissue_like, issue_like), 
	issue_rating_clientdatetime = COALESCE(inissue_rating_clientdatetime, issue_rating_clientdatetime);
END IF;



SELECT LAST_INSERT_ID() as issue_rating_id;

END$$ 
DELIMITER;