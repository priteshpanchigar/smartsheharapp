SET NAMES UTF8;
DROP PROCEDURE IF EXISTS issue_image;
DELIMITER$$
CREATE  PROCEDURE `issue_image`(IN inissueimagename varchar(50), IN inissueuniquekey VARCHAR(255),
IN increationdatetime datetime,  IN inissueid INT, IN inissueimagepath VARCHAR(255))
quit_proc: BEGIN
	
IF increationdatetime IS NULL OR inissueimagename IS NULL OR  inissueuniquekey IS NULL THEN
LEAVE quit_proc;
END IF;
INSERT INTO issue_image( issue_image_name, issue_uniquekey,  creation_datetime, 
	issue_id,issue_image_path)
VALUES (inissueimagename , inissueuniquekey, increationdatetime, 
	inissueid,inissueimagepath)
		ON DUPLICATE KEY UPDATE
		issue_id = IFNULL(CONVERT(inissueid USING latin7) collate latin7_general_ci, 
														 CONVERT(issue_id USING latin7) collate latin7_general_ci),				
		issue_uniquekey = IFNULL(CONVERT(inissueuniquekey USING latin7) collate latin7_general_ci, 
														 CONVERT(issue_uniquekey USING latin7) collate latin7_general_ci),											
		issue_image_name = IFNULL(CONVERT(inissueimagename USING latin7) collate latin7_general_ci, 
														 CONVERT(issue_image_name USING latin7) collate latin7_general_ci),
		issue_image_path = IFNULL(CONVERT(inissueimagepath USING latin7) collate latin7_general_ci, 
														 CONVERT(issue_image_path USING latin7) collate latin7_general_ci);
		

SELECT LAST_INSERT_ID() issue_image_id;


END$$
DELIMITER ;

call issue_image('IMG_20160219_132205.png', 1455868313807, "2016-02-19 13:22:05", 3619, '/v17/svr/php/issue/images/')