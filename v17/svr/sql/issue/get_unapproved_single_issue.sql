DROP PROCEDURE IF EXISTS get_unapproved_single_issue;
DELIMITER$$
CREATE  PROCEDURE `get_unapproved_single_issue`(IN inward VARCHAR(20), 
IN inissuetypcode VARCHAR(20))
BEGIN

SELECT issue_id, ward, formatted_address,issue_time,client_datetime, vw_issue_items.issue_item_description,
vehicle_no, unique_key,submit_report, approved,rejected,
sent_to_authority, approved_rejected_clientdatetime,  sent_to_authority_datetime,  
resolved_unresolved_datetime, other_reason_code_comment,
address_id, location_id, issue_type_code, issue_sub_type_code,lat,lng, issue_image_name,
issue_image_path, appuser_id
FROM vw_issue 
INNER JOIN  vw_issue_items  ON vw_issue.issue_item_code = vw_issue_items.issue_item_code
WHERE issue_id = (SELECT issue_id FROM vw_issue i INNER JOIN vw_issue_items vit
ON i.issue_item_code = vit.issue_item_code
WHERE submit_report = 1 
AND sent_to_authority = 0
AND rejected = 0
AND vit.issue_type_code = inissuetypcode
AND ward =  inward ORDER BY issue_id limit 1);
END$$
DELIMITER;

CALL get_unapproved_single_issue('K/E','RD' );

######################################################################

DROP PROCEDURE IF EXISTS get_unapproved_single_issue;
DELIMITER$$
CREATE  PROCEDURE `get_unapproved_single_issue`(IN inward VARCHAR(20), 
IN inissuetypcode VARCHAR(20))
BEGIN

SELECT issue_id, ward, formatted_address,issue_time,client_datetime, vw_issue_items.issue_item_description,
vehicle_no, unique_key,submit_report, approved,rejected,
sent_to_authority, approved_rejected_clientdatetime,  sent_to_authority_datetime,  
resolved_unresolved_datetime, other_reason_code_comment,
address_id, location_id, issue_type_code, issue_sub_type_code,lat,lng, issue_image_name,
issue_image_path, appuser_id
FROM vw_issue 
INNER JOIN  vw_issue_items  ON vw_issue.issue_item_code = vw_issue_items.issue_item_code
WHERE issue_id = (SELECT issue_id FROM vw_issue i INNER JOIN vw_issue_items vit
ON i.issue_item_code = vit.issue_item_code
WHERE submit_report = 1 
AND sent_to_authority = 0
AND rejected = 0
AND vit.issue_type_code = inissuetypcode  
AND ward =  inward ORDER BY issue_id limit 1);




END$$
DELIMITER;

CALL get_unapproved_single_issue('K/E','RD' );