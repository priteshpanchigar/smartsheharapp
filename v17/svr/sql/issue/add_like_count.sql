DROP PROCEDURE IF EXISTS add_like_count;
DELIMITER$$
CREATE PROCEDURE `add_like_count`(IN inlikeby INT, IN inissueid INT,
IN inlike INT, IN inunlike INT,
IN inlikedatetime datetime)
BEGIN
  
DECLARE likeid int;
SELECT like_id INTO likeid from issue_like
where
issue_id = inissueid AND
liked_by = inlikeby LIMIT 1;   

IF (likeid IS NULL) THEN
	INSERT INTO issue_like(liked_by, issue_id, liked,
	 unliked, like_datetime)
	VALUES(inlikeby,inissueid, inlike,
	inunlike, inlikedatetime);
ELSE 
	UPDATE issue_like SET liked = inlike
	WHERE issue_id = inissueid AND
	liked_by = inlikeby;
END IF;


SELECT SUM(liked) liked ,Sum(unliked) unliked FROM issue_like 
WHERE issue_id = inissueid;

END$$ 
DELIMITER$$