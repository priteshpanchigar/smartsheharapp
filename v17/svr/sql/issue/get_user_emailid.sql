DROP PROCEDURE IF EXISTS get_user_emailid;
DELIMITER$$
CREATE PROCEDURE `get_user_emailid`(IN inissueid INT )
BEGIN
select issue_id, address_id, CONCAT(DAY(issue_time)," ",MONTHNAME(issue_time), ", ", YEAR(issue_time)," at ", TIME_FORMAT(issue_time,'%h:%i%p')) issue_time, client_datetime, formatted_address,
issue_item_code, issue_item_description, vehicle_no, unique_key,
submit_report, discard_report, ward, appuser_id,
 approved, rejected, email, GROUP_CONCAT(",images/",issue_image_name) issue_images,
(SELECT reject_reason FROM rejected_reason 
	WHERE vw_issue.reason_code = rejected_reason.reason_code) reject_reason,
sent_to_authority,resolved, unresolved, unresolved_comment, 
lat, lng, issue_image_id,
issue_image_path, creation_datetime FROM vw_issue
WHERE issue_id  = inissueid
AND submit_report = 1
AND issue_item_code IS NOT NULL
GROUP BY issue_id;  

END
