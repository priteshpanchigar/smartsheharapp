DROP PROCEDURE IF EXISTS show_offence_details;
DELIMITER$$
CREATE  PROCEDURE `show_offence_details`() 

BEGIN

SELECT DISTINCT(offence_detail_id), offence_address,
       CONCAT(DAY(offence_time)," ",MONTHNAME(offence_time), ", ", YEAR(offence_time)," at ", TIME_FORMAT(offence_time,'%h:%i%p'))AS offence_time,
       offence_type, client_datetime, creation_datetime, appuser_id, offence_image_name
FROM vw_offence
WHERE offence_image_name != ""
  AND offence_image_id IN
    (SELECT MIN(offence_image_id) offence_image_id
     FROM offence_image
     GROUP BY offence_detail_id); 

END$$
DELIMITER;

CALL show_offence_details;