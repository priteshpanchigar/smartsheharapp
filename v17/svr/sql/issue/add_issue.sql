
DROP PROCEDURE IF EXISTS add_issue;
CREATE  PROCEDURE `add_issue`(IN inissueaddress varchar(255),  IN inissuedatetime datetime, 
IN inclientdatetime datetime,   IN invehicleno varchar(255), IN inuniquekey varchar(255), 
IN insubmitreport int(11), IN inlocality varchar(255), IN insublocality varchar(255),IN inpostal_code varchar(255), 
IN inroute varchar(255), IN inneighborhood varchar(255), IN inadministrative_area_level_2 varchar(255), 
IN inadministrative_area_level_1 varchar(255), IN  inlatitude double, IN inlongitude double, IN inappuserid int(11),
IN inissueitemcode varchar(255),IN inemailid varchar(40),
IN inacc double,IN inlocationdatetime datetime, IN inprovider varchar(100), IN inspeed float, IN inbearing float,
IN inaltitude double, IN indescard_report int(11),IN inmlaid int(11),IN inmpid int(11), IN ingroupid INT(11),
IN inward varchar(5))
BEGIN
	
DECLARE vaddress_id int;
DECLARE v_location_id INT DEFAULT 0;

SELECT address_id INTO vaddress_id from address
where formatted_address = inissueaddress LIMIT 1;

IF (vaddress_id IS NULL) THEN
		INSERT INTO address(formatted_address, locality, sublocality,postal_code,route,
			neighborhood,administrative_area_level_2,administrative_area_level_1,
			client_datetime,latitude,longitude, ward)
		VALUES(inissueaddress,inlocality, insublocality, inpostal_code, inroute,
			inneighborhood,inadministrative_area_level_2, inadministrative_area_level_1,
			inclientdatetime, inlatitude, inlongitude, COALESCE(inward,
		(SELECT DISTINCT ward FROM municipal_ward
			WHERE ST_Contains(boundary, 
			GeomFromText(CONCAT('POINT(', inlongitude, ' ', inlatitude, ')')))
				LIMIT 1)));
 SELECT LAST_INSERT_ID() INTO vaddress_id from address;
ELSE
	UPDATE address
		SET locality = COALESCE(inlocality, locality), 
			sublocality = COALESCE(insublocality, sublocality), 
			postal_code = COALESCE(inpostal_code, postal_code),
			route = COALESCE(inroute, route), 
			neighborhood = COALESCE(inneighborhood, neighborhood),
			administrative_area_level_2 = COALESCE(inadministrative_area_level_2, administrative_area_level_2),
			administrative_area_level_1 = COALESCE(inadministrative_area_level_1, administrative_area_level_1),
			ward = COALESCE(inward,(SELECT DISTINCT ward FROM municipal_ward
							WHERE ST_Contains(boundary, 
							GeomFromText(CONCAT('POINT(', inlongitude, ' ', inlatitude, ')')))
							LIMIT 1))
			WHERE formatted_address = inissueaddress;
END IF;

SET v_location_id = insert_location(inappuserid, inemailid, 1, 0, 
					inclientdatetime, inlatitude , inlongitude, inacc, inlocationdatetime,
					inprovider, inspeed, inbearing, inaltitude);



INSERT INTO issue( issue_time, client_datetime, 
	vehicle_no,unique_key,submit_report, address_id,appuser_id,lat,lng,issue_item_code,
	 location_id,discard_report, group_id, mla_id, mp_id)
VALUES ( inissuedatetime ,inclientdatetime,  
	invehicleno, inuniquekey, insubmitreport, vaddress_id, inappuserid,inlatitude,inlongitude,
	inissueitemcode, v_location_id,indescard_report, ingroupid, inmlaid, inmpid)
	ON DUPLICATE KEY UPDATE	
		issue_time = inissuedatetime,
		issue_item_code = inissueitemcode,
		submit_report = insubmitreport,
		address_id = vaddress_id,
		discard_report = indescard_report,
		mla_id = inmlaid,
		group_id = ingroupid,
		mp_id = inmpid,
		lat = inlatitude,
		lng = inlongitude,
		vehicle_no = invehicleno;

SELECT  issue_id from issue
WHERE unique_key = inuniquekey;

END;


call add_issue('5, Hanuman Hedit Marg No 2, Om Shri Siddhivinayak Society, Paranjape Nagar, Vile Parle, Mumbai, Maharashtra 400047, India ',"2016-05-18 18:18:29", "2016-05-18 18:20:41",NULL,1463575699691,0, NULL, NULL, NULL,NULL,NULL,NULL, NULL,"19.1046", "72.8506", 2, 'CDD', 'thomsonjames91@gmail.com', "21.0", "2016-05-18 18:20:31", "network", "0.0", "0.0", "0.0", "0", NULL, NULL, "Maharashtra", NULL)