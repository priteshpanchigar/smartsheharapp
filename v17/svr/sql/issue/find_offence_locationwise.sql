DROP PROCEDURE IF EXISTS find_offence_locationwise;
DELIMITER$$
CREATE  PROCEDURE `find_offence_locationwise`(IN inlat DOUBLE, IN inlng DOUBLE) 

BEGIN


SET @inDistance = 1.5;	
SELECT  offence_detail_id, offence_address, 
(DATE_FORMAT(offence_time,'%b %d %Y %h:%i %p')) AS offence_time,client_datetime, offence_type, 
vehicle_no, unique_key, submit_report, discard_report, address_id, location_id, ward_no, 
complaint_type_code, complaint_sub_type_code, municipal_department, approved, rejected, 
resolved, unresolved, unresolved_comment, reason_code, sent_to_authority, lat, lng, 
appuser_id, email, min(offence_image_id) offence_image_id,  offence_image_name, 
offence_image_path, creation_datetime from vw_offence
			WHERE ABS(inlat - lat) < @inDistance / 100 AND 
			ABS(inlng - lng) < @inDistance / 100
			GROUP BY offence_detail_id;

END$$
DELIMITER;

CALL find_offence_locationwise(19.1047271,72.8506279);