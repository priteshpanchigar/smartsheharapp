DROP PROCEDURE IF EXISTS update_sent_to_authority_issue;
DELIMITER$$
CREATE  PROCEDURE `update_sent_to_authority_issue`(IN inissueid INT, 
IN inclientdatetime datetime) 

BEGIN

UPDATE issue
SET sent_to_authority = 1,
sent_to_authority_datetime =  inclientdatetime
WHERE issue_id = inissueid;

SELECT 1 AS result;

END$$
DELIMITER;
