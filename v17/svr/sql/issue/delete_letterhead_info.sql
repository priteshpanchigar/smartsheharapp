DROP PROCEDURE IF EXISTS delete_letterhead_info;
DELIMITER$$
CREATE  PROCEDURE `delete_letterhead_info`(IN inissueid INT)
BEGIN

UPDATE issue
SET letter_client_date_time = NULL, letter_image_name =  NULL,
letter_image_path = NULL, letter_upload_notification = 0
WHERE issue_id = inissueid;



END$$
DELIMITER;

call delete_letterhead_info(1);