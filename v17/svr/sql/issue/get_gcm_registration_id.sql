DROP PROCEDURE IF EXISTS get_gcm_registration_id;
DELIMITER$$
CREATE PROCEDURE `get_gcm_registration_id`(IN inoffencedetailid INT )
BEGIN
SELECT od.offence_address, od.offence_time, od.offence_type as 'subcategory',
(SELECT ct.complaint_description 
FROM offence_details od 
INNER JOIN complaint_lookup_types ct ON od.complaint_type_code = ct.complaint_type_code 
WHERE od.offence_detail_id = inoffencedetailid )'category',
od.unique_key,
a.gcm_registration_id,a.appuser_id
FROM offence_details od 
INNER JOIN address ad ON ad.address_id= od.address_id
INNER JOIN groupdetails gd ON gd.pincode = ad.postal_code
INNER JOIN group_members gm ON gm.group_id = gd.group_id
INNER JOIN appuser a ON a.appuser_id = gm.appuser_id
WHERE od.offence_detail_id = inoffencedetailid  
UNION
(select od.offence_address,od.offence_time,od.offence_type as 'subcategory',
(SELECT ct.complaint_description 
FROM offence_details od 
INNER JOIN complaint_lookup_types ct ON od.complaint_type_code = ct.complaint_type_code 
WHERE od.offence_detail_id = inoffencedetailid )'category',
od.unique_key,
a.gcm_registration_id, a.appuser_id
from offence_details od
INNER JOIN appuser a ON od.appuser_id = a.appuser_id
where offence_detail_id = inoffencedetailid );
END$$
DELIMITER;