DROP PROCEDURE IF EXISTS add_intermediate_progress;
DELIMITER $$
CREATE PROCEDURE `add_intermediate_progress`(IN inappuserid INT, IN inissueid INT,
IN inintermediate_progress_comment varchar(100),
IN inintermediate_progress_clientdatetime datetime, 
 IN inimagename VARCHAR(255))
BEGIN
	
INSERT INTO intermediate_progress(appuser_id, issue_id, intermediate_progress_comment, 
intermediate_progress_clientdatetime, ip_image_name)
VALUES(inappuserid,inissueid, inintermediate_progress_comment, 
inintermediate_progress_clientdatetime, inimagename)
ON DUPLICATE KEY UPDATE
intermediate_progress_comment = inintermediate_progress_comment;

SELECT LAST_INSERT_ID() as intermediate_progress_id;

END$$
DELIMITER;

call add_intermediate_progress(605,2120,'work is in progress',
"2016-07-26 12:34:33",'IMG_20160726_123146.png')