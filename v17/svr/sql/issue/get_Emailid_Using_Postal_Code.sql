DROP PROCEDURE IF EXISTS get_Emailid_Using_Postal_Code;
DELIMITER$$
CREATE  PROCEDURE `get_Emailid_Using_Postal_Code`(IN inissueid INT, IN inignoresent INT)
BEGIN

SELECT  'issues_smartshehar@yahoo.com' as email_id ,a.formatted_address,  a.latitude, a.longitude, 
CONCAT(DAY(issue_time)," ",MONTHNAME(issue_time), ", ", YEAR(issue_time)," at ", TIME_FORMAT(issue_time,'%h:%i%p'))as issue_time,
i.unique_key,a.ward ,item.issue_item_description ,it.issue_type_description as 'category' ,ist.issue_sub_type_description as 'subcategory',
COUNT(im.issue_image_name) image_count ,it.department_code
FROM issue i
LEFT JOIN issue_image im ON i.issue_id = im.issue_id
LEFT JOIN address a ON i.address_id = a.address_id
LEFT JOIN issue_lookup_items item ON i.issue_item_code = item.issue_item_code
LEFT JOIN issue_lookup_subtypes ist ON item.issue_sub_type_code = ist.issue_sub_type_code
LEFT JOIN issue_lookup_types it ON ist.issue_type_code = it.issue_type_code
WHERE i.issue_id = inissueid 
AND  i.issue_registration_notification != 1;

END$$
DELIMITER ;