DROP PROCEDURE IF EXISTS delete_cancelled_image;
DELIMITER$$
CREATE  PROCEDURE `delete_cancelled_image`(IN inissueid INT, 
IN increationdatetime datetime )
BEGIN

DELETE FROM issue_image
WHERE issue_id = inissueid
AND creation_datetime = increationdatetime;


END$$
DELIMITER;

call delete_cancelled_image(15, '2015-09-28 11:37:44');