DROP PROCEDURE IF EXISTS rs_cityfareparameter;

CREATE  PROCEDURE `rs_cityfareparameter`()
BEGIN
		SELECT	vehiclefareparameter_id, serialno	,date,	rc.city,	rc.city_id,	vehicletype,	transport,	vehiclecapacity,	minimumdistance,	minimumfare,
	fareperkm,	metermovesperkm,	meterrounding,	minimumwaitingminutes,	waitchargeperhour,	waitingspeed,	nightextra,	luggagechargeperpiece,
	nightstart,	nightend,	minimumnightfare,	metertype,	farepercentincreaseperpassenger,	courtesy,	link,	city_code,	country,	distance_unit,
	currency,	centerlat, centerlon,	citytoplat,	citytoplon,	citybottomlat,	citybottomlon
	
FROM
	rs_cityinfo rc INNER JOIN vehiclefareparameter vp 
ON rc.city_id = vp.city_id


ORDER BY	city,	transport,	vehicletype;
END;


CALL rs_cityfareparameter;