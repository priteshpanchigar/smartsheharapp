DROP PROCEDURE IF EXISTS get_store_category;
DELIMITER $$
CREATE  PROCEDURE `get_store_category`()
BEGIN

SELECT  category_id, category, category_code FROM store_category;


END$$
DELIMITER;

CALL get_store_category();