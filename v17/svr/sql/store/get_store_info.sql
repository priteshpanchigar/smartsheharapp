DROP PROCEDURE IF EXISTS get_store_info;
DELIMITER $$
CREATE  PROCEDURE `get_store_info`(IN inlat DOUBLE , IN inlng DOUBLE)
BEGIN

SET @inLat = inlat,
		@inLng = inlng,
		@inDistance = 2;


SELECT store_id, store_name, store_no, contact_person, contact_no_1, contact_no_2,
ROUND(getDistanceBetweenPoints(@inLat, @inLng, latitude, longitude),3) distance , 
 (SELECT category FROM store_category sc WHERE s.category_id =sc.category_id )category,
(SELECT category_code FROM store_category sc WHERE s.category_id =sc.category_id )category_code, 
address_line_1, 
latitude, longitude,
debit_card, credit_card, cash, wallet, wallet_name, cheque, UPI
  FROM store s
INNER JOIN  store_address sa
ON s.address_id = sa.address_id
WHERE getDistanceBetweenPoints(@inLat, @inLng, latitude, longitude) <= @inDistance
ORDER BY getDistanceBetweenPoints(@inLat, @inLng, latitude, longitude);


END$$
DELIMITER;

call get_store_info(19.10369,	72.85354);