DROP PROCEDURE IF EXISTS insert_store_data;
DELIMITER $$
CREATE  PROCEDURE `insert_store_data`(IN inclientdatetime datetime, 
IN ingoogleaddress varchar(250), IN inlocality varchar(255), 	 	IN insublocality varchar(255),		
IN inpostal_code varchar(255),   IN inroute varchar(255), 		 	IN inneighborhood varchar(255),	
IN inadministrative_area_level_2 varchar(255), 									IN inadministrative_area_level_1 varchar(255),
IN inlatitude double,			 			 IN inlongitude double,					IN instorename varchar(255), 
IN instoreno varchar(255), 			 IN incontactperson varchar(255),IN incontactemail varchar(255), 
IN incontactno1 varchar(255), 	 IN incontactno2 varchar(255),	IN inworkinghours varchar(255), 
IN incloseddays varchar(255), 	 IN category_id INT,							IN indebitcard TINYINT, 
IN indbtransactionfee double, 	 IN increditcard TINYINT, 			IN incctransactionfee double, 
IN inwallet TINYINT, 						 IN inwalletname VARCHAR(50), 	IN inwallettrasactionfee double, 
IN incheque TINYINT, 						 IN inUPI TINYINT, 							IN incomment varchar(255))

BEGIN

DECLARE vaddress_id int;

SELECT address_id INTO vaddress_id from store_address
WHERE address_line_1 = ingoogleaddress LIMIT 1;


IF vaddress_id IS NULL AND ingoogleaddress IS NOT NULL  THEN
		INSERT INTO store_address(address_line_1, locality, sublocality,postal_code,route,
			neighborhood,administrative_area_level_2,administrative_area_level_1,
			clientdatetime,latitude,longitude)
		VALUES(ingoogleaddress,inlocality, insublocality, inpostal_code, inroute,
			inneighborhood,inadministrative_area_level_2, inadministrative_area_level_1,
		inclientdatetime, inlatitude, inlongitude);
 SELECT LAST_INSERT_ID() INTO vaddress_id from store_address;

ELSE
	UPDATE store_address
		SET locality = inlocality, sublocality = insublocality, postal_code = inpostal_code,
				route = inroute,  neighborhood = inneighborhood, administrative_area_level_2 = inadministrative_area_level_2,
				administrative_area_level_1 = inadministrative_area_level_1
					WHERE address_line_1 = ingoogleaddress;
END IF;


INSERT INTO store (store_name, store_no, contact_person, contact_email, contact_no_1, contact_no_2,
 closed_days, address_id, category_id, debit_card, db_transaction_fee, credit_card,
cc_transaction_fee, wallet, wallet_name, wallet_trasaction_fee, cheque, UPI, comment)

VALUES (instorename, instoreno, incontactperson, incontactemail, incontactno1, incontactno2,
  incloseddays, vaddress_id, category_id, indebitcard, indbtransactionfee, increditcard,
 incctransactionfee, inwallet, inwalletname, inwallettrasactionfee, incheque, inUPI, incomment)
	 ON DUPLICATE KEY UPDATE
		 store_no = COALESCE(instoreno, store_no),
		 contact_person = COALESCE(incontactperson, contact_person),
		 contact_email = COALESCE(incontactemail, contact_email),
		 contact_no_1 = COALESCE(incontactno1, contact_no_1),
		 contact_no_2 = COALESCE(incontactno2, contact_no_2),
		 closed_days = COALESCE(incloseddays, closed_days);

 SELECT LAST_INSERT_ID() AS  store_id;
		


END$$
DELIMITER;

call insert_store_data("2016-12-07 12:46:06",'5, Hanuman Hedit Marg No 2 Om Shri Siddhivinayak Society, Paranjape Nagar, Vile Parle ',
"Mumbai", "null", "400018","null","","","","19.104411", "72.8506", 
'ffff','dddd', 'ssss','aaaa', '554', '2544', '15', 'sunday',2, 1, 2, 1,2, 1, 'hfjfj',2, 1, 1, NULL);