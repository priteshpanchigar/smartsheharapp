DROP PROCEDURE IF EXISTS upgrade_app;
DELIMITER $$
CREATE  PROCEDURE `upgrade_app`(IN `inapp` varchar(30),IN `inversioncode` int)
BEGIN
	SELECT link FROM appversion
		WHERE app = inapp 
		AND inversioncode < versioncode;	
END$$
DELIMITER ;
# Sample query
call upgrade_app('SST',43);
