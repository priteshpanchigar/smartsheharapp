DROP PROCEDURE IF EXISTS `issue_report`;
DELIMITER $$
CREATE  PROCEDURE `issue_report`(IN inissueid INT(10), IN inpincode varchar(20),IN infromissuedate datetime, IN intoissuedate datetime,
IN inPV VARCHAR (20), IN inMV VARCHAR (20), IN inRT VARCHAR (20), IN inAT VARCHAR (20),
IN inRD VARCHAR (20), IN inCL VARCHAR (20), IN inEN VARCHAR (20), IN inSF VARCHAR (20),
IN inWE VARCHAR (20),  IN inOG VARCHAR (20), IN inRS VARCHAR (20),IN inmyissue INT, IN inappuserid INT, 
IN inlat DOUBLE , IN inlng DOUBLE, IN inlimit INT,
IN insubmitreport INT, IN inrejected INT, IN inclosed INT, IN inresolved INT,
IN inmaxlat DOUBLE , IN inmaxlng DOUBLE,IN inminlat DOUBLE , IN inminlng DOUBLE,
IN indepartmentcode VARCHAR (10), IN inward VARCHAR (10), IN insubtypecode VARCHAR(10), 
IN ingroupby VARCHAR (250), IN inorderby VARCHAR (250), IN incount INT, IN inoffset INT, IN inissuesubtypedescription VARCHAR (250),
IN inisnotss INT, IN intrLat DOUBLE, IN intrLng DOUBLE)

BEGIN
IF incount = 1 THEN
SET @sel = 'SELECT Count(*) cnt ';
ELSE

SET @sel = 'SELECT ap.email, i.issue_id, issue_time, 
 formatted_address as issue_address, postal_code, 
ilt.issue_type_code, issue_type_description, ils.issue_sub_type_code, issue_sub_type_description,  
ili.issue_item_code, issue_item_description, d.department_code, department_description, ward,
submit_report,discard_report ,approved, rejected, to_be_sent_to_authority, sent_to_authority,  resolved, unresolved,
closed, opened, approved_rejected_clientdatetime, sent_to_authority_datetime, resolved_unresolved_datetime, 
closed_opened_datetime, reason_code, other_reason_code_comment, unresolved_comment, 
(SELECT  group_name FROM group_members g WHERE g.appuser_id  = i.resolved_unresolved_by LIMIT 1 )resolved_unresolved_by, 
(SELECT  group_name FROM group_members g WHERE g.appuser_id  = i.closed_opened_by LIMIT 1) closed_opened_by, closed_opened_comment, latitude lat, longitude lng, i.appuser_id, vehicle_no, unique_key, 
(SELECT  group_name FROM group_members g WHERE g.appuser_id  = i.letter_submit_by LIMIT 1) letter_submit_by, 
issue_registration_notification, letter_submitted_to, letter_submitted_dept, letter_submitted_desg,
letter_submitted_datetime, letter_remark, letter_upload_notification, resolved_unresolved_notification, letter_client_date_time,
letter_image_name, letter_image_path,
(SELECT group_name FROM group_members g WHERE g.appuser_id = i.appuser_id LIMIT 1) group_name,
(SELECT issue_image_name FROM issue_image oi WHERE oi.issue_id = i.issue_id GROUP BY issue_id)issue_image_name,
(SELECT GROUP_CONCAT(issue_image_name) issue_images FROM issue_image oi WHERE oi.issue_id = i.issue_id GROUP BY issue_id)issue_images,
(SELECT issue_image_path FROM issue_image oi WHERE oi.issue_id = i.issue_id GROUP BY issue_id)issue_image_path,
(SELECT SUM(liked)FROM issue_like il WHERE il.issue_id = i.issue_id)likedCnt ,
(SELECT SUM(unliked)FROM issue_like il WHERE il.issue_id = i.issue_id)unlikedCnt' ;
END IF;
#init vars
SET @whr = NULL;

IF inissueid IS NOT NULL AND (inlat IS NULL OR inlng IS NULL) THEN
	SET @whr = CONCAT(' issue_id = ', inissueid);
ELSE 
 
SET @whr =IF(inisnotss IS NULL, @whr,  CONCAT(IF(@whr IS NULL, '', CONCAT(@whr, '\n AND ')), 
	' ap.email  NOT IN (SELECT email from ss_employees) '));
IF inisnotss  IS NULL THEN
SET @whr =IF(insubmitreport IS NULL, @whr,  CONCAT(IF(@whr IS NULL, '', CONCAT(@whr, '\n AND ')), ' submit_report = ', insubmitreport));

SET @whr =IF(inrejected IS NULL, @whr, CONCAT(IF(@whr IS NULL, '', CONCAT(@whr, '\n AND ')), ' rejected = ', inrejected));

SET @whr = IF(inclosed IS NULL, @whr,  CONCAT(IF(@whr IS NULL, '', CONCAT(@whr, '\n AND ')), ' closed = ', inclosed));
SET @whr =IF(inresolved IS NULL, @whr, CONCAT(IF(@whr IS NULL, '', CONCAT(@whr, '\n AND ')), ' resolved = ', inresolved));
SET @whr = IF(inmyissue = 0 , @whr, CONCAT(IF(@whr IS NULL, '', CONCAT(@whr, '\n AND ')), ' i.appuser_id = ', inappuserid));
SET @whr = IF(inpincode IS NULL, @whr, CONCAT(IF(@whr IS NULL, '', CONCAT(@whr, '\n AND ')), ' postal_code = ', inpincode));
SET @whr = IF(indepartmentcode IS NULL, @whr, CONCAT(IF(@whr IS NULL, '', CONCAT(@whr, '\n AND ')), ' d.department_code = "', indepartmentcode, '"'));
SET @whr = IF(inward IS NULL, @whr, CONCAT(IF(@whr IS NULL, '', CONCAT(@whr, '\n AND ')), ' ward = "', inward, '"'));
SET @whr = IF(inissuesubtypedescription IS NULL, @whr, CONCAT(IF(@whr IS NULL,'',CONCAT(@whr,'\n AND ')),' issue_sub_type_description = "', inissuesubtypedescription, '"'));
SET @whr = IF(insubtypecode IS NULL, @whr, CONCAT(IF(@whr IS NULL, '', CONCAT(@whr, '\n AND ')), '  ils.issue_sub_type_code = "', insubtypecode, '"'));
SET @whr = IF(inmaxlat IS NULL OR inmaxlng IS NULL OR inminlng IS NULL OR inminlng IS NULL, @whr, 
							CONCAT(IF(@whr IS NULL,'',CONCAT(@whr, '\n AND ')),	
								' latitude BETWEEN ', inminlat, ' AND ', inmaxlat,'', 
						' AND longitude BETWEEN ', inminlng, ' AND ', inmaxlng,''));

SET @whr = IF( intrLat IS NULL OR intrLng IS NULL , @whr,
							CONCAT(IF(@whr IS NULL,'',CONCAT(@whr, '\n AND ')),
								' i.resolved = 0 AND closed = 0 ',
								' AND getDistanceBetweenPoints(',intrLat,',', intrLng, ', a.latitude, a.longitude)<= 3 '
								''));

SET @whr = CONCAT(IF(@whr IS NULL, '', CONCAT(@whr, '\n AND ')), '  i.issue_item_code IS NOT NULL ');
SET @whr = CONCAT(IF(@whr IS NULL, '', CONCAT(@whr, '\n AND ')), '  i.discard_report = 0 ');
SET @whr = CONCAT(IF(@whr IS NULL, '', CONCAT(@whr, '\n AND ')), '  i.rejected = 0 ');
SET @whr = IF(infromissuedate IS NULL AND intoissuedate IS NULL , @whr, 
		CONCAT(IF(@whr IS NULL,'',CONCAT(@whr, '\n AND ')),	
						' issue_time BETWEEN "', infromissuedate, '" AND "', intoissuedate,'"'));

IF inPV IS NOT NULL OR inMV IS NOT NULL OR inRT IS NOT NULL OR inAT IS NOT NULL OR inRD IS NOT NULL
OR inCL IS NOT NULL OR inEN IS NOT NULL OR inSF IS NOT NULL OR inWE IS NOT NULL OR inOG IS NOT NULL OR inRS IS NOT NULL
	THEN 
		
		SET @whr = CONCAT(IF(@whr IS NULL, '', CONCAT(@whr, '\n AND ')),
					' ilt.issue_type_code IN( "', 
																											 IFNULL(inPV, ''),'" ,"', IFNULL(inMV, ''),
																							 '" ,"', IFNULL(inRT, ''),'" ,"', IFNULL(inAT, ''),
																							 '" ,"', IFNULL(inRD, ''),'" ,"', IFNULL(inCL, ''),
																							 '" ,"', IFNULL(inEN, ''),'" ,"', IFNULL(inSF, ''),
																							 '" ,"', IFNULL(inWE, ''),'" ,"', IFNULL(inOG, ''),  
																							 '" ,"', IFNULL(inRS, ''),'")');	 

END IF;

END IF;

END IF; #show all issues without ss_employees 
SET @groupby = IF(ingroupby IS NULL, '', concat('\n GROUP BY ', ingroupby));

SET @sel = IF(ingroupby IS NULL, @sel, CONCAT(IF(@sel IS NULL, '', CONCAT(@sel, ', COUNT(*) as cnt,sum(resolved)as resolvedCnt '))));
SET @sel = IF(inappuserid IS NULL, @sel, CONCAT(IF(@sel IS NULL, '',CONCAT(@sel,',(SELECT liked from issue_like l where l.issue_id = i.issue_id and l.liked_by =',inappuserid,')liked'))));

SET @orderby = '\n ORDER BY ';
#SELECT inorderby, inissueid, inlat , inlng;
IF inorderby IS NULL THEN
	IF inissueid IS NOT NULL THEN
		SET @orderby = CONCAT(@orderby, ' (issue_id = ', inissueid, ') DESC, ' );
	END IF;
	IF inlat IS NOT NULL AND inlng IS NOT NULL THEN
		SET @orderby = CONCAT(@orderby, CONCAT('getDistanceBetweenPoints(', inlat, ', ', inlng, ', latitude, longitude)'));
	ELSE 
			SET @orderby = CONCAT(@orderby, ' issue_time DESC ');
	END IF;
ELSE
	SET @orderby = CONCAT(@orderby, inorderby);
END IF;  


SET @dynquery = CONCAT(@sel, ' FROM issue i
															 LEFT JOIN issue_lookup_items ili
															 ON i.issue_item_code = ili.issue_item_code
															 LEFT JOIN issue_lookup_subtypes  ils
															 ON ili.issue_sub_type_code = ils.issue_sub_type_code
															 LEFT JOIN issue_lookup_types ilt
															 ON ilt.issue_type_code = ils.issue_type_code
															 LEFT JOIN address a
															 ON  a.address_id = i.address_id
															 LEFT JOIN department d
															 ON d.department_code = ilt.department_code
															 LEFT JOIN appuser ap 
															 ON ap.appuser_id = i.appuser_id \n', 
			IFNULL(CONCAT(' WHERE ', @whr),''),
			IFNULL(@groupby,''), 
			IFNULL(@orderby,''), 
			IF(inlimit IS NULL, '', CONCAT('\n LIMIT ', inlimit, ' OFFSET ', inoffset)));
		

#SELECT @dynquery;
PREPARE stmt FROM @dynquery; 
EXECUTE stmt; 
DEALLOCATE PREPARE stmt; 

END$$ 
DELIMITER;
call issue_report(NULL,NULL,NULL,NULL,NULL,NULL,NULL,
NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,
NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'TR',NULL,NULL,
'issue_sub_type_code','cnt desc',0,0,NULL,NULL,19.1158054,72.8305011)

