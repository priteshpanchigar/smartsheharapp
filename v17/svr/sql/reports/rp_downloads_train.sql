DROP PROCEDURE IF EXISTS `rp_downloads_train`;
DELIMITER $$
CREATE  PROCEDURE `rp_downloads_train`(IN infromdatetime datetime, IN intodatetime datetime)
BEGIN

SELECT app, ap.appuser_id,		email,		ap.clientlastaccessdatetime,
username,	fullname,
age,	sex,	phoneno,		lat,	lng,		product,	manufacturer,	
android_release_version FROM appuser a
LEFT JOIN   appusage ap 
ON a.appuser_id = ap.appuser_id
WHERE email not in (select email from ss_employees)
AND app = 'SST' 
AND ap.clientfirstaccessdatetime BETWEEN infromdatetime AND intodatetime
GROUP BY appuser_id
ORDER BY appuser_id DESC;


END$$
DELIMITER;

CALL rp_downloads_train('2016-05-16 00:00:00', '2016-05-19 12:00:00');

