DROP PROCEDURE IF EXISTS pb34;
DELIMITER$$
CREATE  PROCEDURE `pb34`()
BEGIN
	

#INSERTING DATA INTO MAIN TABLES#

INSERT INTO t_routedetail(route_id,linecode,directioncode,stationcode,routestations,stationserial,indicatorspeedcode,trainspeedcode,platformside,platformno)
SELECT route_id,linecode,directioncode,stationcode,routestations,stationserial,indicatorspeedcode,trainspeedcode,platformside,platformno from tt5_routedetail;

INSERT INTO t_route(route_id,linecode,directioncode,routestations)
SELECT route_id,linecode,directioncode,routestations from tt_route 
GROUP BY linecode,routestations;

INSERT INTO t_train(trainno,traincode,route_id,linecode,directioncode,longtraincode,car,emu,splcode,sundayonly,holiday,notonsunday)
SELECT trainno,traincode,route_id,linecode,directioncode,longtraincode,car,emu,splcode,sundayonly,holiday,notonsunday from tt_train;



CREATE INDEX t_train_longtraincode
on t_train (longtraincode);

CREATE INDEX t_train_train_id
on t_train (train_id);

CREATE INDEX t_train_route_id
on t_train (route_id);

CREATE INDEX t_routedetail_route_id
on t_routedetail (route_id);


UPDATE t_schedule
set traincode= '-' where LENGTH(traincode)=0;

UPDATE t_train
set traincode ='-' WHERE length(traincode)=0;

UPDATE t_direction
SET line_id=(SELECT line_id FROM t_line
		WHERE t_direction.linecode=t_line.linecode);


UPDATE t_schedule
	SET line_id=(SELECT line_id FROM t_line
		WHERE t_schedule.linecode=t_line.linecode);

UPDATE t_schedule
	SET station_id=(SELECT station_id FROM t_station
		WHERE t_schedule.stationcode=t_station.stationcode);

UPDATE t_train
	SET line_id=(SELECT line_id FROM t_line
		WHERE t_train.linecode=t_line.linecode);

UPDATE t_routedetail
SET line_id=(SELECT line_id FROM t_line
		WHERE t_routedetail.linecode=t_line.linecode);

UPDATE t_routedetail
	SET station_id=(SELECT station_id FROM t_station
		WHERE t_routedetail.stationcode=t_station.stationcode);

UPDATE t_route
	SET line_id =(SELECT line_id from t_line 
			where t_route.linecode=t_line.linecode);

UPDATE t_route
	SET trainspeedcode =(SELECT trainspeedcode from t_routedetail 
			where  t_route.routestations=t_routedetail.routestations   GROUP BY t_routedetail.routestations);

UPDATE t_route r
SET firststationcode=(SELECT stationcode from t_routedetail rd 
where r.linecode=rd.linecode 
AND rd.routestations=r.routestations 
AND stationserial=1 );


UPDATE t_route
SET laststationcode=
(SELECT stationcode FROM t_routedetail rd1 WHERE route_id= t_route.route_id AND routedetail_id=
(SELECT routedetail_id FROM t_routedetail rd2 WHERE rd2.route_id=t_route.route_id  AND rd2.stationserial =
(SELECT MAX(stationserial) FROM t_routedetail rd3 WHERE rd3.route_id=t_route.route_id)));


UPDATE t_route
	SET firststation_id=(SELECT station_id FROM t_station
		WHERE t_route.firststationcode=t_station.stationcode);

UPDATE t_route
	SET laststation_id=(SELECT station_id FROM t_station
		WHERE t_route.laststationcode=t_station.stationcode);

UPDATE t_schedule sc
SET train_id = (SELECT train_id FROM t_train t WHERE t.longtraincode = sc.longtraincode); 


CREATE INDEX t_schedule_train_id
on t_schedule (train_id);


UPDATE t_routedetail
SET platformside='B' WHERE platformside='LR';

UPDATE t_routedetail
SET platformside = '-' WHERE length(platformside)>1;

UPDATE t_schedule
SET timemin = timemin + 24*60  WHERE  linecode='wr' and traintime BETWEEN 0  and 3;

UPDATE t_schedule
SET timemin = timemin + 24*60  WHERE  linecode='HR' and traintime BETWEEN 0  and 3;

UPDATE t_schedule
SET timemin = timemin + 24*60  WHERE  linecode='CR'  AND directioncode='U' and traintime BETWEEN 0  and 2;

UPDATE t_schedule
SET timemin = timemin + 24*60  WHERE  linecode='CR'  AND directioncode='D' and traintime BETWEEN 0  and 3.15;

UPDATE t_station
SET stnabbr = stationcode;

ALTER TABLE t_schedule
ADD mins int;

UPDATE t_schedule
SET mins = CASE WHEN timemin >= 1440 THEN timemin - 1440 else timemin end;  

UPDATE t_route
SET towardsstationcode='DN' WHERE linecode='WR' and directioncode='D';

UPDATE t_route
SET towardsstationcode='C' WHERE linecode='WR' and directioncode='U';






UPDATE t_route
SET towardsstationcode='PN' WHERE linecode='HR' and directioncode='D';

UPDATE t_route
SET towardsstationcode='ST' WHERE linecode='HR' and directioncode='U';

UPDATE t_route
SET towardsstationcode='PN' WHERE  laststationcode='VA' AND linecode='HR';

UPDATE t_route
SET towardsstationcode='ADH' WHERE linecode='HR' and laststationcode='ADH';

UPDATE t_route
SET towardsstationcode='ADH' WHERE linecode='HR' and laststationcode='BA';

UPDATE t_route
SET towardsstationcode='PN' WHERE linecode='TH' and directioncode='D';

UPDATE t_route
SET towardsstationcode='T' WHERE linecode='TH' and directioncode='U';

UPDATE t_route
SET towardsstationcode='DN' WHERE linecode='SH' and directioncode='D';

UPDATE t_route
SET towardsstationcode='BA' WHERE linecode='SH' and directioncode='U';

UPDATE t_route
SET towardsstationcode='ROH' WHERE linecode='KR' and laststationcode='ROH';

UPDATE t_route
SET towardsstationcode='DJ' WHERE linecode='KR' and laststationcode='DJ';

UPDATE t_route
SET towardsstationcode='BSR' WHERE linecode='NS' and laststationcode='BSR';

UPDATE t_route
SET towardsstationcode='DJ' WHERE linecode='NS' and laststationcode='DJ';

UPDATE t_route
SET towardsstationcode='PN' WHERE linecode='NS' and laststationcode='PN';

UPDATE t_route
SET towardsstationcode='VA' WHERE  laststationcode='VA' and linecode='TH';

UPDATE t_route
SET towardsstationcode='WAD' WHERE linecode='MON' and laststationcode = 'WAD';

UPDATE t_route
SET towardsstationcode='CM' WHERE linecode='MON' and laststationcode = 'CM';

UPDATE t_route
SET towardsstationcode='KP' WHERE directioncode='D' and 
			 (laststationcode ='T' OR laststationcode ='DI' 
		OR laststationcode ='G'  OR laststationcode ='T' 
		OR laststationcode ='TT' OR laststationcode ='KU' 
		OR laststationcode ='S'  OR laststationcode ='BL' 
		OR laststationcode ='AM');

UPDATE t_route
SET towardsstationcode='ST' WHERE linecode='CR' and directioncode='U';

UPDATE t_route
SET towardsstationcode='KP' WHERE linecode='CR' and laststationcode='KP';

UPDATE t_route
SET towardsstationcode='N' WHERE directioncode='D' AND 
		(laststationcode ='AN' OR laststationcode ='TT' OR
		laststationcode='N');

UPDATE t_route
SET towardsstationcode = 'KP' WHERE laststationcode = 'K' and directioncode = 'D';



ALTER TABLE t_route
ADD towardsstationcode2 VARCHAR(15);

ALTER TABLE t_route
ADD towardsstation_id2 INT;

UPDATE t_route
SET towardsstationcode2 ='N'
WHERE (laststationcode = 'K'	  OR laststationcode = 'KU'
 OR 	 laststationcode = 'G'    OR laststationcode = 'T'
 OR 	 laststationcode = 'DI')AND linecode='CR'   and directioncode = 'D';

UPDATE t_route
SET towardsstationcode='VER' WHERE linecode='MET' and laststationcode = 'VER';

UPDATE t_route
SET towardsstationcode = 'G' WHERE linecode='MET' and laststationcode = 'G';

UPDATE t_route
SET towardsstation_id= (SELECT t_station.station_id from t_station 
WHERE t_station.stationcode=t_route.towardsstationcode);

UPDATE t_route
SET towardsstation_id2= (SELECT t_station.station_id from t_station 
WHERE t_station.stationcode=t_route.towardsstationcode2);


#DROP TABLE if EXISTS tt3_routedetail;

END$$
DELIMITER;

CALL pb34;