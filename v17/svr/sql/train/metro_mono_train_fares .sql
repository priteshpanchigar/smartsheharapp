DROP TABLE IF EXISTS ta_fare;

CREATE TABLE "ta_fare" (
"_id"  INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
"from_id"  INTEGER,
"fromstation"  TEXT(30),
"tostation"  TEXT(30),
"km"  REAL(5,2),
"fare"  INTEGER,
"linecode"  TEXT(30),
"fare1st"  INTEGER,
"farepass1m"  INTEGER,
"farepass3m"  INTEGER,
"farepass6m"  INTEGER,
"farepass1y"  INTEGER,
"farepass1m1st"  INTEGER,
"farepass3m1st"  INTEGER,
"farepass6m1st"  INTEGER,
"farepass1y1st"  INTEGER,
"token"  INTEGER,
"smartcard"  INTEGER,
"m45trips"  INTEGER,
"istrain"  INTEGER,
"ismetro"  INTEGER,
"ismono"  INTEGER,
"via"  TEXT(30),
"creek"  TEXT(30),
"cidco"  TEXT(30)
);




update ta_fare
set fare=5,farepass1m=100,farepass3m=270, farepass6m = 540, farepass1y = 1080, smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 0.1 and 5 AND creek = "-" AND istrain = 1 AND ismetro = 0 AND ismono = 0;

update ta_fare
set fare=5,farepass1m=100,farepass3m=270, farepass6m = 540, farepass1y = 1080, smartcard = "-", token = "-", m45trips = "-" 
where km BETWEEN 5.01 and 10 AND creek = "-" AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare=10,farepass1m=130,farepass3m=360, farepass6m = 720, farepass1y = 1440, smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 10.01 and 15 AND creek = "-" AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare=10,farepass1m=130,farepass3m=360, farepass6m = 720, farepass1y = 1440, smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 15.01 and 20 AND creek = "-" AND istrain = 1 AND ismetro = 0 AND ismono = 0;


update ta_fare
set fare=10,farepass1m=215,farepass3m=590, farepass6m = 1180, farepass1y = 2360, smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 20.01 and 25 AND creek = "-" AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare=10,farepass1m=215,farepass3m=590, farepass6m = 1180, farepass1y = 2360, smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 25.01 and 30 AND creek = "-" AND istrain = 1 AND ismetro = 0 AND ismono = 0;

update ta_fare
set fare=15,farepass1m=215,farepass3m=590, farepass6m = 1180, farepass1y = 2360, smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 30.01 and 35 AND creek = "-" AND istrain = 1 AND ismetro = 0 AND ismono = 0;


update ta_fare
set fare=15,farepass1m=215,farepass3m=590, farepass6m = 1180, farepass1y = 2360, smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 35.01 and 40 AND creek = "-" AND istrain = 1 AND ismetro = 0 AND ismono = 0;


update ta_fare
set fare=15,farepass1m=215,farepass3m=590, farepass6m = 1180, farepass1y = 2360, smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 40.01 and 45 AND creek = "-" AND istrain = 1 AND ismetro = 0 AND ismono = 0;


update ta_fare
set fare=15,farepass1m=300,farepass3m=820, farepass6m = 1640, farepass1y = 3275, smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 45.01 and 50 AND creek = "-" AND istrain = 1 AND ismetro = 0 AND ismono = 0;


update ta_fare
set fare=15,farepass1m=315,farepass3m=865, farepass6m = 1730, farepass1y = 3455, smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 50.01 and 55 AND creek = "-" AND istrain = 1 AND ismetro = 0 AND ismono = 0;




update ta_fare
set fare=20,farepass1m=315,farepass3m=865, farepass6m = 1730, farepass1y = 3455, smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 55.01 and 60 AND creek = "-" AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare=20,farepass1m=315,farepass3m=865, farepass6m = 1730, farepass1y = 3455, smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 60.01 and 65 AND creek = "-" AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare=20,farepass1m=315,farepass3m=865, farepass6m = 1730, farepass1y = 3455, smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 65.01 and 70 AND creek = "-" AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare=20,farepass1m=400,farepass3m=1095, farepass6m = 2190, farepass1y = 4375, smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 70.01 and 75 AND creek = "-" AND istrain = 1 AND ismetro = 0 AND ismono = 0;


update ta_fare
set fare=20,farepass1m=400,farepass3m=1095, farepass6m = 2190, farepass1y = 4375, smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 75.01 and 80 AND creek = "-" AND istrain = 1 AND ismetro = 0 AND ismono = 0; 



update ta_fare
set fare=25,farepass1m=400,farepass3m=1095, farepass6m = 2190, farepass1y = 4375, smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 80.01 and 85 AND creek = "-" AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare=25,farepass1m=400,farepass3m=1095, farepass6m = 2190, farepass1y = 4375, smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 85.01 and 90 AND creek = "-" AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare=25,farepass1m=400,farepass3m=1095, farepass6m = 2190, farepass1y = 4375, smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 90.01 and 95 AND creek = "-" AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare=30,farepass1m=400,farepass3m=1095, farepass6m = 2190, farepass1y = 4375, smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 95.01 and 100 AND creek = "-" AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare=30,farepass1m=500,farepass3m=1370, farepass6m = 2735, farepass1y = 5475, smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 100.01 and 105 AND creek = "-" AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare=30,farepass1m=500,farepass3m=1370, farepass6m = 2735, farepass1y = 5475, smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 105.01 and 110 AND creek = "-" AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare=30,farepass1m=500,farepass3m=1370, farepass6m = 2735, farepass1y = 5475, smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 110.01 and 115 AND creek = "-" AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare=30,farepass1m=500,farepass3m=1370, farepass6m = 2735, farepass1y = 5475, smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 115.01 and 120 AND creek = "-" AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare=35,farepass1m=500,farepass3m=1370, farepass6m = 2735, farepass1y = 5475, smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 120.01 and 125 AND creek = "-" AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare=35,farepass1m=500,farepass3m=1370, farepass6m = 2735, farepass1y = 5475, smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 125.01 and 130 AND creek = "-" AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare=35,farepass1m=500,farepass3m=1370, farepass6m = 2735, farepass1y = 5475, smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 130.01 and 135 AND creek = "-" AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare=35,farepass1m=585,farepass3m=1600, farepass6m = 3195, farepass1y = 6390, smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 135.01 and 140 AND creek = "-" AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare=35,farepass1m=585,farepass3m=1600, farepass6m = 3195, farepass1y = 6390, smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 140.01 and 145 AND creek = "-" AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare=35,farepass1m=585,farepass3m=1600, farepass6m = 3195, farepass1y = 6390, smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 145.01 and 150 AND creek = "-" AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare1st=50,farepass1m1st = 340,farepass3m1st=920, farepass6m1st = "-", farepass1y1st = "-", smartcard = "-", token = "-", m45trips = "-" 
where km BETWEEN 0.1 and 5 AND creek = "-" AND istrain = 1 AND ismetro = 0 AND ismono = 0;

update ta_fare
set fare1st=50,farepass1m1st = 340,farepass3m1st=920, farepass6m1st = "-", farepass1y1st = "-", smartcard = "-", token = "-", m45trips = "-" 
where km BETWEEN 5.01 and 10 AND creek = "-" AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare1st=70,farepass1m1st = 490,farepass3m1st=1335, farepass6m1st = "-", farepass1y1st = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 10.01 and 15 AND creek = "-" AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare1st=105,farepass1m1st = 565,farepass3m1st=1545, farepass6m1st = "-", farepass1y1st = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 15.01 and 20 AND creek = "-" AND istrain = 1 AND ismetro = 0 AND ismono = 0;


update ta_fare
set fare1st=105,farepass1m1st = 655,farepass3m1st=1785, farepass6m1st = "-", farepass1y1st = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 20.01 and 25 AND creek = "-" AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare1st=140,farepass1m1st = 665,farepass3m1st=1815, farepass6m1st = "-", farepass1y1st = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 25.01 and 30 AND creek = "-" AND istrain = 1 AND ismetro = 0 AND ismono = 0;

update ta_fare
set fare1st=140,farepass1m1st = 750,farepass3m1st=2040, farepass6m1st = "-", farepass1y1st = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 30.01 and 35 AND creek = "-" AND istrain = 1 AND ismetro = 0 AND ismono = 0;


update ta_fare
set fare1st=145,farepass1m1st = 820,farepass3m1st=2235, farepass6m1st = "-", farepass1y1st = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 35.01 and 40 AND creek = "-" AND istrain = 1 AND ismetro = 0 AND ismono = 0;


update ta_fare
set fare1st=145,farepass1m1st = 905,farepass3m1st=2460, farepass6m1st = "-", farepass1y1st = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 40.01 and 45 AND creek = "-" AND istrain = 1 AND ismetro = 0 AND ismono = 0;


update ta_fare
set fare1st=160,farepass1m1st = 985,farepass3m1st=2675, farepass6m1st = "-", farepass1y1st = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 45.01 and 50 AND creek = "-" AND istrain = 1 AND ismetro = 0 AND ismono = 0;


update ta_fare
set fare1st=165,farepass1m1st = 1100,farepass3m1st=2995, farepass6m1st = "-", farepass1y1st = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 50.01 and 55 AND creek = "-" AND istrain = 1 AND ismetro = 0 AND ismono = 0;


update ta_fare
set fare1st=170,farepass1m1st = 1180,farepass3m1st=3205, farepass6m1st = "-", farepass1y1st = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 55.01 and 60 AND creek = "-" AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare1st=170,farepass1m1st = 1190,farepass3m1st=3235, farepass6m1st = "-", farepass1y1st = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 60.01 and 65 AND creek = "-" AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare1st=185,farepass1m1st = 1270,farepass3m1st=3460, farepass6m1st = "-", farepass1y1st = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 65.01 and 70 AND creek = "-" AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare1st=195,farepass1m1st = 1355,farepass3m1st=3685, farepass6m1st = "-", farepass1y1st = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 70.01 and 75 AND creek = "-" AND istrain = 1 AND ismetro = 0 AND ismono = 0;


update ta_fare
set fare1st=195,farepass1m1st = 1435,farepass3m1st=3895, farepass6m1st = "-", farepass1y1st = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 75.01 and 80 AND creek = "-" AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare1st=210,farepass1m1st = 1515,farepass3m1st=4120, farepass6m1st = "-", farepass1y1st = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 80.01 and 85 AND creek = "-" AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare1st=210,farepass1m1st = 1595,farepass3m1st=4335, farepass6m1st = "-", farepass1y1st = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 85.01 and 90 AND creek = "-" AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare1st=210,farepass1m1st = 1605,farepass3m1st=4360, farepass6m1st = "-", farepass1y1st = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 90.01 and 95 AND creek = "-"AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare1st=225,farepass1m1st = 1685,farepass3m1st=4575, farepass6m1st = "-", farepass1y1st = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 95.01 and 100 AND creek = "-" AND istrain = 1 AND ismetro = 0 AND ismono = 0;


update ta_fare
set fare1st=225,farepass1m1st = 1805,farepass3m1st=4905, farepass6m1st = "-", farepass1y1st = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 100.01 and 105 AND creek = "-" AND istrain = 1 AND ismetro = 0 AND ismono = 0;


update ta_fare
set fare1st=235,farepass1m1st = 1810,farepass3m1st=4920, farepass6m1st = "-", farepass1y1st = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 105.01 and 110 AND creek = "-" AND istrain = 1 AND ismetro = 0;



update ta_fare
set fare1st=240,farepass1m1st = 1895,farepass3m1st=5145, farepass6m1st = "-", farepass1y1st = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 110.01 and 115 AND creek = "-" AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare1st=245,farepass1m1st = 1970,farepass3m1st=5360, farepass6m1st = "-", farepass1y1st = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 115.01 and 120 AND creek = "-" AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare1st=245,farepass1m1st = 1990,farepass3m1st=5400, farepass6m1st = "-", farepass1y1st = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 120.01 and 125 AND creek = "-" AND istrain = 1 AND ismetro = 0 AND ismono = 0;


update ta_fare
set fare1st=255,farepass1m1st = 2060,farepass3m1st=5600, farepass6m1st = "-", farepass1y1st = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 125.01 and 130 AND creek = "-" AND istrain = 1 AND ismetro = 0 AND ismono = 0;


update ta_fare
set fare1st=265,farepass1m1st = 2075,farepass3m1st=5640, farepass6m1st = "-", farepass1y1st = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 130.01 and 135 AND creek = "-" AND istrain = 1 AND ismetro = 0 AND ismono = 0;


update ta_fare
set fare1st=270,farepass1m1st = 2155,farepass3m1st=5850, farepass6m1st = "-", farepass1y1st = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 135.01 and 140 AND creek = "-" AND istrain = 1 AND ismetro = 0 AND ismono = 0;


update ta_fare
set fare1st=275,farepass1m1st = 2240,farepass3m1st=6080, farepass6m1st = "-", farepass1y1st = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 140.01 and 145 AND creek = "-" AND istrain = 1 AND ismetro = 0 AND ismono = 0;


update ta_fare
set fare1st=285,farepass1m1st = 2245,farepass3m1st=6090, farepass6m1st = "-", farepass1y1st = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 145.01 and 150 AND creek = "-" AND istrain = 1 AND ismetro = 0 AND ismono = 0;


update ta_fare
set fare=5,farepass1m=150,farepass3m=420, farepass6m = "-", farepass1y = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 0.1 and 5 AND creek = 'T' AND cidco = 'CIDT1' AND istrain = 1 AND ismetro = 0 AND ismono = 0;

update ta_fare
set fare=5,farepass1m=150,farepass3m=420, farepass6m = "-", farepass1y = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 5.01 and 10 AND creek = 'T' AND cidco = 'CIDT1' AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare=10,farepass1m=180,farepass3m=510, farepass6m = "-", farepass1y = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 10.01 and 15 AND creek = 'T' AND cidco = 'CIDT1' AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare=10,farepass1m=180,farepass3m=510, farepass6m = "-", farepass1y = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 15.01 and 20 AND creek = 'T' AND cidco = 'CIDT1' AND istrain = 1 AND ismetro = 0 AND ismono = 0;


update ta_fare
set fare=15,farepass1m=265,farepass3m=740, farepass6m = "-", farepass1y = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 20.01 and 25 AND creek = 'T' AND cidco = 'CIDT1' AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare=15,farepass1m=265,farepass3m=740, farepass6m = "-", farepass1y = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 25.01 and 30 AND creek = 'T' AND cidco = 'CIDT1' AND istrain = 1 AND ismetro = 0 AND ismono = 0;

update ta_fare
set fare=15,farepass1m=265,farepass3m=740, farepass6m = "-", farepass1y = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 30.01 and 35 AND creek = 'T' AND cidco = 'CIDT1' AND istrain = 1 AND ismetro = 0 AND ismono = 0;


update ta_fare
set fare=15,farepass1m=265,farepass3m=740, farepass6m = "-", farepass1y = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 35.01 and 40 AND creek = 'T' AND cidco = 'CIDT1' AND istrain = 1 AND ismetro = 0 AND ismono = 0;


update ta_fare
set fare=15,farepass1m=265,farepass3m=740, farepass6m = "-", farepass1y = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 40.01 and 45 AND creek = 'T' AND cidco = 'CIDT1' AND istrain = 1 AND ismetro = 0 AND ismono = 0;


update ta_fare
set fare=20,farepass1m=350,farepass3m=970, farepass6m = "-", farepass1y = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 45.01 and 50 AND creek = 'T' AND cidco = 'CIDT1' AND istrain = 1 AND ismetro = 0 AND ismono = 0;


update ta_fare
set fare=20,farepass1m=365,farepass3m=1015, farepass6m = "-", farepass1y = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 50.01 and 55 AND creek = 'T' AND cidco = 'CIDT1' AND istrain = 1 AND ismetro = 0 AND ismono = 0;




update ta_fare
set fare=20,farepass1m=365,farepass3m=1015, farepass6m = "-", farepass1y = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 55.01 and 60 AND creek = 'T' AND cidco = 'CIDT1' AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare=20,farepass1m=365,farepass3m=1015, farepass6m = "-", farepass1y = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 60.01 and 65 AND creek = 'T' AND cidco = 'CIDT1' AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare=20,farepass1m=365,farepass3m=1015, farepass6m = "-", farepass1y = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 65.01 and 70 AND creek = 'T' AND cidco = 'CIDT1' AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare=25,farepass1m=450,farepass3m=1245, farepass6m = "-", farepass1y = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 70.01 and 75 AND creek = 'T' AND cidco = 'CIDT1' AND istrain = 1 AND ismetro = 0 AND ismono = 0;


update ta_fare
set fare=25,farepass1m=450,farepass3m=1245, farepass6m = "-", farepass1y = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 75.01 and 80 AND creek = 'T' AND cidco = 'CIDT1' AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare=30,farepass1m=450,farepass3m=1245, farepass6m = "-", farepass1y = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 80.01 and 85 AND creek = 'T' AND cidco = 'CIDT1' AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare=30,farepass1m=450,farepass3m=1245, farepass6m = "-", farepass1y = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 85.01 and 90 AND creek = 'T' AND cidco = 'CIDT1' AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare=30,farepass1m=450,farepass3m=1245, farepass6m = "-", farepass1y = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 90.01 and 95 AND creek = 'T' AND cidco = 'CIDT1' AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare=30,farepass1m=450,farepass3m=1245, farepass6m = "-", farepass1y = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 95.01 and 100 AND creek = 'T' AND cidco = 'CIDT1' AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare=30,farepass1m=550,farepass3m=1520, farepass6m = "-", farepass1y = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 100.01 and 105 AND creek = 'T' AND cidco = 'CIDT1' AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare=30,farepass1m=550,farepass3m=1520, farepass6m = "-", farepass1y = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 105.01 and 110 AND creek = 'T' AND cidco = 'CIDT1' AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare=30,farepass1m=550,farepass3m=1520, farepass6m = "-", farepass1y = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 110.01 and 115 AND creek = 'T' AND cidco = 'CIDT1' AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare=35,farepass1m=550,farepass3m=1520, farepass6m = "-", farepass1y = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 115.01 and 120 AND creek = 'T' AND cidco = 'CIDT1' AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare=35,farepass1m=550,farepass3m=1520, farepass6m = "-", farepass1y = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 120.01 and 125 AND creek = 'T' AND cidco = 'CIDT1' AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare=35,farepass1m=550,farepass3m=1520, farepass6m = "-", farepass1y = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 125.01 and 130 AND creek = 'T' AND cidco = 'CIDT1' AND istrain = 1 AND ismetro = 0 AND ismono = 0;


update ta_fare
set fare1st=55,farepass1m1st = 470,farepass3m1st=1305, farepass6m1st = "-", farepass1y1st = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 0.1 and 5 AND creek = 'T' AND cidco = 'CIDT1' AND istrain = 1 AND ismetro = 0 AND ismono = 0;

update ta_fare
set fare1st=55,farepass1m1st = 470,farepass3m1st=1315, farepass6m1st = "-", farepass1y1st = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 5.01 and 10 AND creek = 'T' AND cidco = 'CIDT1' AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare1st=75,farepass1m1st = 620,farepass3m1st=1725, farepass6m1st = "-", farepass1y1st = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 10.01 and 15 AND creek = 'T' AND cidco = 'CIDT1' AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare1st=110,farepass1m1st = 695,farepass3m1st=1935, farepass6m1st = "-", farepass1y1st = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 15.01 and 20 AND creek = 'T' AND cidco = 'CIDT1' AND istrain = 1 AND ismetro = 0 AND ismono = 0;


update ta_fare
set fare1st=110,farepass1m1st = 785,farepass3m1st=2175, farepass6m1st = "-", farepass1y1st = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 20.01 and 25 AND creek = 'T' AND cidco = 'CIDT1' AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare1st=140,farepass1m1st = 795,farepass3m1st=2205, farepass6m1st = "-", farepass1y1st = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 25.01 and 30 AND creek = 'T' AND cidco = 'CIDT1' AND istrain = 1 AND ismetro = 0 AND ismono = 0;

update ta_fare
set fare1st=145,farepass1m1st = 880,farepass3m1st=2430, farepass6m1st = "-", farepass1y1st = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 30.01 and 35 AND creek = 'T' AND cidco = 'CIDT1' AND istrain = 1 AND ismetro = 0 AND ismono = 0;


update ta_fare
set fare1st=150,farepass1m1st = 955,farepass3m1st=2630, farepass6m1st = "-", farepass1y1st = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 35.01 and 40 AND creek = 'T' AND cidco = 'CIDT1' AND istrain = 1 AND ismetro = 0 AND ismono = 0;


update ta_fare
set fare1st=150,farepass1m1st = 1035,farepass3m1st=2855, farepass6m1st = "-", farepass1y1st = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 40.01 and 45 AND creek = 'T' AND cidco = 'CIDT1' AND istrain = 1 AND ismetro = 0 AND ismono = 0;


update ta_fare
set fare1st=165,farepass1m1st = 1115,farepass3m1st=3065, farepass6m1st = "-", farepass1y1st = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 45.01 and 50 AND creek = 'T' AND cidco = 'CIDT1' AND istrain = 1 AND ismetro = 0 AND ismono = 0;


update ta_fare
set fare1st=170,farepass1m1st = 1230,farepass3m1st=3385, farepass6m1st = "-", farepass1y1st = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 50.01 and 55 AND creek = 'T' AND cidco = 'CIDT1' AND istrain = 1 AND ismetro = 0 AND ismono = 0;


update ta_fare
set fare1st=175,farepass1m1st = 1310,farepass3m1st=3595, farepass6m1st = "-", farepass1y1st = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 55.01 and 60 AND creek = 'T' AND cidco = 'CIDT1' AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare1st=175,farepass1m1st = 1320,farepass3m1st=3625, farepass6m1st = "-", farepass1y1st = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 60.01 and 65 AND creek = 'T' AND cidco = 'CIDT1' AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare1st=185,farepass1m1st = 1400,farepass3m1st=3850, farepass6m1st = "-", farepass1y1st = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 65.01 and 70 AND creek = 'T' AND cidco = 'CIDT1' AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare1st=195,farepass1m1st = 1485,farepass3m1st=4075, farepass6m1st = "-", farepass1y1st = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 70.01 and 75 AND creek = 'T' AND cidco = 'CIDT1' AND istrain = 1 AND ismetro = 0 AND ismono = 0;


update ta_fare
set fare1st=200,farepass1m1st = 1565,farepass3m1st=4290, farepass6m1st = "-", farepass1y1st = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 75.01 and 80 AND creek = 'T' AND cidco = 'CIDT1' AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare1st=210,farepass1m1st = 1650,farepass3m1st=4515, farepass6m1st = "-", farepass1y1st = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 80.01 and 85 AND creek = 'T' AND cidco = 'CIDT1' AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare1st=215,farepass1m1st = 1725,farepass3m1st=4725, farepass6m1st = "-", farepass1y1st = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 85.01 and 90 AND creek = 'T' AND cidco = 'CIDT1' AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare1st=215,farepass1m1st = 1735,farepass3m1st=4755, farepass6m1st = "-", farepass1y1st = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 90.01 and 95 AND creek = 'T' AND cidco = 'CIDT1' AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare1st=225,farepass1m1st = 1815,farepass3m1st=4965, farepass6m1st = "-", farepass1y1st = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 95.01 and 100 AND creek = 'T' AND cidco = 'CIDT1' AND istrain = 1 AND ismetro = 0 AND ismono = 0;


update ta_fare
set fare1st=230,farepass1m1st = 1935,farepass3m1st=5300, farepass6m1st = "-", farepass1y1st = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 100.01 and 105 AND creek = 'T' AND cidco = 'CIDT1' AND istrain = 1 AND ismetro = 0 AND ismono = 0;


update ta_fare
set fare1st=240,farepass1m1st = 1940,farepass3m1st=5315, farepass6m1st = "-", farepass1y1st = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 105.01 and 110 AND creek = 'T' AND cidco = 'CIDT1' AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare1st=245,farepass1m1st = 2025,farepass3m1st=5540, farepass6m1st = "-", farepass1y1st = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 110.01 and 115 AND creek = 'T' AND cidco = 'CIDT1' AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare1st=250,farepass1m1st = 2100,farepass3m1st=5750, farepass6m1st = "-", farepass1y1st = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 115.01 and 120 AND creek = 'T' AND cidco = 'CIDT1' AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare1st=250,farepass1m1st = 2120,farepass3m1st=5790, farepass6m1st = "-", farepass1y1st = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 120.01 and 125 AND creek = 'T' AND cidco = 'CIDT1' AND istrain = 1 AND ismetro = 0 AND ismono = 0;


update ta_fare
set fare1st=260,farepass1m1st = 2190,farepass3m1st=5990, farepass6m1st = "-", farepass1y1st = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 125.01 and 130 AND creek = 'T' AND cidco = 'CIDT1' AND istrain = 1 AND ismetro = 0 AND ismono = 0;


update ta_fare
set fare=15,farepass1m=200,farepass3m=560, farepass6m = "-", farepass1y = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 15.01 and 20 AND creek = 'T' AND cidco = 'CIDT2' AND istrain = 1 AND ismetro = 0 AND ismono = 0;


update ta_fare
set fare=15,farepass1m=285,farepass3m=790, farepass6m = "-", farepass1y = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 20.01 and 25 AND creek = 'T' AND cidco = 'CIDT2' AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare=15,farepass1m=285,farepass3m=790, farepass6m = "-", farepass1y = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 25.01 and 30 AND creek = 'T' AND cidco = 'CIDT2' AND istrain = 1 AND ismetro = 0 AND ismono = 0;

update ta_fare
set fare=15,farepass1m=285,farepass3m=790, farepass6m = "-", farepass1y = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 30.01 and 35 AND creek = 'T' AND cidco = 'CIDT2' AND istrain = 1 AND ismetro = 0 AND ismono = 0;


update ta_fare
set fare=15,farepass1m=285,farepass3m=790, farepass6m = "-", farepass1y = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 35.01 and 40 AND creek = 'T' AND cidco = 'CIDT2' AND istrain = 1 AND ismetro = 0 AND ismono = 0;


update ta_fare
set fare=20,farepass1m=285,farepass3m=790, farepass6m = "-", farepass1y = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 40.01 and 45 AND creek = 'T' AND cidco = 'CIDT2' AND istrain = 1 AND ismetro = 0 AND ismono = 0;


update ta_fare
set fare=20,farepass1m=370,farepass3m=1020, farepass6m = "-", farepass1y = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 45.01 and 50 AND creek = 'T' AND cidco = 'CIDT2' AND istrain = 1 AND ismetro = 0 AND ismono = 0;


update ta_fare
set fare=20,farepass1m=385,farepass3m=1065, farepass6m = "-", farepass1y = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 50.01 and 55 AND creek = 'T' AND cidco = 'CIDT2' AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare=20,farepass1m=385,farepass3m=1065, farepass6m = "-", farepass1y = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 55.01 and 60 AND creek = 'T' AND cidco = 'CIDT2' AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare=20,farepass1m=385,farepass3m=1065, farepass6m = "-", farepass1y = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 60.01 and 65 AND creek = 'T' AND cidco = 'CIDT2' AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare=20,farepass1m=385,farepass3m=1065, farepass6m = "-", farepass1y = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 65.01 and 70 AND creek = 'T' AND cidco = 'CIDT2' AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare=25,farepass1m=470,farepass3m=1295, farepass6m = "-", farepass1y = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 70.01 and 75 AND creek = 'T' AND cidco = 'CIDT2' AND istrain = 1 AND ismetro = 0 AND ismono = 0;


update ta_fare
set fare=25,farepass1m=470,farepass3m=1295, farepass6m = "-", farepass1y = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 75.01 and 80 AND creek = 'T' AND cidco = 'CIDT2' AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare=30,farepass1m=470,farepass3m=1295, farepass6m = "-", farepass1y = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 80.01 and 85 AND creek = 'T' AND cidco = 'CIDT2' AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare=30,farepass1m=470,farepass3m=1295, farepass6m = "-", farepass1y = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 85.01 and 90 AND creek = 'T' AND cidco = 'CIDT2' AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare=30,farepass1m=470,farepass3m=1295, farepass6m = "-", farepass1y = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 90.01 and 95 AND creek = 'T' AND cidco = 'CIDT2' AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare=30,farepass1m=470,farepass3m=1295, farepass6m = "-", farepass1y = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 95.01 and 100 AND creek = 'T' AND cidco = 'CIDT2' AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare=30,farepass1m=570,farepass3m=1570, farepass6m = "-", farepass1y = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 100.01 and 105 AND creek = 'T' AND cidco = 'CIDT2' AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare=30,farepass1m=570,farepass3m=1570, farepass6m = "-", farepass1y = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 105.01 and 110 AND creek = 'T' AND cidco = 'CIDT2' AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare=35,farepass1m=570,farepass3m=1570, farepass6m = "-", farepass1y = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 110.01 and 115 AND creek = 'T' AND cidco = 'CIDT2' AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare=35,farepass1m=570,farepass3m=1570, farepass6m = "-", farepass1y = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 115.01 and 120 AND creek = 'T' AND cidco = 'CIDT2' AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare=35,farepass1m=570,farepass3m=1570, farepass6m = "-", farepass1y = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 120.01 and 125 AND creek = 'T' AND cidco = 'CIDT2' AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare=35,farepass1m=570,farepass3m=1570, farepass6m = "-", farepass1y = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 125.01 and 130 AND creek = 'T' AND cidco = 'CIDT2' AND istrain = 1 AND ismetro = 0 AND ismono = 0;


update ta_fare
set fare=35,farepass1m=570,farepass3m=1570, farepass6m = "-", farepass1y = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 130.01 and 135 AND creek = 'T' AND cidco = 'CIDT2' AND istrain = 1 AND ismetro = 0 AND ismono = 0;


update ta_fare
set fare=40,farepass1m=655,farepass3m=1800, farepass6m = "-", farepass1y = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 135.01 and 140 AND creek = 'T' AND cidco = 'CIDT2' AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare1st=110,farepass1m1st = 740,farepass3m1st=2070, farepass6m1st = "-", farepass1y1st = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 15.01 and 20 AND creek = 'T' AND cidco = 'CIDT2' AND istrain = 1 AND ismetro = 0 AND ismono = 0;


update ta_fare
set fare1st=110,farepass1m1st = 830,farepass3m1st=2310, farepass6m1st = "-", farepass1y1st = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 20.01 and 25 AND creek = 'T' AND cidco = 'CIDT2' AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare1st=145,farepass1m1st = 840,farepass3m1st=2335, farepass6m1st = "-", farepass1y1st = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 25.01 and 30 AND creek = 'T' AND cidco = 'CIDT2' AND istrain = 1 AND ismetro = 0 AND ismono = 0;

update ta_fare
set fare1st=145,farepass1m1st = 925,farepass3m1st=2560, farepass6m1st = "-", farepass1y1st = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 30.01 and 35 AND creek = 'T' AND cidco = 'CIDT2' AND istrain = 1 AND ismetro = 0 AND ismono = 0;


update ta_fare
set fare1st=150,farepass1m1st = 995,farepass3m1st=2760, farepass6m1st = "-", farepass1y1st = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 35.01 and 40 AND creek = 'T' AND cidco = 'CIDT2' AND istrain = 1 AND ismetro = 0 AND ismono = 0;


update ta_fare
set fare1st=150,farepass1m1st = 1080,farepass3m1st=2985, farepass6m1st = "-", farepass1y1st = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 40.01 and 45 AND creek = 'T' AND cidco = 'CIDT2' AND istrain = 1 AND ismetro = 0 AND ismono = 0;


update ta_fare
set fare1st=165,farepass1m1st = 1160,farepass3m1st=3195, farepass6m1st = "-", farepass1y1st = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 45.01 and 50 AND creek = 'T' AND cidco = 'CIDT2' AND istrain = 1 AND ismetro = 0 AND ismono = 0;


update ta_fare
set fare1st=170,farepass1m1st = 1275,farepass3m1st=3515, farepass6m1st = "-", farepass1y1st = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 50.01 and 55 AND creek = 'T' AND cidco = 'CIDT2' AND istrain = 1 AND ismetro = 0 AND ismono = 0;


update ta_fare
set fare1st=175,farepass1m1st = 1350,farepass3m1st=3730, farepass6m1st = "-", farepass1y1st = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 55.01 and 60 AND creek = 'T' AND cidco = 'CIDT2' AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare1st=175,farepass1m1st = 1365,farepass3m1st=3755, farepass6m1st = "-", farepass1y1st = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 60.01 and 65 AND creek = 'T' AND cidco = 'CIDT2' AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare1st=190,farepass1m1st = 1445,farepass3m1st=3980, farepass6m1st = "-", farepass1y1st = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 65.01 and 70 AND creek = 'T' AND cidco = 'CIDT2' AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare1st=200,farepass1m1st = 1530,farepass3m1st=4210, farepass6m1st = "-", farepass1y1st = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 70.01 and 75 AND creek = 'T' AND cidco = 'CIDT2' AND istrain = 1 AND ismetro = 0 AND ismono = 0;


update ta_fare
set fare1st=200,farepass1m1st = 1610,farepass3m1st=4420, farepass6m1st = "-", farepass1y1st = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 75.01 and 80 AND creek = 'T' AND cidco = 'CIDT2' AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare1st=215,farepass1m1st = 1690,farepass3m1st=4645, farepass6m1st = "-", farepass1y1st = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 80.01 and 85 AND creek = 'T' AND cidco = 'CIDT2' AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare1st=215,farepass1m1st = 1770,farepass3m1st=4855, farepass6m1st = "-", farepass1y1st = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 85.01 and 90 AND creek = 'T' AND cidco = 'CIDT2' AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare1st=215,farepass1m1st = 1780,farepass3m1st=4885, farepass6m1st = "-", farepass1y1st = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 90.01 and 95 AND creek = 'T' AND cidco = 'CIDT2' AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare1st=230,farepass1m1st = 1860,farepass3m1st=5095, farepass6m1st = "-", farepass1y1st = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 95.01 and 100 AND creek = 'T' AND cidco = 'CIDT2' AND istrain = 1 AND ismetro = 0 AND ismono = 0;


update ta_fare
set fare1st=235,farepass1m1st = 1980,farepass3m1st=5430, farepass6m1st = "-", farepass1y1st = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 100.01 and 105 AND creek = 'T' AND cidco = 'CIDT2' AND istrain = 1 AND ismetro = 0 AND ismono = 0;


update ta_fare
set fare1st=245,farepass1m1st = 1985,farepass3m1st=5445, farepass6m1st = "-", farepass1y1st = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 105.01 and 110 AND creek = 'T' AND cidco = 'CIDT2' AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare1st=245,farepass1m1st = 2070,farepass3m1st=5670, farepass6m1st = "-", farepass1y1st = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 110.01 and 115 AND creek = 'T' AND cidco = 'CIDT2' AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare1st=250,farepass1m1st = 2145,farepass3m1st=5880, farepass6m1st = "-", farepass1y1st = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 115.01 and 120 AND creek = 'T' AND cidco = 'CIDT2' AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare1st=250,farepass1m1st = 2160,farepass3m1st=5925, farepass6m1st = "-", farepass1y1st = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 120.01 and 125 AND creek = 'T' AND cidco = 'CIDT2' AND istrain = 1 AND ismetro = 0 AND ismono = 0;


update ta_fare
set fare1st=260,farepass1m1st = 2235,farepass3m1st=6120, farepass6m1st = "-", farepass1y1st = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 125.01 and 130 AND creek = 'T' AND cidco = 'CIDT2' AND istrain = 1 AND ismetro = 0 AND ismono = 0;


update ta_fare
set fare1st=270,farepass1m1st = 2250,farepass3m1st=6165, farepass6m1st = "-", farepass1y1st = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 130.01 and 135 AND creek = 'T' AND cidco = 'CIDT2' AND istrain = 1 AND ismetro = 0 AND ismono = 0;


update ta_fare
set fare1st=275,farepass1m1st = 2330,farepass3m1st=6375, farepass6m1st = "-", farepass1y1st = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 135.01 and 140 AND creek = 'T' AND cidco = 'CIDT2' AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare=15,farepass1m=200,farepass3m=560, farepass6m = "-", farepass1y = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 15.01 and 20 AND creek = 'K' AND cidco = 'CIDK1' AND istrain = 1 AND ismetro = 0 AND ismono = 0;


update ta_fare
set fare=15,farepass1m=285,farepass3m=790, farepass6m = "-", farepass1y = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 20.01 and 25 AND creek = 'K' AND cidco = 'CIDK1' AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare=15,farepass1m=285,farepass3m=790, farepass6m = "-", farepass1y = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 25.01 and 30 AND creek = 'K' AND cidco = 'CIDK1' AND istrain = 1 AND ismetro = 0 AND ismono = 0;

update ta_fare
set fare=15,farepass1m=285,farepass3m=790, farepass6m = "-", farepass1y = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 30.01 and 35 AND creek = 'K' AND cidco = 'CIDK1' AND istrain = 1 AND ismetro = 0 AND ismono = 0;


update ta_fare
set fare=15,farepass1m=285,farepass3m=790, farepass6m = "-", farepass1y = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 35.01 and 40 AND creek = 'K' AND cidco = 'CIDK1' AND istrain = 1 AND ismetro = 0 AND ismono = 0;


update ta_fare
set fare=20,farepass1m=285,farepass3m=790, farepass6m = "-", farepass1y = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 40.01 and 45 AND creek = 'K' AND cidco = 'CIDK1' AND istrain = 1 AND ismetro = 0 AND ismono = 0;


update ta_fare
set fare=20,farepass1m=370,farepass3m=1020, farepass6m = "-", farepass1y = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 45.01 and 50 AND creek = 'K' AND cidco = 'CIDK1' AND istrain = 1 AND ismetro = 0 AND ismono = 0;


update ta_fare
set fare=20,farepass1m=385,farepass3m=1065, farepass6m = "-", farepass1y = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 50.01 and 55 AND creek = 'K' AND cidco = 'CIDK1' AND istrain = 1 AND ismetro = 0 AND ismono = 0;




update ta_fare
set fare=20,farepass1m=385,farepass3m=1065, farepass6m = "-", farepass1y = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 55.01 and 60 AND creek = 'K' AND cidco = 'CIDK1' AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare=20,farepass1m=385,farepass3m=1065, farepass6m = "-", farepass1y = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 60.01 and 65 AND creek = 'K' AND cidco = 'CIDK1' AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare=20,farepass1m=385,farepass3m=1065, farepass6m = "-", farepass1y = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 65.01 and 70 AND creek = 'K' AND cidco = 'CIDK1' AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare=25,farepass1m=470,farepass3m=1295, farepass6m = "-", farepass1y = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 70.01 and 75 AND creek = 'K' AND cidco = 'CIDK1' AND istrain = 1 AND ismetro = 0 AND ismono = 0;


update ta_fare
set fare=25,farepass1m=470,farepass3m=1295, farepass6m = "-", farepass1y = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 75.01 and 80 AND creek = 'K' AND cidco = 'CIDK1' AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare=30,farepass1m=470,farepass3m=1295, farepass6m = "-", farepass1y = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 80.01 and 85 AND creek = 'K' AND cidco = 'CIDK1' AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare=30,farepass1m=470,farepass3m=1295, farepass6m = "-", farepass1y = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 85.01 and 90 AND creek = 'K' AND cidco = 'CIDK1' AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare=30,farepass1m=470,farepass3m=1295, farepass6m = "-", farepass1y = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 90.01 and 95 AND creek = 'K' AND cidco = 'CIDK1' AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare=30,farepass1m=470,farepass3m=1295, farepass6m = "-", farepass1y = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 95.01 and 100 AND creek = 'K' AND cidco = 'CIDK1' AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare=30,farepass1m=570,farepass3m=1570, farepass6m = "-", farepass1y = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 100.01 and 105 AND creek = 'K' AND cidco = 'CIDK1' AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare=30,farepass1m=570,farepass3m=1570, farepass6m = "-", farepass1y = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 105.01 and 110 AND creek = 'K' AND cidco = 'CIDK1' AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare=35,farepass1m=570,farepass3m=1570, farepass6m = "-", farepass1y = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 110.01 and 115 AND creek = 'K' AND cidco = 'CIDK1' AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare=35,farepass1m=570,farepass3m=1570, farepass6m = "-", farepass1y = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 115.01 and 120 AND creek = 'K' AND cidco = 'CIDK1' AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare=35,farepass1m=570,farepass3m=1570, farepass6m = "-", farepass1y = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 120.01 and 125 AND creek = 'K' AND cidco = 'CIDK1' AND istrain = 1 AND ismetro = 0 AND ismono = 0;


update ta_fare
set fare1st=110,farepass1m1st = 740,farepass3m1st=2070, farepass6m1st = "-", farepass1y1st = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 15.01 and 20 AND creek = 'K' AND cidco = 'CIDK1' AND istrain = 1 AND ismetro = 0 AND ismono = 0;


update ta_fare
set fare1st=110,farepass1m1st = 830,farepass3m1st=2310, farepass6m1st = "-", farepass1y1st = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 20.01 and 25 AND creek = 'K' AND cidco = 'CIDK1' AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare1st=145,farepass1m1st = 840,farepass3m1st=2335, farepass6m1st = "-", farepass1y1st = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 25.01 and 30 AND creek = 'K' AND cidco = 'CIDK1' AND istrain = 1 AND ismetro = 0 AND ismono = 0;

update ta_fare
set fare1st=145,farepass1m1st = 925,farepass3m1st=2560, farepass6m1st = "-", farepass1y1st = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 30.01 and 35 AND creek = 'K' AND cidco = 'CIDK1' AND istrain = 1 AND ismetro = 0 AND ismono = 0;


update ta_fare
set fare1st=150,farepass1m1st = 995,farepass3m1st=2760, farepass6m1st = "-", farepass1y1st = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 35.01 and 40 AND creek = 'K' AND cidco = 'CIDK1' AND istrain = 1 AND ismetro = 0 AND ismono = 0;


update ta_fare
set fare1st=150,farepass1m1st = 1080,farepass3m1st=2985, farepass6m1st = "-", farepass1y1st = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 40.01 and 45 AND creek = 'K' AND cidco = 'CIDK1' AND istrain = 1 AND ismetro = 0 AND ismono = 0;


update ta_fare
set fare1st=165,farepass1m1st = 1160,farepass3m1st=3195, farepass6m1st = "-", farepass1y1st = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 45.01 and 50 AND creek = 'K' AND cidco = 'CIDK1' AND istrain = 1 AND ismetro = 0 AND ismono = 0;


update ta_fare
set fare1st=170,farepass1m1st = 1275,farepass3m1st=3515, farepass6m1st = "-", farepass1y1st = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 50.01 and 55 AND creek = 'K' AND cidco = 'CIDK1' AND istrain = 1 AND ismetro = 0 AND ismono = 0;


update ta_fare
set fare1st=175,farepass1m1st = 1350,farepass3m1st=3730, farepass6m1st = "-", farepass1y1st = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 55.01 and 60 AND creek = 'K' AND cidco = 'CIDK1' AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare1st=175,farepass1m1st = 1365,farepass3m1st=3755, farepass6m1st = "-", farepass1y1st = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 60.01 and 65 AND creek = 'K' AND cidco = 'CIDK1' AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare1st=190,farepass1m1st = 1445,farepass3m1st=3980, farepass6m1st = "-", farepass1y1st = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 65.01 and 70 AND creek = 'K' AND cidco = 'CIDK1' AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare1st=200,farepass1m1st = 1530,farepass3m1st=4210, farepass6m1st = "-", farepass1y1st = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 70.01 and 75 AND creek = 'K' AND cidco = 'CIDK1' AND istrain = 1 AND ismetro = 0 AND ismono = 0;


update ta_fare
set fare1st=200,farepass1m1st = 1610,farepass3m1st=4420, farepass6m1st = "-", farepass1y1st = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 75.01 and 80 AND creek = 'K' AND cidco = 'CIDK1' AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare1st=215,farepass1m1st = 1690,farepass3m1st=4645, farepass6m1st = "-", farepass1y1st = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 80.01 and 85 AND creek = 'K' AND cidco = 'CIDK1' AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare1st=215,farepass1m1st = 1770,farepass3m1st=4855, farepass6m1st = "-", farepass1y1st = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 85.01 and 90 AND creek = 'K' AND cidco = 'CIDK1' AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare1st=215,farepass1m1st = 1780,farepass3m1st=4885, farepass6m1st = "-", farepass1y1st = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 90.01 and 95 AND creek = 'K' AND cidco = 'CIDK1' AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare1st=230,farepass1m1st = 1860,farepass3m1st=5095, farepass6m1st = "-", farepass1y1st = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 95.01 and 100 AND creek = 'K' AND cidco = 'CIDK1' AND istrain = 1 AND ismetro = 0 AND ismono = 0;


update ta_fare
set fare1st=235,farepass1m1st = 1980,farepass3m1st=5430, farepass6m1st = "-", farepass1y1st = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 100.01 and 105 AND creek = 'K' AND cidco = 'CIDK1' AND istrain = 1 AND ismetro = 0 AND ismono = 0;


update ta_fare
set fare1st=245,farepass1m1st = 1985,farepass3m1st=5445, farepass6m1st = "-", farepass1y1st = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 105.01 and 110 AND creek = 'K' AND cidco = 'CIDK1' AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare1st=245,farepass1m1st = 2070,farepass3m1st=5670, farepass6m1st = "-", farepass1y1st = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 110.01 and 115 AND creek = 'K' AND cidco = 'CIDK1' AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare1st=250,farepass1m1st = 2145,farepass3m1st=5880, farepass6m1st = "-", farepass1y1st = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 115.01 and 120 AND creek = 'K' AND cidco = 'CIDK1' AND istrain = 1 AND ismetro = 0 AND ismono = 0;


update ta_fare
set fare1st=250,farepass1m1st = 2160,farepass3m1st=5925, farepass6m1st = "-", farepass1y1st = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 120.01 and 125 AND creek = 'K' AND cidco = 'CIDK1' AND istrain = 1 AND ismetro = 0 AND ismono = 0;

update ta_fare
set fare=15,farepass1m=295,farepass3m=830, farepass6m = "-", farepass1y = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 25.01 and 30 AND creek = 'K' AND cidco = 'CIDK2' AND istrain = 1 AND ismetro = 0 AND ismono = 0;


update ta_fare
set fare=15,farepass1m=295,farepass3m=830, farepass6m = "-", farepass1y = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 30.01 and 35 AND creek = 'K' AND cidco = 'CIDK2' AND istrain = 1 AND ismetro = 0 AND ismono = 0;


update ta_fare
set fare=20,farepass1m=295,farepass3m=830, farepass6m = "-", farepass1y = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 35.01 and 40 AND creek = 'K' AND cidco = 'CIDK2' AND istrain = 1 AND ismetro = 0 AND ismono = 0;


update ta_fare
set fare=20,farepass1m=295,farepass3m=830, farepass6m = "-", farepass1y = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 40.01 and 45 AND creek = 'K' AND cidco = 'CIDK2' AND istrain = 1 AND ismetro = 0 AND ismono = 0;


update ta_fare
set fare=20,farepass1m=380,farepass3m=1060, farepass6m = "-", farepass1y = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 45.01 and 50 AND creek = 'K' AND cidco = 'CIDK2' AND istrain = 1 AND ismetro = 0 AND ismono = 0;


update ta_fare
set fare=20,farepass1m=395,farepass3m=1105, farepass6m = "-", farepass1y = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 50.01 and 55 AND creek = 'K' AND cidco = 'CIDK2' AND istrain = 1 AND ismetro = 0 AND ismono = 0;




update ta_fare
set fare=20,farepass1m=395,farepass3m=1105, farepass6m = "-", farepass1y = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 55.01 and 60 AND creek = 'K' AND cidco = 'CIDK2' AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare=20,farepass1m=395,farepass3m=1105, farepass6m = "-", farepass1y = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 60.01 and 65 AND creek = 'K' AND cidco = 'CIDK2' AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare=25,farepass1m=395,farepass3m=1105, farepass6m = "-", farepass1y = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 65.01 and 70 AND creek = 'K' AND cidco = 'CIDK2' AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare=25,farepass1m=480,farepass3m=1335, farepass6m = "-", farepass1y = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 70.01 and 75 AND creek = 'K' AND cidco = 'CIDK2' AND istrain = 1 AND ismetro = 0 AND ismono = 0;


update ta_fare
set fare=25,farepass1m=480,farepass3m=1335, farepass6m = "-", farepass1y = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 75.01 and 80 AND creek = 'K' AND cidco = 'CIDK2' AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare=30,farepass1m=480,farepass3m=1335, farepass6m = "-", farepass1y = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 80.01 and 85 AND creek = 'K' AND cidco = 'CIDK2' AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare=30,farepass1m=480,farepass3m=1335, farepass6m = "-", farepass1y = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 85.01 and 90 AND creek = 'K' AND cidco = 'CIDK2' AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare=30,farepass1m=480,farepass3m=1335, farepass6m = "-", farepass1y = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 90.01 and 95 AND creek = 'K' AND cidco = 'CIDK2' AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare=30,farepass1m=480,farepass3m=1335, farepass6m = "-", farepass1y = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 95.01 and 100 AND creek = 'K' AND cidco = 'CIDK2' AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare=35,farepass1m=580,farepass3m=1610, farepass6m = "-", farepass1y = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 100.01 and 105 AND creek = 'K' AND cidco = 'CIDK2' AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare=35,farepass1m=580,farepass3m=1610, farepass6m = "-", farepass1y = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 105.01 and 110 AND creek = 'K' AND cidco = 'CIDK2' AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare=35,farepass1m=580,farepass3m=1610, farepass6m = "-", farepass1y = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 110.01 and 115 AND creek = 'K' AND cidco = 'CIDK2' AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare=35,farepass1m=580,farepass3m=1610, farepass6m = "-", farepass1y = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 115.01 and 120 AND creek = 'K' AND cidco = 'CIDK2' AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare=35,farepass1m=580,farepass3m=1610, farepass6m = "-", farepass1y = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 120.01 and 125 AND creek = 'K' AND cidco = 'CIDK2' AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare1st=145,farepass1m1st =960,farepass3m1st=2665, farepass6m1st = "-", farepass1y1st = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 30.01 and 35 AND creek = 'K' AND cidco = 'CIDK2' AND istrain = 1 AND ismetro = 0 AND ismono = 0;


update ta_fare
set fare1st=150,farepass1m1st =1030,farepass3m1st=2865, farepass6m1st = "-", farepass1y1st = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 35.01 and 40 AND creek = 'K' AND cidco = 'CIDK2' AND istrain = 1 AND ismetro = 0 AND ismono = 0;


update ta_fare
set fare1st=155,farepass1m1st = 1115,farepass3m1st=3090, farepass6m1st = "-", farepass1y1st = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 40.01 and 45 AND creek = 'K' AND cidco = 'CIDK2' AND istrain = 1 AND ismetro = 0 AND ismono = 0;


update ta_fare
set fare1st=165,farepass1m1st = 1195,farepass3m1st=3300, farepass6m1st = "-", farepass1y1st = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 45.01 and 50 AND creek = 'K' AND cidco = 'CIDK2' AND istrain = 1 AND ismetro = 0 AND ismono = 0;


update ta_fare
set fare1st=170,farepass1m1st = 1310,farepass3m1st=3620, farepass6m1st = "-", farepass1y1st = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 50.01 and 55 AND creek = 'K' AND cidco = 'CIDK2' AND istrain = 1 AND ismetro = 0 AND ismono = 0;


update ta_fare
set fare1st=175,farepass1m1st = 1385,farepass3m1st=3830, farepass6m1st = "-", farepass1y1st = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 55.01 and 60 AND creek = 'K' AND cidco = 'CIDK2' AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare1st=175,farepass1m1st = 1395,farepass3m1st=3860, farepass6m1st = "-", farepass1y1st = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 60.01 and 65 AND creek = 'K' AND cidco = 'CIDK2' AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare1st=190,farepass1m1st = 1480,farepass3m1st=4085, farepass6m1st = "-", farepass1y1st = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 65.01 and 70 AND creek = 'K' AND cidco = 'CIDK2' AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare1st=200,farepass1m1st = 1565,farepass3m1st=4310, farepass6m1st = "-", farepass1y1st = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 70.01 and 75 AND creek = 'K' AND cidco = 'CIDK2' AND istrain = 1 AND ismetro = 0 AND ismono = 0;


update ta_fare
set fare1st=200,farepass1m1st = 1645,farepass3m1st=4525, farepass6m1st = "-", farepass1y1st = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 75.01 and 80 AND creek = 'K' AND cidco = 'CIDK2' AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare1st=215,farepass1m1st = 1725,farepass3m1st=4750, farepass6m1st = "-", farepass1y1st = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 80.01 and 85 AND creek = 'K' AND cidco = 'CIDK2' AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare1st=215,farepass1m1st = 1805,farepass3m1st=4960, farepass6m1st = "-", farepass1y1st = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 85.01 and 90 AND creek = 'K' AND cidco = 'CIDK2' AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare1st=215,farepass1m1st = 1815,farepass3m1st=4990, farepass6m1st = "-", farepass1y1st = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 90.01 and 95 AND creek = 'K' AND cidco = 'CIDK2' AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare1st=230,farepass1m1st = 1895,farepass3m1st=5200, farepass6m1st = "-", farepass1y1st = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 95.01 and 100 AND creek = 'K' AND cidco = 'CIDK2' AND istrain = 1 AND ismetro = 0 AND ismono = 0;


update ta_fare
set fare1st=235,farepass1m1st = 2015,farepass3m1st=5535, farepass6m1st = "-", farepass1y1st = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 100.01 and 105 AND creek = 'K' AND cidco = 'CIDK2' AND istrain = 1 AND ismetro = 0 AND ismono = 0;


update ta_fare
set fare1st=245,farepass1m1st = 2020,farepass3m1st=5550, farepass6m1st = "-", farepass1y1st = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 105.01 and 110 AND creek = 'K' AND cidco = 'CIDK2' AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare1st=245,farepass1m1st = 2100,farepass3m1st=5775, farepass6m1st = "-", farepass1y1st = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 110.01 and 115 AND creek = 'K' AND cidco = 'CIDK2' AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare1st=250,farepass1m1st = 2180,farepass3m1st=5985, farepass6m1st = "-", farepass1y1st = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 115.01 and 120 AND creek = 'K' AND cidco = 'CIDK2' AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare1st=250,farepass1m1st = 2195,farepass3m1st=6025, farepass6m1st = "-", farepass1y1st = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 120.01 and 125 AND creek = 'K' AND cidco = 'CIDK2' AND istrain = 1 AND ismetro = 0 AND ismono = 0;


update ta_fare
set fare=5,farepass1m=150,farepass3m=420, farepass6m = "-", farepass1y = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 0.1 and 5 AND creek = 'K' AND cidco = 'CIDK3' AND istrain = 1 AND ismetro = 0 AND ismono = 0;

update ta_fare
set fare=5,farepass1m=150,farepass3m=420, farepass6m = "-", farepass1y = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 5.01 and 10 AND creek = 'K' AND cidco = 'CIDK3' AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare=10,farepass1m=180,farepass3m=510, farepass6m = "-", farepass1y = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 10.01 and 15 AND creek = 'K' AND cidco = 'CIDK3' AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare=10,farepass1m=180,farepass3m=510, farepass6m = "-", farepass1y = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 15.01 and 20 AND creek = 'K' AND cidco = 'CIDK3' AND istrain = 1 AND ismetro = 0 AND ismono = 0;


update ta_fare
set fare=15,farepass1m=265,farepass3m=740, farepass6m = "-", farepass1y = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 20.01 and 25 AND creek = 'K' AND cidco = 'CIDK3' AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare=15,farepass1m=265,farepass3m=740, farepass6m = "-", farepass1y = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 25.01 and 30 AND creek = 'K' AND cidco = 'CIDK3' AND istrain = 1 AND ismetro = 0 AND ismono = 0;

update ta_fare
set fare=15,farepass1m=265,farepass3m=740, farepass6m = "-", farepass1y = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 30.01 and 35 AND creek = 'K' AND cidco = 'CIDK3' AND istrain = 1 AND ismetro = 0 AND ismono = 0;


update ta_fare
set fare=15,farepass1m=265,farepass3m=740, farepass6m = "-", farepass1y = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 35.01 and 40 AND creek = 'K' AND cidco = 'CIDK3' AND istrain = 1 AND ismetro = 0 AND ismono = 0;


update ta_fare
set fare=15,farepass1m=265,farepass3m=740, farepass6m = "-", farepass1y = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 40.01 and 45 AND creek = 'K' AND cidco = 'CIDK3' AND istrain = 1 AND ismetro = 0 AND ismono = 0;


update ta_fare
set fare=20,farepass1m=350,farepass3m=970, farepass6m = "-", farepass1y = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 45.01 and 50 AND creek = 'K' AND cidco = 'CIDK3' AND istrain = 1 AND ismetro = 0 AND ismono = 0;


update ta_fare
set fare=20,farepass1m=365,farepass3m=1015, farepass6m = "-", farepass1y = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 50.01 and 55 AND creek = 'K' AND cidco = 'CIDK3' AND istrain = 1 AND ismetro = 0 AND ismono = 0;




update ta_fare
set fare=20,farepass1m=365,farepass3m=1015, farepass6m = "-", farepass1y = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 55.01 and 60 AND creek = 'K' AND cidco = 'CIDK3' AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare=20,farepass1m=365,farepass3m=1015, farepass6m = "-", farepass1y = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 60.01 and 65 AND creek = 'K' AND cidco = 'CIDK3' AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare=20,farepass1m=365,farepass3m=1015, farepass6m = "-", farepass1y = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 65.01 and 70 AND creek = 'K' AND cidco = 'CIDK3' AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare=25,farepass1m=450,farepass3m=1245, farepass6m = "-", farepass1y = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 70.01 and 75 AND creek = 'K' AND cidco = 'CIDK3' AND istrain = 1 AND ismetro = 0 AND ismono = 0;


update ta_fare
set fare=25,farepass1m=450,farepass3m=1245, farepass6m = "-", farepass1y = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 75.01 and 80 AND creek = 'K' AND cidco = 'CIDK3' AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare=30,farepass1m=450,farepass3m=1245, farepass6m = "-", farepass1y = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 80.01 and 85 AND creek = 'K' AND cidco = 'CIDK3' AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare=30,farepass1m=450,farepass3m=1245, farepass6m = "-", farepass1y = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 85.01 and 90 AND creek = 'K' AND cidco = 'CIDK3' AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare=30,farepass1m=450,farepass3m=1245, farepass6m = "-", farepass1y = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 90.01 and 95 AND creek = 'K' AND cidco = 'CIDK3' AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare=30,farepass1m=450,farepass3m=1245, farepass6m = "-", farepass1y = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 95.01 and 100 AND creek = 'K' AND cidco = 'CIDK3' AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare=30,farepass1m=550,farepass3m=1520, farepass6m = "-", farepass1y = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 100.01 and 105 AND creek = 'K' AND cidco = 'CIDK3' AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare=30,farepass1m=550,farepass3m=1520, farepass6m = "-", farepass1y = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 105.01 and 110 AND creek = 'K' AND cidco = 'CIDK3' AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare=30,farepass1m=550,farepass3m=1520, farepass6m = "-", farepass1y = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 110.01 and 115 AND creek = 'K' AND cidco = 'CIDK3' AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare=35,farepass1m=550,farepass3m=1520, farepass6m = "-", farepass1y = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 115.01 and 120 AND creek = 'K' AND cidco = 'CIDK3' AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare=35,farepass1m=550,farepass3m=1520, farepass6m = "-", farepass1y = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 120.01 and 125 AND creek = 'K' AND cidco = 'CIDK3' AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare=35,farepass1m=550,farepass3m=1520, farepass6m = "-", farepass1y = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 125.01 and 130 AND creek = 'K' AND cidco = 'CIDK3' AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare1st=55,farepass1m1st = 470,farepass3m1st=1315, farepass6m1st = "-", farepass1y1st = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 0.1 and 5 AND creek = 'K' AND cidco = 'CIDK3' AND istrain = 1 AND ismetro = 0 AND ismono = 0;

update ta_fare
set fare1st=55,farepass1m1st = 470,farepass3m1st=1315, farepass6m1st = "-", farepass1y1st = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 5.01 and 10 AND creek = 'K' AND cidco = 'CIDK3' AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare1st=75,farepass1m1st = 620,farepass3m1st=1725, farepass6m1st = "-", farepass1y1st = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 10.01 and 15 AND creek = 'K' AND cidco = 'CIDK3' AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare1st=110,farepass1m1st = 695,farepass3m1st=1935, farepass6m1st = "-", farepass1y1st = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 15.01 and 20 AND creek = 'K' AND cidco = 'CIDK3' AND istrain = 1 AND ismetro = 0 AND ismono = 0;


update ta_fare
set fare1st=110,farepass1m1st = 785,farepass3m1st=2175, farepass6m1st = "-", farepass1y1st = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 20.01 and 25 AND creek = 'K' AND cidco = 'CIDK3' AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare1st=140,farepass1m1st = 795,farepass3m1st=2205, farepass6m1st = "-", farepass1y1st = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 25.01 and 30 AND creek = 'K' AND cidco = 'CIDK3' AND istrain = 1 AND ismetro = 0 AND ismono = 0;

update ta_fare
set fare1st=145,farepass1m1st = 880,farepass3m1st=2430, farepass6m1st = "-", farepass1y1st = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 30.01 and 35 AND creek = 'K' AND cidco = 'CIDK3' AND istrain = 1 AND ismetro = 0 AND ismono = 0;


update ta_fare
set fare1st=150,farepass1m1st = 955,farepass3m1st=2630, farepass6m1st = "-", farepass1y1st = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 35.01 and 40 AND creek = 'K' AND cidco = 'CIDK3' AND istrain = 1 AND ismetro = 0 AND ismono = 0;


update ta_fare
set fare1st=150,farepass1m1st = 1035,farepass3m1st=2855, farepass6m1st = "-", farepass1y1st = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 40.01 and 45 AND creek = 'K' AND cidco = 'CIDK3' AND istrain = 1 AND ismetro = 0 AND ismono = 0;


update ta_fare
set fare1st=165,farepass1m1st = 1115,farepass3m1st=3065, farepass6m1st = "-", farepass1y1st = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 45.01 and 50 AND creek = 'K' AND cidco = 'CIDK3' AND istrain = 1 AND ismetro = 0 AND ismono = 0;


update ta_fare
set fare1st=170,farepass1m1st = 1230,farepass3m1st=3385, farepass6m1st = "-", farepass1y1st = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 50.01 and 55 AND creek = 'K' AND cidco = 'CIDK3' AND istrain = 1 AND ismetro = 0 AND ismono = 0;


update ta_fare
set fare1st=175,farepass1m1st = 1310,farepass3m1st=3595, farepass6m1st = "-", farepass1y1st = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 55.01 and 60 AND creek = 'K' AND cidco = 'CIDK3' AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare1st=175,farepass1m1st = 1320,farepass3m1st=3625, farepass6m1st = "-", farepass1y1st = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 60.01 and 65 AND creek = 'K' AND cidco = 'CIDK3' AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare1st=185,farepass1m1st = 1400,farepass3m1st=3850, farepass6m1st = "-", farepass1y1st = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 65.01 and 70 AND creek = 'K' AND cidco = 'CIDK3' AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare1st=195,farepass1m1st = 1485,farepass3m1st=4075, farepass6m1st = "-", farepass1y1st = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 70.01 and 75 AND creek = 'K' AND cidco = 'CIDK3' AND istrain = 1 AND ismetro = 0 AND ismono = 0;


update ta_fare
set fare1st=200,farepass1m1st = 1565,farepass3m1st=4290, farepass6m1st = "-", farepass1y1st = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 75.01 and 80 AND creek = 'K' AND cidco = 'CIDK3' AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare1st=210,farepass1m1st = 1650,farepass3m1st=4515, farepass6m1st = "-", farepass1y1st = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 80.01 and 85 AND creek = 'K' AND cidco = 'CIDK3' AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare1st=215,farepass1m1st = 1725,farepass3m1st=4725, farepass6m1st = "-", farepass1y1st = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 85.01 and 90 AND creek = 'K' AND cidco = 'CIDK3' AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare1st=215,farepass1m1st = 1735,farepass3m1st=4755, farepass6m1st = "-", farepass1y1st = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 90.01 and 95 AND creek = 'K' AND cidco = 'CIDK3' AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare1st=225,farepass1m1st = 1815,farepass3m1st=4965, farepass6m1st = "-", farepass1y1st = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 95.01 and 100 AND creek = 'K' AND cidco = 'CIDK3' AND istrain = 1 AND ismetro = 0 AND ismono = 0;


update ta_fare
set fare1st=230,farepass1m1st = 1935,farepass3m1st=5300, farepass6m1st = "-", farepass1y1st = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 100.01 and 105 AND creek = 'K' AND cidco = 'CIDK3' AND istrain = 1 AND ismetro = 0 AND ismono = 0;


update ta_fare
set fare1st=240,farepass1m1st = 1940,farepass3m1st=5315, farepass6m1st = "-", farepass1y1st = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 105.01 and 110 AND creek = 'K' AND cidco = 'CIDK3' AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare1st=245,farepass1m1st = 2025,farepass3m1st=5540, farepass6m1st = "-", farepass1y1st = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 110.01 and 115 AND creek = 'K' AND cidco = 'CIDK3' AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare1st=250,farepass1m1st = 2100,farepass3m1st=5750, farepass6m1st = "-", farepass1y1st = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 115.01 and 120 AND creek = 'K' AND cidco = 'CIDK3' AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare1st=250,farepass1m1st = 2120,farepass3m1st=5790, farepass6m1st = "-", farepass1y1st = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 120.01 and 125 AND creek = 'K' AND cidco = 'CIDK3' AND istrain = 1 AND ismetro = 0 AND ismono = 0;


update ta_fare
set fare1st=260,farepass1m1st = 2190,farepass3m1st=5990, farepass6m1st = "-", farepass1y1st = "-", smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 125.01 and 130 AND creek = 'K' AND cidco = 'CIDK3' AND istrain = 1 AND ismetro = 0 AND ismono = 0;

update ta_fare
set fare=5,farepass1m=120,farepass3m=320, farepass6m = '-', farepass1y = '-', smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 0.1 and 5 AND creek = 'N' AND cidco = 'CID' AND istrain = 1 AND ismetro = 0 AND ismono = 0;

update ta_fare
set fare=5,farepass1m=120,farepass3m=320, farepass6m = '-', farepass1y = '-', smartcard = "-", token = "-", m45trips = "-" 
where km BETWEEN 5.01 and 10 AND creek = 'N' AND cidco = 'CID' AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare=10,farepass1m=150,farepass3m=410, farepass6m = '-', farepass1y = '-', smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 10.01 and 15 AND creek = 'N' AND cidco = 'CID' AND istrain = 1 AND ismetro = 0 AND ismono = 0; 



update ta_fare
set fare=10,farepass1m=150,farepass3m=410, farepass6m = '-', farepass1y = '-', smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 15.01 and 20 AND creek = 'N' AND cidco = 'CID' AND istrain = 1 AND ismetro = 0 AND ismono = 0;


update ta_fare
set fare=15,farepass1m=235,farepass3m=640, farepass6m = '-', farepass1y = '-', smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 20.01 and 25 AND creek = 'N' AND cidco = 'CID' AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare=15,farepass1m=235,farepass3m=640, farepass6m = '-', farepass1y = '-', smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 25.01 and 30 AND creek = 'N' AND cidco = 'CID' AND istrain = 1 AND ismetro = 0 AND ismono = 0;

update ta_fare
set fare=15,farepass1m=295,farepass3m=830, farepass6m = '-', farepass1y = '-', smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 30.01 and 35 AND creek = 'N' AND cidco = 'CID' AND istrain = 1 AND ismetro = 0 AND ismono = 0;


update ta_fare
set fare1st=50,farepass1m1st=385,farepass3m1st=1055, farepass6m1st = '-', farepass1y1st = '-', smartcard = "-", token = "-", m45trips = "-" 
where km BETWEEN 0.1 and 5 AND creek = 'N' AND cidco = 'CID' AND istrain = 1 AND ismetro = 0 AND ismono = 0;

update ta_fare
set fare1st=50,farepass1m1st=385,farepass3m1st=1055, farepass6m1st = '-', farepass1y1st = '-', smartcard = "-", token = "-", m45trips = "-" 
where km BETWEEN 5.01 and 10 AND creek = 'N' AND cidco = 'CID' AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare1st=75,farepass1m1st=530,farepass3m1st=1465, farepass6m1st = '-', farepass1y1st = '-', smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 10.01 and 15 AND creek = 'N' AND cidco = 'CID' AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare1st=105,farepass1m1st=610,farepass3m1st=1675, farepass6m1st = '-', farepass1y1st = '-', smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 15.01 and 20 AND creek = 'N' AND cidco = 'CID' AND istrain = 1 AND ismetro = 0 AND ismono = 0;


update ta_fare
set fare1st=105,farepass1m1st=700,farepass3m1st=1915, farepass6m1st = '-', farepass1y1st = '-', smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 20.01 and 25 AND creek = 'N' AND cidco = 'CID' AND istrain = 1 AND ismetro = 0 AND ismono = 0;



update ta_fare
set fare1st=140,farepass1m1st=710,farepass3m1st=1945, farepass6m1st = '-', farepass1y1st = '-', smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 25.01 and 30 AND creek = 'N' AND cidco = 'CID' AND istrain = 1 AND ismetro = 0 AND ismono = 0;

update ta_fare
set fare1st=145,farepass1m1st=960,farepass3m1st=2665, farepass6m1st = '-', farepass1y1st = '-', smartcard = "-", token = "-", m45trips = "-"
where km BETWEEN 30.01 and 35 AND creek = 'N' AND cidco = 'CID' AND istrain = 1 AND ismetro = 0 AND ismono = 0;



Update ta_fare
set smartcard = 10, token = 10, m45trips = 750, fare = "-", farepass1m = "-", farepass3m = "-", farepass6m = "-", farepass1y = "-", fare1st = "-", farepass1m1st = "-", farepass3m1st = "-", farepass6m1st = "-", farepass1y1st = "-"
where km BETWEEN 0.01 and 1.50 AND creek = '-' AND cidco = '-' AND istrain = 0 AND ismetro = 1 AND ismono = 0;

update ta_fare
set smartcard = 18, token = 20, m45trips = 750, fare = "-", farepass1m = "-", farepass3m = "-", farepass6m = "-", farepass1y = "-", fare1st = "-", farepass1m1st = "-", farepass3m1st = "-", farepass6m1st = "-", farepass1y1st = "-"
where km BETWEEN 1.51 and 5 AND creek = '-' AND cidco = '-' AND istrain = 0 AND ismetro = 1 AND ismono = 0;

update ta_fare
set smartcard = 27, token = 30, m45trips = 1050, fare = "-", farepass1m = "-", farepass3m = "-", farepass6m = "-", farepass1y = "-", fare1st = "-", farepass1m1st = "-", farepass3m1st = "-", farepass6m1st = "-", farepass1y1st = "-"
where km BETWEEN 5.01 and 8 AND creek = '-' AND cidco = '-' AND istrain = 0 AND ismetro = 1 AND ismono = 0;

update ta_fare
set smartcard = 35, token = 40, m45trips = 1350, fare = "-", farepass1m = "-", farepass3m = "-", farepass6m = "-", farepass1y = "-", fare1st = "-", farepass1m1st = "-", farepass3m1st = "-", farepass6m1st = "-", farepass1y1st = "-"
where km BETWEEN 8.01 and 11 AND creek = '-' AND cidco = '-' AND istrain = 0 AND ismetro = 1 AND ismono = 0;

Update ta_fare
set smartcard = '-', token = 5, m45trips = '-', fare = "-", farepass1m = "-", farepass3m = "-", farepass6m = "-", farepass1y = "-", fare1st = "-", farepass1m1st = "-", farepass3m1st = "-", farepass6m1st = "-", farepass1y1st = "-"
where km BETWEEN 0.01 and 3 AND creek = '-' AND cidco = '-' AND istrain = 0 AND ismetro = 0 AND ismono = 1;

update ta_fare
set smartcard = '-', token = 7, m45trips = '-', fare = "-", farepass1m = "-", farepass3m = "-", farepass6m = "-", farepass1y = "-", fare1st = "-", farepass1m1st = "-", farepass3m1st = "-", farepass6m1st = "-", farepass1y1st = "-"
where km BETWEEN 3.01 and 5 AND creek = '-' AND cidco = '-' AND istrain = 0 AND ismetro = 0 AND ismono = 1;

update ta_fare
set smartcard = '-', token = 9, m45trips = '-', fare = "-", farepass1m = "-", farepass3m = "-", farepass6m = "-", farepass1y = "-", fare1st = "-", farepass1m1st = "-", farepass3m1st = "-", farepass6m1st = "-", farepass1y1st = "-"
where km BETWEEN 5.01 and 7 AND creek = '-' AND cidco = '-' AND istrain = 0 AND ismetro = 0 AND ismono = 1;

update ta_fare
set smartcard = '-', token = 11, m45trips = '-', fare = "-", farepass1m = "-", farepass3m = "-", farepass6m = "-", farepass1y = "-", fare1st = "-", farepass1m1st = "-", farepass3m1st = "-", farepass6m1st = "-", farepass1y1st = "-"
where km BETWEEN 7.01 and 10 AND creek = '-' AND cidco = '-' AND istrain = 0 AND ismetro = 0 AND ismono = 1;

UPDATE ta_fare
SET fare = token
WHERE linecode= 'MET' OR linecode= 'MON';

UPDATE ta_fare
SET fare1st = smartcard
WHERE linecode= 'MET';

UPDATE ta_fare
SET farepass1m1st = m45trips
WHERE linecode= 'MET';


UPDATE ta_fare
SET from_id = (SELECT station_id FROM ta_station 
WHERE ta_fare.fromstation = ta_station.stationcode);


DELETE  FROM ta_fare
WHERE length(via)>1  AND (ismono = 1 OR ismetro = 1);
