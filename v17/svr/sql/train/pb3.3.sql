DROP PROCEDURE IF EXISTS pb33;
DELIMITER$$
CREATE  PROCEDURE `pb33`()
BEGIN
	

DROP TABLE IF EXISTS tt3_routedetail;
CREATE TABLE `tt3_routedetail` (
`route_id` int(11),
`linecode` varchar(20),
`directioncode` varchar(10),
`stationcode` varchar(10),
`routestations` varchar(500),
 stationserial int,
 trainspeedcode VARCHAR(200),
 indicatorspeedcode VARCHAR(100),
 platformside VARCHAR(100),
 platformno VARCHAR(100)
);


INSERT INTO tt3_routedetail(route_id, linecode, directioncode,stationcode, routestations,stationserial, 
														trainspeedcode, indicatorspeedcode, platformside, platformno)
SELECT route_id, linecode, directioncode,stationcode, routestations,stationserial, 
														trainspeedcode, indicatorspeedcode, platformside, platformno FROM routecrdown
UNION
SELECT route_id, linecode, directioncode,stationcode, routestations,stationserial, 
														trainspeedcode, indicatorspeedcode, platformside, platformno FROM routecrup
UNION
SELECT route_id, linecode, directioncode,stationcode, routestations,stationserial, 
														trainspeedcode, indicatorspeedcode, platformside, platformno FROM routewrdown
UNION
SELECT route_id, linecode, directioncode,stationcode, routestations,stationserial, 
														trainspeedcode, indicatorspeedcode, platformside, platformno FROM routewrup
UNION
SELECT route_id, linecode, directioncode,stationcode, routestations,stationserial, 
														trainspeedcode, indicatorspeedcode, platformside, platformno FROM routehrdown
UNION
SELECT route_id, linecode, directioncode,stationcode, routestations,stationserial, 
														trainspeedcode, indicatorspeedcode, platformside, platformno FROM routehrup
UNION
SELECT route_id, linecode, directioncode,stationcode, routestations,stationserial, 
														trainspeedcode, indicatorspeedcode, platformside, platformno FROM routethdown
UNION
SELECT route_id, linecode, directioncode,stationcode, routestations,stationserial, 
														trainspeedcode, indicatorspeedcode, platformside, platformno FROM routethup
UNION
SELECT route_id, linecode, directioncode,stationcode, routestations,stationserial, 
														trainspeedcode, indicatorspeedcode, platformside, platformno FROM routensdown
UNION
SELECT route_id, linecode, directioncode,stationcode, routestations,stationserial, 
														trainspeedcode, indicatorspeedcode, platformside, platformno FROM routensup
UNION
SELECT route_id, linecode, directioncode,stationcode, routestations,stationserial, 
														trainspeedcode, indicatorspeedcode, platformside, platformno FROM routeshdown
UNION
SELECT route_id, linecode, directioncode,stationcode, routestations,stationserial, 
														trainspeedcode, indicatorspeedcode, platformside, platformno FROM routeshup

UNION
SELECT route_id, linecode, directioncode,stationcode, routestations,stationserial, 
														trainspeedcode, indicatorspeedcode, platformside, platformno FROM routemetup

UNION
SELECT route_id, linecode, directioncode,stationcode, routestations,stationserial, 
														trainspeedcode, indicatorspeedcode, platformside, platformno FROM routemetdown
UNION
SELECT route_id, linecode, directioncode,stationcode, routestations,stationserial, 
														trainspeedcode, indicatorspeedcode, platformside, platformno FROM routekrup

UNION
SELECT route_id, linecode, directioncode,stationcode, routestations,stationserial, 
														trainspeedcode, indicatorspeedcode, platformside, platformno FROM routekrdown
UNION
SELECT route_id, linecode, directioncode,stationcode, routestations,stationserial, 
														trainspeedcode, indicatorspeedcode, platformside, platformno FROM routemonup

UNION
SELECT route_id, linecode, directioncode,stationcode, routestations,stationserial, 
														trainspeedcode, indicatorspeedcode, platformside, platformno FROM routemondown;

														


DROP TABLE if EXISTS tt4_routedetail;
CREATE TABLE tt4_routedetail
select route_id, linecode, directioncode,GROUP_CONCAT(UPPER(stationcode) 
ORDER BY  route_id, stationserial, UPPER(stationcode)) as routestations, stationcode, stationserial 
FROM tt3_routedetail
GROUP BY linecode, routestations, directioncode;

UPDATE tt3_routedetail
SET routestations=(SELECT routestations from tt4_routedetail where tt4_routedetail.route_id=tt3_routedetail.route_id 
									 and  tt4_routedetail.linecode=tt3_routedetail.linecode 
									 and tt4_routedetail.directioncode=tt3_routedetail.directioncode);


DROP TABLE  if EXISTS tt5_routedetail;
CREATE table tt5_routedetail as
SELECT a.route_id,a.linecode,a.directioncode,a.stationcode,a.stationserial,a.routestations,a.trainno,
b.indicatorspeedcode,b.trainspeedcode,b.platformno,b.platformside 
FROM tt_routedetail a 
LEFT  outer JOIN  
tt3_routedetail b ON
a.routestations = b.routestations and 
a.stationcode = b.stationcode and
a.linecode = b.linecode;
#GROUP BY b.route_id,stationserial;


#drop TABLE IF EXISTS tt4_routedetail;
#drop TABLE IF EXISTS tt_routedetail;


END$$
DELIMITER ;

CALL pb33;