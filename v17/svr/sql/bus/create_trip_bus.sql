DROP PROCEDURE IF EXISTS `create_trip_bus`;
DELIMITER $$
CREATE  PROCEDURE `create_trip_bus`(IN `intriptype` varchar(300),
	IN `infromaddress` varchar(300), IN `infromshortaddress` varchar(300), 
	IN `intoaddress` varchar(300), IN `intoshortaddress` varchar(300), 
	IN infromsublocality varchar(200), IN intosublocality varchar(200), 
	IN in_trip_directions_polyline varchar(5000), 
	IN `infromlat` double, IN `infromlng` double, 
	IN `intolat` double, IN `intolng` double, 
	IN `intimems` bigint(20), IN `intripaction` varchar(1), 
	IN `intriptime` int, IN `intripdistance` double, 
	IN nappuserid INT,IN `inclientdatetime` datetime, 
	IN `inemailid` varchar(100), IN `inlastinvited` INT, IN `inplannedstartdatetime` datetime)
proc_label:BEGIN
#DECLARE ndriverid INT DEFAULT 0;
DECLARE ntripid INT DEFAULT 0;
DECLARE lasttripid INT DEFAULT 0;
DECLARE lastfromlat DOUBLE DEFAULT 0;
DECLARE lastfromlng DOUBLE DEFAULT 0;
DECLARE lasttolat DOUBLE DEFAULT 0;
DECLARE lasttolng DOUBLE DEFAULT 0;
DECLARE lasttotaldistance DOUBLE DEFAULT 0;
DECLARE currenttotaldistance DOUBLE DEFAULT 0;
/*DECLARE previousTripId INT DEFAULT 0;

SELECT MAX(trip_id) FROM bus_trips
	WHERE appuser_id = nappuserid
	INTO previousTripId;	

#SELECT 	previousTripId;
	
IF nappuserid IS NULL THEN
	SELECT -1;
	LEAVE proc_label;
END IF;
*/
UPDATE appuser SET clientlastaccessdatetime = inclientdatetime
	WHERE appuser_id = nappuserid;

SELECT trip_id FROM bus_trips t
	WHERE trip_id =
		(SELECT MAX(trip_id) FROM bus_trips
			WHERE appuser_id = nappuserid
			AND trip_action = 'C')
		AND appuser_id = nappuserid		
		AND ((getDistanceBetweenPoints(t.fromlat, t.fromlng, infromlat, infromlng) < .01
		AND getDistanceBetweenPoints(t.tolat, t.tolng, intolat, intolng) < .01)
		OR (infromaddress = fromaddress AND intoaddress = toaddress))
	INTO ntripid;

IF ntripid > 0 THEN
	UPDATE bus_trips
	SET fromlat = infromlat,
		fromlng = infromlng,
		tolat = intolat,
		tolng = intolng,
		tripcreationtime = inclientdatetime
	WHERE trip_id = lasttripid;
	SELECT ntripid as trip_id, 1 as tripFound FROM DUAL ;
	LEAVE proc_label;
END IF; 
/*#IF  previousTripId IS NOT NULL THEN
	#Call jijo_trip_done(previousTripId);
#END IF;
	

	SELECT commercial_driver_id FROM commercial_driver
	WHERE appuser_id = nappuserid
	INTO @driver_id;

	IF (@driver_id IS NULL) THEN
		INSERT INTO commercial_driver(appuser_id, email)
		VALUES((SELECT appuser_id FROM appuser WHERE appuser_id = nappuserid ), inemailid);
	END IF;	
*/	
	INSERT INTO bus_trips(appuser_id, triptype, fromaddress, fromshortaddress, 
		toaddress, toshortaddress, fromsublocality, 
		tosublocality, trip_directions_polyline, fromlat, fromlng, 
		tolat, tolng, trips_unique_ms, tripcreationtime,
		trip_action, triptime, tripdistance, last_invited, planned_start_time, planned_start_datetime)
	VALUES ((SELECT appuser_id FROM appuser WHERE  appuser_id = nappuserid), 
		intriptype, infromaddress, infromshortaddress, intoaddress, intoshortaddress,
		infromsublocality, intosublocality, in_trip_directions_polyline,
		infromlat, infromlng,
		intolat, intolng, intimems, inclientdatetime,
		intripaction, intriptime, intripdistance, inlastinvited,
		DATE_FORMAT(inplannedstartdatetime,'%H:%i') , 
		inplannedstartdatetime );
	SET ntripid = LAST_INSERT_ID(); 
	SELECT ntripid as trip_id, 0 as  tripFound FROM DUAL ;
#END IF;
END$$
DELIMITER ;



call create_trip_bus('CD','5, Hanuman Hedit Marg No 2, Paranjape Nagar, Vile Parle, Mumbai, Maharashtra 400057, India ',
'','24, Nehru Rd','','','Vile Parle','kkrsBgss{LBaA~Bd@rA^Cy@WeDC_AzBObF]d@DdAXxATtDCdBA|@?xCNhBR`Fr@f@{DJm@mKsC','19.1045698',
'72.8505955','19.0966649','72.8525981',1454046287000,'C','5','1450',NULL,NULL,NULL,0,NULL)