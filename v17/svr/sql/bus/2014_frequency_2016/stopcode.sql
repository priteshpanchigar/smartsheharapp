UPDATE stopmaster
SET AreaName = (SELECT AreaName FROM areamaster WHERE A_code = `Area Code`);


UPDATE stopmaster 
SET RoadName = (SELECT roadname FROM b_stopmaster_old WHERE Stop_Code = stopcode);



UPDATE best_routedetails_stopnamechange a
SET `Area Name` = (SELECT `Area Name` FROM stopmaster b 
	WHERE  a.STOPCD = b.Stop_Code)
		
DESCRIBE stopmaster;
DESCRIBE best_routedetails_stopnamechange;

#`Road Name` = (SELECT `Road Name` FROM stopmaster b WHERE  a.STOPCD = b.Stop_Code  LIMIT 1);

SELECT best_routedetails_id, RNO, STOPSR, STOPCD, brs.`Stop Name` , direction, s.AreaName, s.RoadName, Stage No, STAGE, KM 
FROM best_routedetails_stopnamechange brs
LEFT JOIN stopmaster s
ON brs.STOPCD = s.Stop_Code 
ORDER BY best_routedetails_id;