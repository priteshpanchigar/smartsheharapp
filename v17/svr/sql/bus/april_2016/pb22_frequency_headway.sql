DROP TABLE IF EXISTS pb_frequency_headway;
 
CREATE TABLE pb_frequency_headway (
	 _id INTEGER NOT NULL auto_increment PRIMARY KEY,
	`best_routeatlas_id` int(11) DEFAULT NULL,
  `startheadwayhh` int(11) DEFAULT NULL,
  `endheadwayhh` int(11) DEFAULT NULL,
  `startheadwaymm` int(11) DEFAULT NULL,
  `endheadwaymm` int(11) DEFAULT NULL,
  `frequency` int(11) DEFAULT NULL,
  `startdayweek` int(11) DEFAULT NULL,
  `enddayweek` int(11) DEFAULT NULL,
  `holiday` varchar(10) DEFAULT NULL,
  `routecode` varchar(10) DEFAULT NULL,
  `firstfrom` varchar(10) DEFAULT NULL,
  `lastfrom` varchar(10) DEFAULT NULL,
  `firstfromhh` int(11) DEFAULT NULL,
  `lastfromhh` int(11) DEFAULT NULL,
  `firstfrommm` int(11) DEFAULT NULL,
  `lastfrommm` int(11) DEFAULT NULL,
  `schedule` varchar(100) DEFAULT NULL,
  `stopcodefrom` int(11) DEFAULT NULL,
  `stopcodeto` int(11) DEFAULT NULL,
  `stopserialfrom` int(11) DEFAULT NULL,
  `stopserialto` int(11) DEFAULT NULL,
  `direction` varchar(5) DEFAULT NULL,
  `startheadway` int(11) DEFAULT NULL,
  `endheadway` int(11) DEFAULT NULL,
  `headwayserial` int(11) DEFAULT NULL,
  `firstfrominmin` int(11) DEFAULT NULL,
  `lastfrominmin` int(11) DEFAULT NULL

);



INSERT INTO pb_frequency_headway(best_routeatlas_id, routecode, firstfrom, lastfrom,  frequency, stopcodefrom, stopcodeto, SCHEDULE,direction, 
stopserialfrom, stopserialto, firstfrominmin, lastfrominmin, firstfromhh, lastfromhh, firstfrommm, lastfrommm,
startheadway,endheadway,headwayserial)

SELECT best_routeatlas_id, routecode, firstfrom, lastfrom,  frequency, stopcodefrom, stopcodeto, SCHEDULE,direction, 
stopserialfrom, stopserialto, firstfrominmin, lastfrominmin, firstfromhh, lastfromhh, firstfrommm, lastfrommm,
startheadway,endheadway,headwayserial FROM pb_from_headway1
UNION
SELECT best_routeatlas_id, routecode, firstfrom, lastfrom,  frequency, stopcodefrom, stopcodeto, SCHEDULE,direction, 
stopserialfrom, stopserialto, firstfrominmin, lastfrominmin, firstfromhh, lastfromhh, firstfrommm, lastfrommm,
startheadway,endheadway,headwayserial FROM pb_from_headway2
UNION
SELECT best_routeatlas_id, routecode, firstfrom, lastfrom,  frequency, stopcodefrom, stopcodeto, SCHEDULE,direction, 
stopserialfrom, stopserialto, firstfrominmin, lastfrominmin, firstfromhh, lastfromhh, firstfrommm, lastfrommm,
startheadway,endheadway,headwayserial FROM pb_from_headway3
UNION
SELECT best_routeatlas_id, routecode, firstfrom, lastfrom,  frequency, stopcodefrom, stopcodeto, SCHEDULE,direction, 
stopserialfrom, stopserialto, firstfrominmin, lastfrominmin, firstfromhh, lastfromhh, firstfrommm, lastfrommm,
startheadway,endheadway,headwayserial FROM pb_from_headway4
UNION
SELECT best_routeatlas_id, routecode, firstfrom, lastfrom,  frequency, stopcodefrom, stopcodeto, SCHEDULE,direction, 
stopserialfrom, stopserialto, firstfrominmin, lastfrominmin, firstfromhh, lastfromhh, firstfrommm, lastfrommm,
startheadway,endheadway,headwayserial FROM pb_from_headway5;



DELETE from pb_frequency_headway
WHERE startheadway >= lastfrominmin;





UPDATE pb_frequency_headway
SET startheadwayhh = TRUNCATE(startheadway/60, 0),
endheadwayhh = TRUNCATE(endheadway/60, 0),
startheadwaymm = startheadway - (TRUNCATE(startheadway/60, 0))*60, 
endheadwaymm = endheadway -  (TRUNCATE(endheadway/60, 0))*60 ;
