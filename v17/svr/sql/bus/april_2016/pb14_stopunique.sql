
DROP TABLE IF EXISTS pb_stopunique;

CREATE TABLE pb_stopunique (
  stop_id int(11) NOT NULL AUTO_INCREMENT  PRIMARY KEY,
  stopcode int(11),
  stopname varchar(50),
  areaname varchar(90),
  roadname varchar(90),
  noofbuses int(11),
  stopnamedetailid varchar(300),
  stopnamedetail varchar(300),
	lat DOUBLE,
  lon DOUBLE
);

CREATE INDEX u_noofbuses
ON pb_stopunique (noofbuses);


INSERT INTO pb_stopunique(stopcode,areaname,stopname,roadname,stopnamedetailid,noofbuses,lat,lon)
SELECT( select stopcode FROM b_stopmaster s2 WHERE s2.stopnamedetailid = s1.stopnamedetailid  ORDER BY noofbuses DESC LIMIT 1) as stopcode,
areaname, stopname,  roadname, stopnamedetailid,SUM(noofbuses) as noofbuses,
(select lat FROM b_stopmaster s2 WHERE s2.stopnamedetailid = s1.stopnamedetailid and lat is not null ORDER BY noofbuses DESC LIMIT 1) as lat,
(select lon FROM b_stopmaster s2 WHERE s2.stopnamedetailid = s1.stopnamedetailid and lon is not null ORDER BY noofbuses DESC LIMIT 1) as lon
FROM b_stopmaster s1
GROUP BY stopnamedetailid;


CREATE INDEX u_stop_id
ON pb_stopunique (stop_id);

CREATE INDEX u_stopcode
ON pb_stopunique (stopcode);


CREATE INDEX u_stopnamedetailid
ON pb_stopunique (stopnamedetailid);





UPDATE b_stopmaster
SET stop_id = (SELECT stop_id from pb_stopunique WHERE pb_stopunique.stopnamedetailid = b_stopmaster.stopnamedetailid );


set @stopcode = '';
set @num = 1;
DROP TABLE IF EXISTS t;
CREATE TABLE t AS
SELECT b.landmark, getDistanceBetweenPoints(a.lat, a.lon , b.lat, b.lon) landmarkdist, a.*
FROM pb_stopunique a
LEFT OUTER JOIN pb_landmark b
ON ABS(a.lat - b.lat) < 0.01
AND ABS(a.lon - b.lon) < 0.01
ORDER BY a.stop_id, getDistanceBetweenPoints(a.lat, a.lon , b.lat, b.lon);

DROP TABLE IF EXISTS t2 ;
CREATE TABLE t2 AS
SELECT
@num := if(@stopcode = a.stopcode, @num + 1, 1) as row_number,
@stopcode := a.stopcode as dummy, a.*
FROM t a;

DROP TABLE IF EXISTS b_stopunique;
CREATE TABLE b_stopunique AS
SELECT a.stop_id,a.stopcode,a.stopname,a.areaname,a.roadname,a.noofbuses,a.lat,a.lon,
GROUP_CONCAT(CONCAT(landmark, ' (',
TRUNCATE(landmarkdist, 2), ')')) as landmarklist
FROM t2 a
WHERE row_number < 3
GROUP BY stopcode;


ALTER TABLE b_stopunique
ADD stopnamedetail VARCHAR(250);



ALTER TABLE b_stopunique
ADD stopnamedetaillandmarklistid VARCHAR(300);


UPDATE b_stopunique
SET stopnamedetail= CONCAT (CASE WHEN length(areaname)>0 THEN CONCAT('[',areaname,'] ')ELSE '' END,
														stopname);

#SUBSTRING_INDEX(SUBSTRING_INDEX(stopname, "/", 1), "(", 1)



UPDATE b_stopunique
SET stopnamedetaillandmarklistid= fn_RemoveSpecialCharacters(shortname(CONCAT
													(areaname,SUBSTRING_INDEX(SUBSTRING_INDEX(stopname, "/", 1), "(", 1),IFNULL(landmarklist,''))));



