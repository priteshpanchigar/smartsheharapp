DROP TABLE IF EXISTS ba_stopmaster;

CREATE TABLE ba_stopmaster as
SELECT _id,stopcode,noofbuses,stopdisplayname,latu,latd,lonu,lond,lat,lon,stop_id FROM b_stopmaster;



DROP TABLE IF EXISTS ba_stopunique;

CREATE TABLE ba_stopunique as
SELECT stop_id,stopcode,noofbuses,stopnamedetaillandmarklistid ,stopnamedetail,lat,lon,landmarklist FROM b_stopunique;



DROP TABLE if EXISTS ba_routedetail;

CREATE TABLE ba_routedetail AS
SELECT _id,routecode,stopcode,stopserial,direction,distance,stopunique_id FROM b_routedetail;


DROP TABLE IF EXISTS ba_route;

CREATE TABLE ba_route as
SELECT route_id,routecode,laststop,busno,bustype,stopcodefrom,stopcodeto,fstopname,lstopname,buslabel,direction FROM b_route;



DROP TABLE IF EXISTS ba_frequency;

CREATE TABLE ba_frequency AS
SELECT frequency_id,startheadwayhh,endheadwayhh,startheadwaymm,endheadwaymm,frequency,startdayweek,enddayweek,holiday,routecode,
firstfromhh,lastfromhh,firstfrommm,lastfrommm,`schedule`,stopserialfrom,stopserialto,stopcodefrom,stopcodeto,direction
FROM b_frequency;




DROP TABLE IF EXISTS ba_frequency2;

CREATE TABLE ba_frequency2 AS
SELECT frequency_id,startheadwayhh,endheadwayhh,startheadwaymm,endheadwaymm,frequency,startdayweek,enddayweek,holiday,routecode,
firsttohh,lasttohh,firsttomm,lasttomm,`schedule`,stopserialfrom,stopserialto,stopcodefrom,stopcodeto,direction
FROM b_frequency2;


DROP TABLE IF EXISTS ba_frequency_summary;

CREATE TABLE ba_frequency_summary AS
SELECT * FROM b_frequency_summary;


DROP TABLE IF EXISTS ba_frequency_summary2;

CREATE TABLE ba_frequency_summary2 AS
SELECT * FROM b_frequency_summary2;


DROP TABLE IF EXISTS ba_frequencyac;

CREATE TABLE ba_frequencyac AS
SELECT _id,startdayweek,enddayweek,holiday,routecode, stopcodefrom, stopcodeto,`schedule`,
 stopserialfrom, stopserialto,
firststop,laststop,hh,mm,direction FROM b_frequencyac;

