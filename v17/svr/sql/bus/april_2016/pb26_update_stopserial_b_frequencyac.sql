UPDATE b_frequencyac a
SET stopserialfrom = (SELECT b.stopserial FROM b_routedetail b 
												WHERE a.direction = b.direction
													AND a.routecode  = b.routecode
													 AND a.stopcodefrom = b.stopcode);

UPDATE b_frequencyac a
SET stopserialto = (SELECT b.stopserial FROM b_routedetail b 
												WHERE a.direction = b.direction
													AND a.routecode  = b.routecode
													 AND a.stopcodeto = b.stopcode);


UPDATE b_frequencyac
SET hh = TRUNCATE(actiming,0);

UPDATE b_frequencyac
SET mm =  CAST(SUBSTRING_INDEX(actiming, '.', -1) AS CHAR(4));

/*
UPDATE b_frequencyac
SET min = (FLOOR(actiming) * 60) + (actiming % 60 - FLOOR(actiming))*100;
*/
