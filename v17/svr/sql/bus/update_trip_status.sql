DROP PROCEDURE IF EXISTS update_trip_status;
DELIMITER $$
CREATE  PROCEDURE `update_trip_status`(IN intripid INT,IN intripstatus varchar(30))
BEGIN
	
UPDATE bustrip
SET  tripstatus = intripstatus
WHERE trip_id = intripid;

SELECT 1 AS result;

END$$
DELIMITER;