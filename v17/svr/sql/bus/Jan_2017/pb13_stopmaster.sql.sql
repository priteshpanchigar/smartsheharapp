DROP TABLE if EXISTS pb_stopmaster;

CREATE TABLE pb_stopmaster
(best_stopmaster_id int(11) NOT NULL AUTO_INCREMENT PRIMARY key)
SELECT stopcode,stopname,direction from pb_routedetails_foratlasmatching
GROUP BY stopcode;


DROP TABLE IF EXISTS pb_stopmasterdetail;
CREATE TABLE `pb_stopmasterdetail` (
  `stopmaster_id` int(11) NOT NULL AUTO_INCREMENT,
  `stopcode` bigint(21) unsigned DEFAULT NULL,
	`stopname` varchar(255) DEFAULT NULL,
  `areaname` varchar(255) DEFAULT NULL,
  `roadname` varchar(255) DEFAULT NULL,
	`direction` varchar(5) DEFAULT NULL,
  `latu` double DEFAULT NULL,
  `latd` double DEFAULT NULL,
  `lonu` double DEFAULT NULL,
  `lond` double DEFAULT NULL,
  `lat` double DEFAULT NULL,
  `lon` double DEFAULT NULL,
  `map` int(11) DEFAULT NULL,
  `cancelledstop` int(11) DEFAULT NULL,
  `notfound` int(11) DEFAULT NULL,
	`stopnameid` varchar(300) DEFAULT NULL,
	`areanameid` varchar(300) DEFAULT NULL,
	`stopnamedetailid` varchar(300) DEFAULT NULL,
  `stopdisplayname` varchar(300) DEFAULT NULL,
  `noofbuses` int(11) DEFAULT NULL,
  `stop_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`stopmaster_id`),
  KEY `u_stopcode` (`stopcode`),
  KEY `u_noofbuses` (`noofbuses`),
  KEY `d_stopnamedetailid` (`stopnamedetailid`),
  KEY `lat` (`lat`),
  KEY `lon` (`lon`)
) ENGINE=InnoDB;

INSERT INTO pb_stopmasterdetail(stopcode,  direction)
SELECT stopcode, direction FROM pb_stopmaster;


UPDATE pb_stopmasterdetail a
SET areaname  =  NULL;


UPDATE pb_stopmasterdetail a
SET roadname  =  NULL;


UPDATE pb_stopmasterdetail a
SET areaname  = (SELECT areaname FROM stopmaster b WHERE a.stopcode = b.stopcode) WHERE areaname is NULL;


UPDATE pb_stopmasterdetail a
SET roadname  = (SELECT roadname FROM stopmaster b WHERE a.stopcode = b.stopcode) WHERE roadname is NULL;

UPDATE pb_stopmasterdetail a
SET stopname = (SELECT stopname FROM stopmaster b WHERE a.stopcode = b.stopcode);

UPDATE pb_stopmasterdetail a
SET stopnameid = fn_RemoveSpecialCharacters(stopname); 

UPDATE pb_stopmasterdetail a
SET areanameid = fn_RemoveSpecialCharacters(areaname);

UPDATE pb_stopmasterdetail a 
SET latu = (SELECT latu FROM b_stopmaster_old b WHERE a.stopcode = b.stopcode);

UPDATE pb_stopmasterdetail a 
SET latd = (SELECT latd FROM b_stopmaster_old b WHERE a.stopcode =  b.stopcode);
						

UPDATE pb_stopmasterdetail a 
SET lonu = (SELECT lonu FROM b_stopmaster_old b WHERE a.stopcode =  b.stopcode);
						

UPDATE pb_stopmasterdetail a 
SET lond = (SELECT lond FROM b_stopmaster_old b WHERE a.stopcode =  b.stopcode);
						

UPDATE pb_stopmasterdetail a 
SET lat = (SELECT lat FROM b_stopmaster_old b WHERE a.stopcode =  b.stopcode);
						

UPDATE pb_stopmasterdetail a 
SET lon = (SELECT lon FROM b_stopmaster_old b WHERE a.stopcode =  b.stopcode);




UPDATE pb_stopmasterdetail
SET lat = IFNULL (latu, latd),
		lon = IFNULL(lonu, lond);


UPDATE pb_stopmasterdetail a 
SET map = (SELECT map FROM b_stopmaster_old b WHERE a.stopcode = b.stopcode);

UPDATE pb_stopmasterdetail a 
SET cancelledstop = (SELECT cancelledstop FROM b_stopmaster_old b WHERE a.stopcode = b.stopcode);

UPDATE pb_stopmasterdetail a 
SET notfound = (SELECT notfound FROM b_stopmaster_old b WHERE a.stopcode = b.stopcode);


UPDATE pb_stopmasterdetail a
SET stopname  = proper(stopname);


UPDATE pb_stopmasterdetail a
SET areaname  = proper(areaname);


UPDATE pb_stopmasterdetail a
SET roadname  = proper(roadname);

UPDATE pb_stopmasterdetail
SET roadname = NULL WHERE roadname like '%name to %';


UPDATE pb_stopmasterdetail
SET stopnamedetailid = fn_RemoveSpecialCharacters(shortname(CONCAT
													(areaname,SUBSTRING_INDEX(SUBSTRING_INDEX(stopname, "/", 1), "(", 1))));



UPDATE pb_stopmasterdetail
SET stopdisplayname= CONCAT (CASE WHEN  length(areaname)>0 THEN CONCAT('[',areaname,'] ') ELSE '' END,
														 stopname) ;


UPDATE pb_stopmasterdetail
       SET noofbuses = 
       (SELECT COUNT(routecode) AS c
               FROM pb_routedetails_foratlasmatching
               WHERE pb_routedetails_foratlasmatching.stopcode = pb_stopmasterdetail.stopcode
               GROUP BY stopcode);


DROP TABLE IF EXISTS b_stopmaster;
CREATE TABLE `b_stopmaster` (
  `_id` int(11) NOT NULL AUTO_INCREMENT,
  `busesatstop` varchar(1000) DEFAULT NULL,
  `stopmaster_id` int(11) NOT NULL DEFAULT '0',
  `stopcode` bigint(21) unsigned DEFAULT NULL,
  `stopname` varchar(255) DEFAULT NULL,
  `areaname` varchar(255) DEFAULT NULL,
  `roadname` varchar(255) DEFAULT NULL,
  `direction` varchar(5) DEFAULT NULL,
  `latu` DOUBLE DEFAULT NULL,
  `latd` DOUBLE DEFAULT NULL,
  `lonu` DOUBLE DEFAULT NULL,
  `lond` DOUBLE DEFAULT NULL,
  `lat`  DOUBLE DEFAULT NULL,
  `lon`  DOUBLE DEFAULT NULL,
  `map` int(11) DEFAULT NULL,
  `cancelledstop` int(11) DEFAULT NULL,
  `notfound` int(11) DEFAULT NULL,
  `stopnameid` varchar(300) DEFAULT NULL,
  `areanameid` varchar(300) DEFAULT NULL,
  `stopnamedetailid` varchar(300) DEFAULT NULL,
  `stopdisplayname` varchar(300) DEFAULT NULL,
  `noofbuses` int(11) DEFAULT NULL,
  `stop_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


INSERT INTO b_stopmaster(stopmaster_id, stopcode, stopname, areaname, roadname, direction, latu, latd, lonu, lond, lat, lon,
map, cancelledstop, notfound, stopnameid, areanameid, stopnamedetailid, stopdisplayname, noofbuses, stop_id)
SELECT stopmaster_id, stopcode, stopname, areaname, roadname, direction, latu, latd, lonu, lond, lat, lon,
map, cancelledstop, notfound, stopnameid, areanameid, stopnamedetailid, stopdisplayname, noofbuses, stop_id
FROM pb_stopmasterdetail;




UPDATE b_stopmaster
SET stopnamedetailid = fn_RemoveSpecialCharacters(shortname(CONCAT
													(areaname,SUBSTRING_INDEX(SUBSTRING_INDEX(stopname, "/", 1), "(", 1))));


#####################

UPDATE b_stopmaster a 
SET latu = (SELECT latu FROM b_stopmaster_filled_lat_lon b WHERE a.stopcode = b.stopcode)
	WHERE latu IS NULL;

UPDATE b_stopmaster a 
SET latd = (SELECT latd FROM b_stopmaster_filled_lat_lon b WHERE a.stopcode =  b.stopcode)
	WHERE latd IS NULL;
						

UPDATE b_stopmaster a 
SET lonu = (SELECT lonu FROM b_stopmaster_filled_lat_lon b WHERE a.stopcode =  b.stopcode)
 WHERE lonu IS NULL;
						

UPDATE b_stopmaster a 
SET lond = (SELECT lond FROM b_stopmaster_filled_lat_lon b WHERE a.stopcode =  b.stopcode)
	WHERE lond IS NULL;
						

UPDATE b_stopmaster a 
SET lat = (SELECT lat FROM b_stopmaster_filled_lat_lon b WHERE a.stopcode =  b.stopcode)
	WHERE lat IS NULL;
						

UPDATE b_stopmaster a 
SET lon = (SELECT lon FROM b_stopmaster_filled_lat_lon b WHERE a.stopcode =  b.stopcode)
	WHERE lon IS NULL;

#####################


UPDATE b_stopmaster
SET lat = IFNULL (latu, latd),
		lon = IFNULL(lonu, lond);




CREATE INDEX noofbuses
ON b_stopmaster (noofbuses);

CREATE INDEX stopnamedetailid
ON b_stopmaster (stopnamedetailid);


CREATE INDEX stopcode
ON b_stopmaster (stopcode);


CREATE INDEX lat
ON b_stopmaster (lat);


CREATE INDEX lon
ON b_stopmaster (lon);

