DROP TABLE IF EXISTS b_frequencyac;
CREATE TABLE `b_frequencyac` (
  `_id` int(11) NOT NULL AUTO_INCREMENT,
  `startdayweek` int(11) DEFAULT NULL,
  `enddayweek` int(11) DEFAULT NULL,
  `holiday` varchar(5) DEFAULT NULL,
  `routecode` varchar(10) DEFAULT NULL,
  `stopcodefrom` int(11) DEFAULT NULL,
  `stopcodeto` int(11) DEFAULT NULL,
  `schedule` varchar(20) DEFAULT NULL,
  `stopserialfrom` int(11) DEFAULT NULL,
  `stopserialto` int(11) DEFAULT NULL,
  `firststop` varchar(50) DEFAULT NULL,
  `laststop` varchar(50) DEFAULT NULL,
  `actiming` varchar(10) DEFAULT NULL,
  `hh` int(11) DEFAULT NULL,
  `mm` varchar(11) DEFAULT NULL,
  `min` int(11) DEFAULT NULL,
  `direction` varchar(5) DEFAULT NULL,
  PRIMARY KEY (`_id`),
  KEY `frequencyac_routecode` (`routecode`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;


# import data from swapnils sheets

UPDATE b_frequencyac a
SET stopserialfrom = (SELECT b.stopserial FROM b_routedetail b 
												WHERE a.direction = b.direction
													AND a.routecode  = b.routecode
													 AND a.stopcodefrom = b.stopcode);

UPDATE b_frequencyac a
SET stopserialto = (SELECT b.stopserial FROM b_routedetail b 
												WHERE a.direction = b.direction
													AND a.routecode  = b.routecode
													 AND a.stopcodeto = b.stopcode);


UPDATE b_frequencyac
SET hh = TRUNCATE(actiming,0);

UPDATE b_frequencyac
SET mm =  CAST(SUBSTRING_INDEX(actiming, '.', -1) AS CHAR(4));


UPDATE b_frequencyac
SET min = (FLOOR(actiming) * 60) + (actiming % 60 - FLOOR(actiming))*100;

