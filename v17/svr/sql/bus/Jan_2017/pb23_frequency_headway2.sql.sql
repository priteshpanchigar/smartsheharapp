DROP TABLE IF EXISTS pb_frequency_headway2;
 
CREATE TABLE pb_frequency_headway2 (
	 frequency_id INTEGER NOT NULL auto_increment PRIMARY KEY,
	`best_routeatlas_id` int(11) DEFAULT NULL,
  `startheadwayhh` int(11) DEFAULT NULL,
  `endheadwayhh` int(11) DEFAULT NULL,
  `startheadwaymm` int(11) DEFAULT NULL,
  `endheadwaymm` int(11) DEFAULT NULL,
  `frequency` int(11) DEFAULT NULL,
  `startdayweek` int(11) DEFAULT NULL,
  `enddayweek` int(11) DEFAULT NULL,
  `holiday` varchar(10) DEFAULT NULL,
  `routecode` varchar(10) DEFAULT NULL,
  `firstto` varchar(10) DEFAULT NULL,
  `lastto` varchar(10) DEFAULT NULL,
  `firsttohh` int(11) DEFAULT NULL,
  `lasttohh` int(11) DEFAULT NULL,
  `firsttomm` int(11) DEFAULT NULL,
  `lasttomm` int(11) DEFAULT NULL,
  `schedule` varchar(100) DEFAULT NULL,
  `stopcodefrom` int(11) DEFAULT NULL,
  `stopcodeto` int(11) DEFAULT NULL,
  `stopserialfrom` int(11) DEFAULT NULL,
  `stopserialto` int(11) DEFAULT NULL,
  `direction` varchar(5) DEFAULT NULL,
  `startheadway` int(11) DEFAULT NULL,
  `endheadway` int(11) DEFAULT NULL,
  `headwayserial` int(11) DEFAULT NULL,
  `firsttoinmin` int(11) DEFAULT NULL,
  `lasttoinmin` int(11) DEFAULT NULL

);



INSERT INTO pb_frequency_headway2(best_routeatlas_id, routecode, firstto, lastto,  frequency, stopcodefrom, stopcodeto, SCHEDULE,direction, 
stopserialfrom, stopserialto, firsttoinmin, lasttoinmin, firsttohh, lasttohh, firsttomm, lasttomm,
startheadway,endheadway,headwayserial)

SELECT best_routeatlas_id, routecode, firstto, lastto,  frequency, stopcodefrom, stopcodeto, SCHEDULE,direction, 
stopserialfrom, stopserialto, firsttoinmin, lasttoinmin, firsttohh, lasttohh, firsttomm, lasttomm,
startheadway,endheadway,headwayserial FROM pb_to_headway1
UNION
SELECT best_routeatlas_id, routecode, firstto, lastto,  frequency, stopcodefrom, stopcodeto, SCHEDULE,direction, 
stopserialfrom, stopserialto, firsttoinmin, lasttoinmin, firsttohh, lasttohh, firsttomm, lasttomm,
startheadway,endheadway,headwayserial FROM pb_to_headway2
UNION
SELECT best_routeatlas_id, routecode, firstto, lastto,  frequency, stopcodefrom, stopcodeto, SCHEDULE,direction, 
stopserialfrom, stopserialto, firsttoinmin, lasttoinmin, firsttohh, lasttohh, firsttomm, lasttomm,
startheadway,endheadway,headwayserial FROM pb_to_headway3
UNION
SELECT best_routeatlas_id, routecode, firstto, lastto,  frequency, stopcodefrom, stopcodeto, SCHEDULE,direction, 
stopserialfrom, stopserialto, firsttoinmin, lasttoinmin, firsttohh, lasttohh, firsttomm, lasttomm,
startheadway,endheadway,headwayserial FROM pb_to_headway4
UNION
SELECT best_routeatlas_id, routecode, firstto, lastto,  frequency, stopcodefrom, stopcodeto, SCHEDULE,direction, 
stopserialfrom, stopserialto, firsttoinmin, lasttoinmin, firsttohh, lasttohh, firsttomm, lasttomm,
startheadway,endheadway,headwayserial FROM pb_to_headway5;

DELETE from pb_frequency_headway2
WHERE startheadway >=lasttoinmin;




UPDATE pb_frequency_headway2
SET startheadwayhh = TRUNCATE(startheadway/60, 0),
endheadwayhh = TRUNCATE(endheadway/60, 0),
startheadwaymm = startheadway - (TRUNCATE(startheadway/60, 0))*60, 
endheadwaymm = endheadway -  (TRUNCATE(endheadway/60, 0))*60 ;
