DROP TABLE IF EXISTS best_routedetails_stopnamechange;
CREATE TABLE `best_routedetails_stopnamechange` (
  `best_routedetails_id` int(15) NOT NULL AUTO_INCREMENT,
	`rno` varchar(255) DEFAULT NULL,
  `stopserial` int(11) DEFAULT NULL,
  `stopcode` int(11) DEFAULT NULL,
	`stageno` int(11) DEFAULT NULL,  
	`stage` varchar(255) DEFAULT NULL,
  `km` double DEFAULT NULL,
  `stopname` varchar(255) DEFAULT NULL,
	`areaname`	VARCHAR (255),	
	`roadname`	VARCHAR (255),
  `direction` varchar(255) DEFAULT NULL,
	PRIMARY KEY (`best_routedetails_id`)
) ENGINE=InnoDB;

INSERT INTO best_routedetails_stopnamechange(rno, stopserial, stopcode,  stageno, stage, km, stopname, areaname, roadname, direction)
SELECT  rno, stopserial, brs.stopcode, stageno, stage,  km, s.stopname,  s.areaname,  s.roadname, brs.direction
FROM routedetails brs
LEFT JOIN stopmaster s
ON brs.stopcode = s.stopcode
ORDER BY rno, stopserial;


update best_routedetails_stopnamechange
set stopname = 'S.P.Mukherji Chowk'
where stopname = 'DR.SHAMAPRASAD MUKHERJI GARDEN';


update best_routedetails_stopnamechange
set stopname = 'S.P.Mukherji Chowk'
where stopname = 'DR.SHAMAPRASAD MUKHERJI GARDEN';

update best_routedetails_stopnamechange
set stopname = 'S.P.Mukherji Chowk'
where stopname = 'DR.SHYAMAPRASAD MUKHERJI CHK.';


update best_routedetails_stopnamechange
set stopname = 'S.T.OFFICE/JARIMARI MATA MANDIR'
where stopcode = 451;
