DROP TABLE IF EXISTS pb_atlas_with_direction;

	CREATE TABLE pb_atlas_with_direction AS
	SELECT *, IF(stopserialfrom < stopserialto, 'U' , 'D') as direction
	FROM pb_atlas_withstopserial;

	CREATE INDEX best_routeatlas_id ON pb_atlas_with_direction (best_routeatlas_id);