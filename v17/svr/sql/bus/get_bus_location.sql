DROP PROCEDURE IF EXISTS get_bus_location;
DELIMITER $$
CREATE  PROCEDURE `get_bus_location`(IN inbuslabel varchar (40), IN inclientdatetime datetime)
BEGIN
	
SELECT  trip_id, b.appuser_id, vehicle_no, buslabel, 
createdatetime, fromstop, tostop, fromstopserial, tostopserial, 
direction_sign, direction ,tripstatus,  speed, lat,  lng, l.clientaccessdatetime	
FROM bustrip b
LEFT JOIN location l
ON l.location_id = b.last_location_id
	WHERE buslabel = inbuslabel
	AND tripstatus !='E'
	#AND direction = indirection
AND TIMESTAMPDIFF(HOUR,createdatetime,inclientdatetime) < 4
AND trip_id = (SELECT MAX(trip_id) from bustrip b2 WHERE b.appuser_id = b2.appuser_id);

END$$
DELIMITER;

call get_bus_location('2 L', '2016-01-27 16:44:00')