DELETE FROM pb_routeatlas_withroutecode
	WHERE CAST(right(routecode,1) AS unsigned)=7
	OR CAST(right(routecode,1) AS unsigned)= 8
	OR CAST(right(routecode,1) AS unsigned)= 9;

DELETE FROM pb_routeatlas_withroutecode
	WHERE routeno = '506Extra' 
	OR  routeno = 'BKC-3' 
	OR routeno = 'DDR-2' 
	OR routeno = 'DDR-3' 
	OR routeno = 'JVPD-1';

  DROP TABLE IF EXISTS pb_routeatlas_withfromtostopcodes;
	CREATE TABLE pb_routeatlas_withfromtostopcodes
  (_id int not null auto_increment PRIMARY KEY)
   SELECT a.routeno,a.fromstop,a.tostop,a.fromstopnameid,a.tostopnameid,a.best_routeatlas_id,a.routecode,
				(select r.stopcode from pb_routedetails_foratlasmatching r where a.routecode = r.routecode and a.fromstopnameid = r.stopnameid Limit 1) as stopcodefrom,
				(select r.stopcode from pb_routedetails_foratlasmatching r where a.routecode = r.routecode and a.tostopnameid = r.stopnameid Limit 1) as stopcodeto,
        schedule
   FROM pb_routeatlas_withroutecode a;

   CREATE INDEX pb_routeatlas_fromtostopcode_best_routeatlas_id ON 
   pb_routeatlas_withfromtostopcodes (best_routeatlas_id);