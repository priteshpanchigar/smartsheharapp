DROP TABLE IF EXISTS pb_routeatlas_basic;
	CREATE TABLE pb_routeatlas_basic
  (_id int not null auto_increment PRIMARY KEY)
   SELECT best_routeatlas_id,routeno,fromstop,tostop,
					fn_RemoveVowels(fn_RemoveSpecialCharacters(shortname(SUBSTRING_INDEX(LOWER(fromstop), '(', 1)))) as fromstopnameid,
          fn_RemoveVowels(fn_RemoveSpecialCharacters(shortname(SUBSTRING_INDEX(LOWER(tostop), '(', 1)))) as tostopnameid,
					schedule
	 FROM pb_raw_routeatlas;