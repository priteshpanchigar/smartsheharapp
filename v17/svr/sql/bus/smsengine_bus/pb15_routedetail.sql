/*DROP TABLE IF EXISTS deleted_route;
CREATE TABLE deleted_route AS
select * FROM pb_routeatlas_withroutecode
WHERE routecode NOT IN (SELECT routecode from pb_routedetails_foratlasmatching);


SELECT * FROM deleted_route
GROUP BY routecode;
*/
DELETE from pb_routedetails_foratlasmatching 
WHERE routecode =  '1062' OR routecode =  '1200' OR  routecode =  '1830' 
	 or routecode =  '2174' or routecode =  '3890' or routecode =  '3944'
	 or routecode =  '4215' or routecode =  '4560' or routecode =  '7013';

DROP TABLE IF EXISTS pb_routedetail;

CREATE TABLE `pb_routedetail` (
  `_id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `routecode` varchar(10) ,
  `stopcode` int(11) ,
  `stopserial` int(11) ,
	`direction` varchar(5),
	 stopname VARCHAR (90),
	 stopnameid VARCHAR(100),
	 distance DOUBLE
);



INSERT INTO pb_routedetail(routecode,stopcode,stopserial,direction,stopname)
SELECT routecode,stopcode,stopserial,direction,stopname FROM pb_routedetails_foratlasmatching 
GROUP BY LTRIM(rtrim(routecode)),stopserial;

DROP TABLE IF EXISTS pb_routedetail_dist;


CREATE TABLE pb_routedetail_dist AS
SELECT r.*,s.lat,s.lon FROM pb_routedetail r
LEFT OUTER JOIN b_stopmaster s
ON r.stopcode = s.stopcode ORDER BY routecode,stopserial; 



UPDATE pb_routedetail_dist
SET distance = NULL;

UPDATE pb_routedetail_dist
SET lat = NULL WHERE lat = 0;


UPDATE pb_routedetail_dist
SET lon = NULL WHERE lon = 0;

UPDATE pb_routedetail_dist
SET distance = NULL;


DROP TABLE IF EXISTS pb_routedetail_dist2;

CREATE TABLE pb_routedetail_dist2 as
SELECT * FROM pb_routedetail_dist;


CREATE INDEX routedetail_routecode
ON pb_routedetail_dist (routecode ASC);


CREATE INDEX routedetail_stopcode
ON pb_routedetail_dist (stopcode ASC);


CREATE INDEX routedetail_stopserial
ON pb_routedetail_dist (stopserial ASC);



CREATE INDEX routedetail_routecode
ON pb_routedetail_dist2 (routecode ASC);


CREATE INDEX routedetail_stopcode
ON pb_routedetail_dist2 (stopcode ASC);


CREATE INDEX routedetail_stopserial
ON pb_routedetail_dist2 (stopserial ASC);



UPDATE pb_routedetail_dist rd
SET distance=
(SELECT getDistanceBetweenPoints(rd.lat, rd.lon, rd2.lat, rd2.lon) 
FROM pb_routedetail_dist2 rd2 
WHERE rd.routecode = rd2.routecode AND rd2.stopserial > rd.stopserial
ORDER BY rd2.routecode, rd2.stopserial LIMIT 1);



UPDATE pb_routedetail_dist
SET direction = replace(direction,' ','');


DROP TABLE IF EXISTS pb_routedetail_u;

CREATE TABLE pb_routedetail_u (
  _id int(11) AUTO_INCREMENT,
  routecode varchar(10) ,
  stopcode int ,
  stopserial int ,
  direction varchar(5),
  stopname varchar(50),
  side varchar(5),
	distance VARCHAR(30),
	stopserial_new INT,
  PRIMARY KEY (_id,side),
  KEY _id_side (_id,side)
);

INSERT INTO pb_routedetail_u(routecode,stopcode,stopserial,direction,stopname,side,distance)
SELECT routecode,stopcode,stopserial,direction,stopname,'U',distance  FROM pb_routedetail_dist
WHERE LENGTH(direction)=0  OR direction ='U'
ORDER BY routecode,stopserial;



SET @minid = (SELECT min(_id) FROM pb_routedetail_dist);


UPDATE pb_routedetail_u
SET stopserial_new = @minid - _id;





DROP TABLE IF EXISTS pb_routedetail_d;

CREATE TABLE pb_routedetail_d (
  _id int(11) AUTO_INCREMENT,
  routecode varchar(10) ,
  stopcode int ,
  stopserial int ,
  direction varchar(5),
  stopname varchar(50),
  side varchar(5),
	distance VARCHAR(30),
	stopserial_new INT,
  PRIMARY KEY (_id,side),
  KEY _id_side (_id,side)
);

INSERT INTO pb_routedetail_d(routecode,stopcode,stopserial,direction,stopname,side,distance)
SELECT routecode,stopcode,stopserial,direction,stopname,'D',distance  FROM pb_routedetail_dist
WHERE LENGTH(direction)=0 OR direction ='D'
ORDER BY routecode,stopserial;




SET @maxid = (SELECT MAX(_id) FROM pb_routedetail_dist);


UPDATE pb_routedetail_d
SET stopserial_new = @maxid - _id;







CREATE INDEX routedetail_routecode
ON pb_routedetail_U (routecode ASC);


CREATE INDEX routedetail_newstopserial
ON pb_routedetail_u (stopserial_new ASC);


CREATE INDEX routedetail_newstopserial
ON pb_routedetail_d (stopserial_new ASC);


CREATE INDEX routedetail_routecode
ON pb_routedetail_d (routecode ASC);








DROP TABLE IF EXISTS b_routedetail;

CREATE TABLE b_routedetail (
  _id int(11) AUTO_INCREMENT,
  routecode varchar(10) ,
  stopcode int ,
  stopserial int ,
  direction varchar(5),
  stopname varchar(50),
	distance DECIMAL(5,3),
	side varchar(10),
	stopunique_id INT,
  PRIMARY KEY (_id,direction),
  KEY _id_side (_id,direction)
);



INSERT INTO b_routedetail(routecode,stopcode,direction,stopname,distance,stopserial)
SELECT g.routecode,g.stopcode,g.side,g.stopname,g.distance,
(SELECT COUNT(*) FROM pb_routedetail_u o
WHERE o.routecode = g.routecode AND o.stopserial_new >= g.stopserial_new
GROUP BY o.routecode) stopserial
FROM pb_routedetail_u g
ORDER BY routecode, stopserial;

INSERT INTO b_routedetail(routecode,stopcode,direction,stopname,distance,stopserial)
SELECT g.routecode,g.stopcode,g.side,g.stopname,g.distance,
(SELECT COUNT(*) FROM pb_routedetail_d o
WHERE o.routecode = g.routecode AND o.stopserial_new <= g.stopserial_new
GROUP BY o.routecode) stopserial
FROM pb_routedetail_d g
ORDER BY routecode, stopserial;


CREATE INDEX rd_direction
ON b_routedetail (direction);

CREATE INDEX rd_routecode
ON b_routedetail (routecode);

CREATE INDEX rd_side
ON b_routedetail (side);

CREATE INDEX rd_stopcode
ON b_routedetail (stopcode);





UPDATE b_routedetail
SET side=direction;

UPDATE b_routedetail
SET distance = NULL WHERE LENGTH(distance)=0;


UPDATE b_routedetail a
SET stopunique_id = (SELECT stop_id from b_stopmaster b WHERE a.stopcode = b.stopcode); 

CREATE INDEX rd_stopunique_id
ON b_routedetail (stopunique_id);

