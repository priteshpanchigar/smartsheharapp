DROP TABLE IF EXISTS pb_routedetails_foratlasmatching;
	CREATE TABLE pb_routedetails_foratlasmatching 
  (_id int not null auto_increment PRIMARY KEY)
  SELECT RNO as routecode,
				CAST(STOPCD AS UNSIGNED) as stopcode,
        CAST(STOPSR as UNSIGNED) as stopserial,
				`Stop Name` as stopname,
         
				 `Road Name` as roadname,
          `Area Name` as areaname,
				best_routedetails_id as best_routedetails_id,
				fn_RemoveVowels(fn_RemoveSpecialCharacters(shortname(SUBSTRING_INDEX(`Stop Name`, '(', 1)))) as stopnameid
	FROM best_routedetails_stopnamechange
  GROUP BY best_routedetails_stopnamechange.RNO,best_routedetails_stopnamechange.STOPCD
  ORDER BY best_routedetails_stopnamechange.RNO, stopserial;
  
  CREATE INDEX routedetail_index ON pb_routedetails_foratlasmatching (routecode);	
	CREATE INDEX stopcode_index ON pb_routedetails_foratlasmatching (stopcode);
	CREATE INDEX stopname_index ON pb_routedetails_foratlasmatching (stopname);	

