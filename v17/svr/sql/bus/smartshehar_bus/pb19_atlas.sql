	DROP TABLE IF EXISTS bb_atlas;
	CREATE TABLE bb_atlas AS
	SELECT  a.* ,b.Headway1, b.Headway2, b.Headway3, b.Headway4, b.Headway5, b.firstfrom, b.firstto, b.lastfrom , b.lastto 
		FROM pb_atlas_with_direction a
		LEFT OUTER JOIN pb_raw_routeatlas b ON
			a.best_routeatlas_id = b.best_routeatlas_id;

	ALTER TABLE bb_atlas
	ADD COLUMN firstfrominmin INT;
	
	ALTER TABLE bb_atlas
	ADD COLUMN lastfrominmin INT;
	
	ALTER TABLE bb_atlas
	ADD COLUMN firsttoinmin INT;
	
	ALTER TABLE bb_atlas
	ADD COLUMN lasttoinmin INT;


	UPDATE bb_atlas
	SET firstfrominmin = (FLOOR(firstfrom) * 60) + (firstfrom % 60 - FLOOR(firstfrom))*100,
			lastfrominmin	= (FLOOR(lastfrom) * 60) + (lastfrom % 60 - FLOOR(lastfrom))*100,
			firsttoinmin = (FLOOR(firstto) * 60) + (firstto % 60 - FLOOR(firstto))*100,
			lasttoinmin = (FLOOR(lastto) * 60) + (lastto % 60 - FLOOR(lastto))*100;


DROP TABLE  IF EXISTS b_atlas;
CREATE TABLE b_atlas AS
SELECT best_routeatlas_id,  routecode,  firstfrom,  lastfrom,  firstto,  lastto,
			 Headway1,  Headway2,  Headway3,   Headway4,   Headway5,
			 stopcodefrom , stopcodeto ,  `schedule`, direction , 
			 TRUNCATE(firstfrom,0) as firstfromhh, 
			 TRUNCATE(lastfrom,0) as lastfromhh,
			 TRUNCATE(firstto,0) as firsttohh, 
			 TRUNCATE(lastto,0) as lasttohh,
			 CAST(SUBSTRING_INDEX(firstfrom, '.', -1) AS CHAR(4)) as firstfrommm,
			 CAST(SUBSTRING_INDEX(lastfrom, '.', -1) AS CHAR(4)) as lastfrommm,
			 CAST(SUBSTRING_INDEX(firstto, '.', -1) AS CHAR(4)) as firsttomm,
			 CAST(SUBSTRING_INDEX(lastto, '.', -1) AS CHAR(4)) as lasttomm,
			 stopserialfrom,stopserialto,firstfrominmin,lastfrominmin,firsttoinmin,lasttoinmin
from bb_atlas;

/*
SELECT best_routeatlas_id, routecode, 
firstfrom, firstfromhh, firstfrommm, firstfrominmin,
lastfrom, lastfromhh, lastfrommm, lastfrominmin, 
firstto, firsttohh, firsttomm, firsttoinmin, 
lastto, lasttohh,  lasttomm, lasttoinmin,
Headway1, Headway2, Headway3, Headway4, Headway5,
stopcodefrom, stopcodeto, stopserialfrom, stopserialto,  schedule, direction FROM b_atlas;
*/

DELETE  FROM b_atlas
WHERE stopserialfrom is null or stopserialto is NULL;


#We run this query for buses after midnight

UPDATE  b_atlas
SET lasttoinmin = 24*60 + lasttoinmin
WHERE lasttoinmin < firsttoinmin;


UPDATE  b_atlas
SET lastfrominmin = 24*60 + lastfrominmin 
WHERE lastfrominmin < firstfrominmin;
