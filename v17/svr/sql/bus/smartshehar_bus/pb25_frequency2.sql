
DROP table if EXISTS schedulelookup;
create table schedulelookup (
`schedule` VARCHAR(50) PRIMARY KEY,
startday integer,
endday integer,
holiday VARCHAR(10)
);


insert into schedulelookup (`schedule`,startday,endday)
values ('Mon-Sat',1,6);



insert into schedulelookup (`schedule`,holiday)
values ('Hol','Y');



insert into schedulelookup (`schedule`,startday,endday)
values ('Sun',7,7);



insert into schedulelookup (`schedule`,startday,endday,holiday)
values ('Mon-Fri-Hol',1,5,'Y');



insert into schedulelookup (`schedule`,startday,endday)
values ('Sat',6,6);



insert into schedulelookup (`schedule`,startday,endday)
values ('Mon-Fri',1,5);



insert into schedulelookup (`schedule`,startday,endday,holiday)
values ('Sun-Hol',7,7,'Y');



insert into schedulelookup (`schedule`,startday,endday,holiday)
values ('All',1,7,'Y');



insert into schedulelookup (`schedule`,startday,endday)
values ('Sat-Sun',6,7);



insert into schedulelookup (`schedule`,startday,endday,holiday)
values ('Mon-Sat-Hol',1,6,'Y');



insert into schedulelookup (`schedule`,startday,endday,holiday)
values ('Sat-Sun-Hol',6,7,'Y');



insert into schedulelookup (`schedule`,startday,holiday)
values ('Sat-Hol',6,'Y');



insert into schedulelookup (`schedule`,startday,endday)
values ('FW',1,7);


insert into schedulelookup (`schedule`,startday,endday)
values ('MF',1,5);


insert into schedulelookup (`schedule`,startday,endday,holiday)
values ('MF&HOL',1,5,'Y');

insert into schedulelookup (`schedule`,startday,endday)
values ('MS',1,6);


insert into schedulelookup (`schedule`,startday,endday,holiday)
values ('MS&HOL',1,6,'Y');


insert into schedulelookup (`schedule`,startday,endday,holiday)
values ('SAT&HOL',6,6,'Y');


insert into schedulelookup (`schedule`,startday,endday,holiday)
values ('SH',7,7,'Y');





insert into schedulelookup (`schedule`,startday,endday,holiday)
values ('Mon-Fri & Hol',1,5,'Y');


insert into schedulelookup (`schedule`,startday,endday,holiday)
values ('Mon-Sat & Hol',1,6,'Y');


insert into schedulelookup (`schedule`,startday,endday,holiday)
values ('Mon-Sun & Hol',1,7,'Y');


insert into schedulelookup (`schedule`,startday,endday,holiday)
values ('S/H',7,7,'Y');


insert into schedulelookup (`schedule`,startday,endday,holiday)
values ('Sat & Hol',6,6,'Y');


insert into schedulelookup (`schedule`,startday,endday,holiday)
values ('SAT/SH',6,7,'Y');


insert into schedulelookup (`schedule`,startday,endday,holiday)
values ('Sun & Hol',7,7,'Y');


insert into schedulelookup (`schedule`,startday,endday,holiday)
values ('All Days',1,7,'Y');



insert into schedulelookup (`schedule`,startday,endday,holiday)
values ('FH',5,5,'Y');





drop table IF EXISTS  b_frequency2;

 
CREATE TABLE b_frequency2 (
frequency_id  INTEGER not null auto_increment PRIMARY key,
startheadwayhh  int,
endheadwayhh  int,
startheadwaymm  int,
endheadwaymm  int,
frequency  int,
startdayweek  INTEGER,
enddayweek  INTEGER,
holiday  VARCHAR(10),
routecode varchar(10),
firstto varchar(10),
lastto varchar(10),
firsttohh int,
lasttohh int,
firsttomm int,
lasttomm int,
`schedule` varchar (100),
stopcodefrom INT,
stopcodeto int,
stopserialfrom INT,
stopserialto int,
direction VARCHAR(5),
firsttoinmin int,
lasttoinmin int
);



INSERT INTO b_frequency2 (
	startheadwayhh,	endheadwayhh,	startheadwaymm,	endheadwaymm,	frequency,	routecode,	firsttohh,	lasttohh,firsttomm,	lasttomm,	`schedule`,	stopcodefrom,
	stopcodeto,	stopserialfrom,	stopserialto,	direction,	firstto,	lastto,	firsttoinmin,	lasttoinmin) 
SELECT
	startheadwayhh,	endheadwayhh,	startheadwaymm,	endheadwaymm,	frequency,	routecode,	firsttohh,	lasttohh,firsttomm,	lasttomm,	`SCHEDULE`,	stopcodefrom,
	stopcodeto,	stopserialfrom,	stopserialto,	direction,	firstto,	lastto,	firsttoinmin,	lasttoinmin FROM
	pb_frequency_headway2;




CREATE INDEX f_direction
ON b_frequency2 (direction);

CREATE INDEX f_routecode
ON b_frequency2 (routecode);


UPDATE  b_frequency2 
set `schedule` = 'All Days' 
where length(`schedule`)=0;



UPDATE b_frequency2
SET startdayweek=  
(SELECT startday from schedulelookup
where schedulelookup.`schedule`=b_frequency2.`Schedule`); 


UPDATE b_frequency2
SET enddayweek=  
(SELECT endday from schedulelookup
 WHERE schedulelookup.`schedule`=b_frequency2.`Schedule`);



UPDATE b_frequency2
set holiday=(SELECT holiday from schedulelookup 
where schedulelookup.`schedule`=b_frequency2.`Schedule`);




#check routecode which are not there in either of these TABLES
/*
 SELECT *  from b_frequency2 
WHERE  routecode NOT In (SELECT routecode FROM b_route)
GROUP BY routecode;


  SELECT * from b_routedetail
WHERE  routecode NOT In (SELECT routecode FROM b_route)
GROUP BY routecode;
*/



DELETE  from b_frequency2 
WHERE  routecode NOT In (SELECT routecode FROM b_route);

DELETE  from b_routedetail
WHERE  routecode NOT In (SELECT routecode FROM b_route);


DROP TABLE
IF EXISTS b_frequency_summary2;
CREATE TABLE b_frequency_summary2 (
	summary_id INTEGER NOT NULL auto_increment PRIMARY KEY
) SELECT
	startheadwayhh,	endheadwayhh,	startheadwaymm,	endheadwaymm,	frequency,	routecode,	firstto,	lastto,	`SCHEDULE`,	stopcodefrom,
	stopcodeto,	stopserialfrom,	stopserialto,	direction,	min(firsttoinmin) firsttoinmin, max(lasttoinmin) lasttoinmin,
	firsttohh,	lasttohh,	firsttomm,	lasttomm,	startdayweek,	enddayweek,	holiday
FROM
	b_frequency2
GROUP BY
	routecode,	`schedule`, stopcodefrom, stopcodeto;
