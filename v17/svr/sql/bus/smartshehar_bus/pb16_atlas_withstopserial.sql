#SKIP this step
/*
update pb_routeatlas_withfromtostopcodes_updated  a
set stopcodefrom = (select corrected_stopcodefrom  from corrected_stopcode b 
WHERE a.best_routeatlas_id = b.best_routeatlas_id)
WHERE a.best_routeatlas_id 
IN (SELECT best_routeatlas_id FROM corrected_stopcode);



update pb_routeatlas_withfromtostopcodes_updated a
set stopcodeto = (select corrected_stopcodeto  from corrected_stopcode b
WHERE a.best_routeatlas_id = b.best_routeatlas_id)
WHERE a.best_routeatlas_id 
IN (SELECT best_routeatlas_id FROM corrected_stopcode);





	DROP TABLE IF EXISTS pb_atlas_withstopserial;

	CREATE TABLE pb_atlas_withstopserial as
	SELECT a.*,
		(SELECT min(stopserial) FROM b_routedetail b WHERE b.direction = 'U' 
			AND a.routecode = b.routecode
			and a.stopcodefrom = b.stopcode )as stopserialfrom ,
		(SELECT max(stopserial) FROM b_routedetail b WHERE b.direction = 'U' 
			AND a.routecode = b.routecode
			AND a.stopcodeto = b.stopcode ) as stopserialto 
	from pb_routeatlas_withcorrectedstopcodes a;
*/
#We take both direction up because we dont want confusion in stopserial
DROP TABLE IF EXISTS pb_atlas_withstopserial;
CREATE TABLE pb_atlas_withstopserial
(_id INT NOT NULL AUTO_INCREMENT PRIMARY KEY)
	SELECT routeno, fromstop, tostop, fromstopnameid, tostopnameid, best_routeatlas_id, routecode, stopcodefrom,
	stopcodeto, `schedule`,
		(SELECT stopserial FROM b_routedetail b WHERE b.direction = 'U' 
			AND a.routecode = b.routecode
			and a.stopcodefrom = b.stopcode )as stopserialfrom ,
		(SELECT stopserial FROM b_routedetail b WHERE b.direction = 'U' 
			AND a.routecode = b.routecode
			AND a.stopcodeto = b.stopcode ) as stopserialto 
	FROM pb_routeatlas_withfromtostopcodes_updated a;
