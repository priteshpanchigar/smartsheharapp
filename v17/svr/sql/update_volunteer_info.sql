DROP PROCEDURE IF EXISTS update_volunteer_info;
DELIMITER $$
CREATE PROCEDURE `update_volunteer_info`(IN inappuserid INT, IN indescribelookup varchar(500),
IN incomment VARCHAR(200))
BEGIN
	
	
INSERT INTO volunteer_info(appuser_id, describe_lookup, comment)
	VALUES (inappuserid, indescribelookup, incomment);

SELECT LAST_INSERT_ID() as volunteer_info_id;
	
END$$
DELIMITER ;