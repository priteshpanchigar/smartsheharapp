DROP PROCEDURE IF EXISTS nearby_offers;
DELIMITER $$

CREATE PROCEDURE `nearby_offers`(IN inappuserid INT, IN inclientdatetime datetime,  
	IN `inlatno` double, IN `inlngno` double)
BEGIN
	SET @inDistance = 2;	
	
SELECT discount_id, discount_percentage, discount_start_time, discount_end_time, discount_description, d.store_id, store_name, opening_time, closing_time, sa.address_id, sc.category_id, address_line_1,
latitude, longitude, category FROM discount d 
LEFT JOIN store s 
ON d.store_id = s.store_id
INNER JOIN store_address sa
ON sa.address_id = s.address_id
INNER JOIN store_category sc
ON sc.category_id = s.category_id
WHERE  ABS(inlatno - latitude) < @inDistance / 100
AND ABS(inlngno - longitude) < @inDistance / 100
AND inclientdatetime BETWEEN discount_start_time and discount_end_time
ORDER BY getDistanceBetweenPoints(inlatno, inlngno, latitude, longitude);
	
	

	
	
END$$
DELIMITER;

call nearby_offers(3,'2017-06-30 19:14:23','19.104615', '72.850425');