DROP PROCEDURE IF EXISTS dynamic_nearby_offers;
DELIMITER $$

CREATE  PROCEDURE `dynamic_nearby_offers`(IN inappuserid INT, IN inclientdatetime datetime,
IN inmarkerlat DOUBLE , 
IN inmarkerlng DOUBLE, IN inmaxlat DOUBLE , IN inmaxlng DOUBLE,IN inminmarkerlat DOUBLE , IN inminmarkerlng DOUBLE, 
IN inorderby VARCHAR (250), IN inlat DOUBLE, IN inlng DOUBLE)
BEGIN


SET @sel = 'SELECT discount_id, discount_percentage, discount_start_time, discount_end_time, discount_description, d.store_id, store_name, opening_time, closing_time, sa.address_id, sc.category_id, address_line_1,
latitude lat, longitude lng, category' ;


SET @whr = NULL;

/* uncomment this section once apps starts sending lat and lng and order */
/*
SET @whr = IF(inmaxlat IS NULL OR inmaxlng IS NULL OR inminmarkerlng IS NULL OR inminmarkerlng IS NULL, @whr, 
							CONCAT(IF(@whr IS NULL,'',CONCAT(@whr, '\n AND ')),	
								' latitude BETWEEN ', inminmarkerlat, ' AND ', inmaxlat,'', 
						' AND longitude BETWEEN ', inminmarkerlng, ' AND ', inmaxlng,''));

SET @whr = IF(  inclientdatetime IS NULL , @whr,
							CONCAT(IF(@whr IS NULL,'',CONCAT(@whr, '\n AND "')),								
								inclientdatetime,' " BETWEEN  discount_start_time AND discount_end_time'));
*/
SET @dynquery = CONCAT(@sel, ' FROM discount d
										LEFT JOIN store s 
										ON d.store_id = s.store_id
										INNER JOIN store_address sa
										ON sa.address_id = s.address_id
										INNER JOIN store_category sc
										ON sc.category_id = s.category_id \n', 
			IFNULL(CONCAT(' WHERE ', @whr),''),
			IFNULL(@orderby,''));


SET @orderby = '\n ORDER BY ';
IF inorderby IS NULL THEN
	
	IF inlat IS NOT NULL AND inlng IS NOT NULL THEN
		SET @orderby = CONCAT(@orderby, CONCAT('getDistanceBetweenPoints(', inlat, ', ', inlng, ', latitude, longitude) '));
	ELSE 
			SET @orderby = CONCAT(@orderby, ' discount_end_time DESC ');
	END IF;
ELSE
	SET @orderby = CONCAT(@orderby, inorderby);
END IF;  


#SELECT @dynquery;
PREPARE stmt FROM @dynquery; 
EXECUTE stmt; 
DEALLOCATE PREPARE stmt; 

END$$ 
DELIMITER;
call dynamic_nearby_offers(524,'2017-07-10 21:00:00',NULL,NULL,19.117167960,72.858176194,19.092188454,72.842727005,NULL,19.117167960,72.858176194);

