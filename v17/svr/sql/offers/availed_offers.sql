DROP PROCEDURE IF EXISTS availed_offers;
DELIMITER $$

CREATE PROCEDURE `availed_offers`( IN indiscountid INT, IN inappuserid INT, IN inclientdatetime datetime)
BEGIN
	
	INSERT INTO availed_offer(discount_id, appuser_id, availed_datetime)
		values(indiscountid, inappuserid, inclientdatetime);
	

	
	
END$$
DELIMITER;

CALL availed_offers(3,13,'2017-08-28 10:00:00');