DROP VIEW IF EXISTS vw_issue_with_lookup;

CREATE VIEW vw_issue_with_lookup as



SELECT issue_lookup_item_id, i.issue_item_code, issue_item_description, issue_item_order, photo_mandatory ,
issue_lookup_sub_type_id,  issue_sub_type_code, issue_sub_type_description, issue_sub_type_order, vehicle_number_mandatory,
 issue_lookup_type_id, issue_type_code,  issue_type_description, department_code, colour, android_colour,
i.issue_id,
(SELECT formatted_address from
 address ad where ad.address_id = i.address_id) formatted_address,
 issue_time, client_datetime, vehicle_no, unique_key, submit_report, approved, rejected,
approved_rejected_clientdatetime, reason_code, other_reason_code_comment, to_be_sent_to_authority, sent_to_authority,
sent_to_authority_datetime, resolved, unresolved, resolved_unresolved_datetime, unresolved_comment, address_id,
location_id, 
(SELECT ad.ward FROM address ad 
	WHERE(ad.address_id = i.address_id)) AS ward, group_id,
 mla_id, mp_id, lat, lng, appuser_id, discard_report, letter_submit_by,
issue_registration_notification, letter_submitted_to, letter_submitted_dept, letter_submitted_desg, letter_submitted_datetime,
letter_remark, resolved_unresolved_by, letter_upload_notification, resolved_unresolved_notification, letter_client_date_time,
letter_image_name, letter_image_path, closed, opened, closed_opened_datetime, closed_opened_by, closed_opened_comment
FROM issue i INNER JOIN
vw_issue_items  vi ON vi.issue_item_code = i.issue_item_code;


SELECT * FROM vw_issue_with_lookup