DROP PROCEDURE IF EXISTS v17_add_callhome;
DELIMITER $$
CREATE  PROCEDURE `v17_add_callhome`(IN inemailid VARCHAR(40), IN inimeino VARCHAR(40), 
IN inlatno DOUBLE, IN inlngno DOUBLE,  IN inacc DOUBLE, IN inprovider VARCHAR(20), 
IN incarrier VARCHAR(20), IN inproduct VARCHAR(20), IN inmanufacturer VARCHAR(20), 
IN inandroid_release_version VARCHAR(20), IN inandroid_sdk_version INT,    
IN inclientdatetime DATETIME, IN inip VARCHAR(200), IN inuseragent VARCHAR(300), 
IN inversion VARCHAR(20), IN inversioncode VARCHAR(20), IN inapp VARCHAR(20), 
IN inlocationdatetime DATETIME, IN inmodule VARCHAR(20))

BEGIN

DECLARE v_APPUSER_ID int;
DECLARE v_APPUSAGE_ID int;
	

	INSERT IGNORE INTO appuser(email, imei, lat, lng, accuracy, provider,
			carrier, product, manufacturer, android_release_version, 
			android_sdk_version, clientlastaccessdatetime, ip, useragent)
	VALUES(inemailid,  inimeino,  inlatno, inlngno, inacc, inprovider, 
		   incarrier, inproduct, inmanufacturer, inandroid_release_version, 
		   inandroid_sdk_version,  inclientdatetime, inip, inuseragent)
		ON DUPLICATE KEY UPDATE
		   imei = COALESCE(inimeino, imei),
		   lat = COALESCE(inlatno, lat),
		   lng = COALESCE(inlngno, lng),
		   accuracy = COALESCE(inacc, accuracy),
		   provider = COALESCE(inprovider, provider),
		   carrier = COALESCE(incarrier, carrier),
		   product = COALESCE(inproduct, product),
		   manufacturer = COALESCE(inmanufacturer, manufacturer),
		   android_release_version = COALESCE(inandroid_release_version, android_release_version),
		   android_sdk_version = COALESCE(inandroid_sdk_version, android_sdk_version),
		   clientlastaccessdatetime =  COALESCE(inclientdatetime, clientlastaccessdatetime),
		   ip =  COALESCE(inip, ip),
		   useragent =  COALESCE(inuseragent, useragent);
		   
    SELECT appuser_id FROM appuser
		WHERE email = inemailid
	INTO v_APPUSER_ID;
	
	INSERT IGNORE INTO appusage(appuser_id, clientfirstaccessdatetime, 
			clientlastaccessdatetime, serverfirstaccessdatetime, 
			serverlastaccessdatetime, version, versioncode, app)
	VALUES(v_APPUSER_ID, inclientdatetime, inclientdatetime, 
			NOW(), NOW(), inversion, inversioncode, inapp)
		ON DUPLICATE KEY UPDATE    
			clientlastaccessdatetime = COALESCE(inclientdatetime, clientlastaccessdatetime), 
			serverlastaccessdatetime = COALESCE(NOW(), serverlastaccessdatetime),
			version = COALESCE(inversion, version), 
			versioncode = COALESCE(inversioncode, versioncode);
			
	SELECT appusage_id FROM appusage
		WHERE appuser_id  = v_APPUSER_ID
		AND app = inapp
	INTO v_APPUSAGE_ID;	
			

INSERT IGNORE INTO callhome(appusage_id, appuser_id, email, clientdatetime, serverdatetime, 
	lat, lng, accuracy, locationdatetime, `module`, app)
VALUES(v_APPUSAGE_ID, v_APPUSER_ID, inemailid, inclientdatetime ,NOW(), 
	inlatno, inlngno, inacc, inlocationdatetime, inmodule, inapp);

SELECT v_APPUSER_ID as appuser_id;

END$$
DELIMITER;
call v17_add_callhome('tommyvercetti1950@gmail.com',"865645023256965","19.10611","72.84423","0.8771229","gps","40420","armani","Xiaomi",NULL,NULL,"2016-05-26 17:18:23","175.100.181.240","Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36","52",NULL,"T","2016-05-26 17:18:20","TR_R_15_59")