DROP PROCEDURE IF EXISTS get_issue_timeline;
DELIMITER$$
CREATE  PROCEDURE `get_issue_timeline`()

BEGIN
SELECT 
 i.issue_id, issue_time, vehicle_no,  ward,  lat, lng,
issue_item_description,  issue_sub_type_description,  issue_type_description, d.department_code, d.department_description,
formatted_address,  locality, sublocality, area,  route
FROM issue i
LEFT JOIN address a
	ON a.address_id = i.address_id
LEFT JOIN postal_code p
ON p.postal_code = a.postal_code
	LEFT JOIN issue_lookup_items  ilt
	ON i.issue_item_code = ilt.issue_item_code
LEFT JOIN issue_lookup_subtypes ils
	ON ilt.issue_sub_type_code = ils.issue_sub_type_code
LEFT JOIN issue_lookup_types it
	ON it.issue_type_code = ils.issue_type_code
LEFT JOIN  department d
	ON d.department_code = it.department_code	
WHERE i.submit_report = 1
AND (formatted_address IS NOT NULL OR (lat IS NOT NULL AND lng IS NOT NULL))
ORDER BY issue_time DESC LIMIT 10;

 
END$$
DELIMITER ;

CALL get_issue_timeline;