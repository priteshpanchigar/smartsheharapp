DROP PROCEDURE IF EXISTS v17_smartshehar_summary;
DELIMITER$$
CREATE  PROCEDURE `v17_smartshehar_summary`(IN inappuserid INT, IN inlat DOUBLE, IN inlng DOUBLE ,
 IN inradious INT,IN indate date, IN inward VARCHAR(50))

BEGIN
DECLARE v_open_issues INT DEFAULT 0;
DECLARE v_resolved_issues INT DEFAULT 0;
DECLARE v_traffic_issues INT DEFAULT 0;
DECLARE v_municipal_issues INT DEFAULT 0;
DECLARE v_contributors INT DEFAULT 0;
DECLARE vMyIssues INT DEFAULT 0;
DECLARE vOpenNearBy INT DEFAULT 0;
DECLARE vMuNearBy INT DEFAULT 0;
DECLARE vTrNearBy INT DEFAULT 0;
DECLARE v_resolved_tr_issues INT DEFAULT 0;
DECLARE v_resolved_mu_issues INT DEFAULT 0;

	SET @AppUserID = inappuserid;
	SET @Lat = inlat;
	SET @Lng = inlng;
	SET @rad = inradious;
	
	

	
	SELECT COUNT(i.issue_id) INTO v_open_issues FROM issue i
	INNER JOIN issue_lookup_items item ON item.issue_item_code = i.issue_item_code
	INNER JOIN issue_lookup_subtypes ist ON ist.issue_sub_type_code = item.issue_sub_type_code
	INNER JOIN issue_lookup_types it ON it.issue_type_code = ist.issue_type_code
	INNER JOIN department d ON d.department_code = it.department_code
	where submit_report = 1 AND
	rejected = 0 AND
	resolved = 0 
	AND closed = 0
	AND d.department_code IS NOT NULL;

	SELECT COUNT(i.issue_id) INTO v_traffic_issues FROM issue i
	INNER JOIN issue_lookup_items item ON item.issue_item_code = i.issue_item_code
	INNER JOIN issue_lookup_subtypes ist ON ist.issue_sub_type_code = item.issue_sub_type_code
	INNER JOIN issue_lookup_types it ON it.issue_type_code = ist.issue_type_code
	INNER JOIN department d ON d.department_code = it.department_code
	where submit_report = 1 AND
	rejected = 0 AND
	resolved = 0 
	AND closed = 0
	AND d.department_code ='TR';

	SELECT COUNT(i.issue_id) INTO v_municipal_issues FROM issue i
	INNER JOIN issue_lookup_items item ON item.issue_item_code = i.issue_item_code
	INNER JOIN issue_lookup_subtypes ist ON ist.issue_sub_type_code = item.issue_sub_type_code
	INNER JOIN issue_lookup_types it ON it.issue_type_code = ist.issue_type_code
	INNER JOIN department d ON d.department_code = it.department_code
	where submit_report = 1 AND
	rejected = 0 AND
	resolved = 0 
	AND closed = 0
	AND d.department_code ='MU';

	SELECT COUNT(i.issue_id) INTO v_resolved_issues FROM issue i
	INNER JOIN issue_lookup_items item ON item.issue_item_code = i.issue_item_code
	INNER JOIN issue_lookup_subtypes ist ON ist.issue_sub_type_code = item.issue_sub_type_code
	INNER JOIN issue_lookup_types it ON it.issue_type_code = ist.issue_type_code
	INNER JOIN department d ON d.department_code = it.department_code
	INNER JOIN address a ON a.address_id = i.address_id
	where submit_report = 1 
	AND i.resolved = 1 
	AND i.appuser_id = @AppUserID
	AND getDistanceBetweenPoints(@Lat, @Lng, a.latitude, a.longitude)<= inradious
	AND d.department_code IS NOT NULL;

  SELECT COUNT(i.issue_id) INTO v_resolved_tr_issues FROM issue i
	INNER JOIN issue_lookup_items item ON item.issue_item_code = i.issue_item_code
	INNER JOIN issue_lookup_subtypes ist ON ist.issue_sub_type_code = item.issue_sub_type_code
	INNER JOIN issue_lookup_types it ON it.issue_type_code = ist.issue_type_code
	INNER JOIN department d ON d.department_code = it.department_code
	INNER JOIN address a ON a.address_id = i.address_id
	where submit_report = 1 AND
	i.resolved = 1 
	AND d.department_code = 'TR'
	AND i.resolved_unresolved_datetime > DATE_SUB(indate, INTERVAL 1 MONTH)
  AND getDistanceBetweenPoints(@Lat, @Lng, a.latitude, a.longitude)<= inradious;

	SELECT COUNT(i.issue_id) INTO v_resolved_mu_issues FROM issue i
	INNER JOIN issue_lookup_items item ON item.issue_item_code = i.issue_item_code
	INNER JOIN issue_lookup_subtypes ist ON ist.issue_sub_type_code = item.issue_sub_type_code
	INNER JOIN issue_lookup_types it ON it.issue_type_code = ist.issue_type_code
	INNER JOIN department d ON d.department_code = it.department_code
	INNER JOIN address a ON a.address_id = i.address_id
	where submit_report = 1 AND
	i.resolved = 1 
	AND d.department_code = 'MU'
	AND a.ward = inward
	AND i.resolved_unresolved_datetime > DATE_SUB(indate, INTERVAL 1 MONTH)
	AND getDistanceBetweenPoints(@Lat, @Lng, a.latitude, a.longitude)<= inradious;


	IF @AppUserID IS NOT NULL THEN
		SELECT COUNT(i.issue_id) INTO vMyIssues FROM issue i
			INNER JOIN issue_lookup_items item ON item.issue_item_code = i.issue_item_code
			INNER JOIN issue_lookup_subtypes ist ON ist.issue_sub_type_code = item.issue_sub_type_code
			INNER JOIN issue_lookup_types it ON it.issue_type_code = ist.issue_type_code
			INNER JOIN department d ON d.department_code = it.department_code
			INNER JOIN address a ON a.address_id = i.address_id
			where submit_report = 1 
				AND rejected = 0 
				AND resolved = 0
				AND closed = 0
				AND i.appuser_id = @AppUserID
				AND getDistanceBetweenPoints(@Lat, @Lng, a.latitude, a.longitude)<= inradious;
	
	END IF;
	
	IF @Lat IS NOT NULL AND @Lng IS NOT NULL THEN
		SELECT COUNT(i.issue_id) INTO vOpenNearBy FROM issue i
			INNER JOIN issue_lookup_items item ON item.issue_item_code = i.issue_item_code
			INNER JOIN issue_lookup_subtypes ist ON ist.issue_sub_type_code = item.issue_sub_type_code
			INNER JOIN issue_lookup_types it ON it.issue_type_code = ist.issue_type_code
			INNER JOIN department d ON d.department_code = it.department_code
			INNER JOIN address a ON a.address_id = i.address_id
			where submit_report = 1 
				AND rejected = 0 
				AND resolved = 0 AND closed = 0
				AND getDistanceBetweenPoints(@Lat, @Lng, a.latitude, a.longitude)<= inradious;
	END IF;		
	
	IF @Lat IS NOT NULL AND @Lng IS NOT NULL THEN
		SELECT COUNT(i.issue_id) INTO vMuNearBy FROM issue i
			INNER JOIN issue_lookup_items item ON item.issue_item_code = i.issue_item_code
			INNER JOIN issue_lookup_subtypes ist ON ist.issue_sub_type_code = item.issue_sub_type_code
			INNER JOIN issue_lookup_types it ON it.issue_type_code = ist.issue_type_code
			INNER JOIN department d ON d.department_code = it.department_code
			INNER JOIN address a ON a.address_id = i.address_id
			where submit_report = 1 
				AND rejected = 0 
				AND resolved = 0 AND closed = 0
				AND a.ward = inward
				#AND getDistanceBetweenPoints(@Lat, @Lng, a.latitude, a.longitude)<= inradious
				AND d.department_code = 'MU';
	END IF;		
	
	IF @Lat IS NOT NULL AND @Lng IS NOT NULL THEN
		SELECT COUNT(i.issue_id) INTO vTrNearBy FROM issue i
			INNER JOIN issue_lookup_items item ON item.issue_item_code = i.issue_item_code
			INNER JOIN issue_lookup_subtypes ist ON ist.issue_sub_type_code = item.issue_sub_type_code
			INNER JOIN issue_lookup_types it ON it.issue_type_code = ist.issue_type_code
			INNER JOIN department d ON d.department_code = it.department_code
			INNER JOIN address a ON a.address_id = i.address_id
			WHERE submit_report = 1 
				AND rejected = 0 
				AND resolved = 0 AND closed = 0
				AND getDistanceBetweenPoints(@Lat, @Lng, a.latitude, a.longitude)<= inradious
				AND d.department_code = 'TR';
	END IF;		
	
	SELECT COUNT(DISTINCT(appuser_id)) INTO v_contributors FROM issue
	where submit_report = 1;

	SELECT v_open_issues,v_resolved_mu_issues, v_resolved_tr_issues, v_resolved_issues,v_traffic_issues,v_municipal_issues, vOpenNearBy, vTrNearBy, vMuNearBy, 
	vMyIssues, v_contributors;

END$$
DELIMITER ;

call v17_smartshehar_summary(813,19.1228504,72.8333937,3,'2017-02-25','K/W')   