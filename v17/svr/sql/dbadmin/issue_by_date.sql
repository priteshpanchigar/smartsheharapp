DROP PROCEDURE IF EXISTS issue_by_date;
DELIMITER$$
CREATE  PROCEDURE `issue_by_date`()

BEGIN

	SELECT DATE(issue_time) date, COUNT(*) cnt, SUM(IF(department_code = 'MU', 1, 0)) AS mu_cnt,
 SUM(IF(department_code = 'TR', 1, 0)) AS tr_cnt  FROM vw_issue_with_lookup
GROUP BY DATE(issue_time);

 
END$$
DELIMITER ;

call issue_by_date;