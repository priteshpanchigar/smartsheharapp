// Modified By Sanjay 19-09-2013 1:24 PM.
/*global $, oCity, dist, tripTimeSecs, dttm, goTrip, getFare, estimate, PUBLICVEHICLES, PRIVATEVEHICLES, console */
function displayFare(oCity, dist, tripTimeSecs, dttm) {
	var TRANSPORT_PRIVATE = 'V', TRANSPORT_PUBLIC = 'U',
		html = '', htmlBtn = '', i, ctr = 1, aoFare, oVehicle, city, oFare,
		len = oCity.aoVehicles.length, previousTransport = '', delim = '';
	for (i = 0; i < len; i = i + 1) {
		oVehicle = oCity.aoVehicles[i];
		city = oCity.city;
		oFare = getFare(oVehicle, dist, tripTimeSecs, dttm);
		if (i === 0) {
			if (oVehicle.transport === TRANSPORT_PUBLIC) {
				html = '<b>' + PUBLICVEHICLES + '&nbsp;:&nbsp;' + '</b>';
			} else {
				html = '<b>' + PRIVATEVEHICLES + '&nbsp;:&nbsp;' + '</b>';
			}
			delim = '&nbsp;';
		}
		if (i > 0) {
			if (oVehicle.transport === previousTransport) {
				delim = ',&nbsp;';
			} else {
				delim = '&nbsp;';
				html = html + '<br/>' + '<hr>' + '<b>' + PRIVATEVEHICLES + '&nbsp;:&nbsp;' + '</b>';
			}
		}
		html = html + delim + oFare.vehicle + ' <img src="images/' +
			oCity.currency + '" style="padding: 5px; vertical-align:text-top;">' +
			oFare.fare;
		previousTransport = oVehicle.transport;
	}
	if (oFare.fare > 0) {
		$('#vehiclefare').html(html);
		htmlBtn = htmlBtn + '<div class="styledButton" a href="#" onclick="window.history.back();" style="margin-top:5px;">' + 'Done' + '</div>';
		$('#sharevehl').html(htmlBtn);
		$('#fare').show();
	}
}
function afterPlotDirections(oCity, goTrip) {
	$('#fare').hide();
	$('#ridePanel').hide();
	displayFare(oCity, (goTrip.distance / 1000), goTrip.tripTimeSecs, new Date());
}
function initModule() {
	$('#fare').hide();
	$('#ridePanel').hide();
	console.log("Loaded estimatedfare.js");
	afterPlotDirections(oCity, goTrip);
	//displayFare(oCity, (goTrip.distance / 1000), goTrip.tripTimeSecs, new Date());
}
//@ sourceURL=estimatedfare.js
