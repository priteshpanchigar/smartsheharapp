// Modified By Sanjay 19-09-2013 1:24 PM.
/*global $, oCity, dist, tripTimeSecs, dttm, goTrip */
function getFare(oVehicle, dist, tripTimeSecs, dttm) {
	var len, vehType = '', aoFare = [], oFare, transport = '',
		city, fare;
	vehType = oVehicle.vehicletypedescription;
	if (oVehicle.transport === 'PR') {
		transport = 'Private Vehicles';
	} else {
		transport = 'Public Vehicles';
	}
	fare = Math.ceil(dist < parseInt(oVehicle.minimumDistance, 10) / 1000 ? oVehicle.minimumFare :
			dist * oVehicle.farePerKm);
	goTrip.vehicle = oVehicle.vehicleType;
	goTrip.distance = dist;
	goTrip.fare = fare;
	oFare = {vehicle: vehType, fare: fare, currency: "Rs.", transport: transport};
		//aoFare.push({vehicle: vehType, fare: fare, currency: "Rs.", transport: transport});
	//}
	//return aoFare;
	return oFare;
}
