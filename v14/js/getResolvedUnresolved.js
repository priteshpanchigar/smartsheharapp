var displayData;
var selectWardNo;
var displayData;
var selectIssues;
var imgOne;
var imgTwo;
var issueType;
var issueDate;
var issueAddress;
var txtCmnt;
var radionResolved;
var radioUnresolution;
var wardNo;
var issueCat;
var date;
var resolveUnresolvedDateTime='';
var resolveFlag;
var unresolutionFlag;
var submissionDate;
var offencedetailid;
var comment;
var submissionDate;
var imgSrcOne='';
var imgSrcTwo='';

function loadPage(){	
		
		displayData = document.getElementById("displayData");
		selectWardNo = document.getElementById("selectWardNo");
		selectIssues = document.getElementById("selectIssues");
		imgOne = document.getElementById("imgOne");
		imgTwo = document.getElementById("imgTwo");
		txtCmnt = document.getElementById("txtCmnt");
		radionResolved = document.getElementById("radionResolved");
		issueType = document.getElementById("issueType");
		issueDate = document.getElementById("issueDate");
		issueAddress = document.getElementById("issueAddress");
		submissionDate = document.getElementById("submissionDate");
		radioUnresolution = document.getElementById("unresolution");
		getWards();
}
function getWards()
{
		var wardUrl = getSmartSheharPhpScriptsPath() + 'offence/get_ward.php';
    //   unapprovedOffenceUrl
    $.ajax({
        type: 'GET',
        url: wardUrl,
        dataType: 'html',
        success: function(data) {
             var arr = JSON.parse(data);
            
		   var option;
		   option = document.createElement("option");
			option.value= "select";
			option.text = "Select your Wards";
				selectWardNo.add(option);
		    for (i = 0; i < arr.length; i++) {
				option = document.createElement("option");
				option.id = arr[i].ward;
				option.value= arr[i].ward;
				option.text = arr[i].ward;
				selectWardNo.add(option);
			 }
			 option.value=""
			  option.text = "Other"; 
			selectWardNo.add(option);
        },
        error: function(xhr, textStatus, errorThrown) {
            var err = textStatus + ', ' + errorThrown;
        },
        async: false
    });
}
function gotWardnIssueCat()
{
	
	
	wardNo = document.getElementById("selectWardNo").value;
	issueCat = document.getElementById("selectIssues").value;
	if(wardNo!="select"&& issueCat!="")
	{
		getIssues();
	}
	else{
		displayData.style.visibility="hidden";
		imgTwo.style.visibility="hidden";
		
	}
}

function getIssues()
{
	var unResolvedUrl = getSmartSheharPhpScriptsPath() + 'offence/get_resolution_status.php?ward='+wardNo+"&complainttypecode="+issueCat;
    //   unapprovedOffenceUrl
    $.ajax({
        type: 'GET',
        url: unResolvedUrl,
        dataType: 'html',
        success: function(data) {
           displayIssues(data)
        },
        error: function(xhr, textStatus, errorThrown) {
            var err = textStatus + ', ' + errorThrown;
        },
        async: false
    });
}
function displayIssues(data)
{
	imgSrcOne='';
	imgSrcTwo='';
	resolveFlag=0;
	unresolutionFlag = 0;	
	displayData.style.visibility="visible";
	radionResolved.checked=false;
	radioUnResolution.checked=false;
	var arrIssue =  JSON.parse(data);
	var count = arrIssue.count;
	comment='';
	offencedetailid='';
	txtCmnt.value=comment;
	if(count == '0')
	{
		displayData.style.visibility="hidden";
		imgTwo.style.visibility="hidden";
	}
	 for (k= 0; k < arrIssue.length; k++) 
	 {
		 if(k==0)
		 {
			 imgTwo.style.visibility="hidden";
			 imgOne.src = getSmartSheharPhpScriptsPath() + "offence/images/" + arrIssue[k].offence_image_name;
			 imgSrcOne=getSmartSheharPhpScriptsPath() + "offence/images/" + arrIssue[k].offence_image_name;
		 }
		 if(k==1)
		 {
			  imgTwo.style.visibility="visible";
			  imgTwo.src= getSmartSheharPhpScriptsPath() + "offence/images/" + arrIssue[k].offence_image_name;
			   imgSrcTwo=getSmartSheharPhpScriptsPath() + "offence/images/" + arrIssue[k].offence_image_name;
		 }
		 offencedetailid = +arrIssue[k].offence_detail_id;
		 issueType.innerHTML ='Issue: '+arrIssue[k].offence_type;
		 issueDate.innerHTML ='Date: '+arrIssue[k].offence_time;
		 issueAddress.innerHTML ='Address: '+arrIssue[k].offence_address;
		 submissionDate.innerHTML = 'Submitted on: '+arrIssue[k].approved_rejected_clientdatetime;
		
	 }
}
function resolved()
{
	approvedRejectDateTime='';
	resolveFlag='1';
	unresolutionFlag='0';
	date = new Date();
	resolveUnresolvedDateTime = date.getFullYear()  + "-"
                + (date.getMonth()+1)  + "-" 
                + date.getDate() + " "  
                + date.getHours() + ":"  
                + date.getMinutes() + ":" 
                + date.getSeconds();
	
		
}
function radioUnResolution()
{
	approvedRejectDateTime='';
	resolveFlag='0';
	unresolutionFlag='1';
	date = new Date();
	resolveUnresolvedDateTime = date.getFullYear()  + "-"
                + (date.getMonth()+1)  + "-" 
                + date.getDate() + " "  
                + date.getHours() + ":"  
                + date.getMinutes() + ":" 
                + date.getSeconds();
	
		
}
function getComment()
{
	comment  = txtCmnt.value;	
}
function onSubmit()
{
		if(resolveFlag=='0'&& unresolutionFlag=='0') 
		{
			alert("Please select radio button")
		}
		else{
			if(comment=='')
			{
				alert("Please add a comment");
			}
			else{
				var resolveUrl = getSmartSheharPhpScriptsPath() + 'offence/update_resolved_unresolved.php?offencedetailid=' + offencedetailid+'&resolved='+resolveFlag+'&unresolved='+unresolutionFlag+'&unresolvedcomment='+comment+'&clientdatetime='+resolveUnresolvedDateTime;
					$.ajax({
						type: 'POST',
						url: resolveUrl,
						dataType: 'html',
						success: function(data) {
						if(resolveFlag == '1' && unresolutionFlag=='0')
						{
								resolveMail();
						}
						if(resolveFlag == '0' && unresolutionFlag=='1')
						{
							//unresolveMail();
						}
						getIssues();
						},
						error: function(xhr, textStatus, errorThrown) {
							var err = textStatus + ', ' + errorThrown;
						},
						async: false
					});
			}
			
		}
}
function resolveMail()
{
	var resolveMailUrl = getSmartSheharPhpScriptsPath() + 'offence/resolved_issue_email.php?offencedetailid=' + offencedetailid;
    
					$.ajax({
					type: 'POST',
					url: resolveMailUrl,
					dataType: 'html',
					success: function(data) {
						
					},
					error: function(xhr, textStatus, errorThrown) {
					var err = textStatus + ', ' + errorThrown;
					},
					async: false
					});
}
function openImgOne()
{
	var myWindow = window.open("http://www.smartshehar.com/alpha/smartsheharapp/v14/imagezooming.html");
    myWindow.document.write("<img src='"+imgSrcOne+"'/>");
}
function openImgTwo()
{
	var myWindow = window.open("http://www.smartshehar.com/alpha/smartsheharapp/v14/imagezooming.html");
    myWindow.document.write("<img src='"+imgSrcTwo+"'/>");
}
