/*global $, document, getTrainPhpScriptsPath, city, gup */
/*global console */
function getMegaBlock(dt, tm, successCallBack) {
	var getMegaBlockurl	= getTrainPhpScriptsPath +
		't_megablock.php?dt=' + dt + '&tm=' + tm;
	$.ajax({
	    type: 'GET',
	    url: getMegaBlockurl,
	    dataType: 'json',
	    success: function (data) {
	        successCallBack(data);
	    },
	    error: function (xhr, textStatus, errorThrown) {
	        var err = textStatus + ', ' + errorThrown;
	    },
		async: false
	});
}
function displayMegaBlock(dt, tm) {
	var len, i, row, html = '', blocksOn = "",
		megablockHtml = '', speed = '', dirn = '', delim = '',
		fromDtTm, fromDt, toDtTm, toDt;
	getMegaBlock(dt, tm, function (aoMegaBlock) {
		len = aoMegaBlock.length;
		for (i = 0; i < len; i = i + 1) {
			row = aoMegaBlock[i];
			if (blocksOn.indexOf(row.linecode) === -1) {
				blocksOn = blocksOn + delim + row.linecode;
			}
			delim = ", ";
			if (row.up === 1) {
				dirn = 'up';
			} else {
				dirn = 'down';
			}
			if (row.slow === 1) {
				speed = 'Slow';
			} else {
				speed = 'Fast';
			}
			fromDtTm = new Date(row.fromdatetime);
			fromDt = new Date(row.fromdatetime);
			fromDt.setHours(0, 0, 0, 0);
			toDtTm = new Date(row.todatetime);
			toDt = new Date(row.todatetime);
			toDt.setHours(0, 0, 0, 0);
			console.log(fromDt);
			console.log(toDt);
			console.log(fromDt.valueOf() === toDt.valueOf());
			if (row.message) {
				megablockHtml = megablockHtml +
					"<div><b>" + row.linecode + "</b> - " +
					row.message + "</div><a href='" + row.link + "'> (More ..)</a></div><hr>";
				continue;
			}
			
			megablockHtml = megablockHtml + "<div><b>" + row.linecode +
				"</b> - <b>" + fromDtTm.dayNameShort() + ", " + fromDtTm.getDate() + 
				" " + fromDtTm.monthNameShort() +  "</b> at <b>" + fromDtTm.AMPM() +
				"</b> to <b>" + (fromDt.valueOf() !== toDt.valueOf() ? toDtTm.dayNameShort()  + ", " + toDtTm.getDate() +  " " +  toDtTm.monthNameShort() + " - " : "")
				+  toDtTm.AMPM() +
				"</b> from <b>" + row.fromstation + "</b> to <b>" + row.tostation + "</b>" +
				(row.up > 0 && row.down > 0 ? " (both directions)" :
						(row.up > 0 ? " (up)" :
								(row.down > 0 ? " (down)" : ""))) +
				(row.slow > 0 && row.fast > 0 ? ",  on <b>Slow & Fast </b>lines" :
						(row.slow > 0 ? " on <b>Slow</b> line only" :
								(row.fast > 0 ? ", on <b>Fast</b> Line only " : ""))) +
				"</div>" + "<a href='" + row.link + "'> (More ..)</a></div><hr>";
		}
		if (len > 0) {
			megablockHtml = "<div> Mega block on: <b>" + blocksOn + "</b></div><hr>" + megablockHtml;
			$('#megablock').html(megablockHtml + "</div>");
		} else {
			$('#megablock').html('No Mega block information at this time');
		}
	});
}
$(document).ready(function () {
	var dt = gup('dt'), tm = gup('tm'), today = new Date(),
		dd = today.getDate(), mm = today.getMonth() + 1, //January is 0!
		yyyy = today.getFullYear(), currentDate = new Date();
	if (dt) {
		displayMegaBlock(dt, tm);
	} else {
		dt = currentDate.yyyymmdd();
		//yyyy + '-' + mm + '-' + dd;
		tm = currentDate.getHours() + ":"
			+ currentDate.getMinutes() + ":"
			+ currentDate.getSeconds();
		displayMegaBlock(dt, tm);
	}
}); // document.ready
