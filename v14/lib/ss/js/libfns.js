// lib functions - can be used in other sites 
// not specific to this site
/*global $, jQuery, document, console, google, escape, onDeviceReady, window, handleNoGeoLocation, unescape, navigator */
"use strict";
var app = false;
var windowsApp = false;
var phpScriptsPath;
var trainAppVer;
var busAppVer;
var delhiMetro;

function removejscssfile(filename, filetype) {
	var targetelement = (filetype === "js") ? "script" : (filetype === "css") ? "link" : "none", //determine element type to create nodelist from
		targetattr = (filetype === "js") ? "src" : (filetype === "css") ? "href" : "none", //determine corresponding attribute to test for
		allsuspects = document.getElementsByTagName(targetelement),
		i;
	for (i = allsuspects.length; i >= 0; i = i - 1) { //search backwards within nodelist for matching elements to remove
		if (allsuspects[i] && allsuspects[i].getAttribute(targetattr) !== null && allsuspects[i].getAttribute(targetattr).indexOf(filename) !== -1) {
			allsuspects[i].parentNode.removeChild(allsuspects[i]); //remove element by calling parentNode.removeChild()
		}
	}
}

// Load js or css file dynamicallly
function loadjscssfile(filename, filetype) {
	var fileref = null;
	if (filetype === "js") { //if filename is a external JavaScript file
		fileref = document.createElement('script');
		fileref.setAttribute("type", "text/javascript");
		fileref.setAttribute("src", filename);
	} else if (filetype === "css") { //if filename is an external CSS file
		fileref = document.createElement("link");
		fileref.setAttribute("rel", "stylesheet");
		fileref.setAttribute("type", "text/css");
		fileref.setAttribute("href", filename);
	}
	if (fileref !== "undefined") {
		document.getElementsByTagName("head")[0].appendChild(fileref);
	}
}

// Split the url and get the params

function gup(name) {
	name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
	var regexS = "[\\?&]" + name + "=([^&#]*)",
		regex = new RegExp(regexS),
		results = regex.exec(window.location.href);
	if (results) {
		return results[1].replace(/%20/g, ' ');
	}
	return null;
}

function handleNoGeoLocation(errorFlag) {
	var initialLocation;
    if (errorFlag === true) {
//     alert("Geolocation service failed.");
		initialLocation = null;
    } else {
//      alert("Your browser doesn't support geolocation");
		initialLocation = null;
    }
//    map.setCenter(initialLocation);
}

function getLocation(successCallBack, failureCallBack) {
	var gps, options, geo, watch, browserSupportFlag, trackerId;
	gps = navigator.geolocation;
	options = {timeout: 60000, maximumAge: 330000};
	if (gps) {
		browserSupportFlag = true;
		navigator.geolocation.getCurrentPosition(function (position) {
			if (successCallBack) {
				successCallBack(position);
			}
		}, function () {
			handleNoGeoLocation(browserSupportFlag);
		});
	// Try Google Gears Geolocation
	} else if (google.gears) {
		browserSupportFlag = true;
		geo = google.gears.factory.create('beta.geolocation');
		geo.getCurrentPosition(function (position) {
			if (successCallBack) {
				successCallBack(position);
			}
		}, function () {
			handleNoGeoLocation(browserSupportFlag);
		});
	// Browser doesn't support Geolocation
	} else {
		browserSupportFlag = false;
		handleNoGeoLocation(browserSupportFlag);
	}

	if (gps !== undefined) {
		trackerId = gps.watchPosition(function (position) {
			if (successCallBack) {
				successCallBack(position);
			}
		}, failureCallBack, options);
	} else if (geo !== undefined) {
		watch = geo.watchPosition(function (position) {
			if (successCallBack) {
				successCallBack(position);
			}
		}, failureCallBack, options);
	}
}

function geoErrorHandler(err) {

/*  if(err.code == 1) {
    dBug("geo:", "Error: Access is denied!", 5);
  }else if( err.code == 2) {
*/
	console.log('Position error' + err);
//    dBug("geo", "Error: Position is unavailable!", 5);
//  }

}

function setCookie(c_name, value, exdays) {
	var c_value, exdate = new Date();
	exdate.setDate(exdate.getDate() + exdays);
	c_value = escape(value) + ((exdays === null) ? "" : "; expires=" + exdate.toUTCString());
	document.cookie = c_name + "=" + c_value;
}

function getCookie(c_name) {
	var i, x, y, ARRcookies = document.cookie.split(";");
	for (i = 0; i < ARRcookies.length; i = i + 1) {
		x = ARRcookies[i].substr(0, ARRcookies[i].indexOf("="));
		y = ARRcookies[i].substr(ARRcookies[i].indexOf("=") + 1);
		x = x.replace(/^\s+|\s+$/g, "");
		if (x === c_name) {
			return unescape(y);
		}
	}
	return '';
}

// distance between 2 lat, lon pairs
function distanceLatLon(lat1, lon1, lat2, lon2) {
	var R = 6371, dLat, dLon, a, c, d;
	dLat = (lat2 - lat1) * Math.PI / 180;
	dLon = (lon2 - lon1) * Math.PI / 180;
	a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
			Math.cos(lat1 * Math.PI / 180) * Math.cos(lat2 * Math.PI / 180) *
			Math.sin(dLon / 2) * Math.sin(dLon / 2);
	c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
	d = R * c;
	return Math.ceil(d * 1000);	// return distance in meters
}

var DIGITHEIGHT = 28,
	DIGITWIDTH = 16.5,
	DIGITIMAGE = '../lib/ss/images/digits_small.gif',
	DECIMAL = '../lib/ss/images/decimal_small.gif',
	IMAGETRANSPARENT = '"img_trans.gif';

function getDigitalHtml(num) {
	var hr = parseInt(num, 10),
		sHr = (hr < 10 ? '0' : '') + hr,
		min = (num * 100) % 100,
		sMin = (min < 10 ? '0' : '') + min,
		digit1 = parseInt(sHr.substring(0, 1), 10),
		digit2 = parseInt(sHr.substring(1, 2), 10),
		digit3 = parseInt(sMin.substring(0, 1), 10),
		digit4 = parseInt(sMin.substring(1, 2), 10),
		fixedLeft = '<img style="width:' + DIGITWIDTH + 'px;height:' + DIGITHEIGHT +
			'px;background:url(' + DIGITIMAGE + ') -',
		fixedRight = 'px 0"' + 'src="' + IMAGETRANSPARENT + '" width="1" height="1" />',
		html = '<img style="width:' + DIGITWIDTH + 'px;height:' + DIGITHEIGHT +
			'px;background:url(' + DIGITIMAGE + ') -' + DIGITWIDTH * digit1 + 'px 0"' +
			'src="' + IMAGETRANSPARENT + '" width="1" height="1" />';
	html += fixedLeft + DIGITWIDTH * digit2 + fixedRight;
	html += '<img src="' + DECIMAL + '"/>';
	html += fixedLeft + DIGITWIDTH * digit3 + fixedRight;
	html += fixedLeft + DIGITWIDTH * digit4 + fixedRight;
	return html;
}
function shownum() {
	document.getElementById('num').innerHTML = getDigitalHtml(12.34);

}
function getAddress(lat, lon, successCallBack) {
	var addrUrl = 'http://maps.googleapis.com/maps/api/geocode/json?latlng=' +
			lat + ',' + lon + '&sensor=true_or_false';
	$.ajax({
	    type: 'GET',
	    url: addrUrl,
	    dataType: 'json',
	    success: function (data) {
	        successCallBack(data);
	    },
	    error: function (xhr, textStatus, errorThrown) {
	        console.log(textStatus + ', ' + errorThrown);
	    },
	    async: false
	});
}
// get the hour month of a time in hh:ss format 
function getTimeHHSS(dttm) {
	var hh = dttm.getHours(),
		mm = dttm.getMinutes();
		if (mm < 10) {
			mm = '0' + mm;
		}	
	return hh + ':' + mm;
}
function isTouchDevice() {
	try {
		document.createEvent("TouchEvent");
		return true;
	} catch (e) {
		return false;
	}
}
function touchScroll(id) {
	if (isTouchDevice()) { //if touch events exist...
		var /* el = document.getElementById(id), */ scrollStartPos = 0;
		document.getElementById(id).addEventListener("touchstart", function (event) {
			scrollStartPos = this.scrollTop + event.touches[0].pageY;
			event.preventDefault();
		}, false);
		document.getElementById(id).addEventListener("touchmove", function (event) {
			this.scrollTop = scrollStartPos - event.touches[0].pageY;
			event.preventDefault();
		}, false);
	}
}

function checkDeviceReady() {
    window.isphone = false;
    if (document.URL.indexOf("http://") === -1) {
        window.isphone = true;
    }
	onDeviceReady();
/*	
    if (window.isphone) {
		document.addEventListener("deviceready", function () {
			alert('deviceReady');
			onDeviceReady();
		}, true);
    } else {
        onDeviceReady();
    }
*/	
}
function mySqlDate(dateString) {
	var t = dateString.split(/[- :]/),
		d = new Date(t[0], t[1] - 1, t[2], t[3], t[4], t[5]);
	return d;
}