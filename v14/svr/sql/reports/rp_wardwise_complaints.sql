DROP PROCEDURE IF EXISTS `rp_wardwise_complaints`;
DELIMITER $$
CREATE  PROCEDURE `rp_wardwise_complaints`()
BEGIN


SELECT COUNT(*)total, ct.ward,
(SELECT complaint_sub_type_description FROM complaint_lookup_subtypes ct2 
WHERE ct2.complaint_sub_type_code = od.complaint_sub_type_code) complaint_type_description,
(SELECT complaint_description FROM complaint_lookup_types ct2 
WHERE ct2.complaint_type_code = od.complaint_type_code) complaint_sub_type_description
FROM offence_details od
INNER JOIN  municipal_ward ct
ON od.ward = ct.ward
GROUP BY ct.ward,od.complaint_sub_type_code
ORDER BY COUNT(*) DESC;

END$$ 
DELIMITER;

CALL rp_wardwise_complaints()