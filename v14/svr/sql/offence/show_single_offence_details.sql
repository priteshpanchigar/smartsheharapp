DROP PROCEDURE IF EXISTS show_single_offence_details;
DELIMITER$$
CREATE  PROCEDURE `show_single_offence_details`(IN inoffencedetail int)
BEGIN
	
SELECT offence_detail_id, offence_address, offence_time, client_datetime, submit_report, offence_type,
vehicle_no,  unique_key, appuser_id, offence_image_id, offence_images, offence_videos, offence_image_name,
creation_datetime, offence_image_path 
	FROM vw_offence
	WHERE offence_detail_id = inoffencedetail   
	AND offence_image_name != "";

END$$
DELIMITER ;