DROP PROCEDURE IF EXISTS update_closed_opened;
DELIMITER$$
CREATE  PROCEDURE `update_closed_opened`(IN inclosed INT, IN inopened INT, 
IN inoffencedetailid INT,IN inclosedopenedcomment  VARCHAR(300),
IN inclientdatetime datetime,IN inappuserid INT)
BEGIN

UPDATE offence_details
SET closed = inclosed, 
opened = inopened,
closed_opened_datetime = inclientdatetime,
closed_opened_comment  = inclosedopenedcomment,
closed_opened_by = inappuserid 
WHERE offence_detail_id = inoffencedetailid;

select closed, opened, closed_opened_comment , closed_opened_by from offence_details
WHERE offence_detail_id = inoffencedetailid;



END$$
DELIMITER;

call update_closed_opened(0,1,2,"asdasdasdsad", "2015-10-14 10:48:15");