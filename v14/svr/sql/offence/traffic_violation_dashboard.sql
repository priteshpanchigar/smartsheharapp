DROP PROCEDURE IF EXISTS traffic_violation_dashboard;
DELIMITER$$
CREATE  PROCEDURE `traffic_violation_dashboard`(IN `inclientdatetime` datetime)
BEGIN

DECLARE v_total_today_violation INT DEFAULT 0;
DECLARE v_city_violation INT DEFAULT 0;
DECLARE v_postal_code_violation INT DEFAULT 0;
DECLARE v_type_violation INT DEFAULT 0;
DECLARE v_locality VARCHAR(50);



SELECT COUNT(*) INTO v_total_today_violation FROM offence_details
WHERE DATE(offence_time) = DATE(inclientdatetime);

SELECT COUNT(*) INTO v_locality from vw_offence_address
WHERE DATE(offence_time) = DATE(inclientdatetime)
group by locality;

SELECT COUNT(DISTINCT(offence_type)) INTO  v_type_violation FROM vw_offence_address;


SELECT v_total_today_violation,v_locality, v_type_violation;

END$$
DELIMITER ;