DROP PROCEDURE IF EXISTS add_group_details;
DELIMITER $$
CREATE PROCEDURE `add_group_details`(IN inappuserid INT, IN ingroupname varchar(255),
IN ingrouptype varchar(255),IN ingrouparea varchar(255),IN ingroupdescription varchar(255), 
IN inpincode INT, IN ingroupphoneno INT(15), IN ingroupemail varchar(255))
BEGIN
	
INSERT INTO groupdetails(appuser_id, group_name, group_type, group_area, group_description, 
pincode, group_phoneno, group_email)
VALUES(inappuserid,ingroupname, ingrouptype,ingrouparea, ingroupdescription, 
inpincode, ingroupphoneno, ingroupemail);

SELECT LAST_INSERT_ID() as group_id;

END$$
DELIMITER;