
DROP PROCEDURE IF EXISTS update_notification_flag;
DELIMITER $$
CREATE  PROCEDURE `update_notification_flag`(IN `inflagvalue` INT, IN `inappuserid` INT)
BEGIN

UPDATE appuser SET show_notification = inflagvalue 
WHERE appuser_id = inappuserid


END$$
DELIMITER ;

call update_notification_flag(1,2)