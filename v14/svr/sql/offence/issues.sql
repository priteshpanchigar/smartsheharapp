DROP PROCEDURE IF EXISTS issues;
DELIMITER$$
CREATE  PROCEDURE `issues`()
BEGIN

SELECT offence_detail_id, offence_address, offence_time, client_datetime, offence_type, vehicle_no, unique_key, submit_report, discard_report,
address_id, location_id, ward, complaint_type_code, complaint_sub_type_code, colour, android_colour, municipal_department, approved, rejected,
resolved, unresolved, unresolved_comment, reason_code, sent_to_authority, lat, lng, appuser_id, email, offence_image_id, offence_video_path,
offence_video_name, offence_image_name, offence_image_path, creation_datetime FROM vw_offence
GROUP BY offence_detail_id;
END;$$
DELIMITER ;

CALL issues();