DROP PROCEDURE IF EXISTS get_unapproved_single_offence;
DELIMITER$$
CREATE  PROCEDURE `get_unapproved_single_offence`(IN inward VARCHAR(20), 
IN incomplainttypecode VARCHAR(20))
BEGIN

IF inward IS NULL THEN
			SELECT od.offence_detail_id, offence_address, offence_time, client_datetime, offence_type, 
			vehicle_no, unique_key, submit_report, approved,rejected, 
			(SELECT reject_reason FROM rejected_reason 
				WHERE od.reason_code = rejected_reason.reason_code) reject_reason,
			sent_to_authority, approved_rejected_clientdatetime,  sent_to_authority_datetime,  
			resolved_unresolved_datetime, 	other_reason_code_comment,
			address_id, location_id, complaint_type_code, complaint_sub_type_code,lat, lng,
			offence_image_name, offence_image_path, od.appuser_id FROM offence_details od
			LEFT JOIN offence_image oi on od.offence_detail_id = oi.offence_detail_id
			WHERE od.offence_detail_id = (SELECT offence_detail_id FROM offence_details 
			WHERE submit_report = 1 
			AND sent_to_authority = 0
			AND rejected = 0
			AND complaint_type_code = incomplainttypecode 
			AND ward = 'I don\'t know' )
			ORDER BY od.offence_detail_id limit 1 ;

ELSE

			SELECT od.offence_detail_id, offence_address, offence_time, client_datetime, offence_type, 
			vehicle_no, unique_key, submit_report, approved,rejected, 
			(SELECT reject_reason FROM rejected_reason 
				WHERE od.reason_code = rejected_reason.reason_code) reject_reason,
			sent_to_authority, approved_rejected_clientdatetime,  sent_to_authority_datetime,  
			resolved_unresolved_datetime, 	other_reason_code_comment,
			address_id, location_id, complaint_type_code, complaint_sub_type_code,lat, lng,
			offence_image_name, offence_image_path, od.appuser_id FROM offence_details od
			LEFT JOIN offence_image oi on od.offence_detail_id = oi.offence_detail_id
			WHERE od.offence_detail_id = (SELECT offence_detail_id FROM offence_details 
			WHERE submit_report = 1 
			AND sent_to_authority = 0
			AND rejected = 0
			AND complaint_type_code = incomplainttypecode 
			AND ward = inward )
			ORDER BY od.offence_detail_id limit 1;
END IF;


END$$
DELIMITER;

CALL get_unapproved_single_offence('K/W','TR_VPO' );
