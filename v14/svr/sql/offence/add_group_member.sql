DROP PROCEDURE IF EXISTS add_group_member;
DELIMITER $$
CREATE PROCEDURE `add_group_member`(IN inappuserid INT, IN ingroupname varchar(255),
IN inmembercontact varchar(100),IN inmemberemail varchar(100),IN inmembername varchar(255), 
IN ingroupid INT)
BEGIN
	
INSERT INTO group_members(appuser_id, group_name, member_contactno, 
member_emailid, member_name, group_id)
VALUES(inappuserid,ingroupname, inmembercontact,inmemberemail, inmembername, 
ingroupid);

SELECT LAST_INSERT_ID() as group_member_id;

END$$
DELIMITER;
