
DROP PROCEDURE IF EXISTS add_offence_details;
CREATE  PROCEDURE `add_offence_details`(IN inoffenceaddress varchar(255),  IN inoffencedatetime datetime, 
IN inclientdatetime datetime,  IN inoffencetype varchar(255),  IN invehicleno varchar(255), IN inuniquekey varchar(255), 
IN insubmitreport int(11), IN inlocality varchar(255), IN insublocality varchar(255),IN inpostal_code varchar(255), 
IN inroute varchar(255), IN inneighborhood varchar(255), IN inadministrative_area_level_2 varchar(255), 
IN inadministrative_area_level_1 varchar(255), IN  inlatitude double, IN inlongitude double, IN inappuserid int(11),
IN inward varchar(255),IN incomplaint_sub_type_code varchar(255),IN inemailid varchar(40),
IN inacc double,IN inlocationdatetime datetime, IN inprovider varchar(100), IN inspeed float, IN inbearing float,
IN inaltitude double, IN indescard_report int(11),IN inmlaid int(11),IN inmpid int(11), IN ingroupid INT(11))
BEGIN
	
DECLARE vaddress_id int;
DECLARE v_location_id INT DEFAULT 0;

SELECT address_id INTO vaddress_id from address
where formatted_address = inoffenceaddress LIMIT 1;

IF (vaddress_id IS NULL) THEN
		INSERT INTO address(formatted_address, locality, sublocality,postal_code,route,
			neighborhood,administrative_area_level_2,administrative_area_level_1,
			client_datetime,latitude,longitude)
		VALUES(inoffenceaddress,inlocality, insublocality, inpostal_code, inroute,
			inneighborhood,inadministrative_area_level_2, inadministrative_area_level_1,
		inclientdatetime, inlatitude, inlongitude);
 SELECT LAST_INSERT_ID() INTO vaddress_id from address;
ELSE
	UPDATE address
		SET locality = inlocality, sublocality = insublocality, postal_code = inpostal_code,
				route = inroute,  neighborhood = inneighborhood, administrative_area_level_2 = inadministrative_area_level_2,
				administrative_area_level_1 = inadministrative_area_level_1
					WHERE formatted_address = inoffenceaddress;
END IF;

SET v_location_id = insert_location(inappuserid, inemailid, 1, 0, 
			inclientdatetime, inlatitude , inlongitude, inacc, inlocationdatetime,
			inprovider, inspeed, inbearing, inaltitude);



INSERT INTO offence_details(offence_address, offence_time, client_datetime,offence_type, 
	vehicle_no,unique_key,submit_report, address_id,appuser_id,lat,lng,complaint_type_code,
	complaint_sub_type_code, location_id,discard_report,ward, group_id, mla_id, mp_id)
VALUES (inoffenceaddress, inoffencedatetime ,inclientdatetime, inoffencetype, 
	invehicleno, inuniquekey, insubmitreport, vaddress_id, inappuserid,inlatitude,inlongitude,
	(SELECT complaint_type_code FROM complaint_lookup_subtypes 
		WHERE complaint_sub_type_code = incomplaint_sub_type_code),
	incomplaint_sub_type_code, v_location_id,indescard_report,inward, ingroupid,inmlaid, inmpid)
	ON DUPLICATE KEY UPDATE	
	offence_address = inoffenceaddress,
	offence_time = inoffencedatetime,
	offence_type = inoffencetype,
	submit_report = insubmitreport,
  address_id = vaddress_id,
  discard_report = indescard_report,
 ward= inward,
 mla_id = inmlaid,
 group_id = ingroupid,
mp_id = inmpid,
	complaint_sub_type_code =incomplaint_sub_type_code,

complaint_type_code = (SELECT complaint_type_code FROM complaint_lookup_subtypes 
		WHERE complaint_sub_type_code = incomplaint_sub_type_code);


SELECT  offence_detail_id from offence_details
WHERE unique_key = inuniquekey;



END;

call add_offence_details('add new update',"2015-06-01 18:29:02", "2015-06-01 18:29:16",
'Parking in No Parking Zone',NULL,143316343697,'0', "Mumbai", "Vile Parle East", "400057",
"Hanuman Hedit Marg No 2",NULL,NULL, "Maharashtra","19.1045566", "72.8505842",1,2)