DROP PROCEDURE IF EXISTS delete_letterhead_info;
DELIMITER$$
CREATE  PROCEDURE `delete_letterhead_info`(IN inoffencedetailid INT)
BEGIN

UPDATE offence_details
SET letter_client_date_time = NULL, letter_image_name =  NULL,
letter_image_path = NULL, letter_upload_notification = 0
WHERE offence_detail_id = inoffencedetailid;



END$$
DELIMITER;

call delete_letterhead_info(1);