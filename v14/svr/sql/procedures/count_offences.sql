
DROP PROCEDURE IF EXISTS count_offences;
CREATE  PROCEDURE `count_offences`(IN ingetcode varchar(50))
BEGIN

SELECT COUNT(*) total_offence
FROM offence_details
WHERE complaint_type_code = ingetcode;


END;

CALL count_offences('MU_CLNN');