DROP PROCEDURE IF EXISTS update_appuser_detail;
DELIMITER $$
CREATE PROCEDURE `update_appuser_detail`(IN inappuserid INT, IN inusername varchar(50), 
IN infullname varchar(80), IN inage INT, IN insex varchar(10), IN inphoneno varchar(20))
BEGIN
	
UPDATE appuser
SET username = inusername, fullname = infullname, age = inage, sex = insex, phoneno = inphoneno 
WHERE appuser_id = inappuserid;


SELECT 1 AS result;

END$$
DELIMITER ;