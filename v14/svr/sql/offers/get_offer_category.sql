DROP PROCEDURE IF EXISTS get_offer_category;
DELIMITER $$
CREATE PROCEDURE `get_offer_category`( IN `inlat` DOUBLE, IN `inlng` DOUBLE)
BEGIN

SET  @inDistance = 3;
SELECT offer_id, offer_category_id vendor_id, offers, offer_category_description, offer_category_name
FROM vw_offer
where getDistanceBetweenPoints(latitude,longitude,inlat ,inlng)< @inDistance;

END$$
DELIMITER ;