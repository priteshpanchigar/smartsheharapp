DROP VIEW IF EXISTS vw_complaint_category;

CREATE VIEW  vw_complaint_category AS
SELECT complaint_lookup_subtype_id, complaint_sub_type_code, ct.complaint_description, cs.complaint_sub_type_description, 
ct.complaint_type_code,
complaint_lookup_id
FROM complaint_lookup_types ct 
LEFT OUTER JOIN complaint_lookup_subtypes cs
ON ct.complaint_type_code = cs.complaint_type_code
ORDER BY ct.complaint_type_code ASC;