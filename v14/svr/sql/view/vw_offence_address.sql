DROP VIEW IF EXISTS vw_offence_address;

CREATE VIEW  vw_offence_address AS
SELECT `od`.`offence_detail_id` AS `offence_detail_id`, 
       `od`.`offence_address`   AS `offence_address`, 
       `od`.`offence_time`      AS `offence_time`, 
       `od`.`client_datetime`   AS `client_datetime`, 
       `od`.`submit_report`     AS `submit_report`, 
       `od`.`offence_type`      AS `offence_type`, 
       `od`.`vehicle_no`        AS `vehicle_no`, 
       `od`.`unique_key`        AS `unique_key`, 
       `ad`.`address_id`        AS `address_id`, 
       `ad`.`locality`          AS `locality`, 
       `ad`.`sublocality`       AS `sublocality`, 
       `ad`.`postal_code`       AS `postal_code` 
FROM   (`offence_details` `od` 
        JOIN `address` `ad` 
          ON(( `od`.`address_id` = `ad`.`address_id` )));