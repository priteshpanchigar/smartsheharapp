DROP VIEW IF EXISTS vw_ward;

CREATE VIEW vw_ward as
SELECT a.city, b.mp, c.mla,c.mla_age,c.mla_Constituency, c.mla_email,c.mla_gender, c.mla_phoneno, c.political_Affiliations, 
d.ward, a.city_id,b.mp_id,  c.mla_id,  d.ward_id
FROM municipal_city a
INNER JOIN muncipal_mp b ON
a.city_id = b.city_id
INNER JOIN municipal_mla c ON
b.mp_id = c.mp_id
INNER JOIN municipal_ward d ON
d.mla_id = c.mla_id;

SELECT * FROM vw_ward;