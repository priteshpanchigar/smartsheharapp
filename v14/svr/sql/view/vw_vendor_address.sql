DROP VIEW IF EXISTS vw_vendor_address;
CREATE VIEW vw_vendor_address AS
SELECT  v.vendor_id,  vendor_datetime, vendor_name, vendor_address_id, vendor_email,
vendor_phoneno,formatted_address, locality, sublocality, postal_code, route, neighborhood, administrative_area_level_2,
administrative_area_level_1, client_datetime, latitude, longitude FROM address a
INNER JOIN l_vendor v
ON a.address_id = v.vendor_address_id;
