SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for address
-- ----------------------------
DROP TABLE IF EXISTS `address`;
CREATE TABLE `address` (
  `address_id` int(11) NOT NULL AUTO_INCREMENT,
  `formatted_address` varchar(255) NOT NULL,
  `locality` varchar(255) DEFAULT NULL,
  `sublocality` varchar(255) DEFAULT NULL,
  `postal_code` varchar(255) DEFAULT NULL,
  `route` varchar(255) DEFAULT NULL,
  `neighborhood` varchar(255) DEFAULT NULL,
  `administrative_area_level_2` varchar(255) DEFAULT NULL,
  `administrative_area_level_1` varchar(255) DEFAULT NULL,
  `client_datetime` datetime DEFAULT NULL,
  `latitude` double DEFAULT NULL,
  `longitude` double DEFAULT NULL,
  PRIMARY KEY (`address_id`),
  UNIQUE KEY `od_formatted_address` (`formatted_address`) USING BTREE
) ENGINE=InnoDB;

-- ----------------------------
-- Table structure for appusage
-- ----------------------------
DROP TABLE IF EXISTS `appusage`;
CREATE TABLE `appusage` (
  `appusage_id` int(11) NOT NULL AUTO_INCREMENT,
  `appuser_id` int(11) NOT NULL DEFAULT '0',
  `first_location_id` int(11) DEFAULT NULL,
  `last_location_id` int(11) DEFAULT NULL,
  `clientfirstaccessdatetime` datetime DEFAULT NULL,
  `clientlastaccessdatetime` datetime DEFAULT NULL,
  `serverfirstaccessdatetime` datetime DEFAULT NULL,
  `serverlastaccessdatetime` datetime DEFAULT NULL,
  `previousversioncode` int(4) DEFAULT NULL,
  `versioncode` int(4) DEFAULT NULL,
  `version` varchar(25) DEFAULT NULL,
  `app` varchar(5) NOT NULL DEFAULT '',
  `usertype` varchar(2) DEFAULT NULL,
  PRIMARY KEY (`appusage_id`),
  UNIQUE KEY `appuser_id` (`appuser_id`,`app`)
) ENGINE=InnoDB;

-- ----------------------------
-- Table structure for appuser
-- ----------------------------
DROP TABLE IF EXISTS `appuser`;
CREATE TABLE `appuser` (
  `appuser_id` int(11) NOT NULL AUTO_INCREMENT,
  `gcm_registration_id` varchar(500) DEFAULT NULL,
  `email` varchar(40) NOT NULL DEFAULT '',
  `phoneno` varchar(20) DEFAULT NULL,
  `clientlastaccessdatetime` datetime DEFAULT NULL,
  `first_location_id` int(11) DEFAULT NULL,
  `last_location_id` int(11) DEFAULT NULL,
  `imei` varchar(50) NOT NULL DEFAULT '',
  `lat` double DEFAULT NULL,
  `lng` double DEFAULT NULL,
  `accuracy` double DEFAULT NULL,
  `provider` varchar(10) DEFAULT NULL,
  `carrier` varchar(50) DEFAULT NULL,
  `product` varchar(50) DEFAULT NULL,
  `manufacturer` varchar(50) DEFAULT NULL,
  `android_release_version` varchar(20) DEFAULT NULL,
  `android_sdk_version` int(5) DEFAULT NULL,
  `ip` varchar(255) DEFAULT NULL,
  `useragent` varchar(1000) DEFAULT NULL,
  `username` varchar(50) DEFAULT NULL,
  `fullname` varchar(80) DEFAULT NULL,
  `age` int(5) DEFAULT NULL,
  `sex` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`appuser_id`),
  UNIQUE KEY `dashboad__appuser_email` (`email`) USING BTREE
) ENGINE=InnoDB;

-- ----------------------------
-- Table structure for city_authority_emailids
-- ----------------------------
DROP TABLE IF EXISTS `city_authority_emailids`;
CREATE TABLE `city_authority_emailids` (
  `city_authority_emailids_id` int(11) NOT NULL AUTO_INCREMENT,
  `city` varchar(255) DEFAULT NULL,
  `email_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`city_authority_emailids_id`)
) ENGINE=InnoDB;

-- ----------------------------
-- Table structure for city_pincode_authority_emailids
-- ----------------------------
DROP TABLE IF EXISTS `city_pincode_authority_emailids`;
CREATE TABLE `city_pincode_authority_emailids` (
  `city_pincode_authority_emailids_id` int(11) NOT NULL AUTO_INCREMENT,
  `city` varchar(50) DEFAULT NULL,
  `postal_code` varchar(6) DEFAULT NULL,
  `email_id` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`city_pincode_authority_emailids_id`)
) ENGINE=InnoDB;

-- ----------------------------
-- Table structure for complaint_lookup_subtypes
-- ----------------------------
DROP TABLE IF EXISTS `complaint_lookup_subtypes`;
CREATE TABLE `complaint_lookup_subtypes` (
  `complaint_lookup_subtype_id` int(11) NOT NULL AUTO_INCREMENT,
  `complaint_sub_type_code` varchar(255) DEFAULT NULL,
  `complaint_sub_type_description` varchar(255) DEFAULT NULL,
  `complaint_type_code` varchar(255) DEFAULT NULL,
  `department_code` varchar(255) DEFAULT NULL,
  `complaint_type` varchar(255) DEFAULT NULL,
  `complaint_sub_type` varchar(255) DEFAULT NULL,
  `municipal_department` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`complaint_lookup_subtype_id`)
) ENGINE=InnoDB;

-- ----------------------------
-- Table structure for complaint_lookup_types
-- ----------------------------
DROP TABLE IF EXISTS `complaint_lookup_types`;
CREATE TABLE `complaint_lookup_types` (
  `complaint_lookup_id` int(11) NOT NULL AUTO_INCREMENT,
  `complaint_type_code` varchar(255) DEFAULT NULL,
  `complaint_description` varchar(255) DEFAULT NULL,
  `department_code` varchar(255) DEFAULT NULL,
  `complaint_type` varchar(255) DEFAULT NULL,
  `colour` varchar(255) DEFAULT NULL,
  `android_colour` int(255) DEFAULT '0',
  PRIMARY KEY (`complaint_lookup_id`)
) ENGINE=InnoDB;

-- ----------------------------
-- Table structure for department
-- ----------------------------
DROP TABLE IF EXISTS `department`;
CREATE TABLE `department` (
  `department_id` int(11) NOT NULL AUTO_INCREMENT,
  `department_code` varchar(255) DEFAULT NULL,
  `department_description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`department_id`)
) ENGINE=InnoDB;

-- ----------------------------
-- Table structure for group_members
-- ----------------------------
DROP TABLE IF EXISTS `group_members`;
CREATE TABLE `group_members` (
  `group_member_id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `appuser_id` int(11) NOT NULL,
  `member_name` varchar(255) DEFAULT NULL,
  `member_emailid` varchar(100) DEFAULT NULL,
  `member_contactno` varchar(100) DEFAULT NULL,
  `group_name` varchar(255) NOT NULL,
  PRIMARY KEY (`group_member_id`)
) ENGINE=InnoDB;

-- ----------------------------
-- Table structure for groupdetails
-- ----------------------------
DROP TABLE IF EXISTS `groupdetails`;
CREATE TABLE `groupdetails` (
  `group_id` int(11) NOT NULL AUTO_INCREMENT,
  `group_name` varchar(255) NOT NULL,
  `group_type` varchar(255) DEFAULT NULL,
  `group_area` varchar(255) NOT NULL,
  `group_description` varchar(255) DEFAULT NULL,
  `appuser_id` int(11) DEFAULT NULL,
  `pincode` int(11) DEFAULT NULL,
  `group_phoneno` int(15) DEFAULT NULL,
  `group_email` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`group_id`)
) ENGINE=InnoDB;

-- ----------------------------
-- Table structure for l_offer
-- ----------------------------
DROP TABLE IF EXISTS `l_offer`;
CREATE TABLE `l_offer` (
  `offer_id` int(11) NOT NULL AUTO_INCREMENT,
  `offer_category_id` int(11) DEFAULT NULL,
  `vendor_id` int(11) DEFAULT NULL,
  `offers` varchar(255) DEFAULT NULL,
  `offer_description` varchar(255) DEFAULT NULL,
  `offer_creationdate` datetime DEFAULT NULL,
  `actual_price` int(11) DEFAULT NULL,
  `discount_percentage` varchar(255) DEFAULT NULL,
  `discount_amount` int(255) DEFAULT NULL,
  `offer_starttime` datetime DEFAULT NULL,
  `offer_endtime` datetime DEFAULT NULL,
  `comment` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`offer_id`)
) ENGINE=InnoDB;

-- ----------------------------
-- Table structure for l_offer_category
-- ----------------------------
DROP TABLE IF EXISTS `l_offer_category`;
CREATE TABLE `l_offer_category` (
  `offer_category_id` int(11) NOT NULL AUTO_INCREMENT,
  `offer_category_name` varchar(500) DEFAULT NULL,
  `offer_category_description` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`offer_category_id`)
) ENGINE=InnoDB;

-- ----------------------------
-- Table structure for l_offer_image
-- ----------------------------
DROP TABLE IF EXISTS `l_offer_image`;
CREATE TABLE `l_offer_image` (
  `offer_image_id` int(11) NOT NULL AUTO_INCREMENT,
  `offer_image_name` varchar(255) DEFAULT NULL,
  `offer_image_path` varchar(255) DEFAULT NULL,
  `offer_image_datetime` datetime DEFAULT NULL,
  `offer_id` int(11) DEFAULT NULL,
  `offer_category_id` int(11) DEFAULT NULL,
  `vendor_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`offer_image_id`)
) ENGINE=InnoDB;

-- ----------------------------
-- Table structure for l_vendor
-- ----------------------------
DROP TABLE IF EXISTS `l_vendor`;
CREATE TABLE `l_vendor` (
  `vendor_id` int(11) NOT NULL AUTO_INCREMENT,
  `appuser_id` int(11) DEFAULT NULL,
  `vendor_datetime` datetime DEFAULT NULL,
  `vendor_name` varchar(255) DEFAULT NULL,
  `vendor_address_id` int(11) DEFAULT NULL,
  `vendor_email` varchar(255) DEFAULT NULL,
  `vendor_phoneno` int(15) DEFAULT NULL,
  PRIMARY KEY (`vendor_id`)
) ENGINE=InnoDB;

-- ----------------------------
-- Table structure for location
-- ----------------------------
DROP TABLE IF EXISTS `location`;
CREATE TABLE `location` (
  `location_id` int(11) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `appuser_id` int(11) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `active` int(1) DEFAULT NULL,
  `isdriver` int(1) DEFAULT NULL,
  `ispassenger` int(1) DEFAULT NULL,
  `iscommercial` int(1) DEFAULT NULL,
  `isuser` int(1) DEFAULT NULL,
  `lat` double DEFAULT NULL,
  `lng` double DEFAULT NULL,
  `accuracy` int(11) DEFAULT NULL,
  `altitude` double DEFAULT NULL,
  `bearing` float DEFAULT NULL,
  `address` varchar(100) DEFAULT NULL,
  `firsttime` datetime DEFAULT NULL,
  `loctime` datetime DEFAULT NULL,
  `clientaccessdatetime` datetime DEFAULT NULL,
  `speed` float DEFAULT NULL,
  `cumulative_distance` decimal(5,3) DEFAULT NULL,
  `provider` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`location_id`)
) ENGINE=InnoDB;

-- ----------------------------
-- Table structure for muncipal_mp
-- ----------------------------
DROP TABLE IF EXISTS `muncipal_mp`;
CREATE TABLE `muncipal_mp` (
  `mp_id` int(11) NOT NULL AUTO_INCREMENT,
  `mp` varchar(255) DEFAULT NULL,
  `city_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`mp_id`)
) ENGINE=InnoDB;

-- ----------------------------
-- Table structure for municipal_city
-- ----------------------------
DROP TABLE IF EXISTS `municipal_city`;
CREATE TABLE `municipal_city` (
  `city_id` int(11) NOT NULL AUTO_INCREMENT,
  `city` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`city_id`)
) ENGINE=InnoDB;

-- ----------------------------
-- Table structure for municipal_mla
-- ----------------------------
DROP TABLE IF EXISTS `municipal_mla`;
CREATE TABLE `municipal_mla` (
  `mla_id` int(11) NOT NULL AUTO_INCREMENT,
  `mla` varchar(255) DEFAULT NULL,
  `mla_phoneno` varchar(15) DEFAULT NULL,
  `mla_email` varchar(255) DEFAULT NULL,
  `mla_age` int(5) DEFAULT NULL,
  `mla_gender` varchar(10) DEFAULT NULL,
  `mla_Constituency` varchar(255) DEFAULT NULL,
  `political_Affiliations` varchar(255) DEFAULT NULL,
  `mp_id` int(11) DEFAULT NULL,
  `city_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`mla_id`)
) ENGINE=InnoDB;

-- ----------------------------
-- Table structure for municipal_ward
-- ----------------------------
DROP TABLE IF EXISTS `municipal_ward`;
CREATE TABLE `municipal_ward` (
  `ward_id` int(11) NOT NULL AUTO_INCREMENT,
  `ward` varchar(255) DEFAULT NULL,
  `mla` varchar(255) DEFAULT NULL,
  `mla_id` int(11) DEFAULT NULL,
  `mp_id` int(11) DEFAULT NULL,
  `city_id` int(11) DEFAULT NULL,
  `ward_email_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ward_id`)
) ENGINE=InnoDB;

-- ----------------------------
-- Table structure for municipal_ward_copy
-- ----------------------------
DROP TABLE IF EXISTS `municipal_ward_copy`;
CREATE TABLE `municipal_ward_copy` (
  `ward_id` int(11) NOT NULL DEFAULT '0',
  `ward` varchar(255) DEFAULT NULL,
  `mla` varchar(255) DEFAULT NULL,
  `mla_id` int(11) DEFAULT NULL,
  `mp_id` int(11) DEFAULT NULL,
  `city_id` int(11) DEFAULT NULL,
  `ward_email_id` varchar(255) DEFAULT NULL
) ENGINE=InnoDB;

-- ----------------------------
-- Table structure for offence_details
-- ----------------------------
DROP TABLE IF EXISTS `offence_details`;
CREATE TABLE `offence_details` (
  `offence_detail_id` int(255) NOT NULL AUTO_INCREMENT,
  `offence_address` varchar(255) DEFAULT NULL,
  `offence_time` datetime DEFAULT NULL,
  `client_datetime` datetime DEFAULT NULL,
  `offence_type` varchar(255) DEFAULT NULL,
  `vehicle_no` varchar(15) DEFAULT NULL,
  `unique_key` varchar(255) NOT NULL,
  `submit_report` int(11) NOT NULL DEFAULT '0',
  `approved` int(11) NOT NULL DEFAULT '0',
  `rejected` int(11) NOT NULL DEFAULT '0',
  `approved_rejected_clientdatetime` datetime DEFAULT NULL,
  `reason_code` varchar(255) DEFAULT NULL,
  `other_reason_code_comment` varchar(255) DEFAULT NULL,
  `to_be_sent_to_authority` int(11) NOT NULL DEFAULT '0',
  `sent_to_authority` int(11) NOT NULL DEFAULT '0',
  `sent_to_authority_datetime` datetime DEFAULT NULL,
  `resolved` int(11) NOT NULL DEFAULT '0',
  `unresolved` int(11) NOT NULL DEFAULT '0',
  `resolved_unresolved_datetime` datetime DEFAULT NULL,
  `unresolved_comment` varchar(300) DEFAULT NULL,
  `address_id` int(11) NOT NULL,
  `location_id` int(11) DEFAULT NULL,
  `ward` varchar(255) DEFAULT NULL,
  `group_id` int(11) DEFAULT NULL,
  `complaint_type_code` varchar(255) DEFAULT NULL,
  `complaint_sub_type_code` varchar(255) DEFAULT NULL,
  `mla_id` int(5) DEFAULT NULL,
  `mp_id` int(5) DEFAULT NULL,
  `lat` double DEFAULT NULL,
  `lng` double DEFAULT NULL,
  `appuser_id` int(11) DEFAULT NULL,
  `discard_report` int(11) DEFAULT '0',
  `letter_submit_by` int(11) DEFAULT NULL,
  `email_sent_notification` int(11) DEFAULT NULL,
  `letter_submitted_to` varchar(255) DEFAULT '',
  `letter_submitted_dept` varchar(255) DEFAULT NULL,
  `letter_submitted_desg` varchar(255) DEFAULT NULL,
  `letter_submitted_datetime` datetime DEFAULT NULL,
  `letter_remark` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`offence_detail_id`),
  UNIQUE KEY `od_unique_key` (`unique_key`) USING BTREE
) ENGINE=InnoDB;

-- ----------------------------
-- Table structure for offence_image
-- ----------------------------
DROP TABLE IF EXISTS `offence_image`;
CREATE TABLE `offence_image` (
  `appuser_id` int(11) NOT NULL,
  `offence_image_id` int(11) NOT NULL AUTO_INCREMENT,
  `offence_detail_id` int(11) NOT NULL,
  `offence_video_path` varchar(255) DEFAULT NULL,
  `offence_video_name` varchar(255) DEFAULT NULL,
  `offence_image_name` varchar(255) DEFAULT NULL,
  `offence_image_path` varchar(255) CHARACTER SET latin7 DEFAULT NULL,
  `creation_datetime` datetime DEFAULT NULL,
  `letter_datetime` datetime DEFAULT NULL,
  `letter_image_name` varchar(255) DEFAULT NULL,
  `letter_image_path` varchar(255) DEFAULT NULL,
  `letter_submited` int(11) DEFAULT NULL,
  PRIMARY KEY (`offence_image_id`),
  UNIQUE KEY `unique_creation_datetime` (`creation_datetime`) USING BTREE
) ENGINE=InnoDB;

-- ----------------------------
-- Table structure for rejected_reason
-- ----------------------------
DROP TABLE IF EXISTS `rejected_reason`;
CREATE TABLE `rejected_reason` (
  `rejected_id` int(11) NOT NULL AUTO_INCREMENT,
  `reject_reason` varchar(255) DEFAULT NULL,
  `reason_code` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`rejected_id`)
) ENGINE=InnoDB;

-- ----------------------------
-- Table structure for rs_cityinfo
-- ----------------------------
DROP TABLE IF EXISTS `rs_cityinfo`;
CREATE TABLE `rs_cityinfo` (
  `city_id` int(11) NOT NULL,
  `city_code` longtext,
  `city` longtext,
  `country` longtext,
  `distance_unit` longtext,
  `currency` longtext,
  `centerlat` double DEFAULT NULL,
  `centerlon` double DEFAULT NULL,
  `citytoplat` double DEFAULT NULL,
  `citytoplon` double DEFAULT NULL,
  `citybottomlat` double DEFAULT NULL,
  `citybottomlon` double DEFAULT NULL,
  PRIMARY KEY (`city_id`)
) ENGINE=InnoDB;

-- ----------------------------
-- Table structure for rs_farecalculation
-- ----------------------------
DROP TABLE IF EXISTS `rs_farecalculation`;
CREATE TABLE `rs_farecalculation` (
  `_id` int(11) NOT NULL AUTO_INCREMENT,
  `srno` int(11) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `transport` varchar(255) DEFAULT NULL,
  `vehicle_type` varchar(255) DEFAULT NULL,
  `item` varchar(255) DEFAULT NULL,
  `calculation` varchar(255) DEFAULT NULL,
  `style` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`_id`)
) ENGINE=InnoDB;

-- ----------------------------
-- Table structure for rs_lookup
-- ----------------------------
DROP TABLE IF EXISTS `rs_lookup`;
CREATE TABLE `rs_lookup` (
  `field_id` int(11) NOT NULL,
  `fieldname` longtext,
  `code` longtext,
  `description` longtext,
  PRIMARY KEY (`field_id`)
) ENGINE=InnoDB;

-- ----------------------------
-- Table structure for t_megablock
-- ----------------------------
DROP TABLE IF EXISTS `t_megablock`;
CREATE TABLE `t_megablock` (
  `_id` int(11) NOT NULL AUTO_INCREMENT,
  `linecode` varchar(11) DEFAULT NULL,
  `up` int(1) DEFAULT NULL,
  `down` int(1) DEFAULT NULL,
  `slow` int(1) DEFAULT NULL,
  `fast` int(1) DEFAULT NULL,
  `fromdatetime` datetime DEFAULT NULL,
  `todatetime` datetime DEFAULT NULL,
  `fromstation` varchar(20) DEFAULT NULL,
  `tostation` varchar(20) DEFAULT NULL,
  `fromstation_id` int(3) DEFAULT NULL,
  `tostation_id` int(3) DEFAULT NULL,
  `fromstationcode` varchar(11) DEFAULT NULL,
  `tostationcode` varchar(11) DEFAULT NULL,
  `display` longtext,
  `link` longtext,
  `message` longtext,
  `repeatdays` int(2) DEFAULT '0',
  PRIMARY KEY (`_id`)
) ENGINE=InnoDB;

-- ----------------------------
-- Table structure for vehiclefareparameter
-- ----------------------------
DROP TABLE IF EXISTS `vehiclefareparameter`;
CREATE TABLE `vehiclefareparameter` (
  `vehiclefareparameter_id` int(11) NOT NULL,
  `serialno` int(11) DEFAULT NULL,
  `date` longtext,
  `city` longtext,
  `city_id` int(11) DEFAULT NULL,
  `vehicletype` longtext,
  `transport` longtext,
  `vehiclecapacity` int(11) DEFAULT NULL,
  `minimumdistance` double DEFAULT NULL,
  `minimumfare` double DEFAULT NULL,
  `fareperkm` double DEFAULT NULL,
  `metermovesperkm` double DEFAULT NULL,
  `meterrounding` double DEFAULT NULL,
  `minimumwaitingminutes` int(11) DEFAULT NULL,
  `waitchargeperhour` int(11) DEFAULT NULL,
  `waitingspeed` int(11) DEFAULT NULL,
  `nightextra` double DEFAULT NULL,
  `luggagechargeperpiece` double DEFAULT NULL,
  `nightstart` double DEFAULT NULL,
  `nightend` double DEFAULT NULL,
  `minimumnightfare` double DEFAULT NULL,
  `metertype` longtext,
  `farepercentincreaseperpassenger` int(11) DEFAULT NULL,
  `courtesy` longtext,
  `link` longtext,
  `vehicletypedescription` varchar(255) DEFAULT NULL,
  `transportdescription` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`vehiclefareparameter_id`)
) ENGINE=InnoDB;

-- ----------------------------
-- Table structure for walkwithme_contacts
-- ----------------------------
DROP TABLE IF EXISTS `walkwithme_contacts`;
CREATE TABLE `walkwithme_contacts` (
  `_id` int(11) NOT NULL AUTO_INCREMENT,
  `w_contact_user_emailid` varchar(1000) DEFAULT NULL,
  `w_contact_name` varchar(1000) DEFAULT NULL,
  `w_contact_emailid` varchar(1000) DEFAULT NULL,
  `w_creationdatetime` datetime NOT NULL,
  PRIMARY KEY (`_id`)
) ENGINE=InnoDB;

-- ----------------------------
-- Table structure for walkwithmelocupd
-- ----------------------------
DROP TABLE IF EXISTS `walkwithmelocupd`;
CREATE TABLE `walkwithmelocupd` (
  `_id` int(11) NOT NULL AUTO_INCREMENT,
  `fk_w_c_id` int(11) DEFAULT NULL,
  `user_emailid` varchar(100) DEFAULT NULL,
  `imei` varchar(255) DEFAULT NULL,
  `datetm` datetime DEFAULT NULL,
  `lat` double DEFAULT NULL,
  `lon` double DEFAULT NULL,
  `accuracy` double DEFAULT NULL,
  `serverdatetime` datetime DEFAULT NULL,
  `intrip` bit(1) DEFAULT NULL,
  `mode` varchar(5) DEFAULT NULL,
  PRIMARY KEY (`_id`)
) ENGINE=InnoDB;

-- ----------------------------
-- Table structure for walkwithmepermissions
-- ----------------------------
DROP TABLE IF EXISTS `walkwithmepermissions`;
CREATE TABLE `walkwithmepermissions` (
  `w_c_id` int(11) NOT NULL AUTO_INCREMENT,
  `w_c_user_emailid` varchar(100) DEFAULT NULL,
  `randomstring` varchar(1000) DEFAULT NULL,
  `mode` varchar(255) DEFAULT NULL,
  `stopflag` varchar(1) DEFAULT NULL,
  `intrip` bit(1) DEFAULT NULL,
  `user_link` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`w_c_id`)
) ENGINE=InnoDB;

-- ----------------------------
-- Table structure for walkwithmepermissionsstop
-- ----------------------------
DROP TABLE IF EXISTS `walkwithmepermissionsstop`;
CREATE TABLE `walkwithmepermissionsstop` (
  `_id` int(11) NOT NULL AUTO_INCREMENT,
  `w_s_user_emailid` varchar(100) DEFAULT NULL,
  `u_randomstring` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`_id`)
) ENGINE=InnoDB;

-- ----------------------------
-- Table structure for ws_photos
-- ----------------------------
DROP TABLE IF EXISTS `ws_photos`;
CREATE TABLE `ws_photos` (
  `image_id` int(11) NOT NULL AUTO_INCREMENT,
  `lat` double DEFAULT NULL,
  `lon` double DEFAULT NULL,
  `address` varchar(300) DEFAULT NULL,
  `imagefilename` varchar(100) DEFAULT NULL,
  `phoneno` varchar(30) DEFAULT NULL,
  `photodate` datetime DEFAULT NULL,
  `imei` varchar(30) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `to_email` varchar(100) DEFAULT NULL,
  `from_email` varchar(100) DEFAULT NULL,
  `cc_email` varchar(100) DEFAULT NULL,
  `serverdatetime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`image_id`)
) ENGINE=InnoDB;