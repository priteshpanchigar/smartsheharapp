#TRANSPORSE SCHEDULE#
DROP PROCEDURE IF EXISTS xposeschedule;
DELIMITER // 
CREATE PROCEDURE xposeschedule(IN dbname VARCHAR(255), sctablename VARCHAR(255))
BEGIN

       DECLARE m_stationcode VARCHAR(30);
       DECLARE l_loop_end INT default 0;
       DECLARE stations_cur CURSOR FOR 

SELECT TRIM(UPPER(COLUMN_NAME)) AS stationcode  from information_schema.columns
        WHERE table_name=sctablename AND TABLE_SCHEMA=dbname AND
						column_name IN 
               (SELECT stationcode FROM t_station);

OPEN stations_cur;

REPEAT

       FETCH stations_cur into m_stationcode;

       SET @s = " INSERT INTO tempsched(stationcode, traintime,trainno,traincode,linecode,directioncode,car,emu,splcode,sundayonly,holiday,notonsunday) "; 
       SET @s = CONCAT(@s, " SELECT '", m_stationcode, "', ");
       SET @s = CONCAT(@s, " CAST(REPLACE(`", m_stationcode, "`, " );
			 SET @s = CONCAT(@s,  "':','.') AS DECIMAL(5,2)), "); 


			 SET @s = CONCAT(@s, " trim(trainno),  TRIM(traincode),  TRIM(linecode), TRIM(directioncode), trim(car),trim(emu),TRIM(splcode),sundayonly,holiday,notonsunday");
			 SET @s = CONCAT(@s, " FROM  ", sctablename);
			 SET @s = CONCAT(@s, " WHERE LENGTH(`", m_stationcode, "`) > 0 AND `", m_stationcode);
			 SET @s = CONCAT(@s, "` REGEXP '^[0-9]' "); 



	SELECT 'Statement: ', @s;




 PREPARE statmnt FROM @s;

  EXECUTE statmnt; 

 DEALLOCATE PREPARE statmnt; 

until l_loop_end end repeat;

CLOSE stations_cur;
end;
//

#RUN EACH CALL STATEMENT SEPERATELY IT WONT RUN IN ONE SHOT#

CALL xposeschedule('smartshehar_train', 'wrup');

CALL xposeschedule('smartshehar_train', 'wrdown');

CALL xposeschedule('smartshehar_train', 'crup');

CALL xposeschedule('smartshehar_train', 'crdown');

CALL xposeschedule('smartshehar_train', 'hrdown');

CALL xposeschedule('smartshehar_train', 'hrup');

CALL xposeschedule('smartshehar_train', 'nsup');

CALL xposeschedule('smartshehar_train', 'nsdown');

CALL xposeschedule('smartshehar_train', 'thup');

CALL xposeschedule('smartshehar_train', 'thdown');

CALL xposeschedule('smartshehar_train', 'shup');

CALL xposeschedule('smartshehar_train', 'shdown');

CALL xposeschedule('smartshehar_train', 'metup');

CALL xposeschedule('smartshehar_train', 'metdown');

CALL xposeschedule('smartshehar_train', 'monup');

CALL xposeschedule('smartshehar_train', 'mondown');

CALL xposeschedule('smartshehar_train', 'krup');

CALL xposeschedule('smartshehar_train', 'krdown');