CREATE INDEX stationline_stationline_id
ON ta_stationline (stationline_id);


CREATE INDEX ta_line_line_id ON ta_line(line_id);

CREATE INDEX ta_route_route_id ON ta_route(route_id);

CREATE INDEX ta_route_towardsstation_id ON ta_route(towardsstation_id);

CREATE INDEX ta_routedetail_route_id ON ta_routedetail(route_id);

CREATE INDEX ta_schedule_mins
ON ta_schedule (mins);

CREATE INDEX ta_schedule_station_id
ON ta_schedule (station_id);

CREATE INDEX ta_schedule_train_id ON ta_schedule(train_id);

CREATE INDEX ta_station_station_id
ON ta_station (station_id ASC);


CREATE INDEX ta_stationconnection_startstation_id
ON ta_stationconnections (startstation_id);

CREATE INDEX ta_towardsstation_towardsstation_id ON ta_towardsstation(towardsstation_id);

CREATE INDEX ta_towardstation_station_id ON ta_towardsstation(towardsstation_id);


CREATE INDEX ta_train_train_id ON ta_train(train_id);

VACUUM;

