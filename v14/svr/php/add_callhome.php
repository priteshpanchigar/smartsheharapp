<?php
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
$php_name = 'add_callhome';
include("dbconn_sar_apk.php");
include("mobile_common_data_sar.php");
if ( $mysqli ) {
	
	

	$appusageid = isset($_REQUEST['appusageid']) ? $_REQUEST['appusageid'] : 'NULL';
	
	$content = isset($_REQUEST['content']) ? "\"" . $_REQUEST['content'] . "\"" : 'NULL';
	
	$locationdatetime = isset($_REQUEST['locationdatetime']) ? 
		"\"" . $_REQUEST['locationdatetime'] . "\"" : 'NULL';
		
	$clientdatetime = isset($_REQUEST['clientdatetime']) ? 
		"\"" . $_REQUEST['clientdatetime'] . "\"" : 'NULL';	
	
	$sql = "call add_callhome(" . $appuserid . "," . $appusageid .
	"," . $clientdatetime .  "," . $lat . "," . $lng .
	",".  $accuracy . "," . $locationdatetime . "," . $content . "," . $app .")";
	
	if ($verbose != 'N') {
		echo '<br>sql:<br>' . $sql;
	}
	
	if ($result = $mysqli->query($sql)) {	
		if ($result && is_object($result))  {
			while ($row = $result->fetch_assoc()) {
				echo json_encode($row);
				break;
			}
		}
		$mysqli->close();
	}		else {
		echo "-1"; // something went wrong, probably sql failed
	}
} else {
	echo "-2"; // "Connection to db failed";
}