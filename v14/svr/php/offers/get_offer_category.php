<?php 
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
$php_name = 'getOfferCategory';
include("../mobile_common_data_sar.php");
include("../dbconn_sar_apk.php");
if ($mysqli) {
	$offercategoryRows = array();
	$sql = " call get_offer_category(".$lat. "," . $lng.")";
	if ($verbose != 'N') {
		echo $sql . '<br>';
	}	
	if ($result = $mysqli->query($sql)) {
		while ($row = $result->fetch_assoc()) {
			$offercategoryRows[] = $row;			
		}		
		$result->free();	// free result set
	}
	$mysqli->close();		// close connection
	
	if (count($offercategoryRows) > 0) {
		echo json_encode($offercategoryRows);
	} else {
		echo "-1";
	}
}