<?php
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
$php_name = 'addGroupMember';
include("../dbconn_sar_apk.php");
include("../mobile_common_data_sar.php");
if ( $mysqli ) {
	
	$groupname = empty($_REQUEST['groupname']) || !isset($_REQUEST['groupname']) ? 'NULL' : "'" . $_REQUEST['groupname'] . "'" ;
	$membercontact = empty($_REQUEST['membercontact']) || !isset($_REQUEST['membercontact']) ? 'NULL' : "'" . $_REQUEST['membercontact'] . "'" ;
	$memberemail= empty($_REQUEST['memberemail']) || !isset($_REQUEST['memberemail']) ? 'NULL' : "'" . $_REQUEST['memberemail'] . "'" ;	
	$membername = empty($_REQUEST['membername']) || !isset($_REQUEST['membername']) ? 'NULL' : "'" . $_REQUEST['membername'] . "'" ;
	$groupid = isset($_REQUEST['groupid']) ? $_REQUEST['groupid'] : 'NULL';
	
	$sql = "call add_group_member(" . $appuserid . "," . $groupname . "," . $membercontact . 
	"," . $memberemail . "," . $membername . "," . $groupid .")";
	
	if ($verbose != 'N') {
		echo '<br>sql:<br>' . $sql;
	}
	
	if ($result = $mysqli->query($sql)) {	
		if ($result && is_object($result))  {
			while ($row = $result->fetch_assoc()) {
				echo json_encode($row);
				break;
			}
		}
		$mysqli->close();
	}		else {
		echo "-1"; // something went wrong, probably sql failed
	}
} else {
	echo "-2"; // "Connection to db failed";
}