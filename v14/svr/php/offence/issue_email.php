<?php
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
$php_name = 'issueEmail';
include("../dbconn_sar_apk.php");
include("../mobile_common_data_sar.php");
include("../sendmail.php");
$memberemail = "";
$name = "";  
$message = isset($_REQUEST['message']) ? $_REQUEST['message']  : 'NULL';
$email = isset($_REQUEST['email']) ? $_REQUEST['email']  : 'NULL';
$offenceimagefilename = isset($_REQUEST['offenceimagefilename']) ? $_REQUEST['offenceimagefilename']  : 'NULL';
$subject = isset($_REQUEST['subject']) ?  $_REQUEST['subject'] : 'NULL';
$uniquekey= isset($_REQUEST['uniquekey']) ?  $_REQUEST['uniquekey'] : 'NULL';
$cc = $email ." , rtocomplaint@smartshehar.com";
$bcc =  "rtocomplaint@smartshehar.com";
$offencedetailid = isset($_REQUEST['offencedetailid']) ? $_REQUEST['offencedetailid'] : 'NULL';	
if($mysqli)
{	//$sql = "call get_Emailid_Using_Postal_Code(" .$offencedetailid . ")";
		$sql = "call get_Emailid_Using_Ward(" .$offencedetailid . ")";
		
	if ($verbose != 'N') {
		echo '<br> sql ' . $sql . '<br>';
		
	}
	$result = $mysqli->query($sql);  
	
	
	
	if(is_object($result)) {
	
		while ($row = $result->fetch_assoc()) {
			$memberemail = $row["ward_email_id"] . ", sstesting@gmail.com";
			
			if($memberemail=="")
			{
				$memberemail = "sstesting@gmail.com";
			}
			
			$subject = "Reporting an Issue  " . $row['offence_type'] . " at " . $row['offence_address'] ;
			$space = "";
			$date = "Date – ". $row['offence_time'] ;
			$to = "To:<br>".
				"Hon. Assistant Commissioner,<br>". 
				"Ward [". $row['ward'] .".]"; 
			$letter_subject="<b>Subject – Complaint from the residents in your ward:".
					"<br><br>&nbsp;&nbsp;&nbsp;&nbsp;Category – ". $row['category'].
					"<br>&nbsp;&nbsp;&nbsp;&nbsp;Sub-Category - ". $row['subcategory'] ."</b>";
			$image_count = $row['image_count'];
			if($image_count == '0')
			{
				$photo_attached="";
			}
			else{
				$photo_attached="<br><br>Photo of the issue is attached.";
			}
			
			$content = "Dear Sir,<br><br>On behalf of the residents 
			residing in ward-[". $row['ward'] ."], we would like to bring your attention 
			to the following issue. ".
			"<br><br>Issue Category – ". $row['category'].
			"<br>Issue Sub-Category – ". $row['subcategory'].
			"<br>Issue Location  – ". $row['offence_address'].
			"<br>Issue Submission Time – ". $row['offence_time'].
			$photo_attached .
			"<br><br>The following issue was registered using the SmartShehar App, ".
			"and as responsible citizens, we are bringing it to your notice.". 
			"<br><br>We sincerely hope you will direct your staff to give serious ". 
			"attention to this issue and take immediate remedial action. ".
			"<br><br><br>Sincerely,";
			$message = $space."  ".$date.
						"<br>". $to .
						"<br><br>". $letter_subject .
						"<br><br>".$content;
			
			    
			break;
		}
		
		$mysqli->close();
		
	
	}
	
}else{
	echo "connection failed";
}
if (!empty($memberemail)) {
	$arr = explode(",", $memberemail);
	foreach($arr as $emailid ) {
		sendMail($emailid, $name, $cc, $subject, $message, $offenceimagefilename,$bcc);
	}
	$notification_type = 'A new complaint has been registered';
	$notification_sub_type = 'Email is sent for approval';
	include("issue_registration_notification.php");
}