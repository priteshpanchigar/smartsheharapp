<?php
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
$php_name = 'updateApprovedOffence';
include("../dbconn_sar_apk.php"); 
include("../mobile_common_data_sar.php");
$errors = array();
if ( $mysqli ) {
		
	$tobesenttoauthority = isset($_REQUEST['tobesenttoauthority']) ? $_REQUEST['tobesenttoauthority'] : 'NULL';
	
	$rejected = isset($_REQUEST['rejected']) ? $_REQUEST['rejected'] : 'NULL';
	
	$offencedetailid = isset($_REQUEST['offencedetailid']) ? $_REQUEST['offencedetailid'] : 'NULL';
	
	$reasoncode = isset($_REQUEST['reasoncode']) ? "\"" . $_REQUEST['reasoncode'] . "\"" : 'NULL';
	
	$clientdatetime = isset($_REQUEST['clientdatetime']) ? "\"" . $_REQUEST['clientdatetime'] . "\"" : 'NULL';	
	$othercomment  = isset($_REQUEST['othercomment']) ? "\"" . $_REQUEST['othercomment'] . "\"" : 'NULL';

	
	$sql = "call update_approved_offence(" .$tobesenttoauthority .  ",".  $rejected .  
		",". $offencedetailid . ",". $reasoncode . ",". $clientdatetime .  ",". $othercomment .")";
	if ($verbose != 'N') {
		echo $sql . '<br>';
	}
	if ($result = $mysqli->query($sql)) {	
		while ($row = $result->fetch_assoc()) {
			echo json_encode($row);
			break;
		} 
		$mysqli->close();
	}		else {
		echo "-1"; // something went wrong, probably sql failed
	}
} else {
	echo "-2"; // "Connection to db failed";
}
include ("send_to_authority_offence_email.php");
	$notification_type = 'A complaint has been approved';
	$notification_sub_type = 'A issue is approved. Email is sent to authority';
	include("issue_approved_notification.php");