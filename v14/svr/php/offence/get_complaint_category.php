<?php 
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
$php_name = 'getComplaintCategory';
include("../mobile_common_data_sar.php");
include("../dbconn_sar_apk.php");
if ($mysqli) {
	$complaintRows = array();
	$sql = " call get_complaint_category()";
	if ($verbose != 'N') {
		echo $sql . '<br>';
	}	
	if ($result = $mysqli->query($sql)) {
		while ($row = $result->fetch_assoc()) {
			$complaintRows[] = $row;			
		}		
		$result->free();	// free result set
	}
	$mysqli->close();		// close connection
	
	if (count($complaintRows) > 0) {
		echo json_encode($complaintRows);
	} else {
		echo "-1";
	}
}