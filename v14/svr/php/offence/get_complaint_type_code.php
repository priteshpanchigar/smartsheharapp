<?php
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
$php_name = 'getComplaintTypeCode';
include("../dbconn_sar_apk.php"); 
include("../mobile_common_data_sar.php");
$errors = array();
if ($mysqli) {
	$getcomplainttypecodeRows = array();
	
	$typecode = isset($_REQUEST['typecode']) ? "\"" . $_REQUEST['typecode'] . "\"" : 'NULL';
	
	$sql = " call get_complaint_type_code(" .$typecode .")";
	if ($verbose != 'N') {
		echo $sql . '<br>';
	}	
	if ($result = $mysqli->query($sql)) {
		while ($row = $result->fetch_assoc()) {
			$getcomplainttypecodeRows[] = $row;		
			//	var_dump($row);
				//echo "<br>";
		}		
		$result->free();	// free result set
	}
	$mysqli->close();		// close connection
	
	
		echo json_encode($getcomplainttypecodeRows);
	 
}else {
		echo "-1";
	}