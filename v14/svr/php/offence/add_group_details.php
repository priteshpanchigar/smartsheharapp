<?php
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
$php_name = 'addGroupDetails';
include("../dbconn_sar_apk.php");
include("../mobile_common_data_sar.php");
if ( $mysqli ) {
	
	$groupname = empty($_REQUEST['groupname']) || !isset($_REQUEST['groupname']) ? 'NULL' : "'" . $_REQUEST['groupname'] . "'" ;
	$grouparea = empty($_REQUEST['grouparea']) || !isset($_REQUEST['grouparea']) ? 'NULL' : "'" . $_REQUEST['grouparea'] . "'" ;
	$grouptype = empty($_REQUEST['grouptype']) || !isset($_REQUEST['grouptype']) ? 'NULL' : "'" . $_REQUEST['grouptype'] . "'" ;	
	$groupdescription = empty($_REQUEST['groupdescription']) || !isset($_REQUEST['groupdescription']) ? 'NULL' : "'" . $_REQUEST['groupdescription'] . "'" ;
	$pincode = isset($_REQUEST['pincode']) ? $_REQUEST['pincode'] : 'NULL';
	$groupphoneno = isset($_REQUEST['groupphoneno']) ? $_REQUEST['groupphoneno'] : 'NULL';
	$groupemail = empty($_REQUEST['groupemail']) || !isset($_REQUEST['groupemail']) ? 'NULL' : "'" . $_REQUEST['groupemail'] . "'" ;
	
	$sql = "call add_group_details(" . $appuserid . "," . $groupname . "," . $grouptype . 
	"," . $grouparea . "," . $groupdescription . "," . $pincode . "," . $groupphoneno . 
	"," . $groupemail .")";
	
	if ($verbose != 'N') {
		echo '<br>sql:<br>' . $sql;
	}
	
	if ($result = $mysqli->query($sql)) {	
		if ($result && is_object($result))  {
			while ($row = $result->fetch_assoc()) {
				echo json_encode($row);
				break;
			}
		}
		$mysqli->close();
	}		else {
		echo "-1"; // something went wrong, probably sql failed
	}
} else {
	echo "-2"; // "Connection to db failed";
}