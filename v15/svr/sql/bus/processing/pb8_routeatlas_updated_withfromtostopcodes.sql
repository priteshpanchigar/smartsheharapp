DROP TABLE IF EXISTS pb_atlas_newstopname;


CREATE TABLE pb_atlas_newstopname 
(_id int not null auto_increment PRIMARY KEY)
SELECT routeno, 'F' as fromto, fromstop as oldatlasstopname, routecode
FROM pb_routeatlas_withfromtostopcodes
WHERE stopcodefrom is null
UNION
SELECT routeno, 'T' as fromto, fromstop as oldatlasstopname, routecode
FROM pb_routeatlas_withfromtostopcodes
WHERE stopcodeto is null
GROUP BY routeno, oldatlasstopname;

ALTER TABLE pb_atlas_newstopname
ADD routedetailstopname VARCHAR(100);
ALTER TABLE pb_atlas_newstopname
ADD stopcode INT;

###################################################################################################################################

DROP TABLE IF EXISTS pb_atlas_newstopname_tobeupdated;
CREATE TABLE pb_atlas_newstopname_tobeupdated 
(_id int not null auto_increment PRIMARY KEY)
SELECT routeno, 'F' as fromto, tostop as oldatlasstopname, routecode
FROM pb_routeatlas_withfromtostopcodes
WHERE stopcodefrom is null
UNION
SELECT routeno, 'T' as fromto, tostop as oldatlasstopname, routecode
FROM pb_routeatlas_withfromtostopcodes
WHERE stopcodeto is null
GROUP BY routeno, oldatlasstopname;

ALTER TABLE pb_atlas_newstopname_tobeupdated
ADD routedetailstopname VARCHAR(100);
ALTER TABLE pb_atlas_newstopname_tobeupdated
ADD stopcode INT;

