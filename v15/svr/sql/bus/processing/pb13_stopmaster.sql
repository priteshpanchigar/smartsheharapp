DROP TABLE if EXISTS pb_stopmaster;

CREATE TABLE pb_stopmaster
(best_stopmaster_id int(11) NOT NULL AUTO_INCREMENT PRIMARY key)
SELECT stopcode,stopname,direction,areaname,roadname from pb_routedetails_foratlasmatching
GROUP BY stopcode;


DROP TABLE IF EXISTS pb_stopmasterdetail;
CREATE TABLE pb_stopmasterdetail 
(stopmaster_id int(11) NOT NULL AUTO_INCREMENT PRIMARY key)
SELECT a.stopcode,a.stopname as routedetailstopname,a.direction,
b.stopname,b.areaname,
b.roadname,
b.latu,
b.latd,
b.lonu,
b.lond,
b.lat,
b.lon,
b.map,
b.cancelledstop,
b.notfound FROM  pb_stopmaster a LEFT OUTER JOIN
b_stopmaster_old b ON a.stopcode = b.stopcode;

UPDATE pb_stopmasterdetail a
SET areaname  = (SELECT areaname FROM pb_stopmaster b WHERE a.stopcode = b.stopcode) WHERE areaname is NULL;


UPDATE pb_stopmasterdetail a
SET roadname  = (SELECT roadname FROM pb_stopmaster b WHERE a.stopcode = b.stopcode) WHERE roadname is NULL;


UPDATE pb_stopmasterdetail a
SET routedetailstopname  = proper(routedetailstopname);


UPDATE pb_stopmasterdetail a
SET areaname  = proper(areaname);


UPDATE pb_stopmasterdetail a
SET roadname  = proper(roadname);

UPDATE pb_stopmasterdetail
SET roadname = NULL WHERE roadname like '%name to %';

ALTER TABLE pb_stopmasterdetail
Add stopnamedetailid VARCHAR(300);



ALTER TABLE pb_stopmasterdetail
Add stopdisplayname VARCHAR(300);


ALTER TABLE pb_stopmasterdetail
Add noofbuses int;


ALTER TABLE pb_stopmasterdetail
Add stop_id int;


CREATE INDEX u_stopcode
ON pb_stopmasterdetail (stopcode);


UPDATE pb_stopmasterdetail
SET stopnamedetailid = fn_RemoveSpecialCharacters(shortname(CONCAT
													(areaname,SUBSTRING_INDEX(SUBSTRING_INDEX(routedetailstopname, "/", 1), "(", 1))));



UPDATE pb_stopmasterdetail
SET stopdisplayname= CONCAT (CASE WHEN  length(areaname)>0 THEN CONCAT('[',areaname,'] ') ELSE '' END,
														 routedetailstopname) ;

#SUBSTRING_INDEX(SUBSTRING_INDEX(routedetailstopname, "/", 1), "(", 1)

UPDATE pb_stopmasterdetail
       SET noofbuses = 
       (SELECT COUNT(routecode) AS c
               FROM pb_routedetails_foratlasmatching
               WHERE pb_routedetails_foratlasmatching.stopcode = pb_stopmasterdetail.stopcode
               GROUP BY stopcode);


CREATE INDEX u_noofbuses
ON pb_stopmasterdetail (noofbuses);

CREATE INDEX d_stopnamedetailid
ON pb_stopmasterdetail (stopnamedetailid);


CREATE INDEX lat
ON pb_stopmasterdetail (lat);


CREATE INDEX lon
ON pb_stopmasterdetail (lon);

DROP TABLE IF EXISTS b_stopmaster;
CREATE TABLE b_stopmaster 
(_id int(11) NOT NULL AUTO_INCREMENT PRIMARY key)
select * FROM pb_stopmasterdetail ;




UPDATE b_stopmaster
SET stopnamedetailid = fn_RemoveSpecialCharacters(shortname(CONCAT
													(areaname,SUBSTRING_INDEX(SUBSTRING_INDEX(routedetailstopname, "/", 1), "(", 1))));



UPDATE b_stopmaster
SET lat = IFNULL (latu, latd),
		lon = IFNULL(lonu, lond);




CREATE INDEX noofbuses
ON b_stopmaster (noofbuses);

CREATE INDEX stopnamedetailid
ON b_stopmaster (stopnamedetailid);


CREATE INDEX stopcode
ON b_stopmaster (stopcode);


CREATE INDEX lat
ON b_stopmaster (lat);


CREATE INDEX lon
ON b_stopmaster (lon);


