DELETE FROM pb_raw_routeatlas
WHERE CONCAT(routeno,am,noon,pm,bustype,Depot,fromstop,firstfrom,
						lastfrom,tostop,firstto,lastto,routespan,runningtime,F15,F16,F17,Headway1,Headway2,Headway3,
						Headway4,Headway5,reliefpoint,traveltime,schedule) = '';

DROP TABLE IF EXISTS T;
CREATE TABLE T (_id int, id int, routeno varchar(300));
INSERT INTO t
SELECT best_routeatlas_id, 0, null FROM pb_raw_routeatlas WHERE routeno = '' OR routeno IS NULL ORDER BY best_routeatlas_id ;


CREATE INDEX t_id ON t (_id); 
CREATE INDEX _id ON t (id);


UPDATE t
SET id = (SELECT max(best_routeatlas_id) FROM pb_raw_routeatlas WHERE pb_raw_routeatlas.best_routeatlas_id < t._id
AND routeno != '' AND routeno IS NOT NULL
ORDER BY pb_raw_routeatlas.best_routeatlas_id );

UPDATE t
SET routeno = (SELECT routeno FROM pb_raw_routeatlas WHERE pb_raw_routeatlas.best_routeatlas_id = t.id);

UPDATE pb_raw_routeatlas
SET routeno = (SELECT routeno FROM t WHERE pb_raw_routeatlas.best_routeatlas_id = t._id)
WHERE routeno = '' OR routeno IS NULL ORDER BY best_routeatlas_id;

DROP TABLE IF EXISTS T;
CREATE TABLE T (_id int, id int, depot varchar(300));
INSERT INTO t
SELECT best_routeatlas_id, 0, null FROM pb_raw_routeatlas WHERE depot = '' OR depot IS NULL ORDER BY best_routeatlas_id ;



CREATE INDEX t_id ON t (_id); 
CREATE INDEX _id ON t (id);

UPDATE t
SET id = (SELECT max(best_routeatlas_id) FROM pb_raw_routeatlas WHERE pb_raw_routeatlas.best_routeatlas_id < t._id
AND depot != '' AND depot IS NOT NULL
ORDER BY pb_raw_routeatlas.best_routeatlas_id );

UPDATE t
SET depot = (SELECT depot FROM pb_raw_routeatlas WHERE pb_raw_routeatlas.best_routeatlas_id = t.id);

UPDATE pb_raw_routeatlas
SET depot = (SELECT depot FROM t WHERE pb_raw_routeatlas.best_routeatlas_id = t._id)
WHERE depot = '' OR depot IS NULL ORDER BY best_routeatlas_id;

DROP TABLE IF EXISTS T;
CREATE TABLE T (_id int, id int, fromstop varchar(300));
INSERT INTO t
SELECT best_routeatlas_id, 0, null FROM pb_raw_routeatlas WHERE fromstop = '' OR fromstop IS NULL ORDER BY best_routeatlas_id ;



CREATE INDEX t_id ON t (_id); 
CREATE INDEX _id ON t (id);

UPDATE t
SET id = (SELECT max(best_routeatlas_id) FROM pb_raw_routeatlas WHERE pb_raw_routeatlas.best_routeatlas_id < t._id
AND fromstop != '' AND fromstop IS NOT NULL
ORDER BY pb_raw_routeatlas.best_routeatlas_id );

UPDATE t
SET fromstop = (SELECT fromstop FROM pb_raw_routeatlas WHERE pb_raw_routeatlas.best_routeatlas_id = t.id);

UPDATE pb_raw_routeatlas
SET fromstop = (SELECT fromstop FROM t WHERE pb_raw_routeatlas.best_routeatlas_id = t._id)
WHERE fromstop = '' OR fromstop IS NULL ORDER BY best_routeatlas_id;

DROP TABLE IF EXISTS T;
CREATE TABLE T (_id int, id int, firstfrom varchar(300));
INSERT INTO t
SELECT best_routeatlas_id, 0, null FROM pb_raw_routeatlas WHERE firstfrom = '' OR firstfrom IS NULL ORDER BY best_routeatlas_id ;


CREATE INDEX t_id ON t (_id); 
CREATE INDEX _id ON t (id);

UPDATE t
SET id = (SELECT max(best_routeatlas_id) FROM pb_raw_routeatlas WHERE pb_raw_routeatlas.best_routeatlas_id < t._id
AND firstfrom != '' AND firstfrom IS NOT NULL
ORDER BY pb_raw_routeatlas.best_routeatlas_id );

UPDATE t
SET firstfrom = (SELECT firstfrom FROM pb_raw_routeatlas WHERE pb_raw_routeatlas.best_routeatlas_id = t.id);

UPDATE pb_raw_routeatlas
SET firstfrom = (SELECT firstfrom FROM t WHERE pb_raw_routeatlas.best_routeatlas_id = t._id)
WHERE firstfrom = '' OR firstfrom IS NULL ORDER BY best_routeatlas_id;

DROP TABLE IF EXISTS T;
CREATE TABLE T (_id int, id int, lastfrom varchar(300));
INSERT INTO t
SELECT best_routeatlas_id, 0, null FROM pb_raw_routeatlas WHERE lastfrom = '' OR lastfrom IS NULL ORDER BY best_routeatlas_id ;


CREATE INDEX t_id ON t (_id); 
CREATE INDEX _id ON t (id);

UPDATE t
SET id = (SELECT max(best_routeatlas_id) FROM pb_raw_routeatlas WHERE pb_raw_routeatlas.best_routeatlas_id < t._id
AND lastfrom != '' AND lastfrom IS NOT NULL
ORDER BY pb_raw_routeatlas.best_routeatlas_id );

UPDATE t
SET lastfrom = (SELECT lastfrom FROM pb_raw_routeatlas WHERE pb_raw_routeatlas.best_routeatlas_id = t.id);

UPDATE pb_raw_routeatlas
SET lastfrom = (SELECT lastfrom FROM t WHERE pb_raw_routeatlas.best_routeatlas_id = t._id)
WHERE lastfrom = '' OR lastfrom IS NULL ORDER BY best_routeatlas_id;

DROP TABLE IF EXISTS T;
CREATE TABLE T (_id int, id int, tostop varchar(300));
INSERT INTO t
SELECT best_routeatlas_id, 0, null FROM pb_raw_routeatlas WHERE tostop = '' OR tostop IS NULL ORDER BY best_routeatlas_id ;



CREATE INDEX t_id ON t (_id); 
CREATE INDEX _id ON t (id);

UPDATE t
SET id = (SELECT max(best_routeatlas_id) FROM pb_raw_routeatlas WHERE pb_raw_routeatlas.best_routeatlas_id < t._id
AND tostop != '' AND tostop IS NOT NULL
ORDER BY pb_raw_routeatlas.best_routeatlas_id );

UPDATE t
SET tostop = (SELECT tostop FROM pb_raw_routeatlas WHERE pb_raw_routeatlas.best_routeatlas_id = t.id);

UPDATE pb_raw_routeatlas
SET tostop = (SELECT tostop FROM t WHERE pb_raw_routeatlas.best_routeatlas_id = t._id)
WHERE tostop = '' OR tostop IS NULL ORDER BY best_routeatlas_id;

DROP TABLE IF EXISTS T;
CREATE TABLE T (_id int, id int, firstto varchar(300));
INSERT INTO t
SELECT best_routeatlas_id, 0, null FROM pb_raw_routeatlas WHERE firstto = '' OR firstto IS NULL ORDER BY best_routeatlas_id ;


CREATE INDEX t_id ON t (_id); 
CREATE INDEX _id ON t (id);


UPDATE t
SET id = (SELECT max(best_routeatlas_id) FROM pb_raw_routeatlas WHERE pb_raw_routeatlas.best_routeatlas_id < t._id
AND firstto != '' AND firstto IS NOT NULL
ORDER BY pb_raw_routeatlas.best_routeatlas_id );

UPDATE t
SET firstto = (SELECT firstto FROM pb_raw_routeatlas WHERE pb_raw_routeatlas.best_routeatlas_id = t.id);

UPDATE pb_raw_routeatlas
SET firstto = (SELECT firstto FROM t WHERE pb_raw_routeatlas.best_routeatlas_id = t._id)
WHERE firstto = '' OR firstto IS NULL ORDER BY best_routeatlas_id;

DROP TABLE IF EXISTS T;
CREATE TABLE T (_id int, id int, lastto varchar(300));
INSERT INTO t
SELECT best_routeatlas_id, 0, null FROM pb_raw_routeatlas WHERE lastto = '' OR lastto IS NULL ORDER BY best_routeatlas_id ;


CREATE INDEX t_id ON t (_id); 
CREATE INDEX _id ON t (id);

UPDATE t
SET id = (SELECT max(best_routeatlas_id) FROM pb_raw_routeatlas WHERE pb_raw_routeatlas.best_routeatlas_id < t._id
AND lastto != '' AND lastto IS NOT NULL
ORDER BY pb_raw_routeatlas.best_routeatlas_id );

UPDATE t
SET lastto = (SELECT lastto FROM pb_raw_routeatlas WHERE pb_raw_routeatlas.best_routeatlas_id = t.id);

UPDATE pb_raw_routeatlas
SET lastto = (SELECT lastto FROM t WHERE pb_raw_routeatlas.best_routeatlas_id = t._id)
WHERE lastto = '' OR lastto IS NULL ORDER BY best_routeatlas_id;

DROP TABLE IF EXISTS T;
CREATE TABLE T (_id int, id int, headway1 varchar(300));
INSERT INTO t
SELECT best_routeatlas_id, 0, null FROM pb_raw_routeatlas WHERE headway1 = '' OR headway1 IS NULL ORDER BY best_routeatlas_id ;


CREATE INDEX t_id ON t (_id); 
CREATE INDEX _id ON t (id);


UPDATE t
SET id = (SELECT max(best_routeatlas_id) FROM pb_raw_routeatlas WHERE pb_raw_routeatlas.best_routeatlas_id < t._id
AND headway1 != '' AND headway1 IS NOT NULL
ORDER BY pb_raw_routeatlas.best_routeatlas_id );

UPDATE t
SET headway1 = (SELECT headway1 FROM pb_raw_routeatlas WHERE pb_raw_routeatlas.best_routeatlas_id = t.id);

UPDATE pb_raw_routeatlas
SET headway1 = (SELECT headway1 FROM t WHERE pb_raw_routeatlas.best_routeatlas_id = t._id)
WHERE headway1 = '' OR headway1 IS NULL ORDER BY best_routeatlas_id;


DROP TABLE IF EXISTS T;
CREATE TABLE T (_id int, id int, headway2 varchar(300));
INSERT INTO t
SELECT best_routeatlas_id, 0, null FROM pb_raw_routeatlas WHERE headway2 = '' OR headway2 IS NULL ORDER BY best_routeatlas_id ;


CREATE INDEX t_id ON t (_id); 
CREATE INDEX _id ON t (id);


UPDATE t
SET id = (SELECT max(best_routeatlas_id) FROM pb_raw_routeatlas WHERE pb_raw_routeatlas.best_routeatlas_id < t._id
AND headway2 != '' AND headway2 IS NOT NULL
ORDER BY pb_raw_routeatlas.best_routeatlas_id );

UPDATE t
SET headway2 = (SELECT headway2 FROM pb_raw_routeatlas WHERE pb_raw_routeatlas.best_routeatlas_id = t.id);

UPDATE pb_raw_routeatlas
SET headway2 = (SELECT headway2 FROM t WHERE pb_raw_routeatlas.best_routeatlas_id = t._id)
WHERE headway2 = '' OR headway2 IS NULL ORDER BY best_routeatlas_id;

DROP TABLE IF EXISTS T;
CREATE TABLE T (_id int, id int, headway2 varchar(300));
INSERT INTO t
SELECT best_routeatlas_id, 0, null FROM pb_raw_routeatlas WHERE headway2 = '' OR headway2 IS NULL ORDER BY best_routeatlas_id ;


CREATE INDEX t_id ON t (_id); 
CREATE INDEX _id ON t (id);

UPDATE t
SET id = (SELECT max(best_routeatlas_id) FROM pb_raw_routeatlas WHERE pb_raw_routeatlas.best_routeatlas_id < t._id
AND headway2 != '' AND headway2 IS NOT NULL
ORDER BY pb_raw_routeatlas.best_routeatlas_id );

UPDATE t
SET headway2 = (SELECT headway2 FROM pb_raw_routeatlas WHERE pb_raw_routeatlas.best_routeatlas_id = t.id);

UPDATE pb_raw_routeatlas
SET headway2 = (SELECT headway2 FROM t WHERE pb_raw_routeatlas.best_routeatlas_id = t._id)
WHERE headway2 = '' OR headway2 IS NULL ORDER BY best_routeatlas_id;


DROP TABLE IF EXISTS T;
CREATE TABLE T (_id int, id int, headway3 varchar(300));
INSERT INTO t
SELECT best_routeatlas_id, 0, null FROM pb_raw_routeatlas WHERE headway3 = '' OR headway3 IS NULL ORDER BY best_routeatlas_id ;

CREATE INDEX t_id ON t (_id); 
CREATE INDEX _id ON t (id);

UPDATE t
SET id = (SELECT max(best_routeatlas_id) FROM pb_raw_routeatlas WHERE pb_raw_routeatlas.best_routeatlas_id < t._id
AND headway3 != '' AND headway3 IS NOT NULL
ORDER BY pb_raw_routeatlas.best_routeatlas_id );

UPDATE t
SET headway3 = (SELECT headway3 FROM pb_raw_routeatlas WHERE pb_raw_routeatlas.best_routeatlas_id = t.id);

UPDATE pb_raw_routeatlas
SET headway3 = (SELECT headway3 FROM t WHERE pb_raw_routeatlas.best_routeatlas_id = t._id)
WHERE headway3 = '' OR headway3 IS NULL ORDER BY best_routeatlas_id;



DROP TABLE IF EXISTS T;
CREATE TABLE T (_id int, id int, headway4 varchar(300));
INSERT INTO t
SELECT best_routeatlas_id, 0, null FROM pb_raw_routeatlas WHERE headway4 = '' OR headway4 IS NULL ORDER BY best_routeatlas_id ;

CREATE INDEX t_id ON t (_id); 
CREATE INDEX _id ON t (id);


UPDATE t
SET id = (SELECT max(best_routeatlas_id) FROM pb_raw_routeatlas WHERE pb_raw_routeatlas.best_routeatlas_id < t._id
AND headway4 != '' AND headway4 IS NOT NULL
ORDER BY pb_raw_routeatlas.best_routeatlas_id );

UPDATE t
SET headway4 = (SELECT headway4 FROM pb_raw_routeatlas WHERE pb_raw_routeatlas.best_routeatlas_id = t.id);

UPDATE pb_raw_routeatlas
SET headway4 = (SELECT headway4 FROM t WHERE pb_raw_routeatlas.best_routeatlas_id = t._id)
WHERE headway4 = '' OR headway4 IS NULL ORDER BY best_routeatlas_id;



DROP TABLE IF EXISTS T;
CREATE TABLE T (_id int, id int, headway5 varchar(300));
INSERT INTO t
SELECT best_routeatlas_id, 0, null FROM pb_raw_routeatlas WHERE headway5 = '' OR headway5 IS NULL ORDER BY best_routeatlas_id ;

CREATE INDEX t_id ON t (_id); 
CREATE INDEX _id ON t (id);

UPDATE t
SET id = (SELECT max(best_routeatlas_id) FROM pb_raw_routeatlas WHERE pb_raw_routeatlas.best_routeatlas_id < t._id
AND headway5 != '' AND headway5 IS NOT NULL
ORDER BY pb_raw_routeatlas.best_routeatlas_id );

UPDATE t
SET headway5 = (SELECT headway5 FROM pb_raw_routeatlas WHERE pb_raw_routeatlas.best_routeatlas_id = t.id);

UPDATE pb_raw_routeatlas
SET headway5 = (SELECT headway5 FROM t WHERE pb_raw_routeatlas.best_routeatlas_id = t._id)
WHERE headway5 = '' OR headway5 IS NULL ORDER BY best_routeatlas_id;

DROP TABLE IF EXISTS T;
CREATE TABLE T (_id int, id int, schedule varchar(300));
INSERT INTO t
SELECT best_routeatlas_id, 0, null FROM pb_raw_routeatlas WHERE schedule = '' OR schedule IS NULL ORDER BY best_routeatlas_id ;

CREATE INDEX t_id ON t (_id); 
CREATE INDEX _id ON t (id);

UPDATE t
SET id = (SELECT max(best_routeatlas_id) FROM pb_raw_routeatlas WHERE pb_raw_routeatlas.best_routeatlas_id < t._id
AND schedule != '' AND schedule IS NOT NULL
ORDER BY pb_raw_routeatlas.best_routeatlas_id );

UPDATE t
SET schedule = (SELECT schedule FROM pb_raw_routeatlas WHERE pb_raw_routeatlas.best_routeatlas_id = t.id);

UPDATE pb_raw_routeatlas
SET schedule = (SELECT schedule FROM t WHERE pb_raw_routeatlas.best_routeatlas_id = t._id)
WHERE schedule = '' OR schedule IS NULL ORDER BY best_routeatlas_id;


update pb_raw_routeatlas
set fromstop = ' P.THAKRE UDN.BUS STN.'
where fromstop = 'P.Thakre Udyan';

update pb_raw_routeatlas
set tostop = ' P.THAKRE UDN.BUS STN.'
where tostop = 'P.Thakre Udyan';

update pb_raw_routeatlas
set fromstop = 'S.P.Mukherji Chowk'
where fromstop = 'Dr.S.P.M.Chowk';


update pb_raw_routeatlas
set tostop = 'S.P.Mukherji Chowk'
where tostop = 'Dr.S.P.M.Chowk';

update pb_raw_routeatlas
SET firstfrom = NULL WHERE firstfrom like '%-%';


update pb_raw_routeatlas
SET lastfrom = NULL WHERE lastfrom like '%-%';


update pb_raw_routeatlas
SET firstto = NULL WHERE firstto like '%-%';


update pb_raw_routeatlas
SET lastto = NULL WHERE lastto like '%-%';


