DROP PROCEDURE IF EXISTS insert_bustrip;
DELIMITER $$
CREATE  PROCEDURE `insert_bustrip`(IN inappuserid INT, IN invehicleno varchar(25),IN inbuslabel varchar(25),
IN infromstop varchar(1000), IN intostop varchar(1000), IN infromstopserial INT, IN intostopserial INT,
IN intripstatus varchar(25), IN increatedatetime datetime, IN `inclientdatetime` datetime,IN `inemailid` varchar(100), 
	IN `inlatno` double, IN `inlngno` double, IN `inacc` int, 
	IN `inlocationdatetime` datetime, IN `inprovider` varchar(10),
	IN `inspeed` float, IN `inbearing` float, IN `inaltitude` double, IN indirection varchar(5))
BEGIN

DECLARE vTripId INT DEFAULT -1;
DECLARE vLocationId INT DEFAULT -1;	

SET @appuserId = inappuserid;	
	
INSERT INTO bustrip(appuser_id, vehicle_no, buslabel, createdatetime, fromstop, tostop, 
	fromstopserial, tostopserial,  tripstatus, direction_sign, direction)
VALUES (@appuserId, invehicleno, inbuslabel, increatedatetime, infromstop, intostop,
	infromstopserial, intostopserial, intripstatus, 
	IF (fromstopserial < tostopserial, -1, 1), indirection) ;
SELECT LAST_INSERT_ID() INTO vTripId;

SET vLocationId = insert_location(@appUserId, inemailid, 1, 0, inclientdatetime,
			inlatno, inlngno, inacc, inlocationdatetime,  inprovider, inspeed, inbearing, 
			inaltitude);
	UPDATE bustrip
		SET last_location_id = vLocationId
	WHERE trip_id = vTripId;		
	
SELECT vTripId as trip_id;

END$$ 
DELIMITER;

call insert_bustrip(603,NULL,'339','[Juhu] Juhu Bus Stn.','[Jogeshwari (E)] Majas Depot/Shyam Ngr.',2,1,'C',"2016-01-25 17:24:02",
"2016-01-25 17:28:43",'soumenmaity.cse@gmail.com',"18.935910000","72.838280000","48.3390007019043","2016-01-25 17:28:43","F","22.22222328186035","0.0","0.0",'U')