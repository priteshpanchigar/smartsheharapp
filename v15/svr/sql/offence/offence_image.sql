DROP PROCEDURE IF EXISTS offence_image;
DELIMITER$$
CREATE  PROCEDURE `offence_image`(IN inoffenceimagename varchar(50),  IN inappuserid INT, 
IN increationdatetime datetime,  IN inoffencedetailid INT, IN inoffenceimagepath VARCHAR(255))
BEGIN
	

INSERT INTO offence_image( offence_image_name,  appuser_id, creation_datetime, 
	offence_detail_id,offence_image_path)
VALUES (inoffenceimagename , inappuserid, increationdatetime, 
	inoffencedetailid,inoffenceimagepath)
		ON DUPLICATE KEY UPDATE
		offence_image_name = inoffenceimagename,
		offence_image_path = inoffenceimagepath;

SELECT LAST_INSERT_ID() offence_image_id;


END$$
DELIMITER ;