
DROP PROCEDURE IF EXISTS add_letterhead_details;
DELIMITER$$
CREATE  PROCEDURE `add_letterhead_details`(IN inletterimagename varchar(255), IN inletterdatetime datetime,  IN inoffencedetailid INT,
 IN inletterimagepath VARCHAR(255),IN inlettersubmittedby INT)
BEGIN

UPDATE  offence_details SET letter_client_date_time = inletterdatetime,
letter_image_name = inletterimagename,
letter_image_path  =inletterimagepath,
letter_submit_by = inlettersubmittedby 
WHERE offence_detail_id = inoffencedetailid ;


END$$
DELIMITER ;