DROP PROCEDURE IF EXISTS appuserid_show_offence_details;
DELIMITER$$
CREATE PROCEDURE `appuserid_show_offence_details`(IN inappuserid INT , IN insubmit_report int)
BEGIN
SELECT DISTINCT(offence_detail_id), offence_address,  offence_image_path, vehicle_no,
CONCAT(DAY(offence_time)," ",MONTHNAME(offence_time), ", ", YEAR(offence_time)," at ", TIME_FORMAT(offence_time,'%h:%i%p'))as offence_time, 
offence_type , client_datetime,  creation_datetime,  appuser_id,  complaint_type_code,
complaint_sub_type_code,   offence_image_name, unique_key,submit_report,lat,lng,colour,android_colour
	FROM vw_offence
		WHERE offence_image_name != "" 
AND complaint_sub_type_code!=""
		AND appuser_id = inappuserid

		#AND submit_report = insubmit_report
			AND discard_report =0
		AND rejected=0
		AND offence_image_id IN
			(SELECT  MIN(offence_image_id) offence_image_id from 
			offence_image
			GROUP BY offence_detail_id)
			order by offence_detail_id DESC;
END$$
DELIMITER;


call appuserid_show_offence_details(1,1);
----------------------------------------------------------------------------------------------------
DROP PROCEDURE IF EXISTS appuserid_show_offence_details;
DELIMITER$$
CREATE PROCEDURE `appuserid_show_offence_details`(IN inappuserid INT )
BEGIN
SELECT DISTINCT(od.offence_detail_id), offence_address,  offence_image_path, vehicle_no,
CONCAT(DAY(offence_time)," ",MONTHNAME(offence_time), ", ", YEAR(offence_time)," at ", TIME_FORMAT(offence_time,'%h:%i%p'))as offence_time, 
offence_type , client_datetime,  creation_datetime,  od.appuser_id,  od.complaint_type_code,
complaint_sub_type_code,   offence_image_name, unique_key,submit_report,lat,lng,ct.colour,ct.android_colour
	FROM offence_details od
LEFT JOIN offence_image oi
 ON od.offence_detail_id = oi.offence_detail_id
LEFT JOIN complaint_lookup_types ct
ON ct.complaint_type_code = od.complaint_type_code
		WHERE complaint_sub_type_code!=""
			AND od.appuser_id = inappuserid
			#AND submit_report = 1
			AND discard_report =0
			#AND rejected=0
GROUP BY offence_detail_id
		#	AND offence_image_id IN
			#	(SELECT  MIN(offence_image_id) offence_image_id from 
				#	offence_image
					#GROUP BY offence_detail_id)
					order by offence_detail_id DESC;
END$$
DELIMITER;

CALL appuserid_show_offence_details(2)



