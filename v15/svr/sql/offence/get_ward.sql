DROP PROCEDURE IF EXISTS get_ward;
DELIMITER$$
CREATE  PROCEDURE `get_ward`()
BEGIN
	
SELECT ward, ward_id FROM municipal_ward 
GROUP BY ward
ORDER BY ward; 


END$$
DELIMITER ;

CALL get_ward();