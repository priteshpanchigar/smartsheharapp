DROP PROCEDURE IF EXISTS appuserid_show_all_offences;
CREATE  PROCEDURE `appuserid_show_all_offences`(IN inuniquekey varchar(255))
BEGIN
DECLARE submitter_name VARCHAR(255);

select DISTINCT(gd.group_name)  into submitter_name FROM offence_details od
INNER JOIN group_members gm ON od.appuser_id = gm.appuser_id
INNER JOIN groupdetails gd ON gm.group_id = gd.group_id
where unique_key = inuniquekey   ;


IF (submitter_name IS NULL) THEN
			SELECT 
			(SELECT COUNT(issue_like) FROM issue_rating ir WHERE ir.offence_detail_id = od.offence_detail_id				
				GROUP BY ir.offence_detail_id)count,
			(SELECT max(issue_rating_comment) issue_rating_comment  FROM issue_rating ir WHERE ir.offence_detail_id = od.offence_detail_id
				GROUP BY ir.offence_detail_id ) issue_rating_comment,
			od.offence_detail_id, offence_address, offence_time, client_datetime, submit_report, approved, 
			rejected,  sent_to_authority, resolved, unresolved, sent_to_authority_datetime, 
			resolved_unresolved_datetime, other_reason_code_comment, unresolved_comment,
			offence_type, vehicle_no, unique_key, complaint_sub_type_code, complaint_type_code, od.appuser_id,
			offence_image_id, ward, GROUP_CONCAT(offence_image_name) offence_image_name, creation_datetime,
			offence_image_path as offence_image_path, letter_client_date_time, letter_image_name, letter_image_path,
	 (select a.phoneno
			FROM offence_details od
			INNER JOIN appuser a ON od.appuser_id = a.appuser_id
			where unique_key = inuniquekey  ) as 'submitter_name',
(select DISTINCT(gd.group_name) FROM offence_details od
INNER JOIN address a ON od.address_id = a.address_id
INNER JOIN groupdetails gd ON a.postal_code = gd.pincode
where od.unique_key = inuniquekey) as 'submit_to',
			letter_upload_notification,
			(SELECT r.reject_reason FROM  offence_details od INNER JOIN rejected_reason r
			ON od.reason_code = r.reason_code WHERE unique_key = inuniquekey) as 'reject_reason',
			closed,opened,closed_opened_comment,closed_opened_datetime
			FROM offence_details od
			LEFT OUTER JOIN offence_image of ON od.offence_detail_id = of.offence_detail_id
			WHERE unique_key = inuniquekey   
			GROUP BY offence_detail_id;
ELSE
			SELECT 
		(SELECT COUNT(issue_like) FROM issue_rating ir WHERE ir.offence_detail_id = od.offence_detail_id				
				GROUP BY ir.offence_detail_id)count,
			(SELECT max(issue_rating_comment) issue_rating_comment  FROM issue_rating ir WHERE ir.offence_detail_id = od.offence_detail_id
				GROUP BY ir.offence_detail_id ) issue_rating_comment,
		od.offence_detail_id, offence_address, offence_time, client_datetime, submit_report, approved, 
			rejected,  sent_to_authority, resolved, unresolved, sent_to_authority_datetime, 
			resolved_unresolved_datetime, other_reason_code_comment, unresolved_comment,
			offence_type, vehicle_no, unique_key, complaint_sub_type_code, complaint_type_code, od.appuser_id,
			offence_image_id, ward, GROUP_CONCAT(offence_image_name) offence_image_name, creation_datetime,
			offence_image_path as offence_image_path, letter_client_date_time, letter_image_name, letter_image_path,
			 (select gd.group_name  
			FROM offence_details od
			INNER JOIN group_members gm ON od.appuser_id = gm.appuser_id
			INNER JOIN groupdetails gd ON gm.group_id = gd.group_id
			where unique_key = inuniquekey ) as 'submitter_name',
(select DISTINCT(gd.group_name) FROM offence_details od
INNER JOIN address a ON od.address_id = a.address_id
INNER JOIN groupdetails gd ON a.postal_code = gd.pincode
where od.unique_key = inuniquekey) as 'submit_to',
			letter_upload_notification,
			(SELECT r.reject_reason FROM  offence_details od INNER JOIN rejected_reason r
			ON od.reason_code = r.reason_code WHERE unique_key = inuniquekey) as 'reject_reason',
			closed,opened,closed_opened_comment,closed_opened_datetime
			FROM offence_details od
			LEFT OUTER JOIN offence_image of ON od.offence_detail_id = of.offence_detail_id
			WHERE unique_key = inuniquekey  
			GROUP BY offence_detail_id;
END IF;

END

CALL appuserid_show_all_offences('1453901244297');