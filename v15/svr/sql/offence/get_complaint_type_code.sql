DROP PROCEDURE IF EXISTS get_complaint_type_code;
DELIMITER$$
CREATE  PROCEDURE `get_complaint_type_code`(IN intypecode varchar(20))
BEGIN
	
SELECT complaint_lookup_subtype_id, complaint_sub_type_code, complaint_sub_type_description, complaint_type_code 
	FROM complaint_lookup_subtypes
	WHERE complaint_type_code LIKE CONCAT(intypecode, '%')
	ORDER BY complaint_type_code DESC;


END$$
DELIMITER ;







DROP PROCEDURE IF EXISTS get_ward_list;
DELIMITER$$
CREATE  PROCEDURE `get_ward_list`(IN inward varchar(255))
BEGIN
	
SELECT a.city, b.mp,c.mla, d.ward, a.city_id,b.mp_id,  c.mla_id,  d.ward_id
FROM municipal_city a
INNER JOIN muncipal_mp b ON
a.city_id = b.city_id
INNER JOIN municipal_mla c ON
b.mp_id = c.mp_id
INNER JOIN municipal_ward d ON
d.mla_id = c.mla_id
 WHERE ward = inward;

END$$
DELIMITER ;