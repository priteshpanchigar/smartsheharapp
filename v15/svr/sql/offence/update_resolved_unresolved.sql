DROP PROCEDURE IF EXISTS update_resolved_unresolved;
DELIMITER$$
CREATE  PROCEDURE `update_resolved_unresolved`(IN inresolved INT, IN inunresolved INT, 
IN inoffencedetailid INT,IN inunresolvedcomment  VARCHAR(300),IN inclientdatetime datetime,
IN inappuserid INT)
BEGIN

UPDATE offence_details
SET resolved = inresolved, 
unresolved = inunresolved,
resolved_unresolved_datetime = inclientdatetime,
unresolved_comment  = inunresolvedcomment,
resolved_unresolved_by = inappuserid 
WHERE offence_detail_id = inoffencedetailid;

select resolved, unresolved, unresolved_comment,resolved_unresolved_by from offence_details
WHERE offence_detail_id = inoffencedetailid;


END$$
DELIMITER;

call update_resolved_unresolved(0,1,2,"asdasdasdsad", "2015-10-14 10:48:15");