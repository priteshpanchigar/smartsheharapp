DROP PROCEDURE IF EXISTS get_name_who_changed_status;
DELIMITER$$
CREATE  PROCEDURE `get_name_who_changed_status`(IN inoffencedetailid  INT)
BEGIN

DECLARE submitter_name VARCHAR(255);

select DISTINCT(gd.group_name)  into submitter_name FROM offence_details od
INNER JOIN group_members gm ON od.resolved_unresolved_by = gm.appuser_id
INNER JOIN groupdetails gd ON gm.group_id = gd.group_id
where offence_detail_id = inoffencedetailid  ;

IF (submitter_name IS NULL) THEN
	select DISTINCT(a.phoneno)  as 'submitter_name' 
	FROM offence_details od
	INNER JOIN appuser a ON od.resolved_unresolved_by = a.appuser_id
	where offence_detail_id = inoffencedetailid  ;
ELSE
	select DISTINCT(gd.group_name)  as 'submitter_name' 
	FROM offence_details od
	INNER JOIN group_members gm ON od.resolved_unresolved_by = gm.appuser_id
	INNER JOIN groupdetails gd ON gm.group_id = gd.group_id
	where offence_detail_id = inoffencedetailid  ;
END IF;

END$$
DELIMITER ;