DROP PROCEDURE IF EXISTS get_mygroup_complaints;
DELIMITER$$
CREATE PROCEDURE `get_mygroup_complaints`(IN ingroupid INT )
BEGIN
SELECT DISTINCT(offence_detail_id), offence_address,  offence_image_path, vehicle_no,
CONCAT(DAY(offence_time)," ",MONTHNAME(offence_time), ", ", YEAR(offence_time)," at ", TIME_FORMAT(offence_time,'%h:%i%p'))as offence_time, 
offence_type , client_datetime,  creation_datetime,  appuser_id,  complaint_type_code,
complaint_sub_type_code,   offence_image_name, unique_key,submit_report,lat,lng,colour,android_colour
	FROM vw_offence
	WHERE offence_image_name != "" 
			AND complaint_sub_type_code!=""
			AND group_id = ingroupid
			AND discard_report =0
			AND rejected=0
			AND offence_image_id IN
				(SELECT  MIN(offence_image_id) offence_image_id from 
				offence_image
				GROUP BY offence_detail_id)
				order by offence_detail_id DESC;
END$$
DELIMITER;


call get_mygroup_complaints(1,1)