DROP PROCEDURE IF EXISTS get_sent_to_authority_offence;
DELIMITER$$
CREATE  PROCEDURE `get_sent_to_authority_offence`() 

BEGIN

SELECT 
(SELECT email_id from city_pincode_authority_emailids
where postal_code = (SELECT postal_code from address
where address_id = (SELECT address_id  from offence_details od1
where od1.unique_key =od.unique_key ))) authority_email_id ,
(SELECT email FROM appuser a WHERE a.appuser_id = od.appuser_id)email,
od.offence_detail_id,
offence_address,
offence_time,
client_datetime,
offence_type,
vehicle_no,
unique_key,
submit_report,
discard_report,
address_id,
location_id,
complaint_type_code,
complaint_sub_type_code,
approved,
rejected,
sent_to_authority,
lat,
lng,
od.appuser_id,
offence_image_id,
offence_video_path,
offence_video_name,
offence_image_name,
offence_image_path,
creation_datetime FROM offence_details od
 INNER JOIN offence_image oi ON od.offence_detail_id  = oi.offence_detail_id
WHERE to_be_sent_to_authority = 1 
AND sent_to_authority = 0
AND resolved = 0
AND unresolved = 0;


END$$
DELIMITER;

CALL get_sent_to_authority_offence;