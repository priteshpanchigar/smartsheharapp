DROP PROCEDURE IF EXISTS update_sent_to_authority_offence;
DELIMITER$$
CREATE  PROCEDURE `update_sent_to_authority_offence`(IN inoffencedetailid INT, 
IN inclientdatetime datetime) 

BEGIN

UPDATE offence_details
SET sent_to_authority = 1,
sent_to_authority_datetime =  inclientdatetime
WHERE offence_detail_id = inoffencedetailid;

SELECT 1 AS result;

END$$
DELIMITER;

CALL update_sent_to_authority_offence(36,"2015-10-14 10:48:15");