
DROP PROCEDURE IF EXISTS get_intermediate_progress;
CREATE  PROCEDURE `get_intermediate_progress`(IN inoffencedetailid INT)
BEGIN


SELECT  appuser_id, offence_detail_id, intermediate_progress_comment, 
intermediate_progress_clientdatetime FROM intermediate_progress
WHERE offence_detail_id = inoffencedetailid  
ORDER BY intermediate_progress_id DESC;

END;

call get_intermediate_progress(2)

