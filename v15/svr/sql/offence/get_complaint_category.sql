DROP PROCEDURE IF EXISTS get_complaint_category;
DELIMITER$$
CREATE  PROCEDURE `get_complaint_category`()
BEGIN

SELECT complaint_lookup_subtype_id, complaint_sub_type_code, complaint_description,
complaint_sub_type_description, complaint_type_code, complaint_lookup_id
FROM vw_complaint_category;


END$$
DELIMITER ;