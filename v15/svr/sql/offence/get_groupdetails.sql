
DROP PROCEDURE IF EXISTS get_groupdetails;
CREATE  PROCEDURE `get_groupdetails`()
BEGIN

SELECT group_id, group_name, group_area, group_description, appuser_id, pincode,
group_phoneno, group_email FROM groupdetails;

END;

call get_groupdetails()