DROP PROCEDURE IF EXISTS get_complaint_lookup_subtype;
DELIMITER$$
CREATE  PROCEDURE `get_complaint_lookup_subtype`()
BEGIN
	
SELECT complaint_lookup_subtype_id,
complaint_sub_type_code,
complaint_sub_type_description,
complaint_type_code FROM complaint_lookup_subtypes;
END$$
DELIMITER ;