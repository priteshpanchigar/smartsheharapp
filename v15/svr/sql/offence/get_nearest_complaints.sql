DROP PROCEDURE IF EXISTS get_nearest_complaints;
DELIMITER$$
CREATE  PROCEDURE `get_nearest_complaints`(IN inlat DOUBLE, IN inlon DOUBLE)
BEGIN


SELECT od.offence_detail_id, offence_address, offence_time, client_datetime, offence_type, vehicle_no, unique_key, submit_report, 
discard_report, address_id, location_id, ward, od.complaint_type_code, od.complaint_sub_type_code, colour, android_colour,
approved, rejected, approved_rejected_clientdatetime, sent_to_authority_datetime, resolved_unresolved_datetime, other_reason_code_comment, 
resolved, unresolved, unresolved_comment, reason_code, sent_to_authority, od.lat, od.lng,
a.appuser_id, email, phoneno, 
username, fullname, age, sex, offence_image_id, offence_image_name, offence_image_path, creation_datetime,
(SELECT group_name FROM groupdetails 
	WHERE group_id = od.group_id) group_name 
FROM offence_details od
LEFT JOIN offence_image oi
 ON od.offence_detail_id = oi.offence_detail_id
LEFT JOIN complaint_lookup_types ct
ON ct.complaint_type_code = od.complaint_type_code
LEFT JOIN appuser a
ON od.appuser_id = a.appuser_id
WHERE od.submit_report= 1
AND rejected=0 
GROUP BY offence_detail_id			
ORDER BY  getDistanceBetweenPoints( inlat,	inlon, od.lat, od.lng)
		LIMIT 15;


END$$
DELIMITER ;

CALL get_nearest_complaints(19.104, 72.84);
  




  