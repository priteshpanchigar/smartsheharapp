DROP PROCEDURE IF EXISTS get_Emailid_Using_Postal_Code;
DELIMITER$$
CREATE  PROCEDURE `get_Emailid_Using_Postal_Code`(IN inoffencedetailid INT)
BEGIN

SELECT  'issues_smartshehar@yahoo.com' as email_id ,od.offence_type,od.offence_address, od.offence_time,
od.unique_key,od.ward ,ct.complaint_description as 'category' ,cs.complaint_sub_type_description as 'subcategory',
(SELECT count(offence_image_name)FROM offence_details od
LEFT  OUTER JOIN offence_image of ON od.offence_detail_id = of.offence_detail_id
where od.offence_detail_id = inoffencedetailid )'image_count' 
FROM offence_details od
INNER JOIN complaint_lookup_types ct ON od.complaint_type_code = ct.complaint_type_code
INNER JOIN complaint_lookup_subtypes cs ON od.complaint_sub_type_code = cs.complaint_sub_type_code
WHERE od.offence_detail_id = inoffencedetailid ; 

END$$
DELIMITER ;
call get_Emailid_Using_Postal_Code(25)
