DROP PROCEDURE IF EXISTS get_reason_code;
DELIMITER$$
CREATE  PROCEDURE `get_reason_code`() 
BEGIN
select rejected_id, reject_reason, reason_code from rejected_reason;

END$$
DELIMITER;

CALL get_reason_code();