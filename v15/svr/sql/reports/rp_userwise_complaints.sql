DROP PROCEDURE IF EXISTS `rp_userwise_complaints`;
DELIMITER $$
CREATE  PROCEDURE `rp_userwise_complaints`()
BEGIN

SELECT email, phoneno, username, fullname, age, sex, offence_detail_id, offence_address, offence_time, 
vehicle_no, submit_report, discard_report, ward,
(SELECT complaint_sub_type_description FROM complaint_lookup_subtypes ct2 
WHERE ct2.complaint_sub_type_code = vw_offence.complaint_sub_type_code) complaint_type_description,
(SELECT complaint_description FROM complaint_lookup_types ct2 
WHERE ct2.complaint_type_code = vw_offence.complaint_type_code) complaint_sub_type_description, municipal_department, lat, lng FROM vw_offence
GROUP BY offence_detail_id
ORDER BY email, offence_time;

END$$ 
DELIMITER;

CALL rp_userwise_complaints()