DROP PROCEDURE IF EXISTS `rp_total_offence`;
DELIMITER $$
CREATE  PROCEDURE `rp_total_offence`(IN infromdatetime datetime, IN intodatetime datetime)
BEGIN


SELECT COUNT(*) total, email, phoneno, username , fullname, age, sex FROM offence_details jd
INNER JOIN appuser a
ON jd.appuser_id = a.appuser_id
AND jd.offence_time BETWEEN infromdatetime AND intodatetime
GROUP BY  a.appuser_id
ORDER BY COUNT(*) DESC;

END$$ 
DELIMITER;

CALL rp_total_offence( '2015-10-05 00:00:00' , '2015-10-11 23:59:59')