DROP PROCEDURE IF EXISTS `rp_filter_pincodewise`;
DELIMITER $$
CREATE  PROCEDURE `rp_filter_pincodewise`(IN inpincode varchar(20),IN infromoffencedate datetime, IN intooffencedate datetime,
IN inTRPKG VARCHAR (20), IN inTRMVG VARCHAR (20), IN inTRVPO VARCHAR (20), IN inTRAUT VARCHAR (20),
IN inMUROD VARCHAR (20), IN inMUCLN VARCHAR (20), IN inMUINF VARCHAR (20), IN inMUENC VARCHAR (20),
IN inMUOSP VARCHAR (20), IN inmyissue INT, IN inappuserid INT, IN inlat DOUBLE , IN inlng DOUBLE, IN inlimit INT,
IN insubmitreport INT, IN inrejected INT, IN inclosed INT, IN inresolved INT,
IN inmaxlat DOUBLE , IN inmaxlng DOUBLE,IN inminlat DOUBLE , IN inminlng DOUBLE)
BEGIN
SET @sel = 'SELECT offence_detail_id, offence_address, offence_time, appuser_id, client_datetime, offence_type, vehicle_no, unique_key, submit_report, 
discard_report, address_id, location_id, ward,cs.complaint_type_code, cs.complaint_sub_type_code, cs.municipal_department, 
approved, rejected, approved_rejected_clientdatetime, sent_to_authority_datetime, resolved_unresolved_datetime, other_reason_code_comment, 
resolved, unresolved, unresolved_comment, reason_code, sent_to_authority,  lat, lng, 
(SELECT group_name FROM groupdetails 
	WHERE group_id = od.group_id) group_name,
(SELECT offence_image_name FROM offence_image oi
WHERE oi.offence_detail_id = od.offence_detail_id
GROUP BY offence_detail_id)offence_image_name,
(SELECT offence_image_path FROM offence_image oi
WHERE oi.offence_detail_id = od.offence_detail_id
GROUP BY offence_detail_id)offence_image_path  ';

#init vars
SET @whr = NULL;

SET @whr = CONCAT(IF(@whr IS NULL, '', CONCAT(@whr, '\n AND ')), ' submit_report = ', insubmitreport);

SET @whr = CONCAT(IF(@whr IS NULL, '', CONCAT(@whr, '\n AND ')), ' rejected = ', inrejected);

SET @whr = CONCAT(IF(@whr IS NULL, '', CONCAT(@whr, '\n AND ')), ' closed = ', inclosed);

SET @whr = CONCAT(IF(@whr IS NULL, '', CONCAT(@whr, '\n AND ')), ' resolved = ', inresolved);

SET @whr = IF(inmyissue = 0 , @whr, CONCAT(IF(@whr IS NULL, '', CONCAT(@whr, '\n AND ')), ' appuser_id = ', inappuserid));

SET @whr = IF(inpincode IS NULL, @whr, CONCAT(IF(@whr IS NULL, '', CONCAT(@whr, '\n AND ')), ' LOCATE("', inpincode, '", offence_address)'));

SET @whr = IF(inmaxlat IS NULL OR inmaxlng IS NULL OR inminlng IS NULL OR inminlng IS NULL, @whr, 
							CONCAT(IF(@whr IS NULL,'',CONCAT(@whr, '\n AND ')),	
								' lat BETWEEN ', inminlat, ' AND ', inmaxlat,'', 
						' AND lng BETWEEN ', inminlng, ' AND ', inmaxlng,''));

SET @whr = IF(infromoffencedate IS NULL AND intooffencedate IS NULL , @whr, 
		CONCAT(IF(@whr IS NULL,'',CONCAT(@whr, '\n AND ')),	
						' offence_time BETWEEN "', infromoffencedate, '" AND "', intooffencedate,'"'));

IF inTRPKG IS NOT NULL OR inTRMVG IS NOT NULL OR inTRVPO IS NOT NULL OR inTRAUT IS NOT NULL OR inMUROD IS NOT NULL
OR inMUCLN IS NOT NULL OR inMUINF IS NOT NULL OR inMUENC IS NOT NULL OR inMUOSP IS NOT NULL
	THEN 
		
		SET @whr = CONCAT(IF(@whr IS NULL, '', CONCAT(@whr, '\n AND ')),
					' od.complaint_type_code IN( "', 
																											 IFNULL(inTRPKG, ''),'" ,"', IFNULL(inTRMVG, ''),
																							 '" ,"', IFNULL(inTRVPO, ''),'" ,"', IFNULL(inTRAUT, ''),
																							 '" ,"', IFNULL(inMUROD, ''),'" ,"', IFNULL(inMUCLN, ''),
																							 '" ,"', IFNULL(inMUINF, ''),'" ,"', IFNULL(inMUENC, ''),
																							 '" ,"', IFNULL(inMUOSP, ''),'")');	 

END IF;


# ORDER BY has to come after all @whr are done
SET @orderby = NULL;
IF inlat is NOT NULL AND inlng  IS  NOT NULL
	THEN SET @orderby = CONCAT('  getDistanceBetweenPoints(', inlat, ', ', inlng, ', od.lat, od.lng)');
END IF;


SET @dynquery = CONCAT(@sel, ' FROM offence_details  od 
							   LEFT JOIN complaint_lookup_subtypes cs 
							   ON od.complaint_sub_type_code = cs.complaint_sub_type_code \n', 
			IFNULL(CONCAT(' WHERE ', @whr),''),
			IFNULL(CONCAT('\n ORDER BY', @orderby),''), 
			IFNULL(CONCAT('\n LIMIT ', inlimit),''));

#SELECT @dynquery;
PREPARE stmt FROM @dynquery; 
EXECUTE stmt; 
DEALLOCATE PREPARE stmt; 

END$$ 
DELIMITER;

CALL rp_filter_pincodewise(NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,2,NULL,NULL,NULL,1,0,0,0,
	'19.359511848','73.025531955','18.919950076','72.753613293')