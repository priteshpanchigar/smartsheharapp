
DROP PROCEDURE IF EXISTS pb31_train;
DELIMITER$$
CREATE  PROCEDURE `pb31_train`()
BEGIN
	

#MAKE SCHEDULE#

DROP TABLE IF EXISTS t_schedule;
CREATE TABLE t_schedule(
schedule_id INT not null auto_increment PRIMARY key,
train_id int,
trainno varchar(100) ,
traincode varchar(100) DEFAULT '-',
traintime DECIMAL(5,2),
linecode varchar(10) ,
line_id int,
stationno int,
stationcode varchar(10) ,
station_id int,
directioncode varchar(5) ,
traintimeadj DECIMAL(5,2),
longtraincode varchar (100),
splcode varchar(5),
sundayonly int,
holiday INT,
notonsunday INT,
car varchar(20),
emu VARCHAR(10),
timemin int
);

CREATE INDEX t_schedule_longtraincode
on t_schedule (longtraincode);

INSERT INTO t_schedule(traincode,trainno,traintime,linecode,stationcode,directioncode,splcode,sundayonly,holiday,notonsunday,car,emu)
SELECT traincode,trainno,traintime,linecode,stationcode,directioncode,splcode,sundayonly,holiday,notonsunday,car,emu 
from tempsched 
ORDER BY linecode,directioncode,trainno;


UPDATE t_schedule
SET longtraincode=CONCAT(trainno,traincode,linecode,directioncode);

DROP TABLE IF EXISTS tt_schedule;
CREATE TABLE tt_schedule as
SELECT schedule_id, trainno,longtraincode, IF(traintime>=0 AND traintime<4, traintime+24, traintime) traintimeadj
 FROM t_schedule WHERE  longtraincode IN 
(SELECT longtraincode FROM t_schedule WHERE traintime BETWEEN 23 AND 23.59
  AND longtraincode IN
 (SELECT longtraincode FROM t_schedule WHERE traintime BETWEEN 0 AND 1))
 ORDER BY longtraincode, traintimeadj;

CREATE INDEX t_schedule_schedule_id
on t_schedule (schedule_id);

UPDATE t_schedule
set traintimeadj=(SELECT b.traintimeadj from tt_schedule b 
									WHERE t_schedule.schedule_id = b.schedule_id);

UPDATE t_schedule
SET traintimeadj=traintime 
WHERE traintimeadj is NULL;

UPDATE t_schedule
SET traintimeadj = traintimeadj+24 
WHERE traintimeadj 
BETWEEN 0 and 3.15 AND train_id!=2190;

UPDATE t_schedule
SET stationcode = UPPER(stationcode);


UPDATE t_schedule
SET timemin=(FLOOR(traintime) * 60) + (traintime % 60 - FLOOR(traintime))*100;

END$$
DELIMITER ;

CALL pb31_train;