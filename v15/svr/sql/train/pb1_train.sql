
DROP PROCEDURE IF EXISTS pb1_train;
DELIMITER$$
CREATE PROCEDURE `pb1_train`()
BEGIN
#procedure pb1_train 
#updates all flags like sundayonly, ladies special etc.. from manually entered SCHEDULE.
#makes all the columns null in schedule table
#create all empty data table structure eg;train, route, routedetail
UPDATE wrup
SET sundayonly=null;
UPDATE wrup
SET notonsunday=null;
UPDATE wrup
SET holiday=null;


UPDATE wrdown
SET sundayonly=null;
UPDATE wrdown
SET notonsunday=null;
UPDATE wrdown
SET holiday=null;



UPDATE crup
SET sundayonly=null;
UPDATE crup
SET notonsunday=null;
UPDATE crup
SET holiday=null;



UPDATE crdown
SET sundayonly=null;
UPDATE crdown
SET notonsunday=null;
UPDATE crdown
SET holiday=null;



UPDATE hrdown
SET sundayonly=null;
UPDATE hrdown
SET notonsunday=null;
UPDATE hrdown
SET holiday=null;




UPDATE hrup
SET sundayonly=null;
UPDATE hrup
SET notonsunday=null;
UPDATE hrup
SET holiday=null;




UPDATE thdown
SET sundayonly=null;
UPDATE thdown
SET notonsunday=null;
UPDATE thdown
SET holiday=null;



UPDATE thup
SET sundayonly=null;
UPDATE thup
SET notonsunday=null;
UPDATE thup
SET holiday=null;
	

UPDATE nsup
SET sundayonly=null;
UPDATE nsup
SET notonsunday=null;
UPDATE nsup
SET holiday=null;


UPDATE nsdown
SET sundayonly=null;
UPDATE nsdown
SET notonsunday=null;
UPDATE nsdown
SET holiday=null;


UPDATE shup
SET sundayonly=null;
UPDATE shup
SET notonsunday=null;
UPDATE shup
SET holiday=null;


UPDATE shdown
SET sundayonly=null;
UPDATE shdown
SET notonsunday=null;
UPDATE shdown
SET holiday=null;



UPDATE wrdown
SET splcode=NULL;
UPDATE wrup
SET splcode=NULL;


UPDATE crdown
SET splcode=NULL;
UPDATE crup
SET splcode=NULL;

UPDATE hrdown
SET splcode=NULL;
UPDATE hrup
SET splcode=NULL;


UPDATE thdown
SET splcode=NULL;
UPDATE thup
SET splcode=NULL;





#SUNDAYONLY#

UPDATE wrup
SET sundayonly= CASE WHEN locate('$',traincode) THEN 1 
								else 0 end;
UPDATE wrdown
SET sundayonly= CASE WHEN locate('$',traincode) THEN 1 
								else 0 end;

#NOTONSUNDAY#

UPDATE wrup
SET notonsunday= CASE WHEN locate('*',traincode) THEN 1 
								 else 0 end;
UPDATE wrdown
SET notonsunday= CASE WHEN locate('*',traincode) THEN 1 
								 else 0 end;
UPDATE crup
SET notonsunday= CASE WHEN locate('*',trainno) THEN 1 
								 else 0 end;
UPDATE crdown
SET notonsunday= CASE WHEN locate('*',trainno) THEN 1 
								 else 0 end;
UPDATE hrup
SET notonsunday= CASE WHEN locate('*',trainno) THEN 1 
								 else 0 end;
UPDATE hrdown
SET notonsunday= CASE WHEN locate('*',trainno) THEN 1 
								 else 0 end;
UPDATE thup
SET notonsunday= CASE WHEN locate('*',trainno) THEN 1 
								 else 0 end;
UPDATE thdown
SET notonsunday= CASE WHEN locate('*',trainno) THEN 1 
								 else 0 end;



#CAR#

UPDATE wrup
SET car= CASE WHEN locate('#',traincode) THEN 12 
							WHEN locate('@',traincode) THEN 15
							else 9 end;

UPDATE wrdown
SET car= CASE WHEN locate('#',traincode) THEN 12 
							WHEN locate('@',traincode) THEN 15
							else 9 end;
UPDATE crup 
SET car=CASE WHEN locate('#',REPLACE(trainno,' ', '')) THEN 12
						 WHEN locate('@',REPLACE(trainno,' ' ,'')) THEN 15
						 WHEN locate('9CAR', REPLACE(trainno, ' ','')) THEN 9
						 else -1 end;

UPDATE crdown 
SET car=CASE WHEN locate('#',REPLACE(trainno,' ', '')) THEN 12
						 WHEN locate('@',REPLACE(trainno,' ' ,'')) THEN 15
						 WHEN locate('9CAR', REPLACE(trainno, ' ','')) THEN 9
						 else -1 end;

UPDATE hrdown
SET car=9;

UPDATE hrup
SET car=9;

UPDATE metdown
SET car=4;

UPDATE metup
SET car=4;


UPDATE metup
SET traincode = REPLACE(traincode, '!', '');


UPDATE metdown
SET traincode = REPLACE(traincode, '!', '');


#SPLCODE#

UPDATE wrdown
SET splcode='LS' WHERE LOCATE('%',Traincode);

UPDATE wrup
SET splcode='LS' WHERE LOCATE('%',Traincode);

UPDATE crdown
SET splcode='LS' WHERE LOCATE('%',trainno);

UPDATE crup
SET splcode='LS' WHERE LOCATE('%',trainno);

UPDATE hrdown
SET splcode='LS' WHERE LOCATE('%',trainno);

UPDATE hrup
SET splcode='LS' WHERE LOCATE('%',trainno);

UPDATE thdown
SET splcode='LS' WHERE LOCATE('%',trainno);

UPDATE thup
SET splcode='LS' WHERE LOCATE('%',trainno);

#DIRECTIONCODE#

UPDATE wrup
SET directioncode='U';

UPDATE wrdown
SET directioncode='D';

UPDATE crup
SET directioncode='U';

UPDATE crdown
SET directioncode='D';

UPDATE hrup
SET directioncode='U';

UPDATE hrdown
SET directioncode='D';

UPDATE thup
SET directioncode='U';

UPDATE thdown
SET directioncode='D';

UPDATE nsup
SET directioncode='U';

UPDATE nsdown
SET directioncode='D';

UPDATE shup
SET directioncode='U';

UPDATE shdown
SET directioncode='D';


#LINECODE#

UPDATE wrup
SET linecode='WR';

UPDATE wrdown
SET linecode='WR';

UPDATE crup
SET linecode='CR';

UPDATE crdown
SET linecode='CR';

UPDATE hrup
SET linecode='HR';

UPDATE hrdown
SET linecode='HR';

UPDATE thup
SET linecode='TH';

UPDATE thdown
SET linecode='TH';

UPDATE nsup
SET linecode='NS';

UPDATE nsdown
SET linecode='NS';

UPDATE shup
SET linecode='SH';

UPDATE shdown
SET linecode='SH';

#CREATING TABLE STRUCTURES#

drop table if EXISTS t_train;
CREATE TABLE t_train(
train_id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
longtraincode varchar(200),
linecode varchar(10)  not null ,
traincode varchar (100)not null DEFAULT '-',
trainno varchar(100)not null,
splcode varchar(5)  not null DEFAULT '-',
route_id INT,
directioncode varchar(5) not null,
sundayonly int,
holiday int,
notonsunday int,
car VARCHAR(20),
emu VARCHAR(10),
line_id int
);

drop table if EXISTS t_routedetail;
create TABLE t_routedetail(
routedetail_id INT NOT null AUTO_INCREMENT PRIMARY KEY,
route_id int,
linecode varchar(10) not null ,
line_id int,
station_id int,
directioncode varchar(3) not null ,
trainspeedcode varchar(6) ,
stationcode varchar(10) not null ,
stationserial int,
indicatorspeedcode varchar(10) ,
platformno varchar(10) ,
platformside varchar(10) ,
stage varchar(10) ,
km varchar(10) ,
routestations varchar (500)
);


drop table if EXISTS t_route;
CREATE table t_route (
route_id int,
linecode varchar(10) not null ,
line_id int,
directioncode varchar(5) not null ,
trainspeedcode varchar(5),
firststationcode varchar(10),
laststationcode varchar(10),
firststation_id INT,
laststation_id int,
routestations varchar(500),
towardsstationcode VARCHAR(25),
towardsstation_id int
);

DROP TABLE IF EXISTS t_line;
CREATE TABLE t_line (
line_id int not null AUTO_INCREMENT PRIMARY KEY,
linecode varchar(10) not null ,
abbr varchar(10) not null ,
description varchar(30), 
colour varchar(30)
);

INSERT INTO t_line(linecode,abbr,description,colour)
SELECT linecode,abbr,description,colour from line;


DROP TABLE IF EXISTS t_speed;
CREATE TABLE t_speed(
trainspeed_id int not null AUTO_INCREMENT PRIMARY KEY,
trainspeedcode varchar(10) not null ,
abbr varchar (5)  not null ,
description varchar(30) not null 
);

INSERT INTO t_speed(trainspeedcode,abbr,description)
SELECT trainspeedcode,abbr,description FROM speed;


DROP TABLE IF EXISTS t_station;
CREATE TABLE `t_station` (
  `station_id` int(11) DEFAULT NULL,
  `stationname` varchar(30) NOT NULL,
  `stationcode` varchar(10) NOT NULL,
  `lat` DOUBLE DEFAULT NULL,
  `lon` DOUBLE DEFAULT NULL,
  `stnabbr` varchar(10) DEFAULT NULL,
  UNIQUE KEY `st_station_id` (`station_id`)
);

INSERT INTO t_station(station_id,stationname,stationcode,lat,lon,stnabbr)
SELECT station_id,stationname,stationcode,lat,lon,stnabbr FROM station;


DROP TABLE IF EXISTS t_direction;
CREATE TABLE t_direction (
direction_id int AUTO_INCREMENT primary KEY,
linecode varchar(10) not null , 
line_id int,
directioncode varchar(10) not null 
);

INSERT INTO t_direction(linecode,directioncode)
SELECT linecode,directioncode FROM direction;
 
DROP TABLE IF EXISTS tempsched;
CREATE TABLE tempsched (
trainno varchar(100),
traincode varchar(100),
traintime varchar(10),
linecode varchar(10),
stationcode varchar(10) ,
directioncode varchar(5),
splcode varchar(5),
sundayonly int,
holiday int,
notonsunday int,
car VARCHAR(20),
emu VARCHAR(10)
);
END$$
DELIMITER ;

CALL pb1_train;