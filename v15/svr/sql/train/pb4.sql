CREATE DEFINER=`smartshehar`@`%` PROCEDURE `pb4_train`()
BEGIN
DROP TABLE if EXISTS ta_towardsstation;

-- Add the towards stations from the route table
CREATE TABLE ta_towardsstation AS
SELECT station_id, UPPER(rd.stationcode) stationcode, r.linecode, laststation_id,
r.towardsstation_id, rd.directioncode, r.towardsstationcode AS towardsstationcode
FROM t_routedetail rd 
INNER JOIN t_route r ON r.route_id = rd.route_id 
AND rd.station_id != r.laststation_id AND r.towardsstationcode != rd.stationcode
AND ((towardsstationcode='PN' OR 
towardsstationcode='ST' OR towardsstationcode='KP' OR
towardsstationcode='N' OR towardsstationcode='DN') OR
(towardsstationcode='C' AND r.linecode = 'WR') OR
(towardsstationcode='DN' AND r.linecode = 'WR') OR
(towardsstationcode ='ADH' AND r.linecode = 'HR') OR
(towardsstationcode='T' AND r.linecode = 'TH') OR
(towardsstationcode='VA' AND r.linecode = 'TH') OR
(towardsstationcode='ROH' AND r.linecode = 'KR') OR
(towardsstationcode='DJ' AND r.linecode = 'KR') OR
(towardsstationcode='PN' AND r.linecode = 'NS') OR
(towardsstationcode='DJ' AND r.linecode = 'NS') OR
(towardsstationcode='BSR' AND r.linecode = 'NS') OR
(towardsstationcode='D' AND r.linecode = 'SH') OR
(towardsstationcode='DN' AND r.linecode = 'SH')OR
(towardsstationcode='VER' AND r.linecode = 'MET') OR
(towardsstationcode='G' AND r.linecode = 'MET')OR
(towardsstationcode='WAD' AND r.linecode = 'MON') OR
(towardsstationcode='CM' AND r.linecode = 'MON'))
GROUP BY rd.station_id, rd.linecode, towardsstationcode
ORDER BY rd.station_id, rd.linecode, towardsstationcode;




-- Merge column to group towards stations
ALTER TABLE ta_towardsstation
	ADD mrg VARCHAR(15);
-- Initialize to towardsstationcode - default all towards stations separate
UPDATE ta_towardsstation
	SET mrg = towardsstationcode;

-- Stations on WR and HR between Wadala Road and Andheri towards Andheriabd Vurar
UPDATE ta_towardsstation
	SET mrg='M1'  
	WHERE (towardsstationcode='ADH' OR towardsstationcode='DN') AND 
	 (stationcode='VP' OR stationcode='SC' OR stationcode='KR' 
		OR stationcode='BA' OR stationcode='MJ');

-- Stations on WR and HR between Andheri and Wadala Road towards CST and Panvel
UPDATE ta_towardsstation
	SET mrg='M2'  
	WHERE (towardsstationcode='PN' OR towardsstationcode='ST') 
		AND ( stationcode='MJ' OR stationcode='KI');


UPDATE ta_towardsstation
	SET mrg='M3'  
	WHERE (towardsstationcode='PN' OR towardsstationcode='ST' OR towardsstationcode='C') 
		AND ( stationcode='VP' OR stationcode='SC' OR stationcode='KR' );



UPDATE ta_towardsstation
	SET mrg='M4'  
	WHERE (towardsstationcode='PN' OR towardsstationcode='ST' OR towardsstationcode='C' OR towardsstationcode='D') 
		AND ( stationcode='ADH' OR stationcode='BA' );



-- Station on HR between CST and Wadala towards Andheri and Panvel
UPDATE ta_towardsstation
	SET mrg='M1'  
	WHERE (towardsstationcode='ADH' OR towardsstationcode='PN') 
		AND (stationcode='ST' OR stationcode='MB' OR stationcode='SR' OR stationcode='DR' 
			OR stationcode='RR' OR stationcode='CG' OR stationcode='SW');

-- Station between Panvel and Wadala towards CST and Andheri
UPDATE ta_towardsstation
	SET mrg='M1'  
	WHERE (towardsstationcode='ADH' OR towardsstationcode='ST') 
		AND (stationcode='PN' OR stationcode='KW' OR stationcode='MS'
		OR stationcode='KG' OR stationcode='BU' OR stationcode='SD'
		OR stationcode='NU' OR stationcode='JN' 
		OR stationcode='VA' OR stationcode='MK' OR stationcode='GV'
		OR stationcode='CM' OR stationcode='TN' OR stationcode='KU'
		OR stationcode='CB' OR stationcode='GN');

-- TH special stations
UPDATE ta_towardsstation
	SET mrg='M1'  
	WHERE linecode = 'TH' AND (towardsstationcode='VA' OR towardsstationcode='PN') 
		AND (stationcode='T' OR stationcode='AR' OR stationcode='RB'
		OR stationcode='GS' OR stationcode='KK');

-- For Sanpada
UPDATE ta_towardsstation
	SET mrg='M1'  
	WHERE stationcode='SP' AND
	(towardsstationcode='ADH' OR towardsstationcode='ST'
		OR towardsstationcode='VA');

-- Between CST and Kalyan towards Kasara and Khopoli
UPDATE ta_towardsstation
	SET mrg='M2'  WHERE (towardsstationcode='N' OR towardsstationcode='KP') 
	AND( stationcode='ST' OR stationcode='MB'             
	OR stationcode='SR' OR stationcode='BY' OR stationcode='CP' 
	OR stationcode='CU' OR stationcode='PR' 
	OR stationcode='D' OR stationcode='MT' 
	OR stationcode='SI' OR stationcode='KU' 
	OR stationcode='VV' OR stationcode='G' 
	OR stationcode='VK' OR stationcode='KJ'
	OR stationcode='BP' OR stationcode='NH'
	OR stationcode='MU' OR stationcode='T'
	OR stationcode='KL' OR stationcode='MM'
	OR stationcode='DJ' OR stationcode='LK'
	OR stationcode='DI' OR stationcode='TK');




-- Between BSR and DIVA towards DIVA and PANVEL
UPDATE ta_towardsstation
	SET mrg='M2'  WHERE (towardsstationcode='PN' OR towardsstationcode='DJ') 
	AND( stationcode='BSR' OR stationcode='JC'             
	OR stationcode='KM' OR stationcode='KBH' OR stationcode='BR');
	
-- Between PANVEL and DIVA towards DIVA and VASAI
UPDATE ta_towardsstation
	SET mrg='M3'  WHERE (towardsstationcode='BSR' OR towardsstationcode='DJ') 
	AND( stationcode='PN' OR stationcode='KB'             
	OR stationcode='NR' OR stationcode='TL' OR stationcode='NJ');
	

-- Between PANVEL and DIVA towards PANVEL and ROHA
UPDATE ta_towardsstation
	SET mrg='M4'  WHERE (towardsstationcode='ROH' OR towardsstationcode='PN') 
	AND( stationcode='KB' OR stationcode='NR' 
	OR stationcode='TL' OR stationcode='NJ' OR stationcode='DT');


UPDATE ta_towardsstation
SET mrg='M1'
WHERE stationcode='DI' AND linecode='NS'
AND (towardsstationcode='BSR');


-- Station between Panvel and Juinagar towards CST,Andheri and Thane
UPDATE ta_towardsstation
	SET mrg='M1'  
	WHERE (towardsstationcode='ADH' OR towardsstationcode='ST' OR towardsstationcode='T' ) 
		AND (stationcode='PN' OR stationcode='KW' OR stationcode='MS'
		OR stationcode='KG' OR stationcode='BU' OR stationcode='SD'
		OR stationcode='NU');


-- Station between CST and SANDHURST ROAD towards Andheri,PANVEL AND KASARA,KHOPOLI
UPDATE ta_towardsstation
	SET mrg='M1'  
	WHERE (towardsstationcode='ADH' OR towardsstationcode='PN' OR towardsstationcode='N' OR towardsstationcode='KP') 
		AND (stationcode='ST' OR stationcode='MB' );

-- Stations on WR and SH between Dahanu and Dadar towards Churchgate & Dadar

UPDATE ta_towardsstation
       SET mrg='M3'  
       WHERE (towardsstationcode='C' OR towardsstationcode='D') AND 
        ( stationcode='BVI'
			   OR stationcode='BYR' OR stationcode='BSR'  OR stationcode='NS' OR stationcode='VR' 
			   OR stationcode='VT' OR stationcode='SA' OR stationcode='KV' 
			   OR stationcode='PG' OR stationcode='UM' OR stationcode='BS'
			   OR stationcode='VN' OR stationcode='DN');

-- Stations on WR and SH between Churchgate and Dahanu towards Dahanu WR & Dahanu SH
UPDATE ta_towardsstation
       SET mrg='M1'  
       WHERE (towardsstationcode='DN') AND 
        (stationcode='C' OR stationcode='BC' OR stationcode='D' 
               OR stationcode='BA' OR stationcode='ADH'OR stationcode='BVI'
			   OR stationcode='BYR' OR stationcode='BSR' OR stationcode='VR' 
			   OR stationcode='VT' OR stationcode='SA' OR stationcode='KV' 
			   OR stationcode='PG' OR stationcode='UM' OR stationcode='BS'
			   OR stationcode='VN');

DROP TABLE if EXISTS ta_stationline ;
CREATE TABLE ta_stationline (
stationline_id  INTEGER NOT NULL auto_increment primary key,
stationcode  VARCHAR(10) NOT NULL,
linecode  VARCHAR(10) NOT NULL,
line_id  INTEGER,
station_id  INTEGER
);

INSERT INTO ta_stationline(stationcode,linecode,line_id,station_id)
SELECT stationcode,linecode,
(SELECT line_id from t_line l WHERE l.linecode=t_schedule.linecode)as line_id,
(SELECT station_id from t_station s WHERE s.stationcode=t_schedule.stationcode)as station_id from t_schedule
GROUP BY stationcode,linecode
ORDER BY linecode;


#DROP TABLE IF EXISTS tt5_routedetail;
#DROP TABLE IF EXISTS tt_route;
#DROP TABLE IF EXISTS tt_train;

DROP TABLE IF EXISTS t_stationconnections;
CREATE TABLE `t_stationconnections` (
	`stationconnections_id` int(11) NOT NULL AUTO_INCREMENT  PRIMARY KEY ,
  `startstationcode` varchar(255) DEFAULT NULL,
  `deststationcode` varchar(255) DEFAULT NULL,
  `connection` varchar(255) DEFAULT NULL,
  `reverse` varchar(255) DEFAULT NULL,
  `linecode` varchar(255) DEFAULT NULL,
  `tripleconnection` varchar(255) DEFAULT NULL,
  `connection2` varchar(255) DEFAULT NULL,
  `direct` varchar(255) DEFAULT NULL,
  `samelinechange` varchar(255) DEFAULT NULL,
  `diffrentlinechange` varchar(255) DEFAULT NULL,
  `sort` varchar(255) DEFAULT NULL,
  `priority` varchar(255) DEFAULT NULL,  
  `startstation_id` int(11) DEFAULT NULL,
  `deststation_id` int(11) DEFAULT NULL,
  `connection_id` int(11) DEFAULT NULL
) ENGINE=InnoDB;


INSERT into t_stationconnections(startstationcode, deststationcode, `connection`, reverse, linecode, tripleconnection,
	connection2, direct, samelinechange, diffrentlinechange, sort, priority)
SELECT startstationcode, deststationcode, `connection`, reverse, linecode, tripleconnection,
	connection2, direct, samelinechange, diffrentlinechange, sort, priority from stationconnections;

INSERT into t_stationconnections(startstationcode, deststationcode, `connection`, reverse, linecode, tripleconnection,
	connection2, direct, samelinechange, diffrentlinechange, sort, priority)
SELECT startstationcode, deststationcode, `connection`, reverse, linecode, tripleconnection,
	connection2, direct, samelinechange, diffrentlinechange, sort, priority from metrostationconnections;

INSERT into t_stationconnections(startstationcode, deststationcode, `connection`, reverse, linecode, tripleconnection,
	connection2, direct, samelinechange, diffrentlinechange, sort, priority)
SELECT startstationcode, deststationcode, `connection`, reverse, linecode, tripleconnection,
	connection2, direct, samelinechange, diffrentlinechange, sort, priority from monostationconnections;	


UPDATE t_stationconnections
SET `CONNECTION`= null 
WHERE LENGTH(`connection`)=0;

UPDATE t_stationconnections
SET `CONNECTION`= null 
WHERE `connection`='-';

DELETE from t_stationconnections
WHERE LENGTH(tripleconnection)>0;



DELETE from t_stationconnections
WHERE priority > 3;


#DELETE from stationconnections
#WHERE LENGTH(`reverse`)>0;


UPDATE t_stationconnections ts
SET startstation_id=(SELECT station_id from t_station 
WHERE ts.startstationcode= t_station.stationcode);

UPDATE t_stationconnections ts
SET deststation_id=(SELECT station_id from t_station 
WHERE ts.deststationcode= t_station.stationcode);

UPDATE t_stationconnections ts
SET connection_id=(SELECT station_id from t_station 
WHERE ts.`connection`= t_station.stationcode);

UPDATE t_stationconnections
SET connection_id= 0 WHERE connection_id is null;



DROP TABLE IF EXISTS ta_stationconnections;
CREATE TABLE ta_stationconnections as
SELECT stationconnections_id,linecode,startstation_id,deststation_id,connection_id,priority from t_stationconnections;


DROP TABLE IF EXISTS ta_station;
CREATE TABLE IF NOT EXISTS ta_station(
station_id int, 
stationname text(25), 
stationcode text(10), 
lat real(8,6), 
lon real(8,6),
stnabbr text (10));

INSERT INTO ta_station(station_id,stationname,stationcode,lat,lon,stnabbr)
SELECT  station_id,IFNULL(stationname, '-'), IFNULL(stationcode , '-'),IFNULL(lat , -1),IFNULL(lon , -1),stnabbr FROM t_station;


DROP TABLE IF EXISTS ta_train;
CREATE TABLE IF NOT EXISTS ta_train(
train_id int,
linecode text(7),
emu text(10),
directioncode text(5),
traincode text(30),
trainno text(30),
car text(20),
splcode text(3), 
route_id int,
sundayonly int, 
holiday int, 
notonsunday int,
line_id INT);

INSERT INTO ta_train(train_id,linecode,emu,directioncode,traincode,trainno,car,splcode,route_id,sundayonly,holiday,notonsunday,line_id) 
SELECT train_id,linecode,ifnull(emu,' '),directioncode,traincode,trainno,ifnull(car,'  '),IFNULL(splcode, ' ' ),route_id,
IF(sundayonly>0,1,0),IF(holiday>0,1,0),IF(notonsunday>0,1,0),line_id from `t_train`;


DROP TABLE IF EXISTS ta_direction;
CREATE TABLE IF NOT EXISTS ta_direction(
direction_id int, 
line_id int, 
directioncode text(10), 
linecode text(10));

INSERT INTO ta_direction(direction_id,line_id,directioncode,linecode)
SELECT direction_id,line_id,directioncode,linecode	from  t_direction ;


DROP TABLE IF EXISTS ta_speed;
CREATE TABLE IF NOT EXISTS ta_speed(
trainspeedcode  TEXT(10),
abbr  TEXT(5),
description  TEXT(30));

INSERT INTO ta_speed( trainspeedcode, abbr, description)
SELECT trainspeedcode,abbr,description from t_speed;


DROP TABLE IF EXISTS ta_line;
CREATE TABLE IF NOT EXISTS ta_line(
line_id  INT(11),
linecode TEXT(10),
abbr  TEXT(20),
colour text(30),
description  TEXT(30));

INSERT INTO ta_line(line_id,linecode,abbr,colour,description)
SELECT	line_id,linecode,abbr,colour,description from t_line;


DROP TABLE IF EXISTS ta_routedetail;
CREATE TABLE IF NOT EXISTS ta_routedetail(
routedetail_id  INTEGER(11),
route_id INTEGER(11),
line_id text(6),
directioncode text(5),
station_id INTEGER(11),
stationserial INTEGER(11),
indicatorspeedcode text(5),
platformno  TEXT(10),
platformside TEXT(10));

INSERT INTO ta_routedetail(routedetail_id, route_id,line_id,directioncode, station_id,stationserial,indicatorspeedcode,platformno, platformside )
SELECT 	routedetail_id,route_id,line_id,directioncode,ifnull(station_id,-1),stationserial,IFNULL(indicatorspeedcode, '-'),IFNULL(platformno, '-'),
IFNULL(platformside, '-') from t_routedetail;



DROP TABLE IF EXISTS ta_route;
CREATE TABLE IF NOT EXISTS ta_route(
route_id  INTEGER(11),
line_id int,
directioncode text(5),
trainspeedcode text (11),
firststation_id int,
towardsstation_id int,
towardsstation_id2 int,
laststation_id int );

INSERT INTO ta_route(route_id,line_id,directioncode,trainspeedcode,firststation_id,towardsstation_id,towardsstation_id2,laststation_id )
SELECT	route_id,line_id,directioncode,ifnull(trainspeedcode, '-'),ifnull(firststation_id,-1),ifnull(towardsstation_id,-1),ifnull(towardsstation_id2,-1),
ifnull(laststation_id,-1) from t_route;


DROP TABLE IF EXISTS ta_schedule; 
CREATE TABLE IF NOT EXISTS ta_schedule(
train_id int,
traintime DECIMAL(4,2),
station_id int,
mins int);

INSERT into ta_schedule(train_id,traintime,station_id,mins) 
SELECT train_id, traintimeadj,ifnull(station_id,-1), mins from t_schedule ; 


DELETE from ta_station 
WHERE stationcode='LK';

DELETE from ta_station 
WHERE stationcode='KO';

INSERT INTO ta_station(station_id,stationname,stationcode,lat,lon,stnabbr)
VALUES    (125,'Lower Kopar/Kopar',	'LKO',	19.211071,	73.077503,	'LKO');

DELETE  from ta_stationline
where stationcode = 'LK';

DELETE  from ta_stationline
where stationcode = 'KO';

INSERT INTO ta_stationline(stationcode,linecode,line_id,station_id)
VALUES    ('LKO','CR',2,125);

INSERT INTO ta_stationline(stationcode,linecode,line_id,station_id)
VALUES    ('LKO','NS',5,125);

UPDATE ta_schedule
SET station_id = 125 WHERE station_id = 50 or station_id = 109;

UPDATE ta_routedetail
SET station_id = 125 WHERE station_id = 50 or station_id = 109;

UPDATE ta_towardsstation
SET stationcode = 'LKO' WHERE stationcode ='LK';

UPDATE ta_towardsstation
SET stationcode = 'LKO' WHERE stationcode ='KO';

UPDATE ta_towardsstation
SET station_id = 125 WHERE station_id=50;

UPDATE ta_towardsstation
SET station_id= 125 WHERE station_id =109;


UPDATE ta_stationconnections
SET startstation_id = 125 WHERE startstation_id=109;

UPDATE ta_stationconnections
SET deststation_id = 125 WHERE deststation_id=109;

UPDATE ta_stationconnections
SET connection_id = 125 WHERE connection_id=109;


alter TABLE ta_schedule
DROP traintime;



UPDATE ta_train
SET traincode = replace(replace(replace(replace(replace(REPLACE(REPLACE(REPLACE(traincode,'#',''),'*',''),'9 CAR',''),'@',''),'%',''),'$',''),'-',''), '!', '');

UPDATE ta_train
SET trainno = replace(replace(replace(replace(replace(REPLACE(REPLACE(trainno,'#',''),'*',''),'9 CAR',''),'@',''),'%',''),'$',''),'-','');

END