DROP PROCEDURE IF EXISTS pb32_train;
DELIMITER$$
CREATE  PROCEDURE `pb32_train`()
BEGIN
	

#TEMPORARY TABLES#

drop table if EXISTS tt_train;

CREATE TABLE tt_train(
train_id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
linecode nvarchar(10)  not null DEFAULT '-' ,
traincode nvarchar (100)not null DEFAULT '-',
trainno nvarchar(100)not null DEFAULT '-',
routestations nvarchar(500),
route_id INT,
directioncode nvarchar(5) not null DEFAULT '-',
longtraincode nvarchar (100),
splcode VARCHAR(5),
sundayonly INT,
holiday INT,
notonsunday INT,
car VARCHAR(20),
emu varchar(10)
);


INSERT INTO tt_train(trainno,traincode,splcode,sundayonly,holiday,notonsunday,car,emu,linecode,directioncode,routestations,longtraincode)
select trainno,traincode,splcode,sundayonly,holiday,notonsunday,car,emu,linecode,directioncode,GROUP_CONCAT(UPPER(stationcode) 
ORDER BY trainno, traintimeadj, UPPER(stationcode)) as routestations,longtraincode
FROM t_schedule
GROUP BY longtraincode;


DROP TABLE IF EXISTS tt_route;
CREATE TABLE tt_route(
`route_id` int(11) not null AUTO_INcrEMENT PRIMARY KEY,
 train_id int,
 trainno varchar(100),
 traincode varchar(100),
`linecode` varchar(20) ,
 `directioncode` varchar(10) ,
 `routestations` varchar(500),
 longtraincode VARCHAR(500)
);	

INSERT INTO tt_route(trainno,traincode,directioncode,linecode,routestations,longtraincode)
SELECT trainno,traincode,directioncode,linecode, routestations,longtraincode FROM tt_train
GROUP BY linecode,routestations ORDER BY routestations;


UPDATE tt_train
SET route_id=(SELECT route_id from tt_route
 WHERE tt_train.routestations = tt_route.routestations
AND tt_train.linecode = tt_route.linecode);


DROP TABLE IF EXISTS  tt_routedetail;
CREATE TABLE tt_routedetail 
(routedetail_id  int not null auto_increment PRIMARY key) 
SELECT tt_route.route_id as route_id,tt_route.linecode as linecode,tt_route.directioncode as directioncode,
stationcode,traintimeadj,tt_route.trainno,tt_route.traincode,tt_route.routestations from t_schedule
INNER JOIN tt_route on t_schedule.longtraincode=tt_route.longtraincode
ORDER BY linecode,directioncode,t_schedule.trainno,t_schedule.traincode,traintimeadj;

ALTER table tt_routedetail
ADD stationserial int,
ADD trainspeedcode VARCHAR(5),
ADD indicatorspeedcode VARCHAR(5),
ADD platformno VARCHAR(5),
ADD platformside VARCHAR(5);


set @n=0;
set @rid=0;

DROP TABLE IF EXISTS t;
CREATE TABLE t as

select routedetail_id,route_id,linecode,directioncode,stationcode,trainno, 
@n:=if(@rid!=route_id, 1, @n+1)as stationserial, @rid:=if(@rid<route_id, route_id, route_id) as dummy from tt_routedetail;


UPDATE tt_routedetail
SET stationserial=(SELECT stationserial from t
where t.routedetail_id = tt_routedetail.routedetail_id);

DROP TABLE IF EXISTS t;

END$$
DELIMITER ;

CALL pb32_train;