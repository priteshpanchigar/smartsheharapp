DROP PROCEDURE IF EXISTS upgrade_app_notification;
DELIMITER $$
CREATE  PROCEDURE `upgrade_app_notification`(IN inCurrentVersion INT)
BEGIN

# Sending gcm notification code
	SELECT  a.gcm_registration_id, email as emailid
		FROM appuser a
		INNER JOIN appusage au ON 
			a.appuser_id = au.appuser_id AND
			(au.versioncode < inCurrentVersion
			OR au.versioncode IS NULL);

END$$
DELIMITER ;
# Sample query
call upgrade_app_notification(43);