DROP VIEW IF EXISTS vw_offer;
CREATE VIEW vw_offer AS
SELECT o.offer_id, oc.offer_category_id, o.vendor_id, offers, offer_description, offer_creationdate, actual_price, 
discount_percentage, discount_amount,offer_starttime, offer_endtime, `comment`,
offer_category_name, offer_category_description FROM l_offer_category oc
INNER JOIN l_offer o
ON o.offer_category_id = oc.offer_category_id
INNER JOIN l_offer_image oi
ON oi.offer_id = o.offer_id;