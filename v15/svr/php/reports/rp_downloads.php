<?php
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
$php_name = 'rpDownloads';
include("../dbconn_sar_apk.php"); 
include("../mobile_common_data_sar.php");
if ($mysqli) {
	$firstclientdatetime = isset($_REQUEST['firstclientdatetime']) ?
	"\"" . $_REQUEST['firstclientdatetime'] . "\"" : 'NULL';
	$lastclientdatetime = isset($_REQUEST['lastclientdatetime']) ?
	"\"" . $_REQUEST['lastclientdatetime'] . "\"" : 'NULL';
	$sql= " call rp_downloads(" . $firstclientdatetime. "," . $lastclientdatetime . ")";
	if ($verbose != 'N') {
		echo $sql . '<br>';
	}
	echo "<br>downloads";
	if ($result = $mysqli->query($sql)) {
		echo "<table class='table table-striped'> <thead><tr><th>appuser_id</th>
									<th>email</th>
									<th>phoneno</th>
									
									<th>lat</th>
									<th>lng</th>
									<th>manufacturer</th>
									<th>android_release_version</th></tr> </thead>";
		while ($row = $result->fetch_assoc()) {
			echo "<tr>";
			echo "<td>" . $row['appuser_id'] . "</td>";
			echo "<td>" . $row['email'] . "</td>";
			echo "<td>" . $row['phoneno'] . "</td>";
			echo "<td>" . $row['lat'] . "</td>";
			echo "<td>" . $row['lng'] . "</td>";
			echo "<td>" . $row['manufacturer'] . "</td>";
			echo "<td>" . $row['android_release_version'] . "</td>";
			
			
			
		}
		echo "</table><br>";
		
		$result->free(); // free result set
	}
	$mysqli->close(); // close connection
}