<?php
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
$php_name = 'rpUserWiseComplaints';
include("../dbconn_sar_apk.php"); 
include("../mobile_common_data_sar.php");
if ($mysqli) {
	$sql= " call rp_userwise_complaints()";
	if ($verbose != 'N') {
		echo $sql . '<br>';
	}
	
	echo "<br>Total Userwise Complaints";
	if ($result = $mysqli->query($sql)) {
		echo "<table class='table table-striped'><thead><tr><th>email</th>
									<th>phoneno</th>
									<th>username</th>
									<th>fullname</th>
									<th>age</th>
									<th>sex</th>
									<th>offence_detail_id</th>
									<th>offence_address</th>
									<th>offence_time</th>
									<th>vehicle_no</th>
									<th>submit_report</th>
									<th>discard_report</th>
									<th>lat</th>
									<th>lng</th>
									<th>complaint_type_description</th>
									<th>complaint_sub_type_description</th></tr> </thead>";
		while ($row = $result->fetch_assoc()) {
			echo "<tr>";
			echo "<td>" . $row['email'] . "</td>";
			echo "<td>" . $row['phoneno'] . "</td>";
			echo "<td>" . $row['username'] . "</td>";
			echo "<td>" . $row['fullname'] . "</td>";
			echo "<td>" . $row['age'] . "</td>";
			echo "<td>" . $row['sex'] . "</td>";
			echo "<td>" . $row['offence_detail_id'] . "</td>";
			echo "<td>" . $row['offence_address'] . "</td>";
			echo "<td>" . $row['offence_time'] . "</td>";
			echo "<td>" . $row['vehicle_no'] . "</td>";
			echo "<td>" . $row['submit_report'] . "</td>";
			echo "<td>" . $row['discard_report'] . "</td>";
			echo "<td>" . $row['lat'] . "</td>";
			echo "<td>" . $row['lng'] . "</td>";
			echo "<td>" . $row['complaint_type_description'] . "</td>";
			echo "<td>" . $row['complaint_sub_type_description'] . "</td>";
			
		}
		echo "</table><br>";
		
		$result->free(); // free result set
	}
	$mysqli->close(); // close connection
}