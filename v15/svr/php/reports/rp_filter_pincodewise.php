<?php 
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
$php_name = 'rpFilterPincodewise';
include("../mobile_common_data_sar.php");
include("../dbconn_sar_apk.php");
if ($mysqli) {
	$pincodecomplaintRows = array();
	
	
	$fromoffencedate = empty($_REQUEST['fromoffencedate']) || !isset($_REQUEST['fromoffencedate']) ? 'NULL' :
		"'" . $_REQUEST['fromoffencedate'] . "'" ;
	$tooffencedate = empty($_REQUEST['tooffencedate']) || !isset($_REQUEST['tooffencedate']) ? 'NULL' :
		"'" . $_REQUEST['tooffencedate'] . "'" ;
	$pincode = empty($_REQUEST['pincode']) || !isset($_REQUEST['pincode']) ? 'NULL' :
		"'" . $_REQUEST['pincode'] . "'" ;	
			
	
	$TRPKG = empty($_REQUEST['TRPKG']) || !isset($_REQUEST['TRPKG']) ? 'NULL' :
		"'" . $_REQUEST['TRPKG'] . "'" ;
	$TRMVG = empty($_REQUEST['TRMVG']) || !isset($_REQUEST['TRMVG']) ? 'NULL' :
		"'" . $_REQUEST['TRMVG'] . "'" ;
	$TRVPO = empty($_REQUEST['TRVPO']) || !isset($_REQUEST['TRVPO']) ? 'NULL' :
		"'" . $_REQUEST['TRVPO'] . "'" ;		
	$TRAUT = empty($_REQUEST['TRAUT']) || !isset($_REQUEST['TRAUT']) ? 'NULL' :
		"'" . $_REQUEST['TRAUT'] . "'" ;
	$MUROD = empty($_REQUEST['MUROD']) || !isset($_REQUEST['MUROD']) ? 'NULL' :
		"'" . $_REQUEST['MUROD'] . "'" ;
	$MUCLN = empty($_REQUEST['MUCLN']) || !isset($_REQUEST['MUCLN']) ? 'NULL' :
		"'" . $_REQUEST['MUCLN'] . "'" ;
	$MUINF = empty($_REQUEST['MUINF']) || !isset($_REQUEST['MUINF']) ? 'NULL' :
		"'" . $_REQUEST['MUINF'] . "'" ;
	$MUENC = empty($_REQUEST['MUENC']) || !isset($_REQUEST['MUENC']) ? 'NULL' :
		"'" . $_REQUEST['MUENC'] . "'" ;
	$MUOSP = empty($_REQUEST['MUOSP']) || !isset($_REQUEST['MUOSP']) ? 'NULL' :
		"'" . $_REQUEST['MUOSP'] . "'" ;
	
	$myissue = empty($_REQUEST['myissue']) || !isset($_REQUEST['myissue']) ? 0 :
		"'" . $_REQUEST['myissue'] . "'" ;
	$markerlat = empty($_REQUEST['markerlat']) || !isset($_REQUEST['markerlat']) ? 'NULL' :
		"'" . $_REQUEST['markerlat'] . "'" ;
	$markerlng = empty($_REQUEST['markerlng']) || !isset($_REQUEST['markerlng']) ? 'NULL' :
		"'" . $_REQUEST['markerlng'] . "'" ;
	
	$submitreport = empty($_REQUEST['submitreport']) || !isset($_REQUEST['submitreport']) ? 1 :
		"'" . $_REQUEST['submitreport'] . "'" ;
	
	$rejected = empty($_REQUEST['rejected']) || !isset($_REQUEST['rejected']) ? 0 :
		"'" . $_REQUEST['rejected'] . "'" ;
	
	$closed = empty($_REQUEST['closed']) || !isset($_REQUEST['closed']) ? 0 :
		"'" . $_REQUEST['closed'] . "'" ;
		
	$resolved = empty($_REQUEST['resolved']) || !isset($_REQUEST['resolved']) ? 0 :
		"'" . $_REQUEST['resolved'] . "'" ;
		
	$limit = empty($_REQUEST['limit']) || !isset($_REQUEST['limit']) ? 'NULL' :
		"'" . $_REQUEST['limit'] . "'" ;	
	
	$maxlat = empty($_REQUEST['maxlat']) || !isset($_REQUEST['maxlat']) ? 'NULL' :
		"'" . $_REQUEST['maxlat'] . "'" ;
	$maxlng = empty($_REQUEST['maxlng']) || !isset($_REQUEST['maxlng']) ? 'NULL' :
		"'" . $_REQUEST['maxlng'] . "'" ;
	$minlat = empty($_REQUEST['minlat']) || !isset($_REQUEST['minlat']) ? 'NULL' :
		"'" . $_REQUEST['minlat'] . "'" ;		
	$minlng = empty($_REQUEST['minlng']) || !isset($_REQUEST['minlng']) ? 'NULL' :
		"'" . $_REQUEST['minlng'] . "'" ;
	
	
	$sql = " call rp_filter_pincodewise(".$pincode."," .$fromoffencedate.
		   "," .$tooffencedate."," . $TRPKG."," . $TRMVG."," . $TRVPO."," . $TRAUT. "," . $MUROD.
		   "," . $MUCLN."," . $MUINF."," . $MUENC."," . $MUOSP. "," . $myissue."," . $appuserid.
		   "," . $markerlat."," . $markerlng. "," . $limit. "," . $submitreport. "," . $rejected.
		   "," . $closed. "," . $resolved. "," . $maxlat. "," . $maxlng.
		   "," . $minlat. "," . $minlng.")";
		
		
	if ($verbose != 'N') {
		echo $sql . '<br>';
	}	
	if ($result = $mysqli->query($sql)) {
		while ($row = $result->fetch_assoc()) {
			$pincodecomplaintRows[] = $row;			
		}		
		$result->free();	// free result set
	}
	$mysqli->close();		// close connection
	
	if (count($pincodecomplaintRows) > 0) {
		echo json_encode($pincodecomplaintRows);
	} else {
		echo "-1";
	}
}
