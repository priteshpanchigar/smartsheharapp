<?php
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
$php_name = 'rejectedIssueEmail';
include("../dbconn_sar_apk.php");
include("../mobile_common_data_sar.php");
include("../sendmail.php");
$memberemail = "";
$name = "";
$message = isset($_REQUEST['message']) ? $_REQUEST['message']  : 'NULL';
$email = isset($_REQUEST['email']) ? $_REQUEST['email']  : 'NULL';
$offenceimagefilename = isset($_REQUEST['offenceimagefilename']) ? $_REQUEST['offenceimagefilename']  : 'NULL';
$subject = isset($_REQUEST['subject']) ?  $_REQUEST['subject'] : 'NULL';
$offencedetailid = isset($_REQUEST['offencedetailid']) ?  $_REQUEST['offencedetailid'] : 'NULL';

$cc = $email ." , issues_smartshehar@yahoo.com";
$bcc =  "issues_smartshehar@yahoo.com";
$offenceimagefilename;
if($mysqli)
{	$sql = "call rejected_issue_email(" .$offencedetailid . ")";
	if ($verbose != 'N') {
		echo '<br> sql ' . $sql . '<br>';
		
	}
	$result = $mysqli->query($sql);
	if(is_object($result)) {	
		while ($row = $result->fetch_assoc()) {
			$memberemail = $row["email_id"];
			
			$subject = "Issue Rejected " . $row['offence_type'] . " at " . $row['offence_address'] ;
			$message = "Dear Sir/Madam, <br>Your  report has been Rejected because " . $row['reject_reason'] . " (" . $row['offence_type'] .
			") at " . $row['offence_address']. " at " . $row['offence_time'] .
			".<br><br>Photo of the issue is attached. <br>" .
			"<br>Thank You, <br>" .
			"SmartShehar Team ";
			$offenceimagefilename = 'images/'. $row['offence_image_name'];
			
			break;
		}
		$mysqli->close();
	}
}else{
	echo "connection failed";
}
if (!empty($memberemail)) {
	$arr = explode(",", $memberemail);
	foreach($arr as $emailid ) {
		sendMail($emailid, $name, $cc, $subject, $message, $offenceimagefilename,$bcc);
	}
}