<?php
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
$php_name = 'deleteCancelledImage';
include("../dbconn_sar_apk.php");
include("../mobile_common_data_sar.php");
$errors = array();
if ( $mysqli ) {
	
	$offencedetailid = isset($_REQUEST['offencedetailid']) ? $_REQUEST['offencedetailid'] : 'NULL';
	$creationdatetime = isset($_REQUEST['creationdatetime']) ? 
		"\"" . $_REQUEST['creationdatetime'] . "\"" : 'NULL';
	
	$sql = "call delete_cancelled_image(" .$offencedetailid .",". $creationdatetime . ")";
	if ($verbose != 'N') {
		echo $sql;
	}
	if ($result = $mysqli->query($sql)) {
		if ($verbose != 'N') {
			echo "<br>sql result: ";
			var_dump($result);
			echo "<br>";
		}
	}
	if ($result && is_object($result))  {
		$rowcount=mysqli_num_rows($result);
		if ($verbose != 'N') {
			echo "rowcount: " .$rowcount . "<br>";
		}
		if ($rowcount > 0) {
			while ($row = $result->fetch_assoc()) {
				$resultrows[] = $row;
				//echo json_encode($row);
				break;
			}
		}
		$result->free();
	}$mysqli->close();
}