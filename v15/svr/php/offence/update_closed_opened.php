<?php
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
$php_name = 'updateClosedOpened';
include("../dbconn_sar_apk.php"); 
include("../mobile_common_data_sar.php");
$errors = array();
if ( $mysqli ) {
		
	$closed = isset($_REQUEST['closed']) ? $_REQUEST['closed'] : 'NULL';
	
	$opened = isset($_REQUEST['opened']) ? $_REQUEST['opened'] : 'NULL';
	
	$offencedetailid = isset($_REQUEST['offencedetailid']) ? $_REQUEST['offencedetailid'] : 'NULL';
	
	$closedopenedcomment = isset($_REQUEST['closedopenedcomment']) ? "\"" . $_REQUEST['closedopenedcomment'] . "\"" : 'NULL';
	
	$clientdatetime = isset($_REQUEST['clientdatetime']) ? "\"" . $_REQUEST['clientdatetime'] . "\"" : 'NULL';	
	
	$sql = "call update_closed_opened(" .$closed .  ",".  $opened .  
	",". $offencedetailid . ",". $closedopenedcomment . ",". $clientdatetime .
	",". $appuserid .")";
	if ($verbose != 'N') {
		echo $sql;
	}  
	if ($result = $mysqli->query($sql)) {	
		while ($row = $result->fetch_assoc()) {  
			echo json_encode($row);
			break;
		} 
		$mysqli->close();
	}		else {    
		echo "-1"; // something went wrong, probably sql failed
	}
	 
	$message_type = 'Issue Status';
	if($closed == '1' && $opened == '0')
	{
		$message_type = 'The complaint has been marked for closure';
		
	}
	if($closed == '0' && $opened == '1')
	{
		$message_type = 'Issue is still open';
		}
	include("issue_closed_notification.php"); 
} else {
	echo "-2"; // "Connection to db failed";
}