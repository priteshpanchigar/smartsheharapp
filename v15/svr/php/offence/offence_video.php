<?php 
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
$php_name = 'offenceVideo';
include("../dbconn_sar_apk.php");
include("../mobile_common_data_sar.php"); 
$errors = array();
$unregistered = false;
$offencevideofilename=NULL;
$imsrc = '';

if(isset($_REQUEST['offencevideofilename']) &&!empty( $_REQUEST['offencevideofilename'])){
	$offencevideofilename = $_REQUEST['offencevideofilename'];
	if ($verbose != 'N') {
		echo '<br>offencevideofilename: ' .$_REQUEST['offencevideofilename'];
	}	
	$imsrc = base64_decode($_REQUEST['offencevideo']);
	$fp = fopen('videos/' . $offencevideofilename , 'w');
	
	fwrite($fp, $imsrc);
	if(fclose($fp)){
		echo "Video uploaded";
	}else{
		echo "Error uploading Video";
	}
	
} else {
	$offencevideofilename= 'NULL';
}

if ( $mysqli ) {
	
	
	
	$offencevideofilename = empty($_REQUEST['offencevideofilename']) || 
		!isset($_REQUEST['offencevideofilename']) ? 'NULL' : 
		"'" . $_REQUEST['offencevideofilename'] . "'" ;
	$offencevideofilepath= "'/videos/'" ;
		
		
	$offencedetailid = isset($_REQUEST['offencedetailid']) ? $_REQUEST['offencedetailid'] : 'NULL';	
		
	$creationdatetime = isset($_REQUEST['creationdatetime']) ? "\"" . $_REQUEST['creationdatetime'] . "\"" : 'NULL';	
	
	
	$sql = "call offence_video(" . $appuserid . 
		", " . $creationdatetime . ", " . $offencedetailid . ", " . $offencevideofilepath . 
		", " .$offencevideofilename .")";
	if ($verbose != 'N') {
		echo '<br>sql:<br>' . $sql;
	}	
	if ($result = $mysqli->query($sql)) {	
		if ($result && is_object($result))  {
			while ($row = $result->fetch_assoc()) {
				echo json_encode($row);
				break;
			} 
		}
		$mysqli->close();
	}		else {
		echo "-1"; // something went wrong, probably sql failed
	}
	
} else {
	echo "-2"; // "Connection to db failed";
}?>