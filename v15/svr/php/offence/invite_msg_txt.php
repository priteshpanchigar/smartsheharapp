<?php 
$inviteMessage = <<<EOD
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
      <title>JUmpinJumpout-Mailer</title>
      <style type="text/css">
         /* Client-specific Styles */
         #outlook a {padding:0;} /* Force Outlook to provide a "view in browser" menu link. */
         body{width:100% !important; -webkit-text-size-adjust:100%; -ms-text-size-adjust:100%; margin:0; padding:0;}
         /* Prevent Webkit and Windows Mobile platforms from changing default font sizes, while not breaking desktop design. */
         .ExternalClass {width:100%;} /* Force Hotmail to display emails at full width */
         .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height: 100%;} /* Force Hotmail to display normal line spacing.  */
         #backgroundTable {margin:0; padding:0; width:100% !important; line-height: 100% !important;}
         img {outline:none; text-decoration:none;border:none; -ms-interpolation-mode: bicubic;}
         a img {border:none;}
         .image_fix {display:block;}
         p {margin: 0px 0px !important;}
         table td {border-collapse: collapse;}
         table { border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; }
         a {color: #FF0000;text-decoration: none;text-decoration:none!important;}
         /*STYLES*/
         table[class=full] { width: 100%; clear: both; }
         /*IPAD STYLES*/
         @media only screen and (max-width: 640px) {
         a[href^="tel"], a[href^="sms"] {
         text-decoration: none;
         color: #9ec459; /* or whatever your want */
         pointer-events: none;
         cursor: default;
         }
         .mobile_link a[href^="tel"], .mobile_link a[href^="sms"] {
         text-decoration: default;
         color: #9ec459 !important;
         pointer-events: auto;
         cursor: default;
         }
         table[class=devicewidth] {width: 440px!important;text-align:center!important;}
         td[class=devicewidth] {width: 440px!important;text-align:center!important;}
         img[class=devicewidth] {width: 440px!important;text-align:center!important;}
         img[class=banner] {width: 440px!important;height:147px!important;}
         table[class=devicewidthinner] {width: 420px!important;text-align:center!important;}
         table[class=icontext] {width: 345px!important;text-align:center!important;}
         img[class="colimg2"] {width:420px!important;height:243px!important;}
         table[class="emhide"]{display: none!important;}
         img[class="logo"]{width:440px!important;height:110px!important;}
         
         }
         /*IPHONE STYLES*/
         @media only screen and (max-width: 480px) {
         a[href^="tel"], a[href^="sms"] {
         text-decoration: none;
         color: #9ec459; /* or whatever your want */
         pointer-events: none;
         cursor: default;
         }
         .mobile_link a[href^="tel"], .mobile_link a[href^="sms"] {
         text-decoration: default;
         color: #9ec459 !important; 
         pointer-events: auto;
         cursor: default;
         }
         table[class=devicewidth] {width: 280px!important;text-align:center!important;}
         td[class=devicewidth] {width: 280px!important;text-align:center!important;}
         img[class=devicewidth] {width: 280px!important;text-align:center!important;}
         img[class=banner] {width: 280px!important;height:93px!important;}
         table[class=devicewidthinner] {width: 260px!important;text-align:center!important;}
         table[class=icontext] {width: 186px!important;text-align:center!important;}
         img[class="colimg2"] {width:260px!important;height:150px!important;}
         table[class="emhide"]{display: none!important;}
         img[class="logo"]{width:280px!important;height:70px!important;}
        
         }
.style1 {color: #006600}
      .style2 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 15px;
	font-style: italic;
}
      .style7 {font-size: 18px; font-style: italic; }
      .style12 {color: #9EC459}
      .style13 {
	font-family: Arial, Helvetica, sans-serif;
	color: #FFFFFF;
	font-size: 13px;
}
      </style>
   </head>
   <body>
<div align="center">
  <!-- Start of preheader -->
  </td>
  </tr>
  </tbody>
  </table>
  </td>
  </tr>
  </tbody>
  </table>
  <!-- End of preheader --> 
  <!-- Start of LOGO -->
  
</div>
<tbody>
      <tr>
         <td>
           <div align="center">
             <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
               <tbody>
                 <tr>
                   <td width="100%">
                     <!-- end of image -->                    </td>
                  </tr>
                 </tbody>
              </table>
         </div></td>
  </tr>
   </tbody>
<div align="center">
  </table>
  </td>
  </tr>
  </tbody>
  </table>
  <!-- End of LOGO -->    
  <!-- start textbox-with-title -->
</div>
<tbody>
      <tr>
         <td align="center"><div align="center">
           <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
             <tbody>
               <tr>
                 <td width="100%" height="379">
                   <table width="600" border="1" align="center" cellpadding="0" cellspacing="0" class="devicewidth">
  <tbody>
    <!-- Spacing -->
    
    <!-- Spacing -->
    <tr>
      <td>
        
        <table bgcolor="#e8eaed" width="600" align="center" cellspacing="0" cellpadding="0" border="0" class="devicewidth">
          <tbody>
            <tr>
              <!-- start of image -->
              <td height="159" align="center" valign="middle" bgcolor="#9EC459">
                <a href="#" target="_blank" class="style12"></a>
                <table width="600" border="0" cellpadding="0" cellspacing="0" bgcolor="#00121C" class="devicewidth">
                  <tbody>
                    <!-- Spacing -->
                    
                    <!-- Spacing -->
                    <tr>
                      <td height="59"><table width="580" border="0" align="center" cellpadding="0" cellspacing="0" class="devicewidthinner">
                        <tbody>
                          <tr>
                            <td width="216" align="left" valign="middle" style="font-family: Helvetica, arial, sans-serif; font-size: 13px;color: #ffffff">&nbsp;</td>
                                             <td width="143" align="left" valign="middle" style="font-family: Helvetica, arial, sans-serif; font-size: 13px;color: #ffffff"><a href="https://play.google.com/store/apps/details?id=com.jumpinjumpout.apk"><img src="http://jumpinjumpout.com/emailers/2015_02/welcome/download.png" alt="Download" width="129" height="45" /></a></td>
                                             <td width="221" align="right" valign="middle" style="font-family: Helvetica, arial, sans-serif; font-size: 13px;color: #ffffff">&nbsp;</td>
                                           </tr>
                          </tbody>
                        </table></td>
                                   </tr>
                    <!-- Spacing -->
                    
                    <!-- Spacing -->
                    </tbody>
                  </table>                                    <a href="http://jumpinjumpout.com/" target="_blank" class="style12"><img src="http://jumpinjumpout.com/emailers/2015_02/welcome/logo.png" alt="Logo" width="600" height="100" border="0" align="absmiddle" class="logo" style="display:block; border:none; outline:none; text-decoration:none;" /></a> </td>
                           </tr>
            </tbody>
          </table>                      </td>
                                </tr>
    <tr align="center" valign="top">
      <td height="335">
        <table width="600" height="333" border="0" align="center" cellpadding="0" cellspacing="0" class="devicewidthinner">
  <tbody>
    <!-- Title -->
    <tr>
      <td height="28" style="font-family: Helvetica, arial, sans-serif; font-size: 14px; font-weight:bold; color: #333333; text-align:left;line-height: 24px;">&nbsp;</td>
                               <td height="28" colspan="2" valign="bottom" style="font-family: Helvetica, arial, sans-serif; font-size: 14px; font-weight:bold; color: #333333; text-align:left;line-height: 24px;"><span class="style7">Dear $membername, </span></td>
                              <td height="28" style="font-family: Helvetica, arial, sans-serif; font-size: 14px; font-weight:bold; color: #333333; text-align:left;line-height: 24px;">&nbsp;</td>
                              <td height="28" style="font-family: Helvetica, arial, sans-serif; font-size: 14px; font-weight:bold; color: #333333; text-align:left;line-height: 24px;">&nbsp;</td>
                            </tr>
    <!-- End of Title -->
    <!-- spacing -->
    
    <!-- End of spacing -->
    <!-- content -->
    <tr>
      <td height="78" valign="top" style="font-family: Helvetica, arial, sans-serif; font-size: 13px; color: #333333; text-align:left;line-height: 24px;"><p><br />
        </p>                                              </td>
                              <td height="78" colspan="2" valign="top" style="font-family: Helvetica, arial, sans-serif; font-size: 13px; color: #333333; text-align:left;line-height: 24px;"><p><span class="style7">Your friend <b>$name</b> wants you to use Jump.In.Jump.Out for<br />
                                    ridesharing. Join the fast growing JiJo community by<br />
                                    downloading the app today. </span></p>
                              <p><span class="style7">See you around...</span></p></td>
                              <td width="79" align="right" valign="middle" style="font-family: Helvetica, arial, sans-serif; font-size: 13px; color: #333333; text-align:left;line-height: 24px;">&nbsp;</td>
                              <td width="14" rowspan="2" align="right" valign="middle" style="font-family: Helvetica, arial, sans-serif; font-size: 13px; color: #333333; text-align:left;line-height: 24px;">&nbsp;</td>
                                           </tr>
    <tr>
      <td width="13" height="33" align="right" valign="top" bgcolor="#FFFFFF" style="font-family: Helvetica, arial, sans-serif; font-size: 13px; color: #333333; text-align:left;line-height: 24px;"><div align="right"></div></td>
                              <td width="451" align="right" valign="top" bgcolor="#FFFFFF" style="font-family: Helvetica, arial, sans-serif; font-size: 13px; color: #333333; text-align:left;line-height: 24px;"><div align="right"><span class="style2">~Jump.In.Jump.Out team</span></div></td>
                              <td width="39" align="right" valign="top" bgcolor="#FFFFFF" style="font-family: Helvetica, arial, sans-serif; font-size: 13px; color: #333333; text-align:left;line-height: 24px;">&nbsp;</td>
                              <td width="79" align="right" valign="middle" style="font-family: Helvetica, arial, sans-serif; font-size: 13px; color: #333333; text-align:left;line-height: 24px;">&nbsp;</td>
                                            </tr>
    
    <tr bgcolor="#00121C">
      <td height="36" colspan="5" align="center" valign="top" style="font-family: Helvetica, arial, sans-serif; font-size: 13px; color: #00121C; text-align:left;line-height: 24px;"><div align="center"></div></td>
                                            </tr>
    
    
    <tr bgcolor="#9ec459">
      <td height="48" colspan="5" bgcolor="#9ec459" style="font-family: Helvetica, arial, sans-serif; font-size: 13px; color: #333333; text-align:left;line-height: 24px;"><div align="center"><strong>Find out more at <a href="http://www.jumpinjumpout.com" target="_blank" class="style1">www.jumpinjumpout.com</a>. Feel free to write to us  at <br /><a href="mailto:info@jumpinjumpout.com" target="_blank" class="style1">info@jumpinjumpout.com</a> with feedback, comments,  suggestions.&nbsp;</strong></div></td>
                            </tr>
    
    
    <!-- End of content -->
    <!-- Spacing -->
    <!-- Spacing -->
    <!-- button -->
    
    <!-- /button -->
    <!-- Spacing -->
    
    <tr>
      <td height="43" colspan="5" align="center" valign="middle" bgcolor="#FFFFFF"><div align="center"><img src="http://jumpinjumpout.com/emailers/2015_02/welcome/footer.png" alt="footer" width="589" height="34" /></div></td>
                            </tr>
    <tr>
      <td height="23" colspan="5" align="center" valign="middle" bgcolor="#00121C" style="font-family: Helvetica, arial, sans-serif; font-size: 13px;color: #ffffff">
        We respect your privacy and will not share your email details with third parties. Join the Drive!<p></p>                              </td>
                            </tr>
    <!-- Spacing -->
    </tbody>
          </table>                      </td>
                    </tr>
    </tbody>
                   </table>                 </td>
               </tr>
               </tbody>
            </table>
         </div></td>
      </tr>
   </tbody>
<div align="center">
  </table>
</div>
<div align="center">
  <!-- end of textbox-with-title -->
  <!-- Start of 2-columns -->
</div>
<tbody><tr>
  <td>&nbsp;</td>
</tr>
   </tbody>
</table>
<!-- End of postfooter -->
   </body>
   </html>
EOD;
?>