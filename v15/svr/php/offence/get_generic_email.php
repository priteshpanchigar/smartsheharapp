<?php
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
$php_name = 'get_generic_email';
include("../dbconn_sar_apk.php");
include("../mobile_common_data_sar.php");


if($mysqli)
{	$sql = "call get_Emailid_Using_Postal_Code(" .$offencedetailid . ")";
		
		   
	if ($verbose != 'N') {
		echo '<br> sql ' . $sql . '<br>';
		
	}
	$result = $mysqli->query($sql);
	
	if(is_object($result)) {
	
		while ($row = $result->fetch_assoc()) {
			$memberemail = $row['email_id'] ;  
			$subject = "Reporting an Issue  " . $row['offence_type'] . " at " . $row['offence_address'] ;
			$space = "";
			$date = "Date – ". $row['offence_time'] ;
			$to = "To:<br>".
				"Hon. Assistant Commissioner,<br>". 
				"Ward [". $row['ward'] .".]"; 
			$letter_subject="<b>Subject – Complaint from the residents in your ward:".
					"<br><br>&nbsp;&nbsp;&nbsp;&nbsp;Category – ". $row['category'].
					"<br>&nbsp;&nbsp;&nbsp;&nbsp;Sub-Category - ". $row['subcategory'] ."</b>";
			$image_count = $row['image_count'];
			if($image_count == '0')
			{
				$photo_attached="";
			}
			else{
				$photo_attached="<br><br>Photo of the issue is attached.";
			}
			
			$content = "Dear Sir,<br><br>In an effort to help our overstretched governing bodies, 
			on behalf of the residents residing in ward-[<b>". $row['ward'] ."</b>], we would like to bring your attention 
			to the following issue. ".
			"<br><br>Issue Category – <b>". $row['category'].
			"</b><br>Issue Sub-Category – <b>". $row['subcategory'].
			"</b><br>Issue Location  – <b>". $row['offence_address'].
			"</b><br>Issue Submission Time – <b>". $row['offence_time']."</b>".
			$photo_attached .
			"<br><br>The following issue was registered using the SmartShehar App, ".
			"and as responsible citizens, we are bringing it to your notice.". 
			"<br><br>We sincerely hope you will direct your staff to give serious ". 
			"attention to this issue and take immediate remedial action. ".
			"<br><br><br>Sincerely,";
			$message = $space."  ".$date.
						"<br>". $to .
						"<br><br>". $letter_subject .
						"<br><br>".$content;
			
			
			echo $message;
			break;
		}
		
		$mysqli->close();
	}
	
	$arr = explode(",", $memberemail);
	foreach($arr as $emailid ) {
		sendMail($emailid, $name, $cc, $subject, $message, $offenceimagefilename,$bcc);
	}
	

}else{
	echo "connection failed";
}

