<?php
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
$php_name = 'getBusLocation';
include("../dbconn_sar_apk.php"); 
include("../mobile_common_data_short.php");
if ($mysqli){
	$getbuslocationRows = array();
	
	$buslabel = empty($_REQUEST['buslabel']) || !isset($_REQUEST['buslabel']) ? 'NULL' : 
		"'" . $_REQUEST['buslabel'] . "'" ;
		
	$direction = empty($_REQUEST['direction']) || !isset($_REQUEST['direction']) ? 'NULL' : 
		"'" . $_REQUEST['direction'] . "'" ;
	
	
	
	$sql = " call get_bus_location(". $buslabel . "," .$clientdatetime . "," .$direction .")";
	if ($verbose != 'N') {
		echo $sql . '<br>';
	}	
	if ($result = $mysqli->query($sql)) {
		while ($row = $result->fetch_assoc()) {
			$getbuslocationRows[] = $row;			
		}		
		$result->free();	// free result set
	}
	$mysqli->close();		// close connection
	
	
		echo json_encode($getbuslocationRows);
	 
}else {
		echo "-1";
}