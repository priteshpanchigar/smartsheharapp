<?php header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
$php_name = 'UpgApp';
// echo $php_name;
include("mobile_common_data_sar.php");
include("gcmSendMessage.php");
$verbose = isset($_REQUEST['verbose']) ? 
		"'" . $_REQUEST['verbose'] . "'" : 'N';
$appUserId = isset($_REQUEST['appuserid']) ? $_REQUEST['appuserid']  : "0";
$versioncode = isset($_REQUEST['versioncode']) ? $_REQUEST['versioncode']  : "-1";
$resultrows = array();
if ($versioncode == -1) {
	echo 'Please enter the current versioncode for app as parameter';
	return;
}


	include("dbconn_sar_apk.php");
	if ($mysqli) {
		$sql =  "call upgrade_app_notification(" . $versioncode .  ")";
		if ($verbose != 'N') {
			echo $sql . '<br>';
		}
		$result = $mysqli->query($sql) or die(mysql_error());
		$rowcount=mysqli_num_rows($result);
		if ($verbose == 'Y') { 
			echo "<br>Result rows: count " . $rowcount;
			echo "<br>Result rows (vardump): ";
			var_dump($result);
			echo "<br>";
		}	
		if (!$result) {
			throw new Exception("Database Error [{$this->database->errno}] {$this->database->error}");
			$mysqli->close();
			return;
		}
		if ($rowcount == 0) {
			echo "<br> No Action Notification to be sent";
			return;
		}
		
		if ($result) {
			while ($row = $result->fetch_assoc()) {
				array_push($resultrows, $row['gcm_registration_id']);
			}
			$NOTIFICATION_CATEGORY = 'UA';			
			$NOTIFICATION_TYPE = 'NEW';
			$basemessage = "{\"notification_category\":" ."'". $NOTIFICATION_CATEGORY . "'" .
			", \"notification_type\":" ."'". $NOTIFICATION_TYPE . "'" .
			", \"versioncode\":" . $versioncode .	", \"message\":\"Upgrade\"}";	
			gcmSendMessage($resultrows, $basemessage);		
		}
		$mysqli->close();		// close connection 
	
	}

?>