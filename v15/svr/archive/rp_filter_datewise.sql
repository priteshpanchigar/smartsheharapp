DROP PROCEDURE IF EXISTS `rp_filter_datewise`;
DELIMITER $$
CREATE  PROCEDURE `rp_filter_datewise`(IN infromoffencedate datetime, IN intooffencedate datetime)
BEGIN

SELECT offence_detail_id, offence_address, offence_time, client_datetime, offence_type, vehicle_no, unique_key, submit_report, 
discard_report, address_id, location_id, ward, complaint_type_code, complaint_sub_type_code, colour, android_colour, municipal_department, 
approved, rejected, approved_rejected_clientdatetime, sent_to_authority_datetime, resolved_unresolved_datetime, other_reason_code_comment, 
resolved, unresolved, unresolved_comment, reason_code, sent_to_authority,  lat, lng, appuser_id, email, phoneno, 
username, fullname, age, sex, offence_image_id, offence_image_name, offence_image_path, creation_datetime FROM vw_offence
WHERE offence_time BETWEEN infromoffencedate AND intooffencedate;

END$$ 
DELIMITER;

CALL rp_filter_datewise('2016-01-01 11:33:38', '2016-01-20 11:33:38');
