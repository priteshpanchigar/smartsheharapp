DROP PROCEDURE IF EXISTS add_issue_rating;
DELIMITER $$
CREATE PROCEDURE `add_issue_rating`(IN inappuserid INT, IN inoffence_detail_id INT,
IN inissue_like INT,
IN inissue_rating_comment varchar(255),
IN inissue_rating_clientdatetime datetime)
BEGIN


UPDATE issue_rating
SET issue_like = inissue_like,
	issue_rating_comment = inissue_rating_comment,
	issue_rating_clientdatetime = inissue_rating_clientdatetime
WHERE appuser_id = inappuserid AND offence_detail_id = inoffence_detail_id;


END$$
DELIMITER;