<?php 
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
$php_name = 'rpCountIssues';
include("../mobile_common_data_sar.php");
include("../dbconn_sar_apk.php");
if ($mysqli) {
	$countcomplaintRows = array();
	
	$showall = isset($_REQUEST['showall']) ? $_REQUEST['showall'] : 'NULL';
	$myissue = isset($_REQUEST['myissue']) ? $_REQUEST['myissue'] : 'NULL';
	$TRPKG = empty($_REQUEST['TRPKG']) || !isset($_REQUEST['TRPKG']) ? 'NULL' :
		"'" . $_REQUEST['TRPKG'] . "'" ;
	$TRMVG = empty($_REQUEST['TRMVG']) || !isset($_REQUEST['TRMVG']) ? 'NULL' :
		"'" . $_REQUEST['TRMVG'] . "'" ;
	$TRVPO = empty($_REQUEST['TRVPO']) || !isset($_REQUEST['TRVPO']) ? 'NULL' :
		"'" . $_REQUEST['TRVPO'] . "'" ;		
	$TRAUT = empty($_REQUEST['TRAUT']) || !isset($_REQUEST['TRAUT']) ? 'NULL' :
		"'" . $_REQUEST['TRAUT'] . "'" ;
	$MUROD = empty($_REQUEST['MUROD']) || !isset($_REQUEST['MUROD']) ? 'NULL' :
		"'" . $_REQUEST['MUROD'] . "'" ;
	$MUCLN = empty($_REQUEST['MUCLN']) || !isset($_REQUEST['MUCLN']) ? 'NULL' :
		"'" . $_REQUEST['MUCLN'] . "'" ;
	$MUINF = empty($_REQUEST['MUINF']) || !isset($_REQUEST['MUINF']) ? 'NULL' :
		"'" . $_REQUEST['MUINF'] . "'" ;
	$MUENC = empty($_REQUEST['MUENC']) || !isset($_REQUEST['MUENC']) ? 'NULL' :
		"'" . $_REQUEST['MUENC'] . "'" ;
	$MUOSP = empty($_REQUEST['MUOSP']) || !isset($_REQUEST['MUOSP']) ? 'NULL' :
		"'" . $_REQUEST['MUOSP'] . "'" ;
	
	
	$sql = " call rp_count_issues(".$appuserid ."," . $showall.
		   "," . $TRPKG."," . $TRMVG."," . $TRVPO."," . $TRAUT. "," . $MUROD.
		   "," . $MUCLN."," . $MUINF."," . $MUENC."," . $MUOSP.",".$myissue.")";
	if ($verbose != 'N') {
		echo $sql . '<br>';
	}	
	if ($result = $mysqli->query($sql)) {
		while ($row = $result->fetch_assoc()) {
			$countcomplaintRows[] = $row;			
		}		
		$result->free();	// free result set
	}
	$mysqli->close();		// close connection
	
	if (count($countcomplaintRows) > 0) {
		echo json_encode($countcomplaintRows);
	} else {
		echo "-1";
	}
}
