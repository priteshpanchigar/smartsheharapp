<?php 
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
$php_name = 'rpFilterPincodewise';
include("../mobile_common_data_sar.php");
include("../dbconn_sar_apk.php");
if ($mysqli) {
	$datecomplaintRows = array();
	
	$fromoffencedate = isset($_REQUEST['fromoffencedate']) ? "\"" . $_REQUEST['fromoffencedate'] . "\"" : 'NULL';
	$tooffencedate = isset($_REQUEST['tooffencedate']) ? "\"" . $_REQUEST['tooffencedate'] . "\"" : 'NULL';
	
	$sql = " call rp_filter_datewise(" .$fromoffencedate. "," .$tooffencedate. ")";
	if ($verbose != 'N') {
		echo $sql . '<br>';
	}	
	if ($result = $mysqli->query($sql)) {
		while ($row = $result->fetch_assoc()) {
			$datecomplaintRows[] = $row;			
		}		
		$result->free();	// free result set
	}
	$mysqli->close();		// close connection
	
	if (count($datecomplaintRows) > 0) {
		echo json_encode($datecomplaintRows);
	} else {
		echo "-1";
	}
}
