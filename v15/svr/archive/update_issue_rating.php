<?php
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
$php_name = 'updateIssueRating';
include("../dbconn_sar_apk.php");
include("../mobile_common_data_sar.php");
if ( $mysqli ) {
	
	$issue_rating_like = isset($_REQUEST['issueratinglike']) ? $_REQUEST['issueratinglike'] : 'NULL';
	$issue_rating_comment = empty($_REQUEST['issueratingcomment']) || 
		!isset($_REQUEST['issueratingcomment']) ? 'NULL' : "'" . $_REQUEST['issueratingcomment'] . "'" ;
	
	$offence_detail_id = isset($_REQUEST['offencedetailid']) ? $_REQUEST['offencedetailid'] : 'NULL';
	
	$sql = "call update_issue_rating(" . $appuserid . "," . $offence_detail_id .
	"," . $issue_rating_like .
		"," . $issue_rating_comment . "," . $clientdatetime .")";
	
	if ($verbose != 'N') {
		echo '<br>sql:<br>' . $sql;
	}
	
	if ($result = $mysqli->query($sql)) {	
		if ($result && is_object($result))  {
			while ($row = $result->fetch_assoc()) {
				echo json_encode($row);
				break;
			}
		}
		$mysqli->close();
	}		else {
		echo "-1"; // something went wrong, probably sql failed
	}
} else {
	echo "-2"; // "Connection to db failed";
}