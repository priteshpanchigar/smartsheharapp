DROP PROCEDURE IF EXISTS filter_my_complaints_all_complaints;
DELIMITER$$
CREATE  PROCEDURE `filter_my_complaints_all_complaints`(IN `inappuserid` INT, IN `inshowall` INT, 
IN inTRPKG VARCHAR (20), IN inTRMVG VARCHAR (20), IN inTRVPO VARCHAR (20), IN inTRAUT VARCHAR (20),
IN inMUROD VARCHAR (20), IN inMUCLN VARCHAR (20), IN inMUINF VARCHAR (20), IN inMUENC VARCHAR (20),
IN inMUOSP VARCHAR (20),IN inmmyissue INT)
BEGIN
IF inshowall = 1  THEN 
SELECT offence_detail_id, offence_address, offence_time, client_datetime, offence_type, vehicle_no, unique_key, submit_report, 
discard_report, address_id, location_id, ward, complaint_type_code, complaint_sub_type_code, colour, android_colour, municipal_department, 
approved, rejected, approved_rejected_clientdatetime, sent_to_authority_datetime, resolved_unresolved_datetime, other_reason_code_comment, 
resolved, unresolved, unresolved_comment, reason_code, sent_to_authority,  lat, lng, appuser_id, email, phoneno, 
username, fullname, age, sex, offence_image_id, offence_image_name, offence_image_path, creation_datetime FROM vw_offence

WHERE   rejected=0 AND offence_image_id IN
			(SELECT  MIN(offence_image_id) offence_image_id from 
			offence_image
			GROUP BY offence_detail_id)
			order by offence_detail_id DESC;

ELSEIF inmmyissue = 1 THEN  
SELECT offence_detail_id, offence_address, offence_time, client_datetime, offence_type, vehicle_no, unique_key, submit_report, 
discard_report, address_id, location_id, ward, complaint_type_code, complaint_sub_type_code, colour, android_colour, municipal_department, 
approved, rejected, approved_rejected_clientdatetime, sent_to_authority_datetime, resolved_unresolved_datetime, other_reason_code_comment, 
resolved, unresolved, unresolved_comment, reason_code, sent_to_authority, lat, lng, appuser_id, email, phoneno, 
username, fullname, age, sex, offence_image_id, offence_image_name, offence_image_path, creation_datetime FROM vw_offence
WHERE 
 appuser_id = inappuserid AND rejected=0
AND offence_image_id IN
			(SELECT  MIN(offence_image_id) offence_image_id from 
			offence_image
			GROUP BY offence_detail_id)
			order by offence_detail_id DESC;

ELSEIF inmmyissue = 0 AND inshowall = 0 THEN
SELECT offence_detail_id, offence_address, offence_time, client_datetime, offence_type, vehicle_no, unique_key, submit_report, 
discard_report, address_id, location_id, ward, complaint_type_code, complaint_sub_type_code, colour, android_colour, municipal_department, 
approved, rejected, approved_rejected_clientdatetime, sent_to_authority_datetime, resolved_unresolved_datetime, other_reason_code_comment, 
resolved, unresolved, unresolved_comment, reason_code, sent_to_authority, lat, lng, appuser_id, email, phoneno, 
username, fullname, age, sex, offence_image_id, offence_image_name, offence_image_path, creation_datetime FROM vw_offence
WHERE  complaint_type_code IN(inTRPKG, inTRMVG, inTRVPO, inTRAUT, inMUROD, inMUCLN, inMUINF, inMUENC, inMUOSP)
AND  rejected=0 AND offence_image_id IN
			(SELECT  MIN(offence_image_id) offence_image_id from 
			offence_image
			GROUP BY offence_detail_id)
			order by offence_detail_id DESC;





END IF;
END$$
DELIMITER ;

call filter_my_complaints_all_complaints(2,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1);

--------------------------------------------------------------------------------------------------
DROP PROCEDURE IF EXISTS filter_my_complaints_all_complaints;
DELIMITER$$
CREATE  PROCEDURE `filter_my_complaints_all_complaints`(IN `inappuserid` INT, IN `inshowall` INT, 
IN inTRPKG VARCHAR (20), IN inTRMVG VARCHAR (20), IN inTRVPO VARCHAR (20), IN inTRAUT VARCHAR (20),
IN inMUROD VARCHAR (20), IN inMUCLN VARCHAR (20), IN inMUINF VARCHAR (20), IN inMUENC VARCHAR (20),
IN inMUOSP VARCHAR (20),IN inmmyissue INT)
BEGIN
IF inshowall = 1  THEN 
SELECT count(*) count, od.offence_detail_id, offence_address, offence_time, client_datetime, offence_type, vehicle_no, unique_key, submit_report, 
discard_report, address_id, location_id, ward, od.complaint_type_code, cl.complaint_sub_type_code, 
ct.colour, ct.android_colour,
 municipal_department, 
approved, rejected, approved_rejected_clientdatetime, sent_to_authority_datetime, resolved_unresolved_datetime, other_reason_code_comment, 
resolved, unresolved, unresolved_comment, reason_code, sent_to_authority,  od.lat, od.lng, od.appuser_id, email, phoneno, 
username, fullname, age, sex, offence_image_id, offence_image_name, offence_image_path, creation_datetime 
FROM offence_details od
LEFT JOIN offence_image oi
 ON od.offence_detail_id = oi.offence_detail_id
LEFT JOIN complaint_lookup_types ct
ON ct.complaint_type_code = od.complaint_type_code
LEFT JOIN complaint_lookup_subtypes cl
		ON od.complaint_sub_type_code = cl.complaint_sub_type_code
LEFT JOIN appuser a
		ON od.appuser_id = a.appuser_id
WHERE   rejected=0
AND  submit_report=1
GROUP BY od.offence_detail_id
ORDER BY od.offence_detail_id DESC;

ELSEIF inmmyissue = 1 THEN  
SELECT count(*) count, od.offence_detail_id, offence_address, offence_time, client_datetime, offence_type, vehicle_no, unique_key, submit_report, 
discard_report, address_id, location_id, ward, od.complaint_type_code, cl.complaint_sub_type_code,
ct.colour, ct.android_colour,
 municipal_department, 
approved, rejected, approved_rejected_clientdatetime, sent_to_authority_datetime, resolved_unresolved_datetime, other_reason_code_comment, 
resolved, unresolved, unresolved_comment, reason_code, sent_to_authority, od.lat, od.lng, od.appuser_id, email, phoneno, 
username, fullname, age, sex, offence_image_id, offence_image_name, offence_image_path, creation_datetime FROM offence_details od
LEFT JOIN offence_image oi
 ON od.offence_detail_id = oi.offence_detail_id
LEFT JOIN complaint_lookup_types ct
ON ct.complaint_type_code = od.complaint_type_code
LEFT JOIN complaint_lookup_subtypes cl
		ON od.complaint_sub_type_code = cl.complaint_sub_type_code
LEFT JOIN appuser a
		ON od.appuser_id = a.appuser_id
WHERE od.appuser_id = inappuserid AND rejected=0
AND  submit_report=1
GROUP BY od.offence_detail_id
ORDER BY od.offence_detail_id DESC;

ELSEIF inmmyissue = 0 AND inshowall = 0 THEN
SELECT od.offence_detail_id, offence_address, offence_time, client_datetime, offence_type, vehicle_no, unique_key, submit_report, 
discard_report, address_id, location_id, ward, od.complaint_type_code, cl.complaint_sub_type_code, 
ct.colour, ct.android_colour, 
municipal_department, 
approved, rejected, approved_rejected_clientdatetime, sent_to_authority_datetime, resolved_unresolved_datetime, other_reason_code_comment, 
resolved, unresolved, unresolved_comment, reason_code, sent_to_authority, od.lat, od.lng, od.appuser_id, email, phoneno, 
username, fullname, age, sex, offence_image_id, offence_image_name, offence_image_path, creation_datetime FROM offence_details od
LEFT JOIN offence_image oi
 ON od.offence_detail_id = oi.offence_detail_id
LEFT JOIN complaint_lookup_types ct
ON ct.complaint_type_code = od.complaint_type_code
LEFT JOIN complaint_lookup_subtypes cl
		ON od.complaint_sub_type_code = cl.complaint_sub_type_code
LEFT JOIN appuser a
		ON od.appuser_id = a.appuser_id
WHERE  od.complaint_type_code IN(inTRPKG, inTRMVG, inTRVPO, inTRAUT, inMUROD, inMUCLN, inMUINF, inMUENC, inMUOSP)
AND  rejected=0
AND  submit_report=1
GROUP BY od.offence_detail_id
ORDER BY od.offence_detail_id DESC;

END IF;
END$$
DELIMITER ;

CALL filter_my_complaints_all_complaints(598,0,'TR_PKG',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0)