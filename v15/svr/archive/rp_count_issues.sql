DROP PROCEDURE IF EXISTS rp_count_issues;
DELIMITER$$
CREATE  PROCEDURE `rp_count_issues`(IN `inappuserid` INT, IN `inshowall` INT, 
IN inTRPKG VARCHAR (20), IN inTRMVG VARCHAR (20), IN inTRVPO VARCHAR (20), IN inTRAUT VARCHAR (20),
IN inMUROD VARCHAR (20), IN inMUCLN VARCHAR (20), IN inMUINF VARCHAR (20), IN inMUENC VARCHAR (20),
IN inMUOSP VARCHAR (20),IN inmmyissue INT)
BEGIN
IF inmmyissue= 1 THEN 
SELECT COUNT(od.offence_detail_id) count,ct.complaint_description
FROM offence_details od
LEFT JOIN complaint_lookup_types ct
ON od.complaint_type_code = ct.complaint_type_code
WHERE  od.complaint_type_code IN(inTRPKG, inTRMVG, inTRVPO, inTRAUT, inMUROD, inMUCLN, inMUINF, inMUENC, inMUOSP)
AND rejected=0
AND submit_report=1
AND od.appuser_id = inappuserid
GROUP BY  ct.complaint_type_code
ORDER BY count DESC;

ELSEIF inshowall  = 1 THEN  
SELECT COUNT(od.offence_detail_id)count,ct.complaint_description
FROM offence_details od
LEFT JOIN complaint_lookup_types ct
ON od.complaint_type_code = ct.complaint_type_code
WHERE  od.complaint_type_code IN(inTRPKG, inTRMVG, inTRVPO, inTRAUT, inMUROD, inMUCLN, inMUINF, inMUENC, inMUOSP)
AND  rejected=0
AND  submit_report=1
GROUP BY  ct.complaint_type_code
ORDER BY count DESC;

END IF;
END$$
DELIMITER ;


call rp_count_issues(2,1,'TR_PKG','TR_MVG','TR_VPO','TR_AUT','MU_ROD','MU_CLN','MU_INF','MU_ENC','MU_OSP',0)