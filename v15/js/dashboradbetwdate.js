var fromdate, todate;
function check_fileds(){
    $("#fromdate").datepicker();
	$( "#fromdate" ).datepicker( "option", "dateFormat", "yy-mm-dd");
    $("#todate").datepicker();
	$( "#todate" ).datepicker( "option", "dateFormat", "yy-mm-dd");

    $("#show").click(function() {
    	/* var selected = $("#dropdown option:selected").text(); */
			fromdate = $("#fromdate").val();
			todate = $("#todate").val();
        if (fromdate === "" || todate === "") {
			alert("Please select  dates.");
        } 
		
	 	 else if (select && code!="all")
		 {
			dashboardcode(fromdate,todate,code);
		}  
		else {			
			DashboardReport(fromdate,todate);
 
			}
	   	});	

}
	
	
	
function DashboardReport(fromdate,todate) {
    var dashboardUrl = 	getSmartSheharPhpScriptsPath() + 'offence/show_offence_betw_date.php?fromdate='+fromdate+'&todate='+todate;
     //   dashboardUrl
    $.ajax({
        type: 'GET',
        url: dashboardUrl,
        dataType: 'html',
        success: function (data) {	
         displayIssueReport(data);
		showUsersOnMap(data);
        },
        error: function (xhr, textStatus, errorThrown) {
            var err = textStatus + ', ' + errorThrown;
        },
        async: false
    });
}

	
function dashboardcode(fromdate,todate,code) {
    var dashboardUrl = 	getSmartSheharPhpScriptsPath() + 'offence/issues_dateandcode.php?fromdate='+fromdate+'&todate='+todate+'&code='+code;
     //   dashboardUrl
    $.ajax({
        type: 'GET',
        url: dashboardUrl,
        dataType: 'html',
        success: function (data) {
        displayIssueReport(data);
		showUsersOnMap(data);
        },
        error: function (xhr, textStatus, errorThrown) {
            var err = textStatus + ', ' + errorThrown;
        },
        async: false
    });
}

/* 
 function displayIssueReport(data) {
    var arr = JSON.parse(data), i, b;
    var out = "<div class='container'><div class='row'>";
    for (i = 0; i < arr.length; i++) {
        out += "<div class='col-md-3'><a href='http://www.smartshehar.com/alpha/dashboardapp/svr/apk/v1/php/offence/images/" + arr[i].offence_image_name + ".jpg' class='thumbnail'>" +
			"<h4><p style='text-align : center;background-color:hsla(200,100%,80%,0.3);'>" +  arr[i].offence_type + "</p></h4>" + 
			"<p style='text-align : center ; color : blue'>" + arr[i].offence_time + "</p>" + "<br>" +
			"<img src='http://www.smartshehar.com/alpha/dashboardapp/svr/apk/v1/php/offence/images/" + arr[i].offence_image_name + ".jpg' height='140px' width='140px'>" + "<br>" +
			"<p style='text-align : center; background-color:hsla(120,100%,25%,0.3);'>" + arr[i].offence_address + "<br>" +
				"</div></a>";
		}
	 out+="</div></div>";
    document.getElementById("map").innerHTML = out;
}  */




function displayIssueReport(data) {

    var arr = JSON.parse(data), i, b;
    var out = " <div class='table-responsive'><table class='table table-striped'><thead><tr><th>TYPE</th><th>TIME</th><th>IMAGES</th><th>ADDRESS</th></tr></thead><tbody>";
	/*var approvedFlag;
	var senttoAuthFlag;
	var rejectedFlag;
	var rejectedReason;
	var resolvedFlag;
	var unresolvedFlag;
	var unresolvedComment;
	var status='';
	*/
    for (i = 0; i < arr.length; i++) {
		/*approvedFlag = arr[i].approved;
		senttoAuthFlag = arr[i].sent_to_authority;
		 rejectedFlag = arr[i].rejected;
		 rejectedReason = arr[i].reject_reason;
		 resolvedFlag = arr[i].resolved;
		 unresolvedFlag = arr[i].unresolved;
		 unresolvedComment = arr[i].unresolved_comment;
		 
		 if(approvedFlag==1 && senttoAuthFlag==0 && rejectedFlag ==0 )
		 {
			 status='Approved';
		 }
		 
		 if(approvedFlag==1 && senttoAuthFlag ==1 && rejectedFlag ==0)
		 {
			 status='Sent to authority. Complaint is pending';
		 }
		if(approvedFlag==0 && senttoAuthFlag ==0 && rejectedFlag ==1)
		 {
			 status='Rejected as '+rejectedReason;
		 }
		 if(resolvedFlag ==1)
		 {
			 status='Resolved';
		 }
		 if(unresolvedFlag ==1)
		 {
			 status='Closed as '+unresolvedComment;
		 }
		  if(approvedFlag==0 && senttoAuthFlag==0 && rejectedFlag ==0 )
		 {
			 status='Sent to Smartshehar';
		 }
		  */
		
        out += "<tr ><td>" +  arr[i].offence_type + "</td>" + 
			"<td>" + arr[i].offence_time + "</td>" +
			"<td ><img src='" + getSmartSheharPhpScriptsPath() + "/offence/images/" + arr[i].offence_image_name + "' height='140px' width='140px' ></td>" +
			"<td>" + arr[i].offence_address + "</td>" +
				"</td></tr>";
				
		}
	 out+="</tbody></table></div>";
    document.getElementById("tables").innerHTML = out;
} 
$(document).ready(function () {
		check_fileds();
		
});   


function showoffencedetails(successCallBack) {
	var showdashboard = getSmartSheharPhpScriptsPath() + 'offence/show_offence_report.php?';
     //   dashboardUrl
    $.ajax({
        type: 'GET',
        url: showdashboard,
        dataType: 'html',
        success: function (data) {
			displayIssueReport(data);
        },
        error: function (xhr, textStatus, errorThrown) {
            var err = textStatus + ', ' + errorThrown;
        },
        async: false
    });
}