function getPincodewise(pincode, fromoffencedate, tooffencedate, TRPKG, TRMVG,
    TRVPO, TRAUT, MUROD, MUCLN, MUINF, MUENC, MUOSP, myissue,
    appuserid, lat, lng, limit, submitreport, rejected, closed, resolved, successCallBack) {
    var getPincodewiseurl = getReportsPhpScriptsPath() +
        'rp_filter_pincodewise.php?pincode=' + pincode + 
		'&fromoffencedate=' + fromoffencedate +
        '&tooffencedate=' + tooffencedate  + '&TRPKG=' + TRPKG + '&TRMVG=' + TRMVG +
        '&TRVPO=' + TRVPO + '&TRAUT=' + TRAUT + '&MUROD=' + MUROD + '&MUCLN=' + MUCLN +
        '&MUINF=' + MUINF + '&MUENC=' + MUENC + '&MUOSP=' + MUOSP + '&myissue=' + myissue +
        '&appuserid=' + appuserid + '&lat=' + lat + '&lng=' + lng + '&limit=' + limit
		'&submitreport=' + submitreport + '&rejected=' + rejected + '&closed=' + closed + 
		'&resolved=' + resolved;
    $.ajax({
        type: 'GET',
        url: getPincodewiseurl,
        dataType: 'json',
        success: function(data) {
            successCallBack(data);
        },
        error: function(xhr, textStatus, errorThrown) {
            var err = textStatus + ', ' + errorThrown;
        },
        async: false
    });
}

$(document).ready(function() {
   // $('#dp1').datepicker();
    //$('#dp2').datepicker();

    var pincode = gup('pincode');
    var fromoffencedate = gup('fromoffencedate');
    var tooffencedate = gup('tooffencedate');
    var TRPKG = gup('TRPKG');
    var TRMVG = gup('TRMVG');
    var TRVPO = gup('TRVPO');
    var TRAUT = gup('TRAUT');
    var MUROD = gup('MUROD');
    var MUCLN = gup('MUCLN');
    var MUINF = gup('MUINF');
    var MUENC = gup('MUENC');
    var MUOSP = gup('MUOSP');
    var myissue = gup('myissue');
    var appuserid = gup('appuserid');
    var lat = gup('lat');
    var lng = gup('lng');
    var limit = gup('limit');
	var submitreport = gup('submitreport');
	var rejected = gup('rejected');
	var closed = gup('closed');
	var resolved = gup('resolved');
    
	var sHTML = '<div class="container-fluid">';
    var sVehicleNo = '',
        sWard = '';
    var imgOffenceType = '';
    var imgOffenceTime = '';
    var imgOffenceAddress = '';
    var imgWard = '';

    getPincodewise(pincode, fromoffencedate, tooffencedate, TRPKG, TRMVG, TRVPO, TRAUT,
        MUROD, MUCLN, MUINF, MUENC, MUOSP, myissue, appuserid, lat, lng, limit,
		submitreport, rejected, closed, resolved,
        function(data) {
            len = data.length;

            for (i = 0; i < len; i = i + 1) {
                row = data[i];

                sVehicleNo = row.vehicle_no ? row.vehicle_no : '';
                sWard = row.ward ? row.ward : '';
                imgOffenceType = '';
                imgOffenceTime = '';
                imgOffenceAddress = '';
                imgWard = '';

                if (row.complaint_type_code == 'TR_PKG')
                    imgOffenceType = '<img src="images/TR_PKG.png" alt="Parking Violation" height="32" width="32">';
                else if (row.complaint_type_code == 'TR_MVG')
                    imgOffenceType = '<img src="images/TR_MVG.png" alt="Moving Violation" height="32" width="32">';
                else if (row.complaint_type_code == 'TR_VPO')
                    imgOffenceType = '<img src="images/TR_VPO.png" alt="Vehicle & Pollution Related" height="32" width="32">';
                else if (row.complaint_type_code == 'MU_CLN')
                    imgOffenceType = '<img src="images/MU_CLN.png" alt="Cleanliness" height="32" width="32">';
                else if (row.complaint_type_code == 'MU_INF')
                    imgOffenceType = '<img src="images/MU_INF.png" alt="Basic Infrastructure" height="32" width="32">';
                else if (row.complaint_type_code == 'MU_ENC')
                    imgOffenceType = '<img src="images/MU_ENC.png" alt="Hawkers/Encroachment" height="32" width="32">';
                else if (row.complaint_type_code == 'MU_OSP')
                    imgOffenceType = '<img src="images/MU_OSP.png" alt="Public Spaces" height="32" width="32">';

                imgOffenceTime = '<img src="images/time.png" alt="time" height="32" width="32">';
                imgOffenceAddress = '<img src="images/address.png" alt="address" height="32" width="32">';
                imgWard = '<img src="images/bmc.jpg" alt="bmc" height="32" width="32">';

                sHTML = sHTML + '<div class="row" style = "padding-bottom: 10px;">' +
                    '<div class="col-sm-6"style="background-color:yellow;">' + row.offence_detail_id + '</div>' +
                    '</div>' +
                    '<div class="row" style = "padding-bottom: 10px;">' +
                    '<div class="col-sm-3" >' + imgOffenceType +
                    '<b style = "padding-left: 10px;" >' + row.offence_type + '</b></div>' +
                    '<div class="col-sm-3">' + imgOffenceTime +
                    '<b style = "padding-left: 10px;">' + row.offence_time + '</b></div>' + '</div>' +
                    '<div class="row" style = "padding-bottom: 10px;">' +
                    '<div class="col-sm-6">' + imgOffenceAddress +
                    '<span style = "padding-left: 10px;">' + row.offence_address + '</span></div>' +
                    '</div>' +
                    '<div class="row"style = "padding-bottom: 10px;">' +
                    '<div class="col-sm-4">' + sVehicleNo + '</div>' +
                    '</div>' +
                    '<div class="row"style = "padding-bottom: 10px;">' +
                    '<div class="col-sm-3">' + imgWard +
                    '<span style = "padding-left: 10px;">' + sWard + '</span></div>' +
                    '<div class="col-sm-3">' + row.municipal_department + '</div>' +
                    '</div>' +
                    '<div>&nbsp;</div>';

            }
            $('#rp_pincode').html(sHTML + '</div>');

        });

}); // document.ready