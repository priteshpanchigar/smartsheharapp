/*global $, jQuery, getLocation, jsonCityFareParameter, dateMinutesDifference, document, select, jqAutocomplete, showLocation, console, displayLines, updateCurrentTime, google, window, setCookie, localStorage, successCallBack, event, alert, gup, putInsertRides, getCookie, setCookie, getFareLondon, distanceLatLon, createCityList, getTimeHHSS, cityBlock, getSmartSheharPhpScriptsPath() */
"use strict";
var flag = 0,
    select, data;

//-------------------------------ajax for display user on map----------------------------------------------------

function getUsersonMap(successCallBack) {
    var usersonMapUrl = getSmartSheharPhpScriptsPath() + 'offence/issues.php',
        app = true;
    //   usersonMapUrl
    $.ajax({

        type: 'POST',
        url: usersonMapUrl,
        dataType: 'html',
        success: function(data) {
            successCallBack(data);

        },
        error: function(xhr, textStatus, errorThrown) {
            var err = textStatus + ', ' + errorThrown;
        },
        async: false
    });
}
//-------------------------------------------showUsersOnMap------------------------------------------
function showUsersOnMap(data) {

    var locations = JSON.parse(data);

    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 15,
        center: new google.maps.LatLng(19.1046459, 72.8506099),
        mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    var infowindow = new google.maps.InfoWindow();
    var marker, i;

    var allowedBounds = new google.maps.LatLngBounds(
        new google.maps.LatLng(185, -180), // top left corner of map
        new google.maps.LatLng(-85, 180) // bottom right corner
    )
    var latlngbounds = new google.maps.LatLngBounds();
    var infoWindowContent = [];
	var iconpath; 

    for (i = 0; i < locations.length; i++) {
        var pnt = locations[i];
        infoWindowContent[i] = getInfoWindowDetails(pnt);
        latlngbounds.extend(new google.maps.LatLng(pnt.lat, pnt.lng));
			iconpath = "http://maps.google.com/mapfiles/ms/icons/" + 
				pnt.colour + "-dot.png";
		
        marker = new google.maps.Marker({
            position: new google.maps.LatLng(pnt.lat, pnt.lng),
            map: map

			
        });
//		if(pnt.colour = 'blue') {
		try {
			marker.setIcon('http://maps.google.com/mapfiles/ms/icons/' + pnt.colour + '-dot.png');
		} catch(err) {
			marker.setIcon('http://maps.google.com/mapfiles/ms/icons/red-dot.png');
		}
		
		//marker.seticon(iconpath);
		
        google.maps.event.addListener(marker, 'click', (function(marker, i) {
            return function() {
                infowindow.setContent(infoWindowContent[i]);
                infowindow.open(map, marker);
            }
        })(marker, i));

    }
    map.setCenter(latlngbounds.getCenter());
    map.fitBounds(latlngbounds);

    var centerControlDiv = document.createElement('div');
    centerControlDiv.index = 1;

    var myTitle = document.createElement('h1');
    myTitle.style.color = 'blue';
    myTitle.innerHTML = 'Hello World';
    var myTextDiv = document.createElement('div');
    myTextDiv.appendChild(myTitle);
    map.controls[google.maps.ControlPosition.BOTTOM_CENTER].push(myTextDiv);

    map.controls[google.maps.ControlPosition.BOTTOM_LEFT].push(centerControlDiv);
    google.maps.event.addListener(map, 'bounds_changed', function() {
        var bounds = map.getBounds();
        var position, pnt, i;
        var usersInView = 0;
        for (i = 0; i < locations.length; i++) { // looping through my Markers Collection        
            pnt = locations[i];
            position = new google.maps.LatLng(pnt.lat, pnt.lng);
            if (bounds.contains(position)) {
				  usersInView++;
         //       console.log("Marker" + i + " - matched");
		 
            }
        }
        myTitle.innerHTML = "";
        myTitle.innerHTML = usersInView.toString();
    });
	 google.maps.event.addListener(map, 'zoom_changed', function() {
        var bounds = map.getBounds();
        var position, pnt, i;
     
        for (i = 0; i < locations.length; i++) { // looping through my Markers Collection        
            pnt = locations[i];
            position = new google.maps.LatLng(pnt.lat, pnt.lng);
            if (bounds.contains(position)) {
				 sendLatLng(pnt.lat,pnt.lng);
            }
        }
      
    });
}

function getInfoWindowDetails(pnt) {
    var offence_type = "";
    if (pnt.offence_type != null) {
        offence_type = pnt.offence_type;
    }
    return "<div>" + offence_type + "</div>";
}

//----------------------------------------ready function---------------------------
$(document).ready(function() {

    getUsersonMap(function(data) {
        showUsersOnMap(data);
    });
});


function CenterControl(controlDiv, map) {

    // Set CSS for the control border
    var controlUI = document.createElement('div');
    controlUI.style.backgroundColor = '#fff';
    controlUI.style.border = '2px solid #fff';
    controlUI.style.borderRadius = '3px';
    controlUI.style.boxShadow = '0 2px 6px rgba(0,0,0,.3)';
    controlUI.style.cursor = 'pointer';
    controlUI.style.marginBottom = '22px';
    controlUI.style.textAlign = 'center';
    controlUI.title = 'Click to recenter the map';
    controlDiv.appendChild(controlUI);

    // Set CSS for the control interior
    var controlText = document.createElement('div');
    controlText.style.color = 'rgb(25,25,25)';
    controlText.style.fontFamily = 'Roboto,Arial,sans-serif';
    controlText.style.fontSize = '16px';
    controlText.style.lineHeight = '38px';
    controlText.style.paddingLeft = '5px';
    controlText.style.paddingRight = '5px';
    controlText.innerHTML = 'Stats';
    controlUI.appendChild(controlText);

    // Setup the click event listeners: simply set the map to
    // Chicago
    google.maps.event.addDomListener(controlUI, 'click', function() {

    });
}
	function sendLatLng(lat,lng){
		
	 var locationwiseOffenceUrl = getSmartSheharPhpScriptsPath() + 'offence/find_offence_locationwise.php?lat=' + lat+"&lng="+lng;
   
    $.ajax({
       type: 'GET',
       url: locationwiseOffenceUrl,
        dataType: 'html',
        success: function(data) {
           // alert(data);
//           displayIssueReport(data);

        },
        error: function(xhr, textStatus, errorThrown) {
            var err = textStatus + ', ' + errorThrown;
        },
        async: false
    });
}
