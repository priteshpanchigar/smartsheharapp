var code;

function getcode(code) {
    var dashboardUrl = getSmartSheharPhpScriptsPath() + 'offence/filtersearchonmap.php?code=' + code;
    //   dashboardUrl
    $.ajax({
        type: 'GET',
        url: dashboardUrl,
        dataType: 'html',
        success: function(data) {
            //displayIssueReport(data)
			//
           showUsersOnMap(data);
            displayIssueReport(data);

        },
        error: function(xhr, textStatus, errorThrown) {
            var err = textStatus + ', ' + errorThrown;
        },
        async: false
    });
}
//--------------------------------dynamic dropdown--------------------------------------
function selectedoffence() {
    $("#maintype").change(function() {
        select = $("#maintype option:selected").click;
        // select=$("#maintype option:selected");

        code = document.getElementById("maintype").value;
        if (code == "blank") {
            alert("enter one of the option");

        } else if (code == "all") {
            showoffencedetails(function(data) {
                displayIssueReport(data);
            });
            getUsersonMap(function(data) {
                showUsersOnMap(data);
            });

        } else {
            getcode(code);
        }

    });
}

function clickThebtn() {
    $("#mapbtn").click(function() {
        getUsersonMap(function(data) {
            showUsersOnMap(data);
        });
    });

    $("#tablebtn").click(function() {

        showoffencedetails(function(data) {
            displayIssueReport(data);
        });
    });
}

$(document).ready(function() {
    selectedoffence();
    clickThebtn();
    showoffencedetails(function(data) {
        displayIssueReport(data);
    });
});