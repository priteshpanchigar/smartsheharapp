/*global $, document, getAutoTaxiPhpScriptsPath(), city, gup */
function getFareCalculation(city, successCallBack) {
	var getFareCalculationurl	= getAutoTaxiPhpScriptsPath() +
		'getfarecalculation.php?city=' + city;
	$.ajax({
	    type: 'GET',
	    url: getFareCalculationurl,
	    dataType: 'json',
	    success: function (data) {
	        successCallBack(data);
	    },
	    error: function (xhr, textStatus, errorThrown) {
	        var err = textStatus + ', ' + errorThrown;
	    },
		async: false
	});
}
function displayFareCalculation(city) {
	var len, i, row, html = '', autoHtml = '', taxiHtml = '', twoWheelerHtml = '',
		fourWheelerHtml = '', publicTransport = 'Public Transport',
		privateTransport = 'Private Transport', autoTitle = 'Auto ', taxiTitle = 'Taxi ',
		twoWheelerTitle = '2 Wheeler', fourWheelerTitle = '4 Wheeler';
	getFareCalculation(city, function (aoFareCalculation) {
		len = aoFareCalculation.length;
		html = '<table boder="1">';
		for (i = 0; i < len; i = i + 1) {
			row = aoFareCalculation[i];
			if (row.vehicle_type === 'AU') {
				autoHtml = autoHtml + '<tr>' + '<td>' + row.item + '</td>' +
					'<td>' + row.calculation + '</td>' + '</tr>';
			}
			if (row.vehicle_type === 'TA') {
				taxiHtml = taxiHtml + '<tr>' + '<td>' + row.item + '</td>' +
					'<td>' + row.calculation + '</td>' + '</tr>';
			}
			 if (row.vehicle_type === '2W') {
				twoWheelerHtml = twoWheelerHtml + '<tr>' + '<td>' + row.item + '</td>' +
					'<td>' + row.calculation + '</td>' + '</tr>';
			}
			if (row.vehicle_type === '4W') {
				fourWheelerHtml = fourWheelerHtml + '<tr>' + '<td>' + row.item + '</td>' +
					'<td>' + row.calculation + '</td>' + '</tr>';
			} 
		}
		$('#city').html(city);
		$('#pbtransport').html('<h2>' + publicTransport + '</h2>');
		//$('#prtransport').html('<br/>' + '<h2>' + privateTransport + '</h2>');
		$('#autoTitle').html('<h3>' + autoTitle + '</h3>');
		$('#taxiTitle').html('<h3>' + taxiTitle + '</h3>');
		//$('#twowheelerTitle').html('<h3>' + twoWheelerTitle + '</h3>');
		//$('#fourwheelerTitle').html('<h3>' + fourWheelerTitle + '</h3>');
		$('#autovehicle').html('<table border="1">' + autoHtml + '</table>');
		$('#taxivehicle').html('<table border="1">' + taxiHtml + '</table>');
		//$('#twowheeler').html('<table border="1">' + twoWheelerHtml + '</table>');
		//$('#fourwheeler').html('<table border="1">' + fourWheelerHtml + '</table>');
	});
}
$(document).ready(function () {
	city = gup('city');
	if (city) {
		displayFareCalculation(city);
	}
}); // document.ready